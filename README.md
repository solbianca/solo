# ENGINE
Фреймворк для микросервисов.

## Тестирование

Скачать codeception.phar:

````
wget http://codeception.com/codecept.phar
````

Запустить тесты:
````
php codecept.phar run unit
````

## Deployer 
[deployer](https://deployer.org/)

Утилита для деплоинга приложений на сервере.

Установка:
````
curl -LO https://deployer.org/deployer.phar
````

Использование:
````
php deployer.phar 
````

## Качества кода

[Инструменты для првоерки качества кода](docs/code-quality.md).

## Документация

Описание доступного апи фреймворка. Необходимо стараться держать документ в актуальном состоянии.

## Skeleton Service

[Skeleton](https://gitlab.fix.ru/epn/SkeletonService)

Для более быстрого и комфортного старта разработки можно воспользоваться готовым скелетоном.
Для этого необходимо выполнить команды:

````
git clone git@gitlab.fix.ru:epn/SkeletonService.git
cd SkeletonService
composer install
php init
````
Скелетон предлагает базовую структуру проекта и содержит инструмент для инициализации приложения:

**php init** - данная утилита дает возможность выбрать контекст приложения: develope или production.
Она скопирует в папку /path/to/service/public index.php. 
И в папку /path/to/service/App/Configs файл main-local.php в который можно добавлять локальные переменные приложения.

### Application
Инстанс приложения. Синглтон. Доступ - Engine::$app

Engine::$app->dic - Контейнер зависимостей.

### Engine
[Engine](docs/engine.md)

### Dependency Injection Container
[Описание контейнера зависимостей](docs/dependency-injection-conteiner.md).

### Routing
[Описание роутинга](docs/routing.md).

### Middleware

[Middleware](docs/middleware.md)

### Controllers
[Controller](docs/controller.md)

[Web Controller](docs/web-controller.md)

### View and Rendering
[Представление](docs/view.md)

### Forms
[Формы](docs/forms.md)

### Validation
[Валидация данных](docs/validation.md).

### Traits
[AttributesTrait](docs/traits/attributes.md)

[LoadTrait](docs/traits/load.md)

### Database

TODO: Необходимо добавить описание классов.

### Request\Response УСТАРЕЛО. ПЕРЕПИСАТЬ.
[Request](docs/request.md)

[Responce](docs/responce.md)

### Helpers

[ArrayHelper](docs/helpers/array-helper.md)

[VarDumper](src/Helpers/VarDumper.php) Смотри phpdoc к методам.

### Logger
TODO: Необходимо добавить описание классов.

### Console
[Пользовательские команды](docs/console/user-commands.md)
[Встроенные команды](docs/console/core-commands.md)

### RabbitMQ

[RabbitMQ](docs/rabbitmq.md)