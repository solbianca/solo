## Middleware

HTTP Middleware (посредники) - это фильтры обработки HTTP-запроса. 

[Why Care About PHP Middleware?](https://philsturgeon.uk/php/2016/05/31/why-care-about-php-middleware/)

[On HTTP, Middleware, and PSR-7](https://mwop.net/blog/2015-01-08-on-http-middleware-and-psr-7.html)

[О HTTP, Middleware и PSR-7 или что не так с текущим подходом](https://makeyoulivebetter.org.ua/node/568)

Middleware используется в WebApplication для обработки HTTP запросов и формирования HTTP ответа. Реализует PSR-7.

### Config

Для корректной работы необходимо прописать в конфиг настройки:

````
return [
...
    'middleware' => [
        default' => [
            new \Engine\Middleware\Middlewares\FooMiddleware(),
            new \Engine\Middleware\Middlewares\BuzMiddleware(),
            new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
        ],
    ],
    ...
]
````
Данные middleware будут применятся ко всем запросам к нашему приложению. 
То есть наш запрос http://site.com/ Будет последовательно пропущен через три middleware(FooMiddleware, BuzMiddleware, HttpRoutingMiddleware) и сформированный ими response будет отправлен клиенту.

Если же нам потребуется для определенных запросов использовать специфичные middleware, то нам необходимо явным образом указать алиасы для этого в urls и middleware:

````
return [
...
    'urls' => [
        '/' => 'Site::index,
        'aliases' => [
            'epn.bz => [
                '/user' => 'User::index'
            ]
        ],
     ],
    'middleware' => [
        'default' => [
            new \Engine\Middleware\Middlewares\FooMiddleware(),
            new \Engine\Middleware\Middlewares\BuzMiddleware(),
            new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
        ],
        'epn.bz' => [
            new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
        ]
    ],
    ...
]
````

Тогда для запроса http://site.com/ будет использован только один middleware (HttpRoutingMiddleware). 

Важно помнить что если вы указали алиас для url но не указали его в middleware, то будут использованный настройки из блока default.

### Proxy

У MiddlewareRunner есть метод для проксирования очереди. Нужно это для ленивой загрузки либо вызову middleware по определенным условиям.
 
Примеры: 
 
````
$queue = [
    // Ленивая загрузка
    MiddlewareRunner::proxy(function () use ($app) {
        return new FooMiddleware();
    }),
    
    // Middleware будет вызван только в контексте продакшена 
    MiddlewareRunner::proxy(function () {
        return (APP_ENV !== 'prod') ? false : new FooMiddleware();
    }),
    
    // Middleware будет вызван только если в запросе будет присутствовать заголовок 'Foo'
    MiddlewareRunner::proxy(function ($request, $response) {
        if ($request->hasHeader('Foo')) {
            return new FooMiddleware();
        }
    
        return false;
    }),
    
    // Middleware будет вызван только при определенном uri
    MiddlewareRunner::proxy('/admin', function () {
        return new FooMiddleware();
    }),
    
    // Middleware будет вызван только при определенном пути и в запросе будет заголовок 'Foo'
    MiddlewareRunner::proxy('/actions', function ($request, $response) {
        if ($request->hasHeader('Foo')) {
            return new FooMiddleware();
        }
    
        return false;
    }),
];
````
### Разработка middleware

Любой middleware должен имплемитировать класс Engine\Middleware\Interfaces\MiddlewareInterface

