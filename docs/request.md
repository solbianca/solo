## Request

The Request class represents an HTTP request. The object contains HTTP headers, methods and request parameters.

Basic usage.
```php
use Engine\Http\Request;

$request = new Request();

or

$request = Engine\Engine::$app->getDic()->get('request');
```

### Methods

Get request method

@return string
```php
$request->getMethod();
```

Check method of request

@param string $method Checked method name

@return bool
```php
$request->isMethod($method);
```

Check that request method is GET

@return bool
```php
$request->isGet();
```

Check that request method is POST

@return bool
```php
$request->isPost();
```

Check that request method is UPDATE

@return bool
```php
$request->isUpdate();
```

Check that request method is DELETE

@return bool
```php
$request->isDelete();
```

Check that request method is PATCH

@return bool
```php
$request->isPatch();
```

Get specific header from request by name

@param string $key Header name

@return null|string If header not ser return null, otherwise header value
```php
$request->getHeader($key);
```

Get all header from request

@return array Array of headers [key => value,... ]
```php
$request->getHeaders();
```

Get data from the post request.

@param  null|string $property Property name. If not set return all post data.
Otherwise return value by key if exist or default value (null by default).

@param  mixed $default Default value (optional)

@return mixed Post data
```php
$request->post($property = null, $default = null)
```

Get query params from request

@return array Array of parameters [key => value,... ]
```php
$request->queryParams();
```

Get specific query param from request

@param string $property Property name. Return value by key if exist or default value (null by default).

@param mixed|null $default Default value (optional)

@return mixed|null
```php
$request->queryParam($property, $default = null);
```

Checks if it's an AJAX request (probably)

@return boolean
```php
$request->isAjax();
```

Get server and execution environment information

@param string $param Parameter name (optional).
If not set return all data as array [key => value,... ].
Otherwise return value bu parameter name or null if not exist.

@return array|null
```php
$request->server($param = null);
```