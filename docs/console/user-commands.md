##Пользовательские команды
  Конфиг пользовательских команд хранится в общем конфиге под ключом `console_commands`.
  
###Пример конфига:

```
'console_commands' => [
		'testRoute' => 'Test::blabla',
	],

```

Где 
* `testRoute` имя команды, которое указывается в консоли при вызове.
* `Test` имя контроллера расположенного в `App\Console\Controllers`.
* `blabla` имя метода в контроллере `Test` полное имя метода для данного примера: 
```
public function blablaAction(){
  
}
```

Запуск консоли для данного примера: `php console testRoute`.

