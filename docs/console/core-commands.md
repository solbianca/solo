##Встроенные команды
  Встроенные команды хранятся в `Engine\Core\ConsoleApplication::coreCommands()`
  
###Использоватение:
* Спарвка:
```
php console
php console help

```

* создание миграции
```
php console migrate/create
```

* применение миграции
```
php console migrate
```

* откат миграции
```
php console migrate/rollback
```
