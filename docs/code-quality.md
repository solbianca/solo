## Качество кода

### PHP Mess Detector (PHPMD)

[PHPMD](https://phpmd.org/) пытается выявить ошибки, которые не находит компилятор, не оптимальные алгоритмы, переусложнённый код, не используемые выражения и другие подобные проблемы.

PHPMD в своей работе руководствуется списком правил: набором специфических указаний того, как и что искать в исходниках. Есть большое количество [предопределённых правил](https://phpmd.org/rules/index.html).

Пользователь также может расширить систему, написав [собственные правила](https://phpmd.org/documentation/creating-a-ruleset.html).

Установка:
```
wget -c http://static.phpmd.org/php/latest/phpmd.phar
```

Использование:
```
php phpmd.phar
```

### PHP Copy/Paste Detector (PHPCPD)

[PHPCPD](https://github.com/sebastianbergmann/phpcpd) находит дублируемый код в проектe. Помогает придерживаться правилу [DRY](https://ru.wikipedia.org/wiki/Don%E2%80%99t_repeat_yourself).

Установка:
```
wget https://phar.phpunit.de/phpcpd.phar
```

Использование:
```
php phpcpd.phar
```