## Routing

### Роутинг для гейта

Когда приложение начинает обрабатывать запрос, то первое что оно делает это 
парсит URL и на его основе составляет путь до экшена контроллера. 
Путь может быть составлен как на основе заданного конфига, так и на основе URI запроса.

Когда роутер парсит URL, сначало он смотрит в правила конфигов. 
Если подходящего правила не было найдено то создает роут на основе URI.

URL может содержать символ тире ``site.com/some/path-to-page`` . 
При парсинге роута тире будет удален, а роут будет приведен к cammel case виду: SomeController::pathToPageAction

#### Роут на основе конфига

Конфигурация для роутинга задается в файле App\Configs\config.php

````
'urls' => [
    '/' => 'Site::index',
	'/login' => 'Auth::index',
	'/test' => 'Site::test',
	'/catalog/:string/product/:any/:int' => 'Catalog\Product::view',
],
````

В примере выше ключами массива являются запросы, например site.com, site.com/login, site.com/catalog/cars/product/porshe911/123 и тд.

Значения это контроллер::экшон который будет вызван для обработки запроса. 

В конфиге можно задать специальные обозначения: :int, :string, :any. Значения данных специальных обозначений будут переданны в виде параметров action.

 - :int Любые числовые символы 
 - :string Любые символы кроме /
 - :any Можно использовать любые символы 

Так, для запроса site.com/catalog/cars/product/porshe911/123, которому соответствует правило 
'/catalog/:string/product/:any/:int' => 'Catalog\Product::view' будет вызван 
контроллер App\Controllers\Catalog\Products::view($catalog = 'cars', $product = 'porshe911', $id = 123).

Для url можно указывать алиасы. Это нужно для кастомизации middleware для определенных запросов.

````
return [
...
   'urls' => [
       '/' => 'Site::index,
       'aliases' => [
           'epn.bz => [
               '/user' => 'User::index'
           ]
       ],
    ],
]
...
````

В данном случае epn.bz является алиасом для запросов вида http://site.com/user

Важно помнить что конфиг читается сверхзу вниз и если где то uri запроса продублируется, то будет использованно его первое вхождение.


#### Роут на основе URI. 

Не найдя нужного правила в конфигах, роутер создает роут на основе входящего URI:

 - site.com/user/index Из запроса будет получен роут вида App\Controllers\UserController и action IndexAction. 
 - site.com/admin/user/view Из запроса будет получен роут вида App\Controllers\Admin\UserController и action ViewAction
 - site.com/admin/user/ Из запроса будет получен роут вида App\Controllers\Admin\UserController и action IndexAction. В данном случае у нас не указан action, поэтому вызвался action по умолчанию, которым является IndexAction.
 
 
### Роутинг для сервисов
 
 Роутинг для сервисов составляется только на основе конфигов. 
 
 Конфигурация для роутинга задается в файле App\Configs\config.php
 
````
'urls' => [
    'index' => 'Auth::index',
    'login' => 'Auth::login',
    'product.add => 'Catalog\Product::create
],
 ````
 

Так для входящего запроса ``index`` будет вызван контрорллер App\Controllers\AuthController и action indexAction.
А для запроса ``product.add`` будет вызван контроллер App\Controller\Catalog\ProductController и action createAction.