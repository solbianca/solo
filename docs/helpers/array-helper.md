## Array Helper

Хелпер для работы с массивами.

### ArrayHelper::toArray($object, $properties = [], $recursive = true)

Преобразует объект или массив объектов в массив.

@param object|array|string $object Объект, который необходимо преобразовать в массив.

@param array $properties Свойства объекта, котоыре надо будет положить в результирующий массив.
Если не задано, метод достанет все публичные свойства обекта.

@param boolean $recursive Рекурсивное преобразование объектов в массив.

@return array МРезультирующий массив, который получился из переданного в метод объекта.

Пример:

````php
'Foo\Bar' => [
    'id',
    'title',
    // the key name in array result => property name
    'createTime' => 'created_at',
    // the key name in array result => anonymous function
    'length' => function ($post) {
        return strlen($post->content);
    },
],
````

Результат работы `ArrayHelper::toArray($post, $properties)` может выглядеть так:

````php
[
    'id' => 123,
    'title' => 'test',
    'createTime' => '2013-01-01 12:00AM',
    'length' => 301,
]
````

### ArrayHelper::getValue($array, $key, $default = null)

Возвращае значение по переданному ключу или свойству объекта.
Если значения объекта или массива не существует будет возвращенно значение по умолчанию.

Можно использовать запись с точкой для выборки значений из вложенных масивов. Например запись вида `x.y.z`
вернет значение из массива `$array['x']['y']['z']` или объекта `$array->x->y->z`. Если вложенного объекта/массива
`$array['x']` | `$array->x` будет возвращено значение по умолчанию. 
NOTE: Если же в массиве определено свойство с ключем x.y.z, то будет возвращенно оно и метод не будет смотреть во вложенные массивы/объекты.


@param array|object $array Массив или объект в котором будет искать значение.

@param string|\Closure|array $key Ключ, массив ключей или анонимная функция, которая вернет значение по которым будет осуществялтся поиск.
Анонимная функция должна выглядеть: function($array, $defaultValue)

@param mixed $default Значение по умолчанию

@return mixed Найденное значение либо значение по умолчанию если ничего не найдено.

Примеры: 
```php
// working with array
$username = ArrayHelper::getValue($_POST, 'username');

// working with object
$username = ArrayHelper::getValue($user, 'username');

// working with anonymous function
$fullName = rrayHelper::getValue($user, function ($user, $defaultValue) {
    return $user->firstName . ' ' . $user->lastName;
});

// using dot format to retrieve the property of embedded object
$street = ArrayHelper::getValue($users, 'address.street');

// using an array of keys to retrieve the value
$value = ArrayHelper::getValue($versions, ['1.0', 'date']);
````

### ArrayHelper::merge($array1, $array2)

Сливает два и более массива в один рекурсивно. Если массивы обладают одинаковыми строковыми ключами, значение будет перезаписано последним из массивов.
Если ключ числовой, то значение будет дорбавлено с новым числовым ключем.
NOTE: Отличие от array_merge_recursive. Метод херпера перезаписывает значения со строковым числом, 
в то время как array_merge_recursive создает массив значений доступный по строковому ключу. 

@param array $array В метод может быть передано любое колличество массивов котоыре необходимо слить.

@return array Результирующий массив