## Controller

Базовый контроллер.

Пример простого контроллера:

````php
class SiteController extends Controller {
    
    public function indexAction() {
        $formModel = new ModelForm();
        if ($formModel->load($this->request->post()) && $formModel->validate()) {
            if (false !== ($result = $formModel->login())) {
                $this->asJson($result);
            }
            $this->asJson($formModel->);
        }   
    }
}
````

### Свайства

#### protected $request;

Содержит ссылку на инстанс класса \Engine\Http\Request

#### protected $response;

Содержит ссылку на инстанс класса \Engine\Http\Response

### Методы

#### Controller::beforeAction($action);

Данный метод вызывается до авторизации и применения правил дсотупа. Таким образом используя данный метод можно катсомизировать поведение контроллера.
Например, изменить метод авторизации.

@param string $action имя вызываемого действия (например 'indexAction')

#### Controller::asJson($data, $options = 320)

Вернуть овтет клиенту в виде джейсон строки. 
В заголовк ответа 'Content-Type' будет установленно значение 'application/json; charset=UTF-8'.
В тело ответа запишет джейсон строку полученную из $data.
Отправит ответ клиенту и заврешит работу приложения.

#### Controller::getRequest()

Вернет ссылку на инстанс класса Request.

#### Controller::getResponse()

Вернет ссылку на экземпляр класса Response