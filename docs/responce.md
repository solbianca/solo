## Response

The Response class represents an HTTP response. The object contains the response headers, HTTP status code, and response body.

Basic usage.
```php
use Engine\Http\Responce;

$responce = new Responce();

or

$responce = Engine\Engine::$app->getDic()->get('responce');
```

### Methods

Sets the HTTP status of the response.
 
@param int $code HTTP status code.

@return integer|object Self reference

@throws \Exception If invalid status code
```php
$responce->setStatusCode($code = null);
```

Adds a header to the response.

@param string|array $name Header name or array of names and values

@param string $value Header value

@return object Self reference
```php
$responce->addHeader($name, $value = null);
```

Returns the headers from the response

@return array
```php
$responce->getHeaders();
```

Writes content to the response body

@param string $str Response content

@return object Self reference
```php
$responce->write($str);
```

Clears the response.

@return object Self reference
```php
$responce->clear();
```

Sets caching headers for the response.

@param int|string $expires Expiration time

@return object Self reference
```php
$responce->cache($expires);
```

Sends HTTP headers.

@return object Self reference
```php
$responce->sendHeaders();
```

Sends a HTTP response.

```php
$responce->send();
```

Performs an HTTP redirect. This method halts the script execution.

@param string $url An URL to redirect.

@param int $status The redirect HTTP status code.

@param bool $stop Determines whether to stop the script execution.

@return static

@throws \InvalidArgumentException
```php
$responce->redirect();
```