## Engine 

Engine - базовый класс, через который получаем доступ к приложению.

#### Engine::$app 
Инстанс класса приложения

#### Engine::log($level, $message, $context) 

Логирование без обращения к контейнеру зависимостей. В данный момент не реализованно, стоит заглушка.

@param mixed $level - Уровень ошибки
@param string $message - Сообщение ошибки
@param array $context - Контекст вызова

#### Engine::createInstance($class, array $params = [])

Создасть обект из переданного класса

@param string $class Полное имя класса (включая пространство имен)

@param array $params Параметры, которые будут переданны в конструктор класса

@return object Инстанс класса