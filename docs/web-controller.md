## Web Controller

Базовый класс для определения контроллеров для гейтов. Наследует и расширяет поведение от \Engine\Base\Controller
Контроллер позволяет управлять доступом к действиям и методом авторизации.

Пример простого контроллера:

````php
class SiteController extends Controller {
	
    protected $authMehod = HttpBearerAuth::class;
    
    public function rules() {
        return [
            [
                'allow' => true,
                'roles' => ['guest', 'authorized'],
            ],
        ];
    }
    
    public function indexAction() {
        $formModel = new ModelForm();
        if ($formModel->load($this->request->post()) && $formModel->validate()) {
            if (false !== ($result = $formModel->login())) {
                $this->asJson($result);
            }
            $this->asJson($formModel->);
        }   
    }
}
````

### Правила дсотупа

Доступ к действиям метода можно ограничить по: роли, методу запроса или колбеку.

Пример правила: 

````php
[
    [
        'allow' => true,
        'actions' => ['login', 'index']
        'roles' => ['guest', 'authorized', 'admin'],
        'verbs' => ['POST']
        'callback' => function (AccessRule $rule, $action) {
            return true;
        }
    ],
],
````

##### 'allow' (обязательное поле).

Содержит булево значение: true если разрешить дсотуп, false если запретить.

##### 'authorized' (опционально)

Список действий контроллера к которым будет применено правило. 
Если не задано, будет применено ко всем действиям.

##### 'roles' (обязательное поле)

Список ролей для которых будет применено праивло.

Каждый пользователь, зашедший на сайт, имеет роль. Есть две роли по умолчанию и любой пользователь будет обладать одной из них.
Если пользователь не авторизирован, то роль по умолчанию **guest**. Если авторизирован, то **authorized**. 
Данные рорли не связанны с ролями определенными в системе. Они просто указывают авторизован ли пользователь или нет.

##### 'verbs' (опционально)

Список методов (GET, POST и тд).

##### 'callback' (опционально)

Позволяет кастомизировать поведение правила. В коллбак будет переданэкземпляр обекта AccessRule и имя метода. 

````php
'callback' => function (AccessRule $rule, $action) {
    return true;
}

Вы можете передать в коллбак любые свои данные.
````

### Авторизация

В контроллере можно указать метод авторизации.
Для этого необходимо переопределить свойство protected $authMehod.

````php
protected $authMehod = HttpBearerAuth::class;
````

Тогда все запросы к действиям контроллера будет пытаться авторизовать юзера методом Bearer. 
Если закрыть действия для не авторизованных пользователей, то пользователей не будет на них пускать.
 
Если свойству присвоить null, то авторизации происходить не будет и все пользователи будут как **guest**.

### Свайства

#### protected $request;

Содержит ссылку на инстанс класса \Engine\Http\Request

#### protected $response;

Содержит ссылку на инстанс класса \Engine\Http\Response

#### protected $rules;

Содержит массив правил доступа к действиям контроллера.

#### protected $authMehod;

Содержит метод авторизации. 
Если не задано авторизация не производится.

### Методы

#### Controller::__construct(Request $request, Response $response)

Через конструктор в контроллер инжектятся зависимости Request и Response.
Происходит инициализация правил доступа заданных в методе rules() и вызывается метод init().

Если в наследнике необходимо переопределить конструктор, то НЕОБХОДИМО вызвать родительский в самом начале:

````php
SiteController::__contructor(Request $request, Response $response) {
    parent::__constructor($request, $response);
    // ваш код
}
````

#### Controller::init()

Данный метод вызывается сразу после конструктора. Если необходимо кастомизировать поведение контроллера то предпочтительней воспользоваться данным методом а не переопределять конструктор.

#### Controller::rules()

Возвращает массив, в котором описаны правила доступа к действиям. Правила описаны в документе выше.

#### Controller::beforeAction($action);

Данный метод вызывается до авторизации и применения правил дсотупа. Таким образом используя данный метод можно катсомизировать поведение контроллера.
Например, изменить метод авторизации.

@param string $action имя вызываемого действия (например 'indexAction')

#### Controller::asJson($data, $options = 320)

Вернуть овтет клиенту в виде джейсон строки. 
В заголовк ответа 'Content-Type' будет установленно значение 'application/json; charset=UTF-8'.
В тело ответа запишет джейсон строку полученную из $data.
Отправит ответ клиенту и заврешит работу приложения.

#### Controller::render(string $template, array $params = [])

Вернуть response с шаблоном $template. 
Ассоциативный массив $params. Из него будут извлечене переменные, где имя переменной это ключ а значением будет значение массива.
Эти переменные будут переданы в шаблон. 
 
#### Controller::fetch(string $template, array $params = [])  

Вернуть шаблон $template в виде строки. 
Ассоциативный массив $params. Из него будут извлечене переменные, где имя переменной это ключ а значением будет значение массива.
Эти переменные будут переданы в шаблон. 

#### Controller::getRequest()

Вернет ссылку на инстанс класса Request.

#### Controller::getResponse()

Вернет ссылку на экземпляр класса Response

#### Controller::getAccessRules()

Вернет массив, содержащий правила дсотупа к действиям контроллера.
Либо null если не задан. В данном случае правила доступа к действия применены не будут.

#### Controller::getAuthMethod()

Вернет метод которым необходимо авторизовать пользователя. 
Либо null если не задан. В данном случае пользователь не будет авторизован и будет считаться как guest.