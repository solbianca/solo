## Работа с формами

Для обработки входящих данных необходимо определить модель, которая будет этим знаиматься. 
Фреймворк предлагает базовую модель `\Engine\Base\Form`. Все модели форм должны наследоваться от данной базовой модели.

Модель формы использует трейты [LoadTrait](traits/load.md) и [AttributesTrait](traits/attributes.md).

Пример простой формы:

````php
class LoginForm extends Form {

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var array
     */
    protected $validationRules = [
        'username' => 'required|email',
        'password' => 'required|alnumDash|min(4)|max(20)',
    ];

    public function login() {
	    $authService = new Auth();;
	    $result = $authService->send('login', [
	        'username' => $this->username, 
	        'password' => $this->password
	    ]);
	    return (empty($result)) ? false : json_decode($result, true);
	}
}
````
Контроллер:

````php
class AuthController extends Controller {
    public function indexAction() {
        $formModel = new LoginForm();
        if ($formModel->load($this->request->post()) && $formModel->validate()) {
            if (false !== ($result = $formModel->login())) {
                $this->asJson($result);
            }
            $this->asJson('Wrong user login or/and password.');
        }
    }
}
````

### Валидация данных

Валидацией занимается отдельный компонент, более подрогбно про него прочитать по в [документации](validation.md).

В модели необходимо определить публичные свойства. Далее необходимо прописать правила валидации данных свойств. 
Для этого нужно либо передать массив правил в метод Form::setValidationRules(array $rules = []) либо определить свойство $validationRules.

Далее необходимо определить данные в методы объекта и вызвать метод Form::validate().
В случае успеха метод вернет true, в случае неудачи false. Ошибки валидации можно посмотреть вызвав метод Form::getValidationErrors().

### Свойства

#### protected $validator;

Содержит экземпляр класса Engine\Validation\Validator либо null если он еще не инициализиров. 
Экземлер валидатора будет создан при вызове метода Form::getValidator() либо Form::validate()

#### protected $validationRules = [];

Содержит массив правил валидации для свойств объекта. Подробнее в [документации по валидации](validation.md).

### Методы (помимо тех, что дают трейты):

Модель формы предназначена для обработки входящих данных и передачи их дальше в приложении.
Поэтому модель обладает методами (помимо тех что дают трейты) для валидации данных.

#### Form::setValidator(ValidatorInterface $validator)

Устанавливает валидатор для модели. Валидатор должен имплемитировать интерфейс ValidatorInterface. 

#### Form::getValidator()

@return Validator|null Создает экземплеяр класса Vaslidator и возвращает ссылку на валидатор либо null если не задан.
null вернет тольбко в том случае если валидатор не задан в контейнере зависимостей Engine::$app->getDic()->get('validator')

#### Form::validate()

Валидация данных формы.

@return bool true если валидация успешна, иначе false.

#### Form::getValidationErrors()

Возвращает ошибки валидации. Если валидатор не задан, возвращает null. Иначе инстанс класса MessagerBag

@return \Engine\Validation\Support\MessageBag|null

#### Form::setValidationRules(array $rules = [])

Задать правила валидации. Массив $rules содержит правила, по которым будут валидироваться данные.

#### Form::getValidationRules()

Возвращает массив, который содержит правила валидации. Если не заданы то пустой массив.

### Методы трейтов

 - Form::load(array $data, $scope = null)
 - Form::attributes()
 - Form::setAttributes($values)
 - Form::getAttributes($names = null, $except = [])