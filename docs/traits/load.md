## LoadTrait

Позволяет загружать данные в модель из входящего массива.

Данный трейт зависит от [AttributesTrait](attributes.md).

### LoadTrait::load(array $data, $scope = null)

@param array $data Массив данных из которых необходимо выбрать данные в модель. Обычно это $_GET или $_POST.

@param null $scope Облость видимости для выборки. 

@return bool true если найдены данные в переданном скоупе, иначе false.