## RabbitMQ

Обертка над расширением для работы с очередью rabbitmq

### Config

````
return [
...
    'rabbit => [
        'rabbit.main' => [
            'connection' => [
   		        'host' => '127.0.0.1',
   			    'port' => '5672',
   		        'username' => 'guest',
    		    'password' => 'guest',
    		    'vhost' => '/',
    	    ],
    	    'exchange' => [
    		    'name' => 'test.exchange',
    		    'type' => RabbitMQ::EXCHANGE_DIRECT,
    	    ],
        ],
        'rabbit.second' => [
    	    'connection' => [
    		    'host' => '127.0.0.1',
    		    'port' => '5672',
    		    'username' => 'guest',
    		    'password' => 'guest',
    		    'vhost' => '/',
    	    ],
    	    'exchange' => [
    		    'name' => 'test.exchange',
    		    'type' => RabbitMQ::EXCHANGE_DIRECT,
    	    ],
        ],
    ...
    ]
````

Для всех допустимых парамтеров 'connection' необходимо смотреть в метод класса \AMQPConnection

Для 'exchange' нужно указать два обязательных параметра 'name'(имя обменника) и 'type' (тип обменника).
Так же можно указать опциональные параметры 'flags' - битовая маска флагов. И 'arguments' массив вида ключ\значение.

### RabbitMQManager

При инициализации приложения в контейнер зависимостей будет помещен экземпляр класса \Engine\RabbitMQ\RabbitMQManager.
Обратится к нему можно:
$rabbitmqManager = Engine::$app->getDic()->get->('rabbitmqManager');

У менеджера есть публичный метод RabbitMQManager::getRabbitMQ(string $name): RabbitMQInterface.

$rabbitmq = $rabbitmqManager->getRabbitMQ('rabbit.main');

Где $name алиас, например, 'rabbit.main' из конфига выше. При первом обращении будет создан экземпляр класса реализующий интерфейс RabbitMQInterface и вернется клиенту.
При повторном обращении будет возвращенна ссылка на уже созданный объект.

### Consumer и Producer

Обект $rabbitmq может создать consumer'а и producer'a. Первый читает из очереди, второй пишет.

#### Producer

$producer = $rabbitmq->createProducer(string $keyPrefix = '');

Опубликовать сообщение в очередь: 

 $producer->publish(string $key, $value, int $flags = RabbitMQ::NOPARAM, array $attributes = ['delivery_mode' => 2]): bool**

````

$key - ключ очереди. Если был устьановлен префикс, то опубликвано будет в очередь вида $keyPrefix . $key

$value - сообщение, которое будет отправленно в очередь. Перед отправкой сообщение сериализуется командой serialize().

flags - Битовая маска флагов, которая будет установлена.

$attributes - ассоциативный массив атрибутов.

Вернет true в случае успешной публикации сообщения в очередь. Иначе false.


````

$producer->publishJson(string $key, $value, int $flags = RabbitMQ::NOPARAM, array $attributes = ['delivery_mode' => 2]): bool

````

$key - ключ очереди. Если был устьановлен префикс, то опубликвано будет в очередь вида $keyPrefix . $key

$value - сообщение, которое будет отправленно в очередь. Перед отправкой сообщение сериализуется командой json_encode().

flags - Битовая маска флагов, которая будет установлена.

$attributes - ассоциативный массив атрибутов.

Вернет true в случае успешной публикации сообщения в очередь. Иначе false.


````

#### Consumer

$consumer = $rabbitmq->createConsumer(string $keyPrefix = '', string $exchangeName = '', int $flags = RabbitMQ::NOPARAM)

 - Установить битовую маску флагов:

$consumer->setFlags(int $flags) 

 - Получить битовую маску флагов:

$consumer->getFlags()

 - Установить имя обменника
 
$consumer->setExchangeName(string $exchangeName)
 
 - Получить имя обменника
  
 $consumer->getExchangeName()
 
 
**NOTE: Consumer::consume() и Consumer::consumeNoAutoAck() имеют разное поведение. Так получилось изза того, что нам надо учитывать и подстраиватся под существующее поведение epn.bz.**   
 
 - Получить сообщение из очереди
  
$consumer->consume(string $key, int $flags = AMQP_AUTOACK)

````

$key - Имя очереди. Если был устьановлен префикс, то читать будет из очередь вида $keyPrefix . $key

$flags - Битовая маска флагов.

Метод вернет mixed. Метод читает сообщение из очереди и пробует десериализовать его функцией unserialize().
В случае пустого сообщения возвращает false.

````
 
 - Получить сообщение из очереди без удаления
 
$consumer->consumeNoAutoAck(string $key)
 
````

$key - Имя очереди. Если был устьановлен префикс, то читать будет из очередь вида $keyPrefix . $key

Метод вернет mixed. Метод читает сообщение из очереди и если оно не пустове возвращает его в виде строки.
В случае пустого сообщения возвращает false.

````