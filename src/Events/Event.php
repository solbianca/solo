<?php


namespace Engine\Events;


use Engine\Events\Interfaces\EventInterface;

class Event implements EventInterface {

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var array
	 */
	private $params;

	/**
	 * @var bool
	 */
	private $flag;

	/**
	 * @var null|string|object
	 */
	private $target;

	/**
	 * Event constructor.
	 * @param string $name
	 * @param null|string|object $target
	 * @param array $params
	 * @param bool $flag
	 */
	public function __construct(string $name = '', $target = null, array $params = [], bool $flag = false) {
		$this->setName($name);
		$this->setTarget($target);
		$this->setParams($params);
		$this->stopPropagation($flag);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTarget() {
		return $this->target;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParams(): array {
		return isset($this->params) ? $this->params : [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParam($name) {
		if (isset($this->params[$name])) {
			return $this->params[$name];
		}
		return null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setName(string $name) {
		$this->name = strval($name);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setTarget($target) {
		$this->target = $target;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setParams(array $params) {
		$this->params = $params;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function stopPropagation(bool $flag) {
		$this->flag = boolval($flag);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isPropagationStopped(): bool {
		return $this->flag;
	}
}