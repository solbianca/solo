<?php


namespace Engine\Events\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class EventException extends ApplicationException {

}