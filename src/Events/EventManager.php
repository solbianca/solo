<?php


namespace Engine\Events;


use Engine\Events\Exceptions\EventManagerException;
use Engine\Events\Interfaces\EventInterface;
use Engine\Events\Interfaces\EventManagerInterface;

class EventManager implements EventManagerInterface {

	/**
	 * @var EventPriorityQueue[]
	 */
	private $eventsPool = [];

	/**
	 * @var \SplObjectStorage[]
	 */
	private $objectsPool = [];

	/**
	 * {@inheritdoc}
	 */
	public function attach(string $event, callable $callback, int $priority = EventPriorityQueue::DEFAULT_LEVEL) {
		if (!isset($this->eventsPool[$event])) {
			$this->eventsPool[$event] = new EventPriorityQueue();
			$this->objectsPool[$event] = new \SplObjectStorage();
		}

		if ($callback instanceof \Closure) {
			$callback = function ($event) use ($callback) {
				return call_user_func($callback, $event);
			};
		}

		$this->objectsPool[$event]->attach($callback, $priority);
		return $this->eventsPool[$event]->insert($callback, $priority);
	}

	/**
	 * {@inheritdoc}
	 */
	public function detach(string $event, callable $callback) {
		if ($this->objectsPool[$event]->contains($callback)) {
			$this->objectsPool[$event]->detach($callback);
			$this->eventsPool = new EventPriorityQueue($this->objectsPool[$event]);
		}
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearListeners(string $event) {
		$this->objectsPool[$event]->removeAll($this->objectsPool[$event]);
		$this->eventsPool[$event] = new EventPriorityQueue();
	}

	/**
	 * {@inheritdoc}
	 */
	public function trigger($event, $target = null, $argv = []) {
		if (is_string($event)) {
			$name = $event;
			$event = new Event($name, $target, $argv);
		}
		elseif ($event instanceof EventInterface) {
			$name = $event->getName();
			$event->setParams($argv);
			$event->setTarget($target);
		}
		else {
			throw new EventManagerException("First parameter event must be a string or instance of EventInterface, got" . gettype($event) . ".");
		}
		$callbacks = $this->eventsPool[$name];
		$callbacks->rewind();
		while ($callbacks->valid()) {
			if ($event->isPropagationStopped()) {
				break;
			}
			call_user_func($callbacks->current(), $event);
			$callbacks->next();
		}
		return;
	}
}