<?php


namespace Engine\Events;


use Engine\Events\Exceptions\EventPriorityQueueException;

class EventPriorityQueue extends \SplPriorityQueue {

	const DEFAULT_LEVEL = 0;

	/**
	 * EventPriorityQueue constructor.
	 * @param \SplObjectStorage $objectStorage
	 * @param null $flag
	 */
	public function __construct(\SplObjectStorage $objectStorage = null, $flag = null) {
		if ($flag === null) {
			$flag = static::EXTR_DATA;
		}
		$this->refresh($objectStorage);
		$this->setExtractFlags($flag);
	}

	/**
	 * Insert callback to queue with priority
	 *
	 * @param callable $callback
	 * @param int $priority
	 * @return bool
	 * @throws EventPriorityQueueException
	 */
	public function insert($callback, $priority = self::DEFAULT_LEVEL): bool {
		if (!is_callable($callback)) {
			throw new EventPriorityQueueException("Callback must be callable.");
		}
		elseif (!is_numeric($priority)) {
			throw new EventPriorityQueueException("Priority must be integer.");
		}
		parent::insert($callback, $priority);
		return true;
	}

	/**
	 * Refresh event queue
	 *
	 * @param \SplObjectStorage $objectStorage
	 * @return bool
	 */
	public function refresh(\SplObjectStorage $objectStorage = null) {
		if (null === $objectStorage) {
			return true;
		}
		if ($objectStorage->count() > 0) {
			$objectStorage->rewind();
			while ($objectStorage->valid()) {
				$this->insert($objectStorage->current(), $objectStorage->getInfo());
				$objectStorage->next();
			}
		}
		return true;
	}
}