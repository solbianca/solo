<?php


namespace Engine\Events\Interfaces;


/**
 * Representation of an event
 */
interface EventInterface {
	/**
	 * Get event name
	 *
	 * @return string
	 */
	public function getName(): string;

	/**
	 * Get target/context from which event was triggered
	 *
	 * @return null|string|object
	 */
	public function getTarget();

	/**
	 * Get parameters passed to the event
	 *
	 * @return array
	 */
	public function getParams();

	/**
	 * Get a single parameter by name
	 *
	 * @param  string $name
	 * @return mixed
	 */
	public function getParam($name);

	/**
	 * Set the event name
	 *
	 * @param  string $name
	 * @return self
	 */
	public function setName(string $name);

	/**
	 * Set the event target
	 *
	 * @param  null|string|object $target
	 * @return self
	 */
	public function setTarget($target);

	/**
	 * Set event parameters
	 *
	 * @param  array $params
	 * @return self
	 */
	public function setParams(array $params);

	/**
	 * Indicate whether or not to stop propagating this event
	 *
	 * @param  bool $flag
	 * @return self
	 */
	public function stopPropagation(bool $flag);

	/**
	 * Has this event indicated event propagation should stop?
	 *
	 * @return bool
	 */
	public function isPropagationStopped(): bool;
}