<?php

namespace Engine\Cache\Exception;

use Engine\Core\Exceptions\ApplicationException;

class CachePoolException extends ApplicationException
{

}