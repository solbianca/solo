<?php

namespace Engine\Cache\Exception;

use Engine\Core\Exceptions\ApplicationException;

class CacheException extends ApplicationException {

}