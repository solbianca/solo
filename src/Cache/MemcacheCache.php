<?php

namespace Engine\Cache;

use Engine\Cache\Exception\CacheException;
use Engine\Cache\Traits\MemcacheTrait;

/**
 * Class MemcacheCache
 * @package Engine\Cache
 */
class MemcacheCache extends AbstractCache {
	use MemcacheTrait;


	/**
	 * MemcacheCache constructor.
	 * @param string $host
	 * @param string $port
	 * @param string $cacheKeyPrefix
	 */
	public function __construct($host, $port, $cacheKeyPrefix) {
		$this->init($host, $port, $cacheKeyPrefix);
	}


	/**
	 * @param string $host
	 * @param string $port
	 * @param string $cacheKeyPrefix
	 * @throws CacheException
	 */
	private function init($host, $port, $cacheKeyPrefix = '') {
		try {
			$this->cacheKeyPrefix = $cacheKeyPrefix;
			$this->client = new \Memcache;
			$this->client->pconnect($host, $port);
		} catch (\Exception $e) {
			throw new CacheException("Can't connect to Memcache. " . $e->getMessage());
		}
	}

}