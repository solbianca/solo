<?php

namespace Engine\Cache;

use Engine\Cache\Exception\CachePoolException;

/**
 * Пул различных кешей
 *
 * Class Pool
 * @package Engine\Cache
 */
class Pool {

	/**
	 * @var array
	 */
	protected $configs;

	/**
	 * @var CacheInterface[]
	 */
	protected $caches;

	/**
	 * Holds aliases for write-protected definitions
	 *
	 * @var array
	 */
	protected $immutable = [];

	/**
	 * Store connection configuration.
	 *
	 * @param array $config Configuration for connection
	 * @param string $alias Alias for connection name
	 * @param bool $immutable Make connection immutable.
	 * If true you can't override connection.
	 * @throws CachePoolException
	 */
	public function register(array $config, $alias, $immutable = true) {
		if ('' === $alias || !is_string($alias)) {
			throw new CachePoolException("Connection alias can't be empty value.");
		}
		if ($immutable && in_array($alias, $this->immutable)) {
			throw new CachePoolException("Connection already defined and can't be overridden.");
		}

		if ($immutable) {
			$this->immutable[] = $alias;
		}
		$this->configs[$alias] = $config;
	}

	/**
	 * Create connection for cache by given config
	 *
	 * @param $alias
	 * @param array $config Configuration for connection
	 * @return CacheInterface
	 * @throws CachePoolException
	 */
	public function connect($alias, array $config) {
		switch ($alias) {
			case 'memcache':
				$cache = $this->connectMemcache($config);
				break;
			default:
				throw new CachePoolException("Connection method not defined.");
		}
		return $cache;
	}

	/**
	 * Get existing cache.
	 *
	 * @param $alias
	 * @return CacheInterface
	 * @throws CachePoolException
	 */
	public function get($alias) {
		if (isset($this->caches[$alias])) {
			return $this->caches[$alias];
		}

		if (!isset($this->configs[$alias])) {
			throw new CachePoolException("Connection configuration not stored. You try get not existing cache connection configuration.");
		}

		$this->caches[$alias] = $this->connect($alias, $this->configs[$alias]);

		return $this->caches[$alias];
	}

	/**
	 * @param $config
	 * @return MemcacheCache
	 * @throws CachePoolException
	 */
	private function connectMemcache($config) {

		if (!(isset($config['host']) || isset($config['port']))) {
			throw new CachePoolException("Memcache config must have host and port parameters");
		}

		if (empty($config['cacheKeyPrefix'])) {
			throw new CachePoolException("Cache prefix can't be empty.");
		}

		try {
			return new MemcacheCache($config['host'], $config['port'], $config['cacheKeyPrefix']);
		} catch (\Exception $e) {
			throw new CachePoolException("Memcache connection error. " . $e->getMessage());
		}

	}
}