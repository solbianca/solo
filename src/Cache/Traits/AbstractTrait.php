<?php


namespace Engine\Cache\Traits;


trait AbstractTrait {

	/** @var  string */
	protected $cacheKeyPrefix;

	/**
	 * @param string $url
	 * @return mixed|string
	 */
	public function normalizeUrlForCacheKey($url) {

		$rv = 'none';
		if (preg_match('{^https?://aliexpress.com(/|$)}ui',
				$url) || preg_match('{^https?://[^/]+\.aliexpress.com(/|$)}ui', $url)
		) {
			if (preg_match('{^https?://[^/]+/item/[^/]+/(\d+)\.htm}ui', $url)) {
				$rv = preg_replace('{^https?://[^/]+/item/[^/]+/(\d+)\.ht.+$}ui', "offer_$1", $url);
			}
			elseif (preg_match('{^https?://[^/]+/store/product/[^/]+/(\d+)_(\d+)\.htm}ui', $url)) {
				$rv = preg_replace('{^https?://[^/]+/store/product/[^/]+/(\d+)_(\d+)\.ht.+$}ui', "group_offer_$1-$2",
					$url);
			}
			elseif (preg_match('{^https?://group\.aliexpress\.com/(\d+)-(\d+)-detail\.htm}ui', $url)) {
				$rv = preg_replace('{^https?://group\.aliexpress\.com/(\d+)-(\d+)-detail\.ht.+$}ui',
					"group_offer_$1-$2", $url);
			}
			elseif (preg_match('{^https?://[^/]+/category/(\d+)/[^/]+\.htm}ui', $url)) {
				$rv = preg_replace('{^https?://[^/]+/category/(\d+)/[^/]+\.ht.+$}ui', "category_$1", $url);
			}
			elseif (preg_match('{^https?://[^/]+/category/(\d+)/[^/]+/(\d+)\.htm}ui', $url)) {
				$rv = preg_replace('{^https?://[^/]+/category/(\d+)/[^/]+/(\d+)\.ht.+$}ui', "category_$1-$2", $url);
			}
		}
		elseif (preg_match('{^https?://ozon.ru(/|$)}ui', $url) || preg_match('{^https?://[^/]+\.ozon.ru(/|$)}ui',
				$url)
		) {
			if (preg_match('{^https?://(?:[^\.]+\.)?ozon.ru/context/detail/id/\d+}ui', $url)) {
				$url = preg_replace('{https?://[^/]+/context/detail/id/}ui', '$1', $url);
				$url = preg_replace('{^(\d+).*$}', '$1', $url);
				$rv = 'ozon_' . $url;
			}
		}
		return $rv;
	}

	/**
	 * @param $key
	 * @return string
	 */
	protected function formatKey($key) {
		return $this->cacheKeyPrefix . $key;
	}
}