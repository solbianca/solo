<?php

namespace Engine\Cache\Traits;

use Engine\Cache\Exception\CacheException;


trait MemcacheTrait {
	/** @var  \Memcache */
	private $client;

	/**
	 * {@inheritdoc}
	 */
	public function get($key, $default = null) {
		try {
			$value = json_decode($this->client->get($this->formatKey($key)), true);

			return $value ? $value : $default;
		} catch (\Exception $e) {
			throw new CacheException("Can't get key '$key' " . $e->getMessage());
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function set($key, $value, $ttl = null) {
		try {
			return $this->client->set($this->formatKey($key), json_encode($value), MEMCACHE_COMPRESSED, $ttl);
		} catch (\Exception $e) {
			throw new CacheException("Can't set key: $key. value: " . json_encode($value) . " " . $e->getMessage());
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function delete($key) {
		return $this->client->delete($this->formatKey($key));
	}


	/**
	 * {@inheritdoc}
	 */
	public function clear() {
		$this->client->flush();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getMultiple($keys, $default = null) {
		$result = [];
		foreach ($keys as $key) {
			$result[$key] = $this->get($key, $default);
		}
		return $result;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setMultiple($values, $ttl = null) {
		foreach ($values as $key => $value) {
			$this->set($key, $value, $ttl);
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function deleteMultiple($keys) {
		foreach ($keys as $key) {
			$this->delete($key);
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function has($key) {
		return false !== $this->get($key);
	}

	/**
	 * @param $key
	 * @param int $ttl
	 * @return int
	 * @throws CacheException
	 */
	public function incrementKey($key, $ttl = 3600) {
		$value = $this->get($key);
		if (null === $value) {
			$this->set($key, 1);
			return 1;
		}

		if (!is_numeric($value)) {
			throw new CacheException("Can't increment non numeric value of key: $key");
		}
		$this->set($key, ++$value, $ttl);
		return $this->get($key);
	}

	/**
	 * @param $key
	 * @param int $ttl
	 * @return bool|int
	 * @throws CacheException
	 */
	public function decrementKey($key, $ttl = 3600) {
		$value = $this->get($key);
		if (null === $value) {
			$this->set($key, 1);
			return 1;
		}
		if (!is_numeric($value)) {
			throw new CacheException("Can't decrement non numeric value of key: $key");
		}
		$this->set($key, --$value, $ttl);
		return $this->get($key);
	}

}