<?php

namespace Engine\Cache;

use Engine\Cache\Traits\AbstractTrait;

/**
 * Class AbstractCache
 * @package Engine\Cache
 */
abstract class AbstractCache implements CacheInterface
{
	use AbstractTrait;

}