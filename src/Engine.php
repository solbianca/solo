<?php

namespace Engine;


use Engine\Core\WebApplication;
use Engine\Core\ServiceApplication;

class Engine {

	/**
	 * @var WebApplication|ServiceApplication
	 */
	public static $app;

	/**
	 * Logs with an arbitrary level.
	 *
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 *
	 * @return void
	 * @throws \Exception
	 *
	 * @TODO Метод для логирования ошибок без дерганья контейнера зависимостей
	 */
	public static function log($level, $message, array $context = array()) {
		throw new \Exception('Заглушка.');
	}

	/**
	 * Create instance from class string with parameters.
	 *
	 * @param string $class Full class name with namespace
	 * @param array $params Parameters that must be given in class constructor
	 * @return object
	 */
	public static function createInstance($class, array $params = []) {
		$reflectionClass = new \ReflectionClass($class);
		return $reflectionClass->newInstanceArgs($params);
	}
}