<?php

namespace Engine\Console;

use Engine\Helpers\ConsoleHelper;


/**
 * Class Console
 * @package Engine\Console
 *
 * Console helper provides useful methods for command line related tasks
 * such as getting input or formatting and coloring output.
 *
 */
class Console extends ConsoleHelper
{

}