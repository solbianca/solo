<?php


namespace Engine\Console;


use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Interfaces\RequestInterface;
use Engine\Core\Interfaces\ResponseInterface;
use Engine\Core\Interfaces\RouterInterface;

/**
 * Class ConsoleRouter
 * @package Engine\Console
 *
 * Usage. Set in config:
 * return [
 *        'console_commands' => [
 *              'login' => 'Site::login',
 *              'user.add' => 'Site\User::create',
 *        ],
 * ];
 */
class ConsoleRouter implements RouterInterface {

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * ConsoleRouter constructor.
	 *
	 * @param Request|RequestInterface $request
	 * @param Response|ResponseInterface $response
	 */
	public function __construct(RequestInterface $request, ResponseInterface $response) {
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * @inheritdoc
	 */
	public function resolve(array $urls) {
		list($commandName, $params) = $this->request->resolve();

		if (!isset($urls[$commandName])) {
			throw new ApplicationException("Command '$commandName' not found. Please check config 'console_commands'");
		}

		$controllerPattern = explode('::', $urls[$commandName]);
		if (count($controllerPattern) !== 2) {
			throw new ApplicationException("Bad command class pattern for commmandNmae '$commandName'");
		}

		if (empty($controllerPattern[0]) || empty($controllerPattern[1])) {
			throw new ApplicationException("Bad command controller class or command router class. Please check config");
		}

		$class = 'App\Console\Controllers\\' . ucfirst($controllerPattern[0]) . 'Controller';
		$action = $controllerPattern[1] . 'Action';

		return ['controller' => $class, 'action' => $action, 'params' => []];
	}
}