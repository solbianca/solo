<?php

namespace Engine\Console\Components\Traits;

use Engine\Console\Models\Migration;

trait UpdateMigrationTrait {

	/**
	 * update new migrations
	 */
	public function update() {
		$migration = new Migration();
		$history = $migration->getMigrationHistory();

		$existingMigrations = array_map(function ($item) {
			return $item['name'];
		}, $history);

		$migrationFiles = $this->getMigrationsFromDirectory($this->findMigrationPath($this->migrationNamespace));

		$migrations = [];
		if (!$migrationFiles) {
			self::output("There is nothing to migrate. Your system is already up-to-date");
			return;
		}

		foreach ($migrationFiles as $migrationFileName) {
			if (in_array($migrationFileName, $existingMigrations)) {
				continue;
			}
			$migrations[] = $migrationFileName;
		}

		if (!$migrations) {
			self::output("There is nothing to migrate. Your system is already up-to-date");
			return;
		}

		self::output("\n**** migrations for update: \n-------------------------------");
		foreach ($migrations as $m) {
			self::output($m);
		}

		if (self::confirm("Run all this migrations?")) {
			foreach ($migrations as $m) {
				if (!$this->migrateUp($m)) {
					break;
				}
				$migration->addMigration($m);
			}
		}

	}

	/**
	 * применение миграции
	 *
	 * @param $name
	 * @return bool
	 */
	private function migrateUp($name) {
		$class = $this->migrationNamespace . '\\' . $name;
		self::stdout("*** applying $class\n");
		if (false !== (new $class())->up()) {
			self::stdout("*** applied $class \n\n");
			return true;
		}
		else {
			self::stdout("*** failed to apply $class \n\n");
			return false;
		}
	}
}