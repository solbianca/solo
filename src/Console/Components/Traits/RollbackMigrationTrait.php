<?php

namespace Engine\Console\Components\Traits;

use Engine\Console\Models\Migration;

trait RollbackMigrationTrait {

	/**
	 * revert migrations
	 *
	 * @param int $count - count of migrations for rollback
	 */
	public function rollback($count = 1) {

		$migration = new Migration();
		$migrationsForRollback = $migration->getMigrationHistory($count);

		if (!$migrationsForRollback) {
			self::output("There is nothing to rollback.");
			return;
		}

		self::output("\n**** migrations for rollback: \n-------------------------------");
		foreach ($migrationsForRollback as $m) {
			self::output($m['name']);
		}

		$migration = new Migration();
		if (self::confirm("Rollback all this migrations?")) {
			foreach ($migrationsForRollback as $m) {
				if (!$this->migrateDown($m['name'])) {
					break;
				}
				$migration->removeMigration($m['name']);
			}
		}

	}

	/**
	 * migration rollback
	 *
	 * @param $name
	 * @return bool
	 */
	private function migrateDown($name) {
		$class = $this->migrationNamespace . '\\' . $name;
		self::stdout("*** reverting $class\n");
		if (false !== (new $class())->down()) {
			self::stdout("*** reverted $class \n\n");
			return true;
		}
		else {
			self::stdout("*** failed to revert $class \n\n");
			return false;
		}
	}
}