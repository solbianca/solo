<?php

namespace Engine\Console\Components\Traits;

use Engine\Helpers\FileHelper;

trait CreateMigrationTrait {
	/**
	 * create new migration
	 *
	 * @param $name - users migration name
	 */
	public function create($name) {
		$className = $this->generateClassName($name);
		$migrationPath = $this->findMigrationPath($this->migrationNamespace);

		$file = $migrationPath . DIRECTORY_SEPARATOR . $className . '.php';
		if (self::confirm("Create new migration '$file'?")) {
			$content = $this->generateMigrationSourceCode($className, $this->migrationNamespace);
			FileHelper::createDirectory($migrationPath);
			file_put_contents($file, $content);
			self::stdout("New migration created successfully.\n");
		}
	}
}