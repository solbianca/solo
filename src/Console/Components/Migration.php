<?php

namespace Engine\Console\Components;

use Engine\Console\Components\Traits\CreateMigrationTrait;
use Engine\Console\Components\Traits\RollbackMigrationTrait;
use Engine\Console\Components\Traits\UpdateMigrationTrait;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Engine;

class Migration extends BaseComponent {
	use CreateMigrationTrait;
	use UpdateMigrationTrait;
	use RollbackMigrationTrait;

	/**
	 * @var string
	 */
	protected $migrationNamespace = 'App\Databases\Migrations';

	/**
	 * @var string
	 */
	protected $migrationTemplateFile = __DIR__ . '/../Templates/migration.template';

	/**
	 * Generates class base name from migration name from user input.
	 *
	 * @param string $name migration name from user input.
	 * @return string
	 */
	private function generateClassName($name) {
		return substr('M_' . gmdate('ymdHis') . '_' . ucfirst($name), 0, 254);
	}

	/**
	 * Возвращает полный путь к файлу миграции на основе неймспейса.
	 *
	 * @param string|null $namespace migration namespace.
	 * @return string migration file path.
	 * @throws ApplicationException
	 */
	private function findMigrationPath($namespace) {
		$root = Engine::$app->getConfig('path.root');
		if (!$root) {
			throw new ApplicationException("path.root not found in config");
		}
		return $root . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $namespace);
	}

	/**
	 *  Достает текст шаблона новой миграции и заменяет в ней имя класса и неймспейс
	 *
	 * @param string $class
	 * @param string $namespace
	 * @return string
	 */
	private function generateMigrationSourceCode($class, $namespace) {

		$migrationSourceCode = file_get_contents($this->migrationTemplateFile);

		return strtr($migrationSourceCode, array_combine(['__CLASS__', '__NAMESPACE__'], [$class, $namespace]));
	}

	/**
	 * получаем все имеющиеся файлы миграций
	 *
	 * @param $migrationPath
	 * @return array
	 */
	private function getMigrationsFromDirectory($migrationPath) {
		$migrations = [];

		$handle = opendir($migrationPath);
		while (($file = readdir($handle)) !== false) {
			if ($file === '.' || $file === '..') {
				continue;
			}
			$path = $migrationPath . DIRECTORY_SEPARATOR . $file;
			if (preg_match('/^(m_(\d{6}_?\d{6})\D.*?)\.php$/is', $file, $matches) && is_file($path)) {
				$class = $matches[1];

				$time = str_replace('_', '', $matches[2]);
				$migrations[$time] = $class;
			}
		}
		closedir($handle);

		ksort($migrations);

		return $migrations;
	}


}