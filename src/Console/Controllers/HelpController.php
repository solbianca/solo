<?php

namespace Engine\Console\Controllers;

use Engine\Base\Console\Controller;


/**
 * Class HelpController
 * @package Engine\Console\Controllers
 */
class HelpController extends Controller {
	/**
	 * TODO дописать index action чтобы выводились все доступные команды
	 *
	 * Заглушка, нужно дописать
	 */
	public function indexAction() {
		echo "\n"
			. "available commands: \n--------------------\n"
			. "help \n"
			. "migrate/create your_migration_short_name \n"
			. "migrate \n"
			. "migrate/rollback \n";
	}
}