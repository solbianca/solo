<?php

namespace Engine\Console\Controllers;

use Engine\Base\Console\Controller;
use Engine\Console\Components\Migration;

/**
 * Class MigrateController
 * @package Engine\Console\Controllers
 */
class MigrateController extends Controller {


	/**
	 * update new migrations
	 */
	public function indexAction() {
		(new Migration())->update();
	}

	/**
	 * Creates a new migration.
	 *
	 * This command creates a new migration using the available migration template.
	 * After using this command, developers should modify the created migration
	 * skeleton by filling up the actual migration logic.
	 *
	 * ```
	 * console migrate/create create_user_table
	 * ```
	 *
	 * @throws \Exception
	 *
	 */
	public function createAction() {
		$name = $this->getParam(0); //users migration name

		if (null === $name) {
			throw new \Exception("Method migrate create should contain migration_short_name");
		}

		if (!preg_match('/^[A-Za-z_0-9]+$/', $name)) {
			throw new \Exception('The migration name should contain latin letters, digits, underscore characters only.');
		}

		(new Migration())->create($name);
	}

	/**
	 * Downgrades the application by reverting old migrations.
	 * for example:
	 *
	 * console migrate/rollback     -- rollback all migrations
	 * console migrate/rollback 2   -- rollback last 2 migrations
	 */
	public function rollbackAction() {
		$count = $this->getParam(0); //count of migrations for rollback
		(new Migration())->rollback($count);
	}


}