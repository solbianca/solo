<?php

namespace Engine\Console\Models;

use Engine\Base\Model;
use Engine\Core\Exceptions\ApplicationException;

class Migration extends Model {
	/**
	 * @var string Table name
	 */
	protected static $table = 'migrations';

	protected $tableDefinition = [
		'name' => 'string',
		'createdAt' => 'timestamp',
	];


	/**
	 * получить список прмененых миграций из БД
	 *
	 * @param null $limit
	 * @return array
	 */
	public function getMigrationHistory($limit = null) {

		if (null !== $limit) {
			$limit = intval($limit);
		}

		if (!$this->isTableExists()) {
			$this->createMigrationHistoryTable();
		}

		$result = $this->db->select("SELECT `name`, `createdAt`  FROM " . static::getTable()
			. " ORDER BY `name` DESC "
			. ($limit ? " LIMIT $limit " : '')
		);

		return $result ? $result : [];
	}

	/**
	 * Creates the migration history table.
	 */
	protected function createMigrationHistoryTable() {
		try {
			$result = $this->db->query("CREATE TABLE `" . static::getTable() . "` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL COMMENT 'Название миграции',
		  `createdAt` DATETIME,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB COMMENT='Таблица миграций'
		");
			return (bool)$result;
		} catch (\Exception $e) {
			throw new ApplicationException("Can't create migration table. " . $e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * Check if a table exists in the current database.
	 *
	 * @return bool          True if table exists
	 */
	function isTableExists() {
		try {
			$result = $this->db->query("SELECT 1 FROM `" . static::getTable() . "` LIMIT 1");
		} catch (\Exception $e) {
			// We got an exception == table not found or connection problem
			return false;
		}

		return false !== $result;
	}

	/**
	 * @param $name
	 * @return string
	 */
	public function addMigration($name) {
		return $this->insert([
			'name' => $name,
			'createdAt' => (new \DateTime())->format('Y-m-d H:i:s'),
		]);
	}

	/**
	 * @param $name
	 * @return int
	 */
	public function removeMigration($name) {
		return $this->db->query("DELETE FROM `" . static::getTable() . "` WHERE `name` = :name LIMIT 1", [
			'name' => $name,
		]);
	}
}