<?php

namespace Engine\Console;

use Engine\Core\Interfaces\RequestInterface;

/**
 * The console Request represents the environment information for a console application.
 *
 * It is a wrapper for the PHP `$_SERVER` variable which holds information about the
 * currently running PHP script and the command line arguments given to it.
 */
class Request implements RequestInterface {
	private $params;


	/**
	 * Returns the command line arguments.
	 * @return array the command line arguments. It does not include the entry script name.
	 */
	public function getParams() {
		if ($this->params === null) {
			if (isset($_SERVER['argv'])) {
				$this->params = $_SERVER['argv'];
				array_shift($this->params);
			}
			else {
				$this->params = [];
			}
		}

		return $this->params;
	}

	/**
	 * Sets the command line arguments.
	 * @param array $params the command line arguments
	 */
	public function setParams($params) {
		$this->params = $params;
	}

	/**
	 * Resolves the current request into a route and the associated parameters.
	 * @return array the first element is the route, and the second is the associated parameters.
	 * @throws ConsoleException
	 */
	public function resolve() {
		$rawParams = $this->getParams();
		$endOfOptionsFound = false;
		if (isset($rawParams[0])) {
			$route = array_shift($rawParams);

			if ($route === '--') {
				$endOfOptionsFound = true;
				$route = array_shift($rawParams);
			}
		}
		else {
			$route = '';
		}

		$params = [];
		foreach ($rawParams as $param) {
			if ($endOfOptionsFound) {
				$params[] = $param;
			}
			elseif ($param === '--') {
				$endOfOptionsFound = true;
			}
			elseif (preg_match('/^--([\w-]+)(?:=(.*))?$/', $param, $matches)) {
				$name = $matches[1];
				if (is_numeric(substr($name, 0, 1))) {
					throw new ConsoleException('Parameter "' . $name . '" is not valid');
				}

				$params[$name] = isset($matches[2]) ? $matches[2] : true;
			}
			elseif (preg_match('/^-([\w-]+)(?:=(.*))?$/', $param, $matches)) {
				$name = $matches[1];
				if (is_numeric($name)) {
					$params[] = $param;
				}
				else {
					$params['_aliases'][$name] = isset($matches[2]) ? $matches[2] : true;
				}
			}
			else {
				$params[] = $param;
			}
		}

		return [$route, $params];
	}
}
