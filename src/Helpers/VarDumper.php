<?php

namespace Engine\Helpers;

class VarDumper {
	/**
	 * Simple variable dump.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::d(1,'test',[2,3]);
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * int(1)
	 * Classname->method() line XXX (in Filename.php)
	 * string(4) "test"
	 * Classname->method() line XXX (in Filename.php)
	 * array(2) {
	 *   [0]=>
	 *   int(2)
	 *   [1]=>
	 *   int(3)
	 * }
	 *
	 * @param mixed $arguments
	 */
	public static function d() {
		$arguments = func_get_args();
		if (empty($arguments)) {
			return;
		}
		foreach ($arguments as $argument) {
			echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
			echo self::dtrace();
			echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
			var_dump($argument);
			echo '</pre>' . PHP_EOL;
			echo '</div>' . PHP_EOL;
		}
	}

	/**
	 * Dump variable and die.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::dd(1,'test',[2,3]);
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * int(1)
	 * Classname->method() line XXX (in Filename.php)
	 * string(4) "test"
	 * Classname->method() line XXX (in Filename.php)
	 * array(2) {
	 *   [0]=>
	 *   int(2)
	 *   [1]=>
	 *   int(3)
	 * }
	 *
	 * @param mixed $arguments
	 */
	public static function dd() {
		$arguments = func_get_args();
		if (empty($arguments)) {
			echo self::dtrace();
			die();
		}
		foreach ($arguments as $argument) {
			self::d($argument);
		}
		die();
	}

	/**
	 * Simple print variable.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::p(1,'test',[2,3]);
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * 1
	 * Classname->method() line XXX (in Filename.php)
	 * test
	 * Classname->method() line XXX (in Filename.php)
	 * array (
	 *   [0] => 2
	 *   [1]=> 3
	 * )
	 *
	 * @param mixed $arguments
	 */
	public static function p() {
		$arguments = func_get_args();
		if (empty($arguments)) {
			return;
		}
		foreach ($arguments as $argument) {
			echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
			echo self::dtrace();
			echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
			print_r($argument);
			echo '</pre>' . PHP_EOL;
			echo '</div>' . PHP_EOL;
		}
	}

	/**
	 * Print variable and die.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::pd(1,'test',[2,3]);
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * 1
	 * Classname->method() line XXX (in Filename.php)
	 * test
	 * Classname->method() line XXX (in Filename.php)
	 * array (
	 *   [0] => 2
	 *   [1]=> 3
	 * )
	 *
	 * @param mixed $arguments
	 */
	public static function pd() {
		$arguments = func_get_args();
		if (empty($arguments)) {
			echo self::dtrace();
			die();
		}
		foreach ($arguments as $argument) {
			self::p($argument);
		}
		die();
	}

	/**
	 * Dump variable as string.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::ds(1,'test',[2,3]);
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * string(1) "1"
	 * Classname->method() line XXX (in Filename.php)
	 * string(4) "test"
	 * Classname->method() line XXX (in Filename.php)
	 * string(8) "Array{2}"
	 *
	 * @param mixed $arguments
	 */
	public static function ds() {
		$arguments = func_get_args();
		if (empty($arguments)) {
			return;
		}
		foreach ($arguments as $argument) {
			echo '<div style="background:#fafafa;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
			echo self::dtrace();
			echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
			if (is_object($argument)) {
				var_dump('Class: ' . get_class($argument));
			}
			elseif (is_array($argument)) {
				var_dump('Array{' . count($argument) . '}');
			}
			else {
				var_dump((string)$argument);
			}
			echo '</pre>' . PHP_EOL;
			echo '</div>' . PHP_EOL;
		}
	}

	/**
	 * Dump variable as string and die.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::dsd(1,'test',[2,3]);
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * string(1) "1"
	 * Classname->method() line XXX (in Filename.php)
	 * string(4) "test"
	 * Classname->method() line XXX (in Filename.php)
	 * string(8) "Array{2}"
	 *
	 * @param mixed $arguments
	 */
	public static function dsd() {
		$arguments = func_get_args();
		if (empty($arguments)) {
			echo self::dtrace();
			die();
		}
		foreach ($arguments as $argument) {
			self::ds($argument);
		}
		die();
	}

	/**
	 * Print peak memory usage.
	 *
	 * Usage:
	 * Engine_Libs_Helpers_VarDumper::dmem();
	 * You will see:
	 * Classname->method() line XXX (in Filename.php)
	 * XXXK of YYYM
	 */
	public static function dmem() {
		echo '<div style="background:#fafafa;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
		echo self::dtrace();
		echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
		echo sprintf('%sK of %s', round(memory_get_peak_usage() / 1024), ini_get('memory_limit'));
		echo '</pre>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
	}

	/**
	 * Measure execution time.
	 *
	 * @param array $timers
	 * @param int $status
	 * @param string $label
	 */
	public static function dtimer(&$timers, $status = 0, $label = null) {
		if (!is_array($timers) || $status === -1) {
			$timers = array();
		}
		$where = self::dtrace();
		if (null !== $label) {
			$where = $label . ' - ' . $where;
		}
		$timers[] = array('where' => $where, 'time' => microtime(true));
		if ($status === 1) {
			echo '<table style="border-color: black;" border="1" cellpadding="3" cellspacing="0">';
			echo '<tr style="background-color:black;color:white;"><th>Trace</th><th>dT [ms]</th><th>dT(cumm) [ms]</th></tr>';
			$lastTime = $timers[0]['time'];
			$firstTime = $timers[0]['time'];
			foreach ($timers as $timer) {
				echo sprintf('<tr><td>%s</td><td>%s</td><td>%s</td></tr>',
					$timer['where'],
					sprintf('%01.6f', round(($timer['time'] - $lastTime) * 1000, 6)),
					sprintf('%01.6f', round(($timer['time'] - $firstTime) * 1000, 6))
				);
				$lastTime = $timer['time'];
			}
			echo '</table>';
		}
	}

	/**
	 * Backtrace.
	 *
	 * @return string backtrace
	 */
	private static function dtrace() {
		$bt = debug_backtrace();
		while (!isset($bt[0]['file']) || $bt[0]['file'] === __FILE__) {
			array_shift($bt);
		}
		$trace = $bt[0];
		$line = $trace['line'];
		$file = basename($trace['file']);
		$function = $trace['function'];
		$class = (isset($bt[1]['class']) ? $bt[1]['class'] : basename($trace['file']));
		if (isset($bt[1]['class'])) {
			$type = $bt[1]['type'];
		}
		else {
			$type = ' ';
		}
		$function = isset($bt[1]['function']) ? $bt[1]['function'] : '';
		return sprintf('%s%s%s() line %s <small>(in %s)</small>', $class, $type, $function, $line, $file);
	}
}

function __d() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'd'), $args);
}

function __dd() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'dd'), $args);
}

function __p() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'p'), $args);
}

function __pd() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'pd'), $args);
}

function __ds() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'ds'), $args);
}

function __dsd() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'dsd'), $args);
}

function __dtrace() {
	$args = func_get_args();
	call_user_func_array(array('VarDumper', 'dtrace'), $args);
}