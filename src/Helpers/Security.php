<?php


namespace Engine\Helpers;


class Security {

	/**
	 * Generate unique id to specific length from given characters
	 *
	 * @param int $len
	 * @param string $symbols
	 * @return string
	 */
	public static function generateRandomString($len = 32, $symbols = null) {
		if (empty($symbols)) {
			$symbols = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}
		$length = ceil($len / strlen($symbols));
		return substr(str_shuffle(str_repeat($symbols, $length)), 1, $len);
	}
}