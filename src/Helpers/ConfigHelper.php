<?php

namespace Engine\Helpers;

class ConfigHelper {

    /**
     * Load configs folder as one array
     * @param string $path
     * @return array
     */
    public static function loadFolder($path) {
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        $configs = [];
        if (file_exists($path)) {
            $configFiles = scandir($path);
            foreach ($configFiles as $configFile) {
                if (strpos($configFile, '.php') !== false) {
                    $config = include_once $path . DIRECTORY_SEPARATOR . $configFile;
                    if (is_array($config)) {
                        $configs = \Engine\Helpers\ArrayHelper::merge($configs, $config);
                    }
                }
            }
        }

        return $configs;
    }

}