<?php


namespace Engine\Helpers\Traits\Page;

/**
 * Class PaginationHelperTrait
 * @package Engine\Helpers\Traits\Page
 */
trait PaginationHelperTrait
{

	/**
	 * возращает массив для отрисовки пагинации на фронте
	 *
	 * @param int $pageBase
	 * @param int $page
	 * @param int $totalFound
	 * @param int $perPage
	 * @param string $pageSeparator
	 * @param string $pagePostFix
	 * @return array
	 */
	public static function getPageStructure(
		$pageBase,
		$page,
		$totalFound,
		$perPage = 20,
		$pageSeparator = '/',
		$pagePostFix = ''
	) {
		//метка для перевода (локализации)
		$next = 'pagination.nextPageButtonLabel';
		$prev = 'pagination.prevPageButtonLabel';

		//общее число страниц
		$pageCount = intval($totalFound / $perPage);
		$pageCount = $totalFound % $perPage == 0 ? $pageCount : $pageCount + 1;

		// Получаем минимальный номер страницы для перебора
		$pageStart = $page - 4;
		if ($pageStart < 1) {
			$pageStart = 1;
		}
		// Получаем максимальный номер страницы для перебора
		$pageEnd = $pageStart + 7;
		if ($pageEnd > $pageCount) {
			$pageEnd = $pageCount;
		}
		// формируем список страниц
		$pages = [];
		// Если страниц больше одной
		if ($pageCount > 1) {
			// Если номер страницы больше нуля
			if ($page > 1 && $page <= $pageCount) {
				// Формируем ссылку
				$pageLink = $pageBase;
				$pageLink .= $pageSeparator . ($page - 1);
				$pageLink .= $pagePostFix;
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => $prev,
					'page_number' => $page - 1,
				];
				// Добавляем элемент к списку
				$pages[] = $item;
			}
			// Если далеко от первой страницы
			if ($pageStart > 3) {
				// Формируем ссылку
				$pageLink = $pageBase . '/1' . $pagePostFix;
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => '1',
					'page_number' => 1,
				];
				// Добавляем элемент к списку
				$pages[] = $item;
				// Формируем ссылку
				$pageLink = '';
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => '...',
				];
				// Добавляем элемент к списку
				$pages[] = $item;
			}
			for ($i = $pageStart; $i <= $pageEnd; $i++) {
				// Формируем ссылку
				$pageLink = $pageBase;
				$pageLink .= $pageSeparator . $i;
				$pageLink .= $pagePostFix;
				if ($i == $page) {
					$pageLink = '';
				}
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => $i,
					'page_number' => $i,
				];
				// Добавляем элемент к списку
				$pages[] = $item;
			}
			// Если номер верхней страницы сильно меньше максимума
			if ($pageEnd + 3 < $pageCount) {
				// Формируем ссылку
				$pageLink = '';
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => '...',
				];
				// Добавляем элемент к списку
				$pages[] = $item;
				// Формируем ссылку
				$pageLink = $pageBase;
				$pageLink .= $pageSeparator . $pageCount . $pagePostFix;
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => $pageCount,
					'page_number' => $pageCount,
				];
				// Добавляем элемент к списку
				$pages[] = $item;
			}
			// Если номер страницы меньше максимума
			if ($page < $pageCount) {
				// Формируем ссылку
				$pageLink = $pageBase;
				$pageLink .= $pageSeparator . ($page + 1) . $pagePostFix;
				// Формируем элемент
				$item = [
					'link' => $pageLink,
					'page' => $next,
					'page_number' => $page + 1,
				];
				// Добавляем элемент к списку
				$pages[] = $item;
			}
		}

		return $pages;
	}
}