<?php

use Engine\Console\Request as ConsoleRequest;
use Engine\Console\Response as ConsoleResponse;
use Engine\Log\FileLogger;
use Engine\Log\LoggerAggregator;

/**
 * Set application dependency
 */
$dic = \Engine\Engine::$app->getDic();

$dic->register('console_request', function () {
	return new ConsoleRequest();
});
$dic->register('console_response', function () {
	return new ConsoleResponse();
});
$dic->register('logger', function () {
	$logger = new LoggerAggregator();
	$logger->add(new FileLogger());
	return $logger;
});
$dic->register('validator', function () {
	return new \Engine\Validation\Validator();
});
$dic->register('dbpool', function () {
	return new \Engine\Db\Pool();
});
$dic->register('cachepool', function () {
	return new \Engine\Cache\Pool();
});
