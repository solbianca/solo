<?php

namespace Engine\RabbitMQ\Traits;

trait KeyPrefixTrait {

	/**
	 * @var string
	 */
	protected $keyPrefix;

	/**
	 * @param string $keyPrefix
	 * @return $this
	 */
	public function setKeyPrefix(string $keyPrefix) {
		$this->keyPrefix = $keyPrefix;
		return $this;
	}

	public function getKeyPrefix(): string {
		return $this->keyPrefix;
	}

	/**
	 * @param $key
	 * @return string
	 */
	protected function formatKey($key) {
		return $this->keyPrefix . $key;
	}
}