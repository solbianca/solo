<?php


namespace Engine\RabbitMQ;


use Engine\RabbitMQ\Consumer\ConsumerInterface;
use Engine\RabbitMQ\Exceptions\RabbitMQException;
use Engine\RabbitMQ\Producer\ProducerInterface;

interface RabbitMQInterface {

	/**
	 * Passing in this constant as a flag will forcefully disable all other flags.
	 * Use this if you want to temporarily disable the amqp.auto_ack ini setting.
	 */
	const NOPARAM = AMQP_NOPARAM;

	/**
	 * Exchange types
	 */
	const EXCHANGE_DIRECT = AMQP_EX_TYPE_DIRECT;
	const EXCHANGE_TOPIC = AMQP_EX_TYPE_TOPIC;
	const EXCHANGE_FANOUT = AMQP_EX_TYPE_FANOUT;

	/**
	 * Durable exchanges and queues will survive a broker restart, complete with all of their data.
	 */
	const DURABLE = AMQP_DURABLE;

	/**
	 * RabbitMQInterface constructor.
	 *
	 * Create connection to rabbitmq by given config.
	 * Config MUST contain associate arrays 'connection' with connection infos and 'exchange' for initiate exchange.
	 *
	 * Example:
	 * $config = 'connection' => [
	 *        'host' => '127.0.0.1',
	 *        'port' => '5672',
	 *        'username' => 'guest',
	 *        'password' => 'guest',
	 *        'vhost' => '/',
	 *    ],
	 *    'exchange' => [
	 *        'name' => 'exchange_test',
	 *        'type' => RabbitMQ::EXCHANGE_DIRECT,
	 *        'flags' => AMQP_DURABLE,
	 *        'arguments => []
	 *    ],
	 *
	 * To full list available parameters for connection see the original method \AMQPConnection.
	 *
	 * For 'exchange' you MUST define 'name' and type.
	 *
	 * Parameter 'flag' is bitmask of flags. See original method \AMQPExchange::setFlags() for full description.
	 *
	 * Parameter 'arguments' an array of key/value pairs of arguments. See original method \AMQPExchange::setArguments() for full description.
	 *
	 * @param array $config
	 * @throws RabbitMQException
	 */
	public function __construct(array $config);

	/**
	 * Create producer
	 *
	 * @param string $keyPrefix
	 * @return ProducerInterface
	 */
	public function createProducer(string $keyPrefix = ''): ProducerInterface;

	/**
	 * Create consumer
	 *
	 * @param string $keyPrefix
	 * @param string $exchangeName
	 * @param int $flags
	 * @return ConsumerInterface
	 */
	public function createConsumer(
		string $keyPrefix = '',
		string $exchangeName = '',
		int $flags = RabbitMQ::NOPARAM
	): ConsumerInterface;
}