<?php


namespace Engine\RabbitMQ\Consumer;


interface ConsumerInterface {

	/**
	 * @param string $key
	 * @param int $flags
	 * @return false|mixed
	 */
	public function consume(string $key, int $flags = AMQP_AUTOACK);

	/**
	 * @param string $key
	 * @return false|string
	 */
	public function consumeNoAutoAck(string $key);
}