<?php


namespace Engine\RabbitMQ\Consumer;


use Engine\RabbitMQ\RabbitMQ;
use Engine\RabbitMQ\Traits\KeyPrefixTrait;

class Consumer implements ConsumerInterface {

	use KeyPrefixTrait;

	/**
	 * @var \AMQPChannel
	 */
	public $channel;

	/**
	 * @var \AMQPQueue[]
	 */
	protected $queues;

	/**
	 * @var int
	 */
	protected $flags = RabbitMQ::NOPARAM;

	/**
	 * @var string
	 */
	protected $exchangeName;

	/**
	 * Consumer constructor.
	 * @param \AMQPChannel $channel
	 * @param string $keyPrefix
	 * @param string $exchangeName
	 * @param int $flags
	 */
	public function __construct(
		\AMQPChannel $channel,
		string $keyPrefix,
		string $exchangeName,
		int $flags = RabbitMQ::NOPARAM
	) {
		$this->channel = $channel;
		$this->keyPrefix = $keyPrefix;
		$this->exchangeName = $exchangeName;
		$this->flags = $flags;
	}

	/**
	 * @param int $flags
	 * @return $this
	 */
	public function setFlags(int $flags) {
		$this->flags = $flags;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getFlags(): int {
		return $this->flags;
	}

	/**
	 * @param string $exchangeName
	 * @return $this
	 */
	public function setExchangeName(string $exchangeName) {
		$this->exchangeName = $exchangeName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getExchangeName(): string {
		return $this->exchangeName;
	}

	/**
	 * @param $key
	 * @return \AMQPQueue
	 */
	protected function getQueue(string $key): \AMQPQueue {
		$key = $this->formatKey($key);

		if (isset($this->queues[$key])) {
			return $this->queues[$key];
		}

		$this->queues[$key] = $this->createQueue($key);
		return $this->queues[$key];
	}

	/**
	 * @param $key
	 * @return \AMQPQueue
	 */
	protected function createQueue(string $key): \AMQPQueue {
		$queue = new \AMQPQueue($this->channel);
		$queue->setName($key);

		if (RabbitMQ::NOPARAM !== $this->flags) {
			$queue->setFlags($this->flags);
		}

		$queue->declareQueue();
		$queue->bind($this->exchangeName, $key);
		return $queue;
	}

	/**
	 * @param string $key
	 * @param int $flags
	 * @return false|mixed
	 */
	public function consume(string $key, int $flags = AMQP_AUTOACK) {
		$queue = $this->getQueue($key);

		$envelope = $queue->get($flags);
		$message = false;
		if ($envelope) {
			$message = unserialize($envelope->getBody());
		}
		return $message;
	}

	/**
	 * @param string $key
	 * @return false|string
	 */
	public function consumeNoAutoAck(string $key) {
		$queue = $this->getQueue($key);
		$envelope = $queue->get();
		$message = false;
		if ($envelope) {
			$message = $envelope->getBody();
		}
		return $message;
	}

	/**
	 * @param string $key
	 * @param \AMQPEnvelope $envelope
	 * @return bool
	 */
	public function ackEnvelope($key, \AMQPEnvelope $envelope): bool {
		$queue = $this->getQueue($key);
		if (method_exists($envelope, 'getDeliveryTag')) {
			return $queue->ack($envelope->getDeliveryTag());
		}
		return false;
	}
}