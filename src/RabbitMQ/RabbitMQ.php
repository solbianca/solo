<?php


namespace Engine\RabbitMQ;


use Engine\Helpers\ArrayHelper;
use Engine\RabbitMQ\Consumer\Consumer;
use Engine\RabbitMQ\Consumer\ConsumerInterface;
use Engine\RabbitMQ\Exceptions\RabbitMQException;
use Engine\RabbitMQ\Producer\Producer;
use Engine\RabbitMQ\Producer\ProducerInterface;

class RabbitMQ implements RabbitMQInterface {

	/**
	 * @var \AMQPConnection
	 */
	protected $connection;

	/**
	 * @var \AMQPChannel
	 */
	protected $channel;

	/**
	 * @var \AMQPExchange
	 */
	protected $exchange;

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(array $config) {
		$this->config = $config;
		if (!isset($config['connection'], $config['exchange'])) {
			throw new RabbitMQException("Bad config for RabbitMQ. Check parameters: connection, exchange.");
		}
		try {
			$this->connection = $this->connect($config['connection']);
			$this->channel = $this->createChannel($this->connection);
			$this->exchange = $this->createExchange($this->channel, $config['exchange']);
		} catch (\Exception $e) {
			throw new RabbitMQException('Failed to build connection: ' . $e->getMessage());
		}
	}

	/**
	 * @param array $config
	 * @return \AMQPConnection
	 * @throws RabbitMQException
	 */
	protected function connect(array $config) {
		if (!ArrayHelper::keysExist(['host', 'port', 'username', 'password'], $config)) {
			throw new RabbitMQException("Bad RabbitMQ connection configuration. Check parameters: host, port, username, password.");
		}
		$connection = new \AMQPConnection($config);
		$connection->connect();
		return $connection;
	}

	/**
	 * @param $connection
	 * @return \AMQPChannel
	 */
	protected function createChannel($connection) {
		return new \AMQPChannel($connection);
	}

	/**
	 * @param \AMQPChannel $channel
	 * @param array $config
	 * @return \AMQPExchange
	 * @throws RabbitMQException
	 */
	protected function createExchange(\AMQPChannel $channel, array $config) {
		if (!isset($config['name'], $config['type'])) {
			throw new RabbitMQException("Bad RabbitMQ exchange configuration. Check parameters: name, type.");
		}
		$exchange = new \AMQPExchange($channel);
		$exchange->setName($config['name']);
		$exchange->setType($config['type']);
		if (!empty($config['flags'])) {
			$exchange->setFlags($config['flags']);
		}
		if (!empty($config['arguments']) && is_array($config['arguments'])) {
			$exchange->setArguments($config['arguments']);
		}
		$exchange->declareExchange();
		return $exchange;
	}

	/**
	 * @param string $keyPrefix
	 * @return Producer
	 */
	public function createProducer(string $keyPrefix = ''): ProducerInterface {
		$producer = new Producer($this->exchange, $keyPrefix);
		return $producer;
	}

	/**
	 * @param string $keyPrefix
	 * @param string $exchangeName
	 * @param int $flags
	 * @return Consumer
	 */
	public function createConsumer(
		string $keyPrefix = '',
		string $exchangeName = '',
		int $flags = RabbitMQ::NOPARAM
	): ConsumerInterface {
		if ('' === $exchangeName) {
			$exchangeName = $this->config['exchange']['name'];
		}

		$consumer = new Consumer($this->channel, $keyPrefix, $exchangeName, $flags);
		return $consumer;
	}
}