<?php


namespace Engine\RabbitMQ;


class RabbitMQFactory {

	/**
	 * @param array $config
	 * @return RabbitMQInterface
	 */
	public function createRabbitMQ(array $config): RabbitMQInterface {
		return new RabbitMQ($config);
	}
}