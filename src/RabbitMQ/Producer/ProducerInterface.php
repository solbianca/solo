<?php


namespace Engine\RabbitMQ\Producer;


interface ProducerInterface {

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return mixed
	 */
	public function publish(string $key, $value, int $flags, array $attributes): bool;

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return mixed
	 */
	public function publishJson(string $key, $value): bool;
}