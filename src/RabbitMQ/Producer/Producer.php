<?php


namespace Engine\RabbitMQ\Producer;

use Engine\RabbitMQ\RabbitMQ;
use Engine\RabbitMQ\Traits\KeyPrefixTrait;


/**
 * Class Producer
 * @package Engine\RabbitMQ\Producer
 */
class Producer implements ProducerInterface {

	use KeyPrefixTrait;

	/**
	 * @var \AMQPExchange
	 */
	protected $exchange;

	/**
	 * Producer constructor.
	 * @param \AMQPExchange $exchange
	 * @param string $keyPrefix
	 */
	public function __construct(\AMQPExchange $exchange, string $keyPrefix = '') {
		$this->exchange = $exchange;
		$this->keyPrefix = $keyPrefix;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @param int $flags
	 * @param array $attributes
	 * @return bool
	 */
	public function publish(
		string $key,
		$value,
		int $flags = RabbitMQ::NOPARAM,
		array $attributes = ['delivery_mode' => 2]
	): bool {
		return $this->exchange->publish(serialize($value), $this->formatKey($key), $flags, $attributes);
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @param int $flags
	 * @param array $attributes
	 * @return bool
	 */
	public function publishJson(
		string $key,
		$value,
		int $flags = RabbitMQ::NOPARAM,
		array $attributes = ['delivery_mode' => 2]
	): bool {
		return $this->exchange->publish(json_encode($value), $this->formatKey($key), $flags, $attributes);
	}
}