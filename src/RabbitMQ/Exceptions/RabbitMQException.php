<?php


namespace Engine\RabbitMQ\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class RabbitMQException extends ApplicationException {

}