<?php


namespace Engine\RabbitMQ;


use Engine\RabbitMQ\Exceptions\RabbitMQManagerException;

class RabbitMQManager {

	/**
	 * @var array
	 */
	protected $container;

	/**
	 * @var RabbitMQFactory
	 */
	protected $factory;

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * RabbitMQManager constructor.
	 * @param RabbitMQFactory $factory
	 * @param array $config
	 */
	public function __construct(RabbitMQFactory $factory, array $config) {
		$this->factory = $factory;
		$this->config = $config;
	}

	/**
	 * @param string $name
	 * @return RabbitMQInterface
	 * @throws RabbitMQManagerException
	 */
	public function getRabbitMQ(string $name): RabbitMQInterface {
		if (isset($this->container[$name])) {
			return $this->container[$name];
		}

		if (!isset($this->config[$name])) {
			throw new RabbitMQManagerException("Config not contain data for '$name'.");
		}

		$this->container[$name] = $this->factory->createRabbitMQ($this->config[$name]);
		return $this->container[$name];
	}
}