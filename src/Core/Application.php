<?php


namespace Engine\Core;


use Engine\Base\Traits\MagicGetTrait;
use Engine\Console\ConsoleRouter;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Interfaces\RequestInterface;
use Engine\Core\Interfaces\ResponseInterface;
use Engine\Core\Traits\InitDependencyTrait;
use Engine\Debug\Debugger;
use Engine\Engine;
use Engine\Http\HttpRouter;
use Engine\Http\Request;
use Engine\Http\Response;
use Engine\JsonRpc2\Exceptions\JsonRpcException;
use Engine\JsonRpc2\Exceptions\JsonRpcResponseException;
use Engine\JsonRpc2\JsonRpc2Router;

/**
 * Class Application
 * @package Engine\Core
 *
 * @property DIC $dic
 */
abstract class Application {

	use MagicGetTrait;
	use InitDependencyTrait;

	/**
	 * @var string
	 */
	protected $applicationType;

	/**
	 * @var DIC
	 */
	protected $dic;

	/**
	 * @var array
	 */
	protected $urls = [];

	/**
	 * @var array
	 */
	protected $consoleCommands = [];

	/**
	 * Default router
	 *
	 * @var string
	 */
	protected $router;

	/**
	 * @var array
	 */
	protected $config = [];

	/**
	 * Application constructor.
	 *
	 * @param array $config
	 * @throws ApplicationException
	 */
	public function __construct(array $config = []) {
		Engine::$app = $this;

		$this->dic = new DIC();
		require __DIR__ . '/../bootstrap.php';

		$this->parseConfig($config);

		if (!empty($config['components']) && is_array($config['components'])) {
			$this->initDependency($config['components']);
		}
	}

	/**
	 * Parse config
	 *
	 * @param array $config
	 */
	public function parseConfig(array $config) {
		if (isset($config['urls'])) {
			$this->urls = $config['urls'];
		}

		if (isset($config['router'])) {
			$this->router = $config['router'];
		}

		if (isset($config['console_commands'])) {
			$this->consoleCommands = $config['console_commands'];
		}

		$this->config = \Engine\Helpers\ArrayHelper::merge($this->config, $config);
	}

	/**
	 * возвращает ключ конфига. Можно обращаться как getConfig('key1.subkey2.subsubkey3');
	 *
	 * @param string|null $key
	 * @return mixed|null
	 */
	public function getConfig($key = null) {
		if (null === $key) {
			return $this->config;
		}

		$keys = explode('.', $key);

		$result = null;
		$data = $this->config;
		foreach ($keys as $k) {
			if (key_exists($k, $data)) {
				$data = $data[$k];
				$result = $data;
			}
			else {
				return null;
			}
		}

		return $result;
	}

	/**
	 * Run application
	 */
	public function run() {
		try {
			$this->handleRequest();
		} catch (JsonRpcResponseException $exception) {
			if (true === APP_DEBUG) {
				Debugger::traceJsonRpcResponceException($exception);
			}
			Debugger::saveInLog($exception);
		} catch (JsonRpcException $exception) {
			if (true === APP_DEBUG) {
				Debugger::traceJsonRpcException($exception);
			}
			Debugger::saveInLog($exception);
		} catch (\Exception $exception) {
			if (true === APP_DEBUG) {
				Debugger::trace($exception);
			}
			Debugger::saveInLog($exception);
		}
	}

	/**
	 * @return mixed
	 */
	abstract public function handleRequest();

	/**
	 * Resolve request route
	 *
	 * @param array $urls
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @return array|null
	 */
	public function route(array $urls, RequestInterface $request, ResponseInterface $response) {
		$router = $this->createRouter($request, $response);
		return $router->resolve($urls);
	}

	/**
	 * Create router
	 *
	 * @param RequestInterface|Request $request
	 * @param ResponseInterface|Response $response
	 * @return HttpRouter|JsonRpc2Router|ConsoleRouter
	 * @throws ApplicationException
	 */
	public function createRouter(RequestInterface $request, ResponseInterface $response) {
		if (empty($this->router) || !class_exists($this->router)) {
			throw new ApplicationException('Bad router definition.');
		}

		switch ($this->router) {
			case (JsonRpc2Router::class):
				return new JsonRpc2Router($request, $response);
			case (HttpRouter::class):
				return new HttpRouter($request, $response);
			case (ConsoleRouter::class):
				return new ConsoleRouter($request, $response);
		}
	}

	/**
	 * @return DIC
	 */
	public function getDic() {
		return $this->dic;
	}
}