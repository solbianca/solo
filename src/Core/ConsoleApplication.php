<?php


namespace Engine\Core;


use Engine\Base\Console\Controller;
use Engine\Console\ConsoleRouter;
use Engine\Console\Controllers\HelpController;
use Engine\Console\Controllers\MigrateController;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Traits\InitCacheTrait;
use Engine\Core\Traits\InitDatabasesTrait;
use Engine\Debug\Debugger;

/**
 * Class Application
 * @package Engine\Core
 *
 * @property DIC $dic
 */
class ConsoleApplication extends Application {
	use InitDatabasesTrait;
	use InitCacheTrait;

	/**
	 * @var string
	 */
	protected $applicationType = 'console';

	/**
	 * @inheritdoc
	 */
	protected $router = ConsoleRouter::class;

	/** @var  string */
	protected $defaultControllerClassName = HelpController::class;

	/**
	 * @inheritdoc
	 */
	public function run() {
		try {
			$code = $this->handleRequest();
			exit($code);
		} catch (\Exception $exception) {
			Debugger::saveInLog($exception);
			var_dump($exception->getMessage() . " \t " . $exception->getFile() . " \t " . $exception->getLine());
			die();
		}
	}

	/**
	 * TODO убрать добавить вызов initCache, а саму функцию вынести в трейт  initCache
	 * Parse config
	 *
	 * @param array $config
	 */
	public function parseConfig(array $config) {
		parent::parseConfig($config);

		if (null !== ($connections = $this->getConfig('databases.connections'))) {
			$this->initDatabase($connections);
		}

		if (null !== ($connections = $this->getConfig('cache.connections'))) {
			$this->initCache($connections);
		}
	}

	/**
	 * Handles the specified request.
	 * Создание контроллера, передача в него внешних парамеров, вызов action'a  этого контроллера
	 *
	 * @return mixed the resulting response
	 * @throws ApplicationException
	 */
	public function handleRequest() {
		/** @var \Engine\Console\Request $request */
		$request = $this->dic->get('console_request');
		/** @var \Engine\Console\Response $response */
		$response = $this->dic->get('console_response');
		list ($route, $params) = $request->resolve();

		/**
		 * Проверяем на попытку запустить команду ядра (migration, help etc.)
		 */
		$controllerPattern = $this->routeCoreCommand($route);


		if ($controllerPattern) {
			$class = $controllerPattern['controller'];
			$action = $controllerPattern['action'];
			$this->checkController($class);

			/** @var \Engine\Base\Console\Controller $controller */
			$controller = new $class($request, $response, $params);

			$this->checkAction($controller, $action);

			return $this->runAction($controller, $action);
		}

		/**
		 * попытка запустить пользовательскую комманду
		 */
		$controllerPattern = $this->route($this->consoleCommands, $request, $response);

		if (empty($controllerPattern)) {
			throw new ApplicationException("Bad routing.");
		}

		$class = $controllerPattern['controller'];
		$action = $controllerPattern['action'];

		$this->checkController($class);

		/** @var \Engine\Base\Console\Controller $controller */
		$controller = new $class($request, $response, $params);

		$this->checkAction($controller, $action);


		return $this->runAction($controller, $action);
	}

	/**
	 * Returns the configuration of the built-in commands.
	 * @return array the configuration of the built-in commands.
	 */
	public function coreCommands() {
		return [
			'help' => HelpController::class,
			'migrate' => MigrateController::class,
		];
	}

	/**
	 * Проверяем вызвана ли команда ядра (migrate, help...)
	 * и возрвщаем имя контроллера и метод этого контроллера
	 *
	 * @param string $route
	 * @return array|null
	 */
	protected function routeCoreCommand($route) {
		$controllerClassName = null;

		if (!$route) {
			$controllerClassName = $this->defaultControllerClassName;
		}
		else {
			$route = explode('/',
				$route); // route   может быть вида 'migrate/create' controller = migrate, action = create
			foreach ($this->coreCommands() as $coreCommandName => $controller) {
				if ($coreCommandName === $route[0]) {
					$controllerClassName = $controller;
				}
			}
		}

		$action = isset($route[1]) ? $route[1] . 'Action' : 'indexAction';

		if ($controllerClassName && $action) {
			return ['controller' => $controllerClassName, 'action' => $action];
		}

		return null;
	}


	/**
	 * @param string $class
	 * @return bool
	 * @throws ApplicationException
	 */
	protected function checkController($class) {
		if (!class_exists($class)) {
			throw new ApplicationException("Class $class not found.");
		}
		return true;
	}

	/**
	 * @param Controller $controller
	 * @param string $action
	 * @return bool
	 * @throws ApplicationException
	 */
	protected function checkAction(Controller $controller, $action) {
		if (!($controller instanceof Controller)) {
			throw new ApplicationException('Controller must be instance of \'\Engine\Base\Console\Controller\'');
		}
		if (!method_exists($controller, $action)) {
			$class = get_class($controller);
			throw new ApplicationException("Method {$action} in class {$class} not found.");
		}
		return true;
	}

	/**
	 * Непосредственное выполнение action'a
	 *
	 * @param Controller $controller
	 * @param $action
	 * @return mixed
	 */
	protected function runAction(Controller $controller, $action) {
		$controller->beforeAction($action);
		return $controller->$action();
	}

}