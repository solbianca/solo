<?php

namespace Engine\Core\Traits;

use Engine\Core\Exceptions\ApplicationException;
use Engine\Cache\Pool as CachePool;
use Engine\Engine;

/**
 * Class InitDatabasesTrait
 * @package Engine\Core\Traits
 */
trait  InitCacheTrait{
	/**
	 * Register cache configs
	 *
	 * @param array $cacheConnections
	 * @throws ApplicationException
	 */
	protected function initCache(array $cacheConnections) {
		if (!is_array($cacheConnections) || 0 === count($cacheConnections)) {
			throw new ApplicationException('Bad cache connections definition.');
		}

		foreach ($cacheConnections as $alias => $connection) {
			/** @var  CachePool $pool */
			$pool = Engine::$app->getDic()->get('cachepool');
			$immutable = (isset($connection['immutable']) && true === $connection['immutable']);
			$pool->register($connection, $alias, $immutable);
		}
	}
}