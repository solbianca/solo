<?php


namespace Engine\Core\Traits;


use Engine\Core\Exceptions\ApplicationException;
use Engine\Engine;
use Engine\RabbitMQ\RabbitMQFactory;
use Engine\RabbitMQ\RabbitMQManager;

trait InitRabbitMQTrait {

	public function initRabbitMQ($connections) {
		if (!is_array($connections) || count($connections) === 0) {
			throw new ApplicationException('Bad rabbitmq connections definition.');
		}

		Engine::$app->getDic()->register('rabbitmqManager', function () use ($connections) {
			return new RabbitMQManager(new RabbitMQFactory(), $connections);
		});
	}
}