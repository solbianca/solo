<?php

namespace Engine\Core\Traits;

use Engine\Core\Exceptions\ApplicationException;
use Engine\Db\Pool as DbPool;
use Engine\Engine;

/**
 * Class InitDatabasesTrait
 * @package Engine\Core\Traits
 */
trait InitDatabasesTrait {
	/**
	 * Register connections with database
	 *
	 * @param array $connections
	 * @throws ApplicationException
	 */
	protected function initDatabase($connections) {
		if (!is_array($connections) || count($connections) === 0) {
			throw new ApplicationException('Bad connections definition.');
		}

		foreach ($connections as $alias => $connection) {
			/** @var DbPool $pool */
			$pool = Engine::$app->getDic()->get('dbpool');
			$immutable = (isset($connection['immutable']) && true === $connection['immutable']);
			$pool->register($connection, $alias, $immutable);
		}
	}
}