<?php

namespace Engine\Core\Traits;

use Engine\Core\Exceptions\ApplicationException;

/**
 * Class InitDependencyTrait
 * @package Engine\Core\Traits
 */
trait InitDependencyTrait {

	/**
	 * Init application dependency
	 *
	 * @param array $components
	 * @return $this
	 * @throws ApplicationException
	 */
	protected function initDependency(array $components) {
		foreach ($components as $key => $component) {
			if (!isset($component['callback'])) {
				throw new ApplicationException('Component {$key} must have callback parameter.');
			}
			$immutable = isset($component['immutable']) && true === $component['immutable'];
			$this->dic->register($key, $component['callback'], $immutable);
		}
		return $this;
	}
}