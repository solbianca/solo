<?php


namespace Engine\Core\Interfaces;

use Engine\Core\Exceptions\DependencyContainerException;

/**
 * Describes the interface of a container that exposes methods to read its entries.
 */
interface ContainerInterface {

	/**
	 * Finds an entry of the container by its identifier and returns it.
	 *
	 * @param string $alias Identifier of the entry to look for.
	 *
	 * @return mixed Entry|null.
	 *
	 * @throws DependencyContainerException
	 */
	public function get($alias);

	/**
	 * Returns true if the container can return an entry for the given identifier.
	 * Returns false otherwise.
	 *
	 * @param string $alias Identifier of the entry to look for.
	 *
	 * @return bool
	 */
	public function has($alias);
}