<?php


namespace Engine\Core\Interfaces;

use Engine\Renderers\Interfaces\RenderInterface;

interface ViewInterface {

	/**
	 * Render the content, return as response to client and exit from application.
	 *
	 * @param string $template Template to render
	 * @param array $params Array of parameters transfer to template
	 * @return mixed http response to client
	 */
	public function render(string $template, array $params = []);

	/**
	 * Render the content and return it as a string.
	 * Application continius to work.
	 *
	 * @param string $template Template to render
	 * @param array $params Array of parameters transfer to template
	 * @return string
	 */
	public function fetch(string $template, array $params = []);

	/**
	 * Set renderer for view.
	 * Renderer will render content
	 *
	 * @param RenderInterface $renderer
	 * @return $this
	 */
	public function setRenderer(RenderInterface $renderer);

	/**
	 * Get renderer from view.
	 *
	 * @return RenderInterface
	 */
	public function getRenderer();
}