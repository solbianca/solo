<?php


namespace Engine\Core\Interfaces;


interface RouterInterface {

	/**
	 * Resolve route and call specific controller/action or throw exception
	 *
	 * @param array $urls
	 * @return mixed
	 */
	public function resolve(array $urls);
}