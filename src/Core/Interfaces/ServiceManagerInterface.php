<?php


namespace Engine\Core\Interfaces;


use Engine\Core\Exceptions\InvalidConfigException;

interface ServiceManagerInterface {

	/**
	 * Get full configuration for all services
	 *
	 * @return array Full config for all services
	 */
	public function getConfig(): array;

	/**
	 * Check that configuration for specific serice exist
	 *
	 * @param string $name Service name
	 * @return bool true if config for service exist, otherwise false
	 */
	public function isServiceExist(string $name): bool;

	/**
	 * Return configuration for specific service
	 *
	 * @param string $name Service name
	 * @return array Return array with configuration data for service. Return empty array if config not exist
	 */
	public function getServiceConfig(string $name): array;

	/**
	 * Get metadata array. If metadata not set, return empty array.
	 *
	 * @return array
	 */
	public function getMetadata(): array;

	/**
	 * Set metadata that will passed with all requests
	 *
	 * @param array $metadata
	 * @return ServiceManagerInterface
	 */
	public function setMetadata(array $metadata = []): ServiceManagerInterface;

	/**
	 * Get json-rpc clients for specific service
	 *
	 * @param string $name Service name
	 * @return \Engine\JsonRpc2\Client\Client[] Array of json-rpc clients for specific service
	 * @throws InvalidConfigException
	 */
	public function getClients(string $name): array;
}