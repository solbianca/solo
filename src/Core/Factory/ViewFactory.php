<?php


namespace Engine\Core\Factory;


use Engine\Core\View;
use Engine\Http\Interfaces\ResponseInterface;

class ViewFactory {

	/**
	 * Create View object.
	 *
	 * @param ResponseInterface $response
	 * @param array $config
	 * $config = [
	 *    'renderer' => \Engine\Renderers\TwigRenderer::class, - Renderer class
	 *    'viewDirectory' => 'Path/To/View', - Path to View directory in 'App'
	 *    'rendererOptions' => [], - Options will pass in renderer constructor
	 *    'fileExtension' => '.phtml', - File extensions for renderer
	 * ];
	 *
	 * @return View
	 */
	public function create(ResponseInterface $response, array $config = []) {
		return new View($response, $config);
	}
}