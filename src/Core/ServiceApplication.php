<?php


namespace Engine\Core;


use Engine\Base\Controller;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Traits\InitCacheTrait;
use Engine\Core\Traits\InitDatabasesTrait;
use Engine\Core\Traits\InitRabbitMQTrait;
use Engine\Debug\Debugger;
use Engine\Http\Factory\StreamFactory;
use Engine\JsonRpc2\JsonRpc2Router;
use Engine\JsonRpc2\Server\Request;
use Engine\JsonRpc2\Server\Server;

/**
 * Class Application
 * @package Engine\Core
 *
 * @property DIC $dic
 */
class ServiceApplication extends Application {
	use InitDatabasesTrait;
	use InitCacheTrait;
	use InitRabbitMQTrait;

	/**
	 * @var string
	 */
	protected $applicationType = 'service';

	/**
	 * @inheritdoc
	 */
	protected $router = JsonRpc2Router::class;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(array $config = []) {
		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function run() {
		try {
			$this->handleRequest();
		} catch (\Exception $exception) {
			Debugger::saveInLog($exception);
			$code = (empty($exception->getCode())) ? '-32099' : $exception->getCode();
			$message = (empty($exception->getMessage())) ? 'Application exception' : $exception->getMessage();
			echo json_encode([
				'jsonrpc' => Request::JSON_RPC_VERSION,
				'error' => [
					'code' => $code,
					'message' => $message,
					'data' => [
						'line' => $exception->getLine(),
						'file' => $exception->getFile(),
						'trace' => $exception->getTrace(),
					],
				],
				'id' => null,
			]);
			die();
		}
	}

	/**
	 * Parse config
	 *
	 * @param array $config
	 */
	public function parseConfig(array $config) {

		parent::parseConfig($config);

		if (null !== ($connections = $this->getConfig('databases.connections'))) {
			$this->initDatabase($connections);
		}

		if (null !== ($connections = $this->getConfig('cache.connections'))) {
			$this->initCache($connections);
		}

		if (null !== ($connections = $this->getConfig('rabbitmq'))) {
			$this->initRabbitMQ($connections);
		}
	}

	public function handleRequest() {
		$request = $this->createRequest();
		$response = $this->createResponse();

		$controllerPattern = $this->route($this->urls, $request, $response);

		if (empty($controllerPattern)) {
			throw new ApplicationException("Bad routing.");
		}

		$class = $controllerPattern['controller'];
		$action = $controllerPattern['action'];

		if (!class_exists($class)) {
			throw new ApplicationException("Class $class not found.");
		}

		$controller = new $class($request, $response);

		if (!method_exists($controller, $action)) {
			throw new ApplicationException("Action {$action} in class {$class} not found.");
		}
		elseif (!($controller instanceof Controller)) {
			throw new ApplicationException("Controller must be instance of '\Engine\Base\Controller'");
		}

		$controller = new $class($request, $response);
		$jsonRpc2Server = new Server($controller);
		$jsonRpc2Server->process($action);
	}

	/**
	 * @return \Engine\Http\Request
	 */
	protected function createRequest() {
		$request = new \Engine\Http\Request('POST', '/');
		$stream = (new StreamFactory())->createStreamFromFile('php://input');
		$request = $request->withBody($stream);
		return $request;
	}

	/**
	 * @return \Engine\Http\Interfaces\ResponseInterface|\Engine\Http\Response
	 */
	protected function createResponse() {
		return (new \Engine\Http\Factory\MessageFactory())->createResponse();
	}
}