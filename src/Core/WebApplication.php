<?php


namespace Engine\Core;


use Engine\Base\Interfaces\IdentityInterface;
use Engine\Base\Interfaces\UserInterface;
use Engine\Base\UserIdentity;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Factory\ViewFactory;
use Engine\Engine;
use Engine\Helpers\ArrayHelper;
use Engine\Http\Factory\MessageFactory;
use Engine\Http\Factory\ServerRequestFactory;
use Engine\Http\HttpRouter;
use Engine\JsonRpc2\Client\ClientFactory;
use Engine\Middleware\MiddlewareRunner;
use Engine\Middleware\MiddlewareRunnerFactory;

/**
 * Class WebApplication
 * @package Engine\Core
 *
 * @property DIC $dic
 * @property UserInterface|IdentityInterface $user
 */
class WebApplication extends Application {

	/**
	 * @var string
	 */
	protected $applicationType = 'web';

	/**
	 * @inheritdoc
	 */
	protected $router = HttpRouter::class;

	/**
	 * @var string
	 */
	protected $clientId = '';

	/**
	 * @var UserInterface|IdentityInterface
	 */
	protected $user;

	/**
	 * @var \Engine\Http\Interfaces\ServerRequestInterface
	 */
	protected $request;

	/**
	 * @var \Engine\Http\Interfaces\ResponseInterface
	 */
	protected $response;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(array $config = []) {
		parent::__construct($config);
		$this->request = $this->createRequest();
		$this->response = $this->createResponse();
	}

	/**
	 * Parse config
	 *
	 * @param array $config
	 * @throws ApplicationException
	 */
	public function parseConfig(array $config) {
		parent::parseConfig($config);

		if (!isset($config['clientId'])) {
			throw new ApplicationException("You MUST define 'clientId' in config file.");
		}
		$this->clientId = $config['clientId'];

		if (empty($config['path']) || !ArrayHelper::keysExist(['app', 'web', 'root'], $config['path'])) {
			throw  new ApplicationException("Config must contain row 'path' and subrows 'app', 'web', 'root' with proper path");
		}

		$viewConfig = [];
		if (isset($config['view']) && is_array($config['view'])) {
			$viewConfig = $config['view'];
		}
		$this->dic->register('view', function () use ($viewConfig) {
			return (new ViewFactory())->create($this->response, $viewConfig);
		});

		if (isset($config['user']['identity'])) {
			$this->user = Engine::createInstance($config['user']['identity']);
			if (!($this->user instanceof UserInterface)) {
				throw new ApplicationException('Bad user identity definition. User MUST implement UserInterface.');
			}
		}
		else {
			$this->user = new UserIdentity();
		}
	}

	/**
	 * Initiate ServiceManager object
	 *
	 * @param string $config Json representation of configuration files
	 * @return $this
	 */
	public function initServicesManager($config) {
		$this->dic->register('serviceManager', function () use ($config) {
			return new ServiceManager(new ClientFactory(), $config);
		});

		return $this;
	}


	/**
	 * Handle user request
	 *
	 * @throws ApplicationException
	 */
	public function handleRequest() {
		$path = $this->request->getUri()->getPath();

		$middlewareQueue = $this->resolveMiddlewareConfig($path);

		$runner = $this->createMiddlewareRunner($middlewareQueue);
		$response = $runner($this->request, $this->response);
		$response->send();
	}

	/**
	 * @param array $queue
	 * @return \Engine\Middleware\MiddlewareRunner
	 */
	protected function createMiddlewareRunner(array $queue): MiddlewareRunner {
		return (new MiddlewareRunnerFactory())->createRunner($queue);
	}

	/**
	 * @param string $path
	 * @return array
	 */
	protected function resolveMiddlewareConfig(string $path): array {
		if (empty($this->config['urls']['aliases'])) {
			return $this->config['middleware']['default'];
		}

		foreach ($this->config['urls']['aliases'] as $alias => $urls) {
			if (!array_key_exists($path, $urls)) {
				continue;
			}

			if (isset($this->config['middleware'][$alias])) {
				return $this->config['middleware'][$alias];
			}

			break;
		}

		return $this->config['middleware']['default'];
	}

	/**
	 * @return IdentityInterface|UserInterface
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @return string
	 */
	public function getClientId() {
		return $this->clientId;
	}

	/**
	 * @return \Engine\Http\Interfaces\ServerRequestInterface
	 */
	protected function createRequest() {
		return (new ServerRequestFactory())->createServerRequestFromGlobals();
	}

	/**
	 * @return \Engine\Http\Interfaces\ResponseInterface
	 */
	protected function createResponse() {
		return (new MessageFactory())->createResponse();
	}
}