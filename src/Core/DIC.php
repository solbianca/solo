<?php

namespace Engine\Core;

/**
 * Class DIC
 * Simple dependency injection container.
 * Implements lazy loads for services - objects created only when they called.
 *
 * Usage:
 *
 * Class Foo
 * {
 *      public function say()
 *      {
 *          return 'I say: ';
 *      }
 *
 *      public function hello()
 *      {
 *          return "Hello, world!";
 *      }
 * }
 *
 * Class Bar
 * {
 *      private $argument;
 *
 *      public function __construct(Foo $argument)
 *      {
 *          $this->argument = $argument;
 *      }
 *
 *      public function working()
 *      {
 *          return $this->argument->say() . 'Yep!';
 *      }
 * }
 *
 *
 * $dic = new DIC();
 *
 * $dic->register('greeter', function () {
 *      return new FooBar();
 * });
 *
 * $dic->register('status', function () use ($dic) {
 *      return new Baz($dic->get('greeter'));
 * });
 *
 * $dic->factory('buzz', function() {
 *      return new Buzz();
 * })
 *
 * $greeter = $dic->get('greeter');
 * $status = $dic->get('status');
 * print $greeter->say() . $greeter->hello();
 * print $status->working();
 *
 * You will see:
 * I say: Hello , World!
 * I say: Yep!
 */

use Engine\Core\Exceptions\DependencyContainerException;
use Engine\Core\Interfaces\ContainerInterface;

class DIC implements ContainerInterface {
	/**
	 * Holds blueprints of clesses
	 *
	 * @var array
	 */
	protected $blueprints = [];

	/**
	 * Holds aliases for write-protected definitions
	 *
	 * @var array
	 */
	protected $immutable = [];

	/**
	 * Holds all the definitions
	 *
	 * @var array
	 */
	protected $container = [];

	/**
	 * Register dependency in container.
	 *
	 * @param string $alias Alias to access the definition
	 * @param callable $callback The calback which constructs the dependency
	 * @param boolean $immutable Can the definition be overridden?
	 * @return $this
	 *
	 * @throws DependencyContainerException
	 */
	public function register($alias, $callback, $immutable = false) {
		if (!method_exists($callback, '__invoke')) {
			throw new DependencyContainerException('Service definition is not a Closure or invokable object.');
		}

		if (in_array($alias, $this->immutable)) {
			throw new DependencyContainerException('Service already defined and can\'t be overridden.');
		}

		if ($immutable) {
			$this->immutable[] = $alias;
		}

		if (isset($this->container[$alias])) {
			unset($this->container[$alias]);
		}

		$this->blueprints[$alias] = $callback;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function get($alias) {
		if (array_key_exists($alias, $this->container)) {
			return $this->container[$alias];
		}
		elseif (array_key_exists($alias, $this->blueprints)) {
			$this->container[$alias] = call_user_func($this->blueprints[$alias]);
			return $this->container[$alias];
		}

		throw new DependencyContainerException("No entry was found for '{$alias}' identifier.");
	}

	/**
	 * {@inheritdoc}
	 */
	public function has($alias) {
		if (array_key_exists($alias, $this->container) || array_key_exists($alias, $this->blueprints)) {
			return true;
		}
		return false;
	}

	/**
	 * Unregister dependency in container.
	 *
	 * @param string $alias
	 * @return $this
	 */
	public function unregister($alias) {
		unset($this->container[$alias]);
		unset($this->blueprints[$alias]);
		if (false !== ($index = array_search($alias, $this->immutable))) {
			unset($this->immutable[$index]);
		}
		return $this;
	}
}