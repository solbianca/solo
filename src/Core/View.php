<?php


namespace Engine\Core;


use Engine\Core\Exceptions\InvalidConfigException;
use Engine\Core\Interfaces\ViewInterface;
use Engine\Engine;
use Engine\Http\Interfaces\ResponseInterface;
use Engine\Renderers\Interfaces\RenderInterface;
use Engine\Renderers\SimpleRenderer;

class View implements ViewInterface {

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * @var RenderInterface
	 */
	protected $renderer;

	/**
	 * @var string
	 */
	protected $defaultViewDirectory = 'Views';

	/**
	 * @var string
	 */
	protected $defaultRendererClass = SimpleRenderer::class;

	/**
	 * @var string
	 */
	protected $path;

	/**
	 * @var array
	 */
	protected $rendererOptions = [];

	/**
	 * View constructor.
	 * @param ResponseInterface $response
	 * @param array $config
	 */
	public function __construct(ResponseInterface $response, array $config = []) {
		$this->response = $response;
		$this->parseConfig($config);
	}

	/**
	 * Parse view configuration
	 *
	 * @param array $config
	 * @throws InvalidConfigException
	 */
	protected function parseConfig(array $config) {
		if (isset($config['viewDirectory']) && is_string($config['viewDirectory'])) {
			$this->path = Engine::$app->getConfig('path.app') . '/' . $config['viewDirectory'];
		}
		else {
			$this->path = Engine::$app->getConfig('path.app') . '/' . $this->defaultViewDirectory;
		}

		if (isset($config['rendererOptions']) && is_array($config['rendererOptions'])) {
			$this->rendererOptions = $config['rendererOptions'];
		}

		if (isset($config['renderer']) && is_string($config['renderer'])) {
			$class = $config['renderer'];
			if (!class_exists($class)) {
				throw new InvalidConfigException("Given renderer class '{$class}' not exist.");
			}
			$this->renderer = new $class($this->path, $this->rendererOptions);
		}
		else {
			$this->renderer = new $this->defaultRendererClass($this->path, $this->rendererOptions);
		}

		if (isset($config['fileExtension']) && is_array($config['fileExtension'])) {
			$this->renderer->setFileExtension($config['fileExtension']);
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function render(string $template, array $params = []) {
		$this->response->getBody()->write($this->renderer->render((string)$template, $params));
		return $this->response;
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetch(string $template, array $params = []) {
		return $this->renderer->fetch((string)$template, $params);
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRenderer(RenderInterface $renderer) {
		$this->renderer = $renderer;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRenderer() {
		return $this->renderer;
	}

	/**
	 * Render data as json with proper headers.
	 *
	 * @param mixed $data
	 * @param int $options
	 * @return null
	 */
	public function renderAsJson($data, $options = 320) {
		$this->response = $this->response->withHeader('Content-Type', 'application/json; charset=UTF-8');
		$this->response->getBody()->write(json_encode($data, $options));
		return $this->response;
	}
}