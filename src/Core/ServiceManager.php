<?php


namespace Engine\Core;


use Engine\Core\Exceptions\InvalidConfigException;
use Engine\Core\Interfaces\ServiceManagerInterface;
use Engine\JsonRpc2\Client\ClientFactory;

/**
 * Class ServiceManager
 * @package Engine\Core
 */
class ServiceManager implements ServiceManagerInterface {

	/**
	 * @var ClientFactory
	 */
	protected $clientFactory;

	/**
	 * @var array
	 */
	protected $config = [];

	/**
	 * Metadata
	 *
	 * @var array
	 */
	protected $metadata = [];

	/**
	 * ServiceManager constructor.
	 *
	 * @param ClientFactory $factory
	 * @param string $config Json string with services configurations
	 * @param array $metadata Metadata that will passed with request
	 */
	public function __construct(ClientFactory $factory, $config, array $metadata = []) {
		$this->clientFactory = $factory;
		$this->config = json_decode($config, true);
		$this->metadata = $metadata;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getConfig(): array {
		return $this->config;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isServiceExist(string $name): bool {
		return (isset($this->config[$name]));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getServiceConfig(string $name): array {
		if (isset($this->config[$name])) {
			return $this->config[$name];
		}

		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getMetadata(): array {
		return $this->metadata;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setMetadata(array $metadata = []): ServiceManagerInterface {
		$this->metadata = $metadata;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getClients(string $name): array {
		if (!($serviceConfig = $this->getServiceConfig($name))) {
			throw new InvalidConfigException("Config don't contain requested service '{$name}''.");
		}
		return $clients = $this->clientFactory->createClients($serviceConfig, $this->getMetadata());
	}
}