<?php


namespace Engine\Core;


use Engine\Base\Interfaces\UserInterface;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Exceptions\InvalidConfigException;
use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Http\Request;

/**
 * Class AccessControl
 * @package Engine\Core
 */
class AccessControl {

	const DEFAULT_ROLE_GUEST = 'guest';
	const DEFAULT_ROLE_AUTHORIZED = 'authorized';

	/**
	 * Array of AccessRule objects
	 *
	 * @var array
	 */
	protected $rules;

	/**
	 * @var bool
	 */
	protected $result = false;

	/**
	 * AccessControl constructor.
	 *
	 * @param array $rules
	 */
	public function __construct(array $rules = []) {
		foreach ($rules as $rule) {
			$this->rules[] = new AccessRule($rule);
		}
	}

	/**
	 * @param UserInterface $user
	 * @param string $action
	 * @param Request $request
	 * @return bool
	 * @throws InvalidConfigException
	 */
	public function resolve(UserInterface $user, $action, ServerRequestInterface $request) {
		if (empty($this->rules)) {
			return true;
		}

		$action = $this->prepareAction($action);
		$this->result = null;

		foreach ($this->rules as $rule) {
			/**
			 * @var AccessRule $rule
			 */
			// Если в правилах не задана роль, выкидываем исключение
			if (!isset($rule->roles)) {
				throw new InvalidConfigException("Access rules for controller must contain field 'roles'.");
			}

			if (!is_array($rule->roles)) {
				throw new InvalidConfigException("Bad 'roles' definition for controller access rules.");
			}

			if ($this->matchRole($user, $rule)) {
				$allow = $rule->allows($action, $request);
				if (false === $allow) {
					return false;
				}
				elseif (true === $allow) {
					$this->result = true;
				}
			}
		}
		return (null === $this->result) ? false : true;
	}

	/**
	 * @param UserInterface $user
	 * @param AccessRule $rule
	 * @return bool
	 */
	public function matchRole(UserInterface $user, AccessRule $rule) {
		if ($user->isGuest()) {
			return in_array(AccessControl::DEFAULT_ROLE_GUEST, $rule->roles);
		}

		$role = $user->getRole();

		if (!isset($role) || '' === $role) {
			$role = AccessControl::DEFAULT_ROLE_AUTHORIZED;
		}

		return (in_array($role, $rule->roles) || in_array(AccessControl::DEFAULT_ROLE_AUTHORIZED, $rule->roles));
	}

	/**
	 * @param string $action
	 * @return string
	 * @throws ApplicationException
	 */
	protected function prepareAction($action) {
		if (false !== ($position = strrpos($action, 'Action'))) {
			return substr($action, 0, $position);
		}
		throw new ApplicationException("Bad action method.");
	}
}