<?php


namespace Engine\Core;


use Engine\Http\Request;

class AccessRule {

	/**
	 * @var bool
	 */
	public $allow;

	/**
	 * @var array
	 */
	public $actions;

	/**
	 * @var array
	 */
	public $roles;

	/**
	 * Request methods: POST, GET, PUT etc..
	 *
	 * @var array
	 */
	public $verbs;

	/**
	 * @var callable
	 */
	public $callback;

	/**
	 * AccessRule constructor.
	 * @param array $rule
	 */
	public function __construct(array $rule) {
		if (empty($rule)) {
			return;
		}

		foreach ($rule as $key => $value) {
			if (property_exists($this, $key)) {
				$this->{$key} = $value;
			}
		}

		if (true !== $this->allow) {
			$this->allow = false;
		}
	}

	/**
	 * @param string $action
	 * @param Request $request
	 * @return bool|null
	 */
	public function allows($action, Request $request) {
		if ($this->matchAll($action, $request)) {
			return $this->allow;
		}
		return null;
	}

	/**
	 * @param string $action
	 * @param Request $request
	 * @return bool
	 */
	protected function matchAll($action, Request $request) {
		return ($this->matchAction($action)
			&& $this->matchVerb($request->getMethod())
			&& $this->matchCallback($action)
		);
	}

	/**
	 * @param string $action
	 * @return bool whether the rule applies to the action
	 */
	public function matchAction($action) {
		return (empty($this->actions) || in_array($action, $this->actions, true));
	}

	/**
	 * @param $verb
	 * @return bool
	 */
	public function matchVerb($verb) {
		return empty($this->verbs) || in_array(strtoupper($verb), array_map('strtoupper', $this->verbs), true);
	}

	/**
	 * @param $action
	 * @return bool|mixed
	 */
	public function matchCallback($action) {
		if (!isset($this->callback)) {
			return true;
		}

		if (!is_callable($this->callback)) {
			return true;
		}

		return call_user_func($this->callback, $this, $action);
	}
}