<?php

namespace Engine\Auth\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class UnauthorizedException extends ApplicationException {

}