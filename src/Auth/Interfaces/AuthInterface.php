<?php

namespace Engine\Auth\Interfaces;


use Engine\Auth\Exceptions\UnauthorizedException;
use Engine\Http\Interfaces\ResponseInterface;

/**
 * AuthInterface is the interface that should be implemented by auth method classes.
 */
interface AuthInterface {

	/**
	 * Authenticates the current user.
	 *
	 * @param string $clientId
	 * @return mixed the authenticated user identity. If authentication information is not provided, null will be returned.
	 * @throws UnauthorizedException if authentication information is provided but is invalid.
	 */
	public function authenticate($clientId);

	/**
	 * Generates challenges upon authentication failure.
	 * For example, some appropriate HTTP headers may be generated.
	 *
	 * @param ResponseInterface $response
	 */
	public function challenge(ResponseInterface $response);

	/**
	 * Handles authentication failure.
	 * The implementation should normally throw UnauthorizedException to indicate authentication failure.
	 *
	 * @param ResponseInterface $response
	 * @throws UnauthorizedException
	 */
	public function handleFailure(ResponseInterface $response);
}