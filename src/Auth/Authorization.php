<?php


namespace Engine\Auth;


use Engine\Auth\Methods\AbstractAuthMethod;
use Engine\Base\Interfaces\UserInterface;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Engine;
use Engine\Http\Interfaces\RequestInterface;
use Engine\Http\Interfaces\ResponseInterface;

class Authorization {

	/**
	 * @var UserInterface
	 */
	protected $user;

	/**
	 * @var RequestInterface
	 */
	protected $request;

	/**
	 * @var ResponseInterface
	 */
	protected $response;

	/**
	 * Authorization constructor.
	 *
	 * @param UserInterface $user
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 */
	public function __construct(UserInterface $user, RequestInterface $request, ResponseInterface $response) {
		$this->user = $user;
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * @param string $method
	 * @param string $clientId
	 * @return mixed
	 * @throws ApplicationException
	 */
	public function resolve($method, $clientId) {
		if (!class_exists($method)) {
			throw new ApplicationException("Class '$method' not exist.");
		}
		/**
		 * @var AbstractAuthMethod $authMethod
		 */
		$authMethod = Engine::createInstance($method,
			['user' => $this->user, 'request' => $this->request, 'response' => $this->response]);
		return $authMethod->authenticate($clientId);

	}
}