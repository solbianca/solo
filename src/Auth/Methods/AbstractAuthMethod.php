<?php

namespace Engine\Auth\Methods;


use Engine\Auth\Exceptions\UnauthorizedException;
use Engine\Auth\Interfaces\AuthInterface;
use Engine\Base\Interfaces\UserInterface;
use Engine\Http\Interfaces\RequestInterface;
use Engine\Http\Interfaces\ResponseInterface;

abstract class AbstractAuthMethod implements AuthInterface {
	/**
	 * @var mixed the user object representing the user authentication status. If not set, the `user` application component will be used.
	 */
	protected $user;
	/**
	 * @var RequestInterface the current request. If not set, the `request` application component will be used.
	 */
	protected $request;
	/**
	 * @var ResponseInterface the response to be sent. If not set, the `response` application component will be used.
	 */
	protected $response;

	/**
	 * @var string
	 */
	protected $clientId;

	/**
	 * AbstractAuthMethod constructor.
	 * @param UserInterface $user
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 */
	public function __construct(UserInterface $user, RequestInterface $request, ResponseInterface $response) {
		$this->user = $user;
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * @param $clientId
	 * @return mixed
	 */
	abstract public function authenticate($clientId);

	/**
	 * @inheritdoc
	 */
	public function challenge(ResponseInterface $response) {
	}

	/**
	 * @inheritdoc
	 */
	public function handleFailure(ResponseInterface $response) {
		throw new UnauthorizedException('Your request was made with invalid credentials.');
	}

}