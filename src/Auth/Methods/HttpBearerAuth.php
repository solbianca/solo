<?php


namespace Engine\Auth\Methods;


use Engine\Http\Interfaces\ResponseInterface;
use Engine\Http\Response;

/**
 * HttpBearerAuth is an authentication method based on HTTP Bearer token.
 */
class HttpBearerAuth extends AbstractAuthMethod {

	/**
	 * @var string the HTTP authentication realm
	 */
	public $realm = 'api';

	/**
	 * @inheritdoc
	 */
	public function authenticate($clientId) {
		$authHeader = $this->request->getHeaderLine('HTTP_AUTHORIZATION');
		if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
			$identity = $this->user->loginByAccessToken($matches[1], $clientId);
			if ($identity === null) {
				$this->handleFailure($this->response);
			}
			return $identity;
		}
		return null;
	}

	/**
	 * @inheritdoc
	 */
	public function challenge(ResponseInterface $response) {
		return $response->withAddedHeader('WWW-AUTHENTICATE', "Bearer realm=\"{$this->realm}\"");
	}
}