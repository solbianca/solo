<?php


namespace Engine\Base;


use Engine\Base\Interfaces\IdentityInterface;
use Engine\Base\Interfaces\UserInterface;

class UserIdentity implements UserInterface, IdentityInterface {
	/**
	 * User information like id, token, role, etc.
	 *
	 * @var array
	 */
	protected $identity;

	/**
	 * @var string
	 */
	protected $role = 'guest';

	/**
	 * UserIdentity constructor.
	 */
	public function __construct() {
	}

	/**
	 * @inheritdoc
	 */
	public function isGuest() {
		return (empty($this->identity));
	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		if (isset($this->identity['id'])) {
			return $this->identity['id'];
		}

		return null;
	}

	/**
	 * @inheritdoc
	 */
	public function getRole() {
		if (isset($this->identity['role'])) {
			return $this->identity['role'];
		}

		return null;
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $clientId = null) {
		return null;
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return false;
	}

	public function loginByAccessToken($token, $clientId) {
		return false;
	}

	public function login($login, $password) {
		return false;
	}

	protected function switchIdentity($identity) {

	}

	/**
	 * @param array $identity
	 */
	public function setIdentity(array $identity) {
		$this->identity = $identity;
	}

	/**
	 * @return array
	 */
	public function getIdentity() {
		return $this->identity;
	}
}