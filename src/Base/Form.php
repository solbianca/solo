<?php


namespace Engine\Base;


use Engine\Base\Interfaces\ValidateInterface;
use Engine\Base\Traits\LoadTrait;
use Engine\Core\Exceptions\InvalidConfigException;
use Engine\Engine;
use Engine\Validation\Interfaces\ValidatorInterface;
use Engine\Validation\Validator;

/**
 * Class Form
 * @package Engine\Base
 */
abstract class Form implements ValidateInterface {

	use LoadTrait;

	/**
	 * @var Validator
	 */
	protected $validator;

	/**
	 * @var array
	 */
	protected $validationRules = [];

	/**
	 * Form constructor.
	 */
	public function __construct() {

	}

	/**
	 * Set new validator for form data
	 *
	 * @param ValidatorInterface $validator
	 */
	public function setValidator(ValidatorInterface $validator) {
		$this->validator = $validator;
		return $this;
	}

	/**
	 * Get vform validator
	 *
	 * @return Validator|null
	 */
	public function getValidator() {
		if (!isset($this->validator)) {
			$this->validator = $this->createValidator();
		}
		return $this->validator;
	}

	/**
	 * Get form data validation errors. If validator not initiated, return null.
	 * In othet case instance of MessageBag class.
	 *
	 * @return \Engine\Validation\Support\MessageBag|null
	 */
	public function getValidationErrors() {
		if (!isset($this->validator)) {
			return null;
		}
		return $this->validator->errors();
	}

	/**
	 * Performs the data validation.
	 *
	 * @param array $attributes
	 * @return bool
	 * @throws InvalidConfigException
	 */
	public function validate(array $attributes = []) {
		if (empty($this->validationRules)) {
			return true;
		}

		if (!isset($this->validator) && null === ($this->validator = $this->createValidator())) {
			throw new InvalidConfigException('Validator not defined in dependency container.');
		}

		if (empty($attributes)) {
			$attributes = $this->getAttributes();
		}
		else {
			$attributes = $this->getAttributes($attributes);
		}

		$this->validator->validate($attributes, $this->validationRules);
		return $this->validator->passes();
	}

	/**
	 * @return null|Validator
	 */
	protected function createValidator() {
		return Engine::$app->getDic()->get('validator');
	}

	/**
	 * @param array $rules
	 */
	public function setValidationRules(array $rules = []) {
		if (empty($rules)) {
			$rules = [];
		}
		$this->validationRules = $rules;
	}

	/**
	 * @return array
	 */
	public function getValidationRules() {
		return $this->validationRules;
	}
}