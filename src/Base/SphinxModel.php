<?php

namespace Engine\Base;

use Engine\Core\Exceptions\ApplicationException;
use Engine\Db\Database;
use Engine\Engine;

/**
 * Class Model
 * @package Engine\Base
 *
 * @property Database $sphinx - иснтанс подключения к sphinx
 */
abstract class SphinxModel extends Model {

	/**
	 * @var string Sphinx index name
	 */
	protected static $table;


	/**
	 * SphinxModel constructor.
	 *
	 * @param null $connection
	 * @throws ApplicationException
	 */
	public function __construct($connection = null) {

		if ($connection instanceof Database) {
			$this->setConnection($connection);
		}
		else {
			$pdo = Engine::$app->getDic()->get('dbpool')->get('sphinx');
			parent::__construct(new Database($pdo));
		}
	}

}