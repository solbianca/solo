<?php


namespace Engine\Base\Interfaces;


interface ServiceInterface {
	/**
	 * Send request to service
	 *
	 * @param string $method Requested method
	 * @param array $params Parameters that passed to service
	 * @return mixed
	 */
	public function send($method, array $params = []);

	/**
	 * Get service name
	 *
	 * @return string
	 */
	public function getName();
}