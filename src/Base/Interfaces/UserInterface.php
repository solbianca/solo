<?php


namespace Engine\Base\Interfaces;


interface UserInterface {

	/**
	 * Returns a value indicating whether the user is a guest (not authenticated).
	 *
	 * @return bool whether the current user is a guest.
	 */
	public function isGuest();

	/**
	 * Returns a value that uniquely represents the user.
	 *
	 * @return string|int the unique identifier for the user. If `null`, it means the user is a guest.
	 */
	public function getId();

	/**
	 * Return user role or null if user is haven't role.
	 * This method do not return default roles guest|authorized. For that you must use method isGuest()
	 *
	 * @return null|string return user role or null
	 */
	public function getRole();

	/**
	 * Log in a user by the given access token.
	 * This method will first authenticate the user by calling [[IdentityInterface::findIdentityByAccessToken()]]
	 * with the provided access token. If successful, it will call [[login()]] to log in the authenticated user.
	 * If authentication fails or [[login()]] is unsuccessful, it will return null.
	 *
	 * @param string $token Access token
	 * @param string $clientId Client id
	 * @return bool Return true if login was successful.
	 * False is returned if the access token is invalid or [[login()]] is unsuccessful.
	 */
	public function loginByAccessToken($token, $clientId);

	/**
	 * Logs in a user.
	 *
	 *
	 * @param string $username
	 * @param string $password
	 * @return mixed
	 */
	public function login($username, $password);

	/**
	 * @return mixed
	 */
	public function getIdentity();

	/**
	 * @param array $identity
	 * @return mixed
	 */
	public function setIdentity(array $identity);
}