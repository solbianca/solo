<?php


namespace Engine\Base\Interfaces;


interface ValidateInterface {

	/**
	 * Validate given attributes.
	 * Is validation successful return true, else false.
	 *
	 * @param array $attributes
	 * @return bool
	 */
	public function validate(array $attributes = []);
}