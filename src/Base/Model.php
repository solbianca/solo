<?php

namespace Engine\Base;


use Engine\Base\Traits\DbConnectionTrait;
use Engine\Base\Traits\LoadTrait;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Db\Database;
use Engine\Db\Exception\DbException;
use Engine\Engine;

/**
 * Class Model
 * @package Engine\Base
 *
 * @property Database $sphinx - иснтанс подключения к sphinx
 */
abstract class Model {

	use LoadTrait;
	use DbConnectionTrait;

	/**
	 * @var string Table name
	 */
	protected static $table;

	/**
	 * 'id' => 'int',
	 * 'name'  => 'string',
	 * 'type'  => [
	 *     '12',
	 *     '234',
	 *     345
	 * ]
	 * @var array
	 */
	protected $tableDefinition = [];

	const PARAM_STR = 'string';
	const PARAM_INT = 'int';
	const PARAM_BOOL = 'boolean';
	const PARAM_NULL = 'null';
	const PARAM_FIXED = 'fixed';

	/**
	 * Model constructor.
	 *
	 * @param null $connection
	 * @throws ApplicationException
	 */
	public function __construct($connection = null) {

		if ($connection instanceof Database) {
			$this->setConnection($connection);
		}
		else {
			if (null === ($name = Engine::$app->getConfig('databases.defaultConnectionName'))) {
				throw new ApplicationException("Bad default connection name definition: define 'defaultConnectionName' in config file.");
			}
			$this->setDefaultConnection($name);
		}
	}

	/**
	 * Get table name fro model
	 *
	 * @return string
	 */
	public static function getTable() {
		return static::$table;
	}

	/**
	 * getter
	 *
	 * @param $name
	 * @return Database|null
	 */
	public function __get($name) {
		switch ($name) {
			case 'sphinx':
				return $this->getSphinxConnection();
			default:
				return null;
		}
	}

	/**
	 * Return prepared select fields, ready to insert in sql statement
	 *
	 * @param array $fields Fields to select
	 * @return string Prepared select string
	 * @throws ApplicationException
	 */
	protected function prepareSelectFields(array $fields): string {
		$table = static::$table;
		$fieldList = [];
		foreach ($fields as $field) {
			if (!isset($this->tableDefinition[$field])) {
				throw new ApplicationException("Bad table field definition. Field '{$field}' not set in \$this->tableDefinition.");
			}
			$fieldList[] = "`{$table}`." . "`$field`";
		}

		return implode(', ', $fieldList);
	}

	/**
	 * Return prepared where sql string with data
	 *
	 * @param array $where
	 * @param string $sqlGlue
	 * @return array
	 * @throws ApplicationException
	 */
	protected function prepareDataFields(array $where = [], string $sqlGlue = ' AND '): array {
		$result = [
			'sql' => '',
			'data' => [],
		];
		if (!empty($where)) {
			$whereTerms = [];
			foreach ($where as $field => $value) {
				if (!isset($this->tableDefinition[$field])) {
					throw new ApplicationException("Bad table field definition. Field '{$field}' not set in \$this->tableDefinition.");
				}
				$whereTerms[] = "`{$field}` = :{$field}";
				$result['data'][$field] = $value;
			}
			$result['sql'] = implode($sqlGlue, $whereTerms);
		}

		return $result;
	}

	/**
	 * Get row from current table by id
	 *
	 * @param $id
	 * @param array $fields
	 * @return array
	 */
	public function getById($id, $fields) {
		$data = [
			'id' => [
				'value' => $id,
				'type' => 'int',
			],
		];
		return $this->select($fields, $data, 1, 0, true);
	}

	/**
	 * $this->db->select(['id', 'name'], $where = ['id' = > 1])
	 *
	 * @param array $fields
	 * @param array $where
	 * @param int $limit
	 * @param int $offset
	 * @param bool $single
	 * @return array
	 */
	public function select(
		$fields = [],
		array $where = [],
		$limit = 0,
		$offset = 0,
		$single = false
	) {
		if (!is_array($fields)) {
			$fields = [$fields];
		}
		$table = static::$table;
		$fieldList = $this->prepareSelectFields($fields);

		$whereList = $this->prepareDataFields($where, ' AND ');
		$data = $whereList['data'];

		$query = "SELECT {$fieldList} FROM {$table} "
			. (!empty($whereList['sql']) ? "WHERE " . $whereList['sql'] : '');

		if ($limit = intval($limit)) {
			$query .= " LIMIT ";
			if ($offset = intval($offset)) {
				$query .= "{$offset}, {$limit}";
			}
			else {
				$query .= "{$limit}";
			}
		}

		return $this->db->select($query, $data, $single);
	}

	/**
	 * @param $data
	 * @return string
	 */
	public function insert($data) {
		$table = static::$table;
		$dataList = $this->prepareDataFields($data, ', ');

		$data = $dataList['data'];
		$query = "INSERT INTO `{$table}` SET " . $dataList['sql'];

		return $this->db->insert($query, $data);
	}

	/**
	 * Оставлю комментарий автора:
	 * -----
	 * Как однажды сказал один великий человек:
	 * --> этот код - упоротая хрень
	 *
	 * // пока используется только в баннерах ($this->insert)
	 * --> пока с существующим insert не получилось
	 *
	 * @return int/null - количество затронутых строк / запрос не выполнился
	 * -----
	 *
	 * @param array $data
	 * @param bool $checkForTableDefinition
	 * @return int|null
	 * @throws DbException
	 */
	public function insertMultiple(array $data) {
		$columns = [];
		$values = [];
		$placeholders = [];
		$table = static::$table;
		foreach ($data as $item) {
			if (empty($columns)) {
				$columns = $this->quoteColumn(array_keys($item));
			}

			if (count($item) !== count($columns)) {
				throw new DbException("Number of columns != number of items.");
			}
			foreach ($item as $column => $value) {
				if (!isset($this->tableDefinition[$column]) || !is_scalar($value)) {
					throw new DbException("Undeclareted `insertMultiple` columns");
				}
				$values[] = $value;
			}

			$placeholders[] = rtrim(str_repeat('?,', count($columns)), ',');
		}
		if (!empty($data)) {
			$query = sprintf('INSERT INTO `%s` (%s) VALUES (%s)',
				$table,
				implode(',', $columns),
				implode('),(', $placeholders));
			return $this->db->query($query, $values);
		}

		return null;
	}

	/**
	 * Ахтунг! Данной функцией нельзя пользоваться, елси обновляешь поле id
	 * Хотя, поле id и не предназначено
	 *
	 * @param $id
	 * @param array $data
	 * @param bool $checkForTableDefinition
	 * @return int
	 */
	public function update($id, $data = []) {
		$table = static::$table;
		$dataList = $this->prepareDataFields($data, ', ');

		$data = $dataList['data'];
		$data['id'] = $id;
		$query = "UPDATE `{$table}` SET " . $dataList['sql']
			. " WHERE `id` = :id "
			. " LIMIT 1";

		return $this->db->update($query, $data);
	}

	/**
	 * Delete row from current table
	 * @param $id
	 * @return int
	 */
	public function delete($id) {
		$id = intval($id);
		$table = static::$table;
		if ($id) {
			$query = "DELETE FROM `{$table}` WHERE `id` = :id LIMIT 1";
			$data = [
				'id' => $id,
			];
			return $this->db->delete($query, $data);
		}
		return 0;
	}

	/**
	 * @param array|string $value
	 * @return array
	 */
	protected function quoteColumn($value) {
		if (is_string($value)) {
			$value = (array)$value;
		}
		return array_map(function ($str) {
			return "`{$str}`";
		}, $value);
	}

	/**
	 * Begin transaction
	 *
	 * @return bool
	 */
	public function beginTransaction() {
		return $this->getConnection()->beginTransaction();
	}

	/**
	 * Commit transaction
	 *
	 * @return bool
	 */
	public function commit() {
		return $this->getConnection()->commit();
	}

	/**
	 * Rollback transaction
	 *
	 * @return bool
	 */
	public function rollBack() {
		return $this->getConnection()->rollBack();
	}
}