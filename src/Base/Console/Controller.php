<?php

namespace Engine\Base\Console;

use Engine\Console\Console;
use Engine\Console\Request;
use Engine\Console\Response;

class Controller extends Console {
	/**
	 * @var array
	 */
	protected $params;

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * Controller constructor.
	 *
	 * @param Request $request
	 * @param Response $response
	 * @param $params - параметры, переданные в консоль
	 */
	public function __construct(Request $request, Response $response, $params) {
		$this->request = $request;
		$this->response = $response;
		$this->params = is_array($params) ? $params : [];
	}

	/**
	 * выполняется перед вызовом action'a
	 *
	 * @param string $action
	 */
	public function beforeAction($action) {

	}

	/**
	 * @return Request
	 */
	public function getRequest() {
		return $this->request;
	}

	/**
	 * @return Response
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 * @param null $key
	 * @return array|mixed|null
	 */
	protected function getParam($key = null) {
		if (null === $key) {
			return $this->params;
		}

		if (isset($this->params[$key])) {
			return $this->params[$key];
		}

		return null;
	}
}