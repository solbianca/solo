<?php


namespace Engine\Base;


use Engine\Core\View;
use Engine\Http\Response;
use Engine\Http\ServerRequest;

abstract class WebController extends Controller {
	/**
	 * @var array
	 */
	protected $rules = [
		[
			'allow' => true,
			'roles' => ['guest', 'authorized'],
		],
	];

	/**
	 * Handler for user authorization
	 *
	 * @var string
	 */
	protected $authMehod;

	/**
	 * @var View
	 */
	protected $view;

	/**
	 * @var ServerRequest;
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * @inheritdoc
	 */
	public function __construct(ServerRequest $request, Response $response, View $view) {
		parent::__construct($request, $response);
		$this->view = $view;
		$this->init();
	}

	/**
	 * Render data as json with proper headers.
	 *
	 * @param mixed $data
	 * @param int $options
	 * @return null
	 */
	public function renderAsJson($data, $options = 320) {
		return $this->view->renderAsJson($data, $options);
	}

	/**
	 * @return string
	 */
	public function getAuthMethod() {
		return $this->authMehod;
	}

	/**
	 * @return array
	 */
	public function getAccessRules() {
		return $this->rules;
	}

	/**
	 * Send response with template
	 *
	 * @param string $template
	 * @param array $params
	 * @return mixed
	 */
	public function render($template, array $params = []) {
		return $this->view->render($template, $params);
	}

	/**
	 * Get template as string
	 *
	 * @param string $template
	 * @param array $params
	 * @return mixed
	 */
	public function fetch($template, array $params = []) {
		return $this->fetch($template, $params);
	}
}