<?php

namespace Engine\Base;

use Engine\Http\Request;
use Engine\Http\Response;

abstract class Controller {
	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * @var array
	 */
	protected $params = [];

	/**
	 * @var array
	 */
	protected $metadata = [];

	/**
	 * Controller constructor.
	 * Set controller dependency and call init method.
	 *
	 * @param Request $request
	 * @param Response $responce
	 */
	public function __construct(Request $request, Response $response) {
		$this->request = $request;
		$this->response = $response;
		$this->init();
	}

	/**
	 * @param array $params
	 * @return $this
	 */
	public function setParams(array $params) {
		$this->params = $params;
		return $this;
	}

	/**
	 * @param array $params
	 * @return $this
	 */
	public function setMetadata(array $metadata) {
		$this->metadata = $metadata;
		return $this;
	}

	/**
	 * @param string $name Parameter name
	 * @param mixed $value Value for parameter
	 * @return $this
	 */
	public function setParam($name, $value) {
		$this->params[$name] = $value;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getParams() {
		return $this->params;
	}

	/**
	 * @return array
	 */
	public function getMetadata($name = null) {
		if ($name && isset($this->metadata[$name])) {
			return $this->metadata[$name];
		}
		elseif (!$name) {
			return $this->metadata;
		}

		return null;
	}

	/**
	 * Get parameter by name or default value if not exist
	 *
	 * @param string $name Parameter name
	 * @param null $defaultValue Default value for parameter if not exist
	 * @param bool $required Check that parameter required
	 * @return mixed|null
	 * @throws \Exception
	 */
	public function getParam($name, $defaultValue = null, $required = false) {
		if ($required && !isset($this->params[$name])) {
			throw new \Exception("Parameter `{$name}` not found!");
		}
		return isset($this->params[$name]) ? $this->params[$name] : $defaultValue;
	}

	/**
	 * Init method that called after constructor.
	 */
	public function init() {

	}

	/**
	 * @param string $action action name (for example 'indexAction')
	 */
	public function beforeAction($action) {

	}

	/**
	 * @param mixed $data
	 * @param int $options
	 * @return null
	 */
	public function asJson($data) {
		return json_encode($data);
	}

	/**
	 * @return Request
	 */
	public function getRequest() {
		return $this->request;
	}

	/**
	 * @return Response
	 */
	public function getResponse() {
		return $this->response;
	}
}