<?php


namespace Engine\Base;


use Engine\Base\Interfaces\ServiceInterface;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Interfaces\ServiceManagerInterface;
use Engine\JsonRpc2\Client\Client;

abstract class Service implements ServiceInterface {

	/**
	 * @var Client[]
	 */
	protected $clients;

	/**
	 * Service name
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * @var ServiceManagerInterface
	 */
	protected $serviceManager;

	/**
	 * Service constructor.
	 *
	 * @param ServiceManagerInterface $serviceManager
	 * @param null $name Service name
	 * @throws ApplicationException
	 */
	public function __construct(ServiceManagerInterface $serviceManager, $name = null) {
		$this->serviceManager = $serviceManager;
		if (isset($name)) {
			$this->name = $name;
		}
		if (empty($this->name)) {
			throw new ApplicationException("Bad service name. You must define property 'name' or pass parameter 'name' through constructor.");
		}
		$this->clients = $this->createClients($this->name);
	}

	/**
	 * @inheritdoc
	 */
	public function send($method, array $params = []) {
		$client = $this->takeClient();
		return $client->$method($params);
	}

	/**
	 * Take random client from clients array
	 *
	 * @return Client
	 */
	protected function takeClient() {
		return $this->clients[array_rand($this->clients)];
	}

	/**
	 * @inheritdoc
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return Client[]
	 */
	public function getClients() {
		return $this->clients;
	}

	/**
	 * @param string $name
	 * @return Client[]
	 */
	protected function createClients($name) {
		return $this->serviceManager->getClients($name);
	}
}