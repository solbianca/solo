<?php

namespace Engine\Base;

use Engine\Base\Traits\DbConnectionTrait;
use Engine\Base\Traits\SphixConnectionTrait;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Db\Database;
use Engine\Engine;

/**
 * Class AbstractMigration
 * @package Engine\Base
 *
 */
abstract class AbstractMigration {
	use DbConnectionTrait;
	use SphixConnectionTrait;

	/**
	 * AbstractMigration constructor.
	 *
	 * @param null $connection
	 * @throws ApplicationException
	 */
	public function __construct($connection = null) {

		if ($connection instanceof Database) {
			$this->setConnection($connection);
		}
		else {
			if (null === ($name = Engine::$app->getConfig('databases.defaultConnectionName'))) {
				throw new ApplicationException("Bad default connection name definition: define 'defaultConnectionName' in config file.");
			}
			$this->setDefaultConnection($name);
		}
	}

	/**
	 * This method contains the logic for "upgrading" the database
	 * and to be executed when applying this migration.
	 *
	 * @return bool return a false value to indicate the migration fails
	 * and should not proceed further. All other return values mean the migration succeeds.
	 */
	abstract public function up();

	/**
	 * This method contains the logic for the "downgrading" database
	 * and to be executed when removing this migration.
	 * The default implementation throws an exception indicating the migration cannot be removed.
	 *
	 * @return bool return a false value to indicate the migration fails
	 * and should not proceed further. All other return values mean the migration succeeds.
	 */
	abstract public function down();

}