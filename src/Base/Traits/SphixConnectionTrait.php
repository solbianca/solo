<?php


namespace Engine\Base\Traits;


use Engine\Core\Exceptions\ApplicationException;
use Engine\Db\Database;
use Engine\Engine;

trait SphixConnectionTrait {

	/**
	 * Sphinx connection instance
	 *
	 * @var Database
	 */
	protected $sphinxQl;

	/**
	 * возращает подключение к sphinx, обернутое в Engine\Db\Database
	 *
	 * @return Database
	 * @throws ApplicationException
	 */
	public function getSphinxConnection() {
		if ($this->sphinxQl instanceof Database) {
			return $this->sphinxQl;
		}
		$pdo = Engine::$app->getDic()->get('dbpool')->get('sphinx');
		$this->sphinxQl = new Database($pdo);
		if (!($this->sphinxQl instanceof Database)) {
			throw new ApplicationException("Bad Sphinx connection definition: connection MUST be instance of 'Database'.");
		}
		return $this->sphinxQl;
	}
}