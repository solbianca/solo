<?php


namespace Engine\Base\Traits;


trait LoadTrait {

	use AttributesTrait;

	/**
	 * Populates the model with input data.
	 *
	 * @param array $data the data array to load, typically `$_POST` or `$_GET`.
	 * @param null $scope the scope to use to load the data into the model.
	 * @return bool whether `load()` found the expected form in `$data`.
	 */
	public function load($data, $scope = null) {
		if (!is_array($data)) {
			return false;
		}
		if (empty($scope) && !empty($data)) {
			$this->setAttributes($data);
			return true;
		}
		elseif (isset($data[$scope])) {
			$this->setAttributes($data[$scope]);
			return true;
		}
		return false;
	}
}