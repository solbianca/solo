<?php

namespace Engine\Base\Traits;

use Engine\Core\Exceptions\ApplicationException;


trait ModelIndexTrait {
	/**
	 * @param array $filter
	 * @param int $limit
	 * @param int $offset
	 * @return mixed
	 *
	 * Возвращает список значений по параметрам filter
	 */
	public function index($filter = [], $limit = 10, $offset = 0) {
		return $this->select('*', $filter, $limit, $offset, false, true);
	}
}

trait ModelGetTrait {
	/**
	 * @param $id
	 * @param string $fields
	 * @return mixed
	 *
	 * Если id это число, то воспринимается как getById.
	 * Иначе - выбирает по заданным полям  [поле => значение, поле => значение]
	 */
	public function get($id, $fields = '*') {

		if (is_numeric($id)) {
			$data = ['id' => ['value' => $id, 'type' => 'int']];
		}
		elseif (is_array($id)) {
			foreach ($id as $k => $v) {
				if (isset($this->tableDefinition[$k])) {
					$data[$k] = ['value' => $v, 'type' => $this->tableDefinition[$k]];
				}
				else {
					throw new ApplicationException("Undefined field $k");
				}
			}
		}
		else {
			throw new ApplicationException("Can't use argument");
		}


		return $this->select($fields, $data, 1, 0, true, false);
	}
}


trait CRUDModelTrait {
	use ModelIndexTrait, ModelGetTrait;
}
