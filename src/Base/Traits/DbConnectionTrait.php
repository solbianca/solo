<?php

namespace Engine\Base\Traits;


use Engine\Core\Exceptions\ApplicationException;
use Engine\Db\Database;
use Engine\Engine;

trait DbConnectionTrait {
	/**
	 * Database class instance
	 *
	 * @var Database
	 */
	protected $db;

	/**
	 * Set database connection for model
	 *
	 * @param Database $database
	 * @return $this
	 */
	public function setConnection(Database $database) {
		$this->db = $database;
		return $this;
	}

	/**
	 * Set default database connection for model
	 *
	 * @param $name
	 * @return $this
	 * @throws ApplicationException
	 */
	public function setDefaultConnection($name) {
		$pdo = Engine::$app->getDic()->get('dbpool')->get($name);
		$database = new Database($pdo);
		if (!($database instanceof Database)) {
			throw new ApplicationException("Bad connection definition: connection MUST be instance of 'Database'.");
		}
		$this->setConnection($database);
		return $this;
	}

	/**
	 * @return Database
	 */
	public function getConnection() {
		return $this->db;
	}
}