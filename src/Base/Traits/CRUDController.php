<?php

namespace Engine\Base\Traits;

use Engine\Core\Exceptions\ApplicationException;

trait ControllerIndexTrait {
	public function indexAction() {

		if (!$this->model) {
			throw new ApplicationException('Model not initialized');
		}

		return $this->model->index(
			$this->getParam('filter', []),
			$this->getParam('limit', 10),
			$this->getParam('offset', 0)
		);
	}
}

trait ControllerGetTrait {
	public function getAction() {
		return $this->model->get(
			$this->getParam('id', 0, true)
		);
	}
}

trait ControllerCreateTrait {

	public function createAction() {
		return $this->model->create(
			$this->getParam('data', [], true)
		);
	}
}

trait ControllerUpdateTrait {

	public function updateAction() {
		return $this->model->update(
			$this->getParam('id', 0, true),
			$this->getParam('data', [])
		);
	}
}

trait ControllerDeleteTrait {

	public function deleteAction() {
		return $this->model->delete(
			$this->getParam('id', 0, true)
		);
	}
}


trait CRUDControllerTrait {
	use controllerIndexTrait, controllerGetTrait, controllerCreateTrait, controllerUpdateTrait, controllerDeleteTrait;
}
