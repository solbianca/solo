<?php


namespace Engine\Base\Traits;

use Engine\Core\Exceptions\ApplicationException;

/**
 * Trait MagicGetTrait
 * @package Engine\Base\Traits
 *
 * Make some magic!
 */
trait MagicGetTrait {
	/**
	 * Magic __get
	 *
	 * @param string $name
	 * @return mixed
	 * @throws \Exception
	 */
	public function __get($name) {
		$method = 'get' . ucfirst($name);
		if (method_exists($this, $method)) {
			return call_user_func([$this, $method]);
		}

		throw new ApplicationException("Magic method get: requested parameter '{$name}' not exist.");
	}
}