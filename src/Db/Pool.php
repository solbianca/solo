<?php


namespace Engine\Db;


use Engine\Db\Exception\DbPoolException;
use Engine\Helpers\ArrayHelper;

/**
 * Class Pool
 * @package Engine\Db
 */
class Pool {

	/**
	 * @var array
	 */
	protected $configs;

	/**
	 * @var \PDO[]
	 */
	protected $connections;

	/**
	 * Holds aliases for write-protected definitions
	 *
	 * @var array
	 */
	protected $immutable = [];

	/**
	 * Store connection configuration.
	 *
	 * @param array $config Configuration for connection
	 * @param string $alias Alias for connection name
	 * @param bool $immutable Make connection immutable.
	 * If true you can't override connection.
	 * @throws DbPoolException
	 */
	public function register(array $config, $alias, $immutable = true) {
		if (!isset($config['dsn'])) {
			throw new DbPoolException("Bad database connection definition: missing 'dsn' parameter.");
		}
		if ('' === $alias || !is_string($alias)) {
			throw new DbPoolException("Connection alias can't be empty value.");
		}
		if ($immutable && in_array($alias, $this->immutable)) {
			throw new DbPoolException("Connection already defined and can't be overridden.");
		}

		if ($immutable) {
			$this->immutable[] = $alias;
		}
		$this->configs[$alias] = $config;
	}

	/**
	 * Create connection for database by given config
	 *
	 * @param array $config Configuration for connection
	 * @return \PDO
	 * @throws DbPoolException
	 */
	public function connect(array $config) {
		switch ($config['dsn']) {
			case 'mysql':
				$connection = $this->connectMysql($config);
				break;
			case 'sqlite':
				$connection = $this->connectSqlite($config);
				break;
			case 'sphinx':
				$connection = $this->connectSphinxQL($config);
				break;
			default:
				throw new DbPoolException("Connection method not defined.");
		}
		return $connection;
	}

	/**
	 * Get existing PDO connection.
	 *
	 * @param $alias
	 * @return \PDO
	 * @throws DbPoolException
	 */
	public function get($alias) {
		if (isset($this->connections[$alias])) {
			$this->connections[$alias];
		}

		if (!isset($this->configs[$alias])) {
			throw new DbPoolException("Connection configuration not stored. You try get not existing pdo connection configuration.");
		}

		$this->connections[$alias] = $this->connect($this->configs[$alias]);

		return $this->connections[$alias];
	}

	/**
	 * Create MySQL PDO connection.
	 *
	 * @param array $config
	 * @return \PDO
	 * @throws DbPoolException
	 */
	protected function connectMysql(array $config) {
		if (!ArrayHelper::keysExist(['host', 'dbname', 'user', 'password'], $config)) {
			throw new DbPoolException("Bad MySQL connection configuration. Check parameters: host, dbname, user, password.");
		}

		try {
			$dsn = $config['dsn'];
			$host = $config['host'];
			$dbname = $config['dbname'];
			$charset = (isset($config['charset'])) ? $config['charset'] : 'utf8';
			$user = $config['user'];
			$password = $config['password'];

			$pdoConnection = new \PDO("$dsn:host=$host;dbname=$dbname;charset=$charset", $user, $password,
				[\PDO::ATTR_PERSISTENT => true]);
			$pdoConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
			$pdoConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

			return $pdoConnection;
		} catch (\Exception $e) {
			throw new DbPoolException('MySql connection error. ' . $e->getMessage());
		}
	}

	/**
	 * Create SqLite connection.
	 *
	 * @param array $config
	 * @return \PDO
	 * @throws DbPoolException
	 */
	protected function connectSqlite(array $config) {
		if (!ArrayHelper::keysExist(['dsn', 'path'], $config)) {
			throw new DbPoolException('Bas SQLite connection: check parameters: path, dsn');
		}

		try {
			$dsn = $config['dsn'];
			$path = $config['path'];

			$connection = new \PDO("$dsn:$path");

			return $connection;
		} catch (\Exception $e) {
			throw new DbPoolException('SqLite connection error. ' . $e->getMessage());
		}
	}

	/**
	 * Create Sphinx connection
	 *
	 * @param array $config
	 * @return \PDO
	 * @throws DbPoolException
	 */
	protected function connectSphinxQL(array $config) {
		if (!ArrayHelper::keysExist(['host', 'port'], $config)) {
			throw new DbPoolException("Bad Sphinx connection configuration. Check parameters: host, port");
		}

		try {
			$pdoConnection = new \PDO('mysql:host=' . $config['host'] . ':' . $config['port'], null, null,
				[\PDO::ATTR_PERSISTENT => true]);
			$pdoConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
			$pdoConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);

			return $pdoConnection;
		} catch (\Exception $e) {
			throw new DbPoolException('Sphinx connection error. ' . $e->getMessage());
		}
	}
}