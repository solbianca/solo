<?php


namespace Engine\Db;

/**
 * Interface DatabaseInterface
 * @package Engine\Db
 */
interface DatabaseInterface {

	/**
	 * @return \PDO
	 */
	public function getConnection();

	/**
	 * @param \PDO $connection
	 * @return mixed
	 */
	public function setConnection(\PDO $connection);
}