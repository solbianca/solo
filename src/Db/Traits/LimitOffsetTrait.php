<?php

namespace Engine\Db\Traits;

/**
 * Class LimitOffsetTrait
 * @package Engine\Db\Traits
 */
trait LimitOffsetTrait
{
	/**
	 * @param int $page
	 * @param int $rowCount - limit
	 * @return string
	 */
	public function limitPage($page, $rowCount) {
		$page = intval($page > 0 ? $page : 1);
		$rowCount = intval($rowCount > 0 ? $rowCount : 1);
		$offset = $rowCount * ($page - 1);
		$result = " LIMIT {$offset},{$rowCount}";
		return $result;
	}

	/**
	 * @param int $offset
	 * @param int $rowCount - limit
	 * @return string
	 */
	public function limitOffset($offset, $rowCount) {
		$offset = intval($offset > 0 ? $offset : 0);
		$rowCount = intval($rowCount > 0 ? $rowCount : 1);
		$result = " LIMIT {$offset},{$rowCount}";
		return $result;
	}
}