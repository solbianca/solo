<?php


namespace Engine\Db\Traits;


trait ImplodeFieldsTrait {

	/**
	 * Implode given fields in string
	 *
	 * @param array $fields
	 * @return string
	 */
	public function implodeFields(array $fields) {
		if (empty($fields)) {
			return "";
		}
		$result = "";
		$firstIteration = true;
		foreach ($fields as $alias => $col) {
			if ($firstIteration) {
				$firstIteration = false;
			}
			else {
				$result .= ',';
			}
			
			if (is_string($alias)) {
				$result .= "{$col} as {$alias}";
			}
			else {
				$result .= "{$col}";
			}
		}
		return $result;
	}
}