<?php

namespace Engine\Db;

use Engine\Db\Traits\LimitOffsetTrait;


/**
 * Class Database
 *
 * @package Engine\Db
 */
class Database
{

	use LimitOffsetTrait;

	/**
	 * Active pdo connection
	 *
	 * @var \PDO
	 */
	protected $connection;

	/**
	 * Types o binding variables
	 *
	 * @var array
	 */
	protected $types = [
		'string' => \PDO::PARAM_STR,
		'int' => \PDO::PARAM_INT,
		'boolean' => \PDO::PARAM_BOOL,
		'null' => \PDO::PARAM_NULL,
		'fixed' => \PDO::PARAM_STR,
	];

	/**
	 * Database constructor.
	 *
	 * @param \PDO $connection
	 */
	public function __construct(\PDO $connection) {
		$this->setConnection($connection);
	}

	/**
	 * @return \PDO
	 */
	public function getConnection() {
		return $this->connection;
	}

	/**
	 * @param \PDO $connection
	 */
	public function setConnection(\PDO $connection) {
		$this->connection = $connection;
	}

	/**
	 * Normalize inout data array in functions for binding
	 *
	 * @param array $data
	 * @return array
	 * @throws Exception\DbNotPossibleValueException
	 * @throws Exception\DbValueNotFoundException
	 */
	protected function normalizeData(array $data) {
		foreach ($data as $field => &$params) {
			if (!is_array($params)) {
				$params = [
					'value' => (string)$params,
					'type' => 'string',
				];
			}

			if (!isset($params['value'])) {
				throw new Exception\DbValueNotFoundException("Value in query is not set");
			}

			if (!isset($params['type']) || !isset($this->types[$params['type']])) {
				$params['type'] = 'string';
			}

			if ($params['type'] == 'fixed') {
				if (!isset($params['values']) || !is_array($params['values']) || !in_array($params['value'],
						$params['values'])
				) {
					throw new Exception\DbNotPossibleValueException('Value is not in fixed values array');
				}
			}

			$params['type'] = $this->types[$params['type']];
		}

		return $data;
	}

	/**
	 * Bind values to statement with bindValue() method
	 *
	 * @param \PDOStatement $statement
	 * @param array $data
	 * @return \PDOStatement
	 */
	protected function bindValues(\PDOStatement $statement, $data = []) {
		$data = $this->normalizeData($data);
		if (!empty($data)) {
			foreach ($data as $field => $params) {
				$statement->bindValue($field, $params['value'], $params['type']);
			}
		}
		return $statement;
	}

	/**
	 * Bind values to statement with bindParam() method
	 *
	 * @param \PDOStatement $statement
	 * @param array $data
	 * @return \PDOStatement
	 */
	protected function bindParams(\PDOStatement $statement, $data = []) {
		$data = $this->normalizeData($data);
		if (!empty($data)) {
			foreach ($data as $field => $params) {
				$statement->bindParam($field, $params['value'], $params['type']);
			}
		}
		return $statement;
	}

	/**
	 * @param $query - sql statement
	 * @param array $data
	 * @param bool $single
	 * @return array
	 */
	public function select($query, $data = [], $single = false) {
		$data = empty($data) ? [] : $data;
		$sth = $this->connection->prepare($query);
		$sth = $this->bindValues($sth, $data);
		$sth->execute();

		return $single ? $sth->fetch() : $sth->fetchAll();
	}

	/**
	 * Example:
	 * $this->db->query("SELECT * FROM `tableName` WHERE `id` = :id AND `name` = :name", [
	 *  'id' => 12,
	 *  'name' => [
	 *    'type' => 'string',
	 *    'value' => 12
	 *  ]
	 * ])
	 * @param $query - sql statement
	 * @param array $data
	 * @return integer
	 */
	public function update($query, $data = null) {
		$sth = $this->connection->prepare($query);
		$sth = $this->bindValues($sth, $data);
		$sth->execute();

		return $sth->rowCount();
	}

	/**
	 * @param $query - sql statement
	 * @param array $data
	 * @see FunctionsTrait::update() example
	 * @return string
	 */
	public function insert($query, $data = null) {
		$sth = $this->connection->prepare($query);
		$sth = $this->bindValues($sth, $data);
		$sth->execute();

		$insertId = $this->connection->lastInsertId();
		return $insertId;
	}

	/**
	 * @param $query - sql statement
	 * @param null $data
	 * @return int
	 */
	public function delete($query, $data = null) {
		$sth = $this->connection->prepare($query);
		$sth = $this->bindValues($sth, $data);
		$sth->execute();

		return $sth->rowCount();
	}

	/**
	 * @param $query - sql statement
	 * @param null $data
	 * @return int
	 */
	function query($query, $data = null) {
		$sth = $this->connection->prepare($query);
		$sth->execute($data);

		return $sth->rowCount();
	}

	/**
	 * Begin transaction
	 *
	 * @return bool
	 */
	public function beginTransaction() {
		return $this->connection->beginTransaction();
	}

	/**
	 * Commit transaction
	 *
	 * @return bool
	 */
	public function commit() {
		return $this->connection->commit();
	}

	/**
	 * Rollback transaction
	 *
	 * @return bool
	 */
	public function rollBack() {
		return $this->connection->rollBack();
	}
}