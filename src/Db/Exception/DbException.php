<?php

namespace Engine\Db\Exception;

use Engine\Core\Exceptions\ApplicationException;

class DbException extends ApplicationException {

}
