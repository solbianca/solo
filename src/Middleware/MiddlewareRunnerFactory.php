<?php


namespace Engine\Middleware;


use Engine\Engine;

/**
 * Class MiddlewareRunnerFactory
 * @package Engine\Middleware
 */
class MiddlewareRunnerFactory {

	/**
	 * @param array $queue
	 * @return MiddlewareRunner
	 */
	public function createRunner(array $queue): MiddlewareRunner {
		return new MiddlewareRunner($queue, Engine::$app->getDic());
	}
}