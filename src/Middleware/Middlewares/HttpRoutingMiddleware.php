<?php


namespace Engine\Middleware\Middlewares;


use Engine\Base\WebController;
use Engine\Core\AccessControl;
use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Exceptions\ForbiddenException;
use Engine\Engine;
use Engine\Http\Factory\HttpRouterFactory;
use Engine\Http\HttpRouter;
use Engine\Http\Interfaces\ResponseInterface as Response;
use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Middleware\Interfaces\MiddlewareInterface;

class HttpRoutingMiddleware implements MiddlewareInterface {

	const CONTROLLER_KEY = 'CONTROLLER';
	const ACTION_KEY = 'ACTION';
	const PARAMS_KEY = 'PARAMS';

	/**
	 * Urls from config for HttpRouter.
	 *
	 * @var array
	 */
	protected $urls = [];

	/**
	 * Namespace for HttpRouter. Used for setting path for router on which the full path to the controllers will be formed.
	 * Namespace MUST ended with a backslash.
	 * Example: $routerNamespace = 'Foo\Bar\Buzz\\' will form path Foo\Bar\Buz\SiteController
	 *
	 *
	 * @var string
	 */
	protected $routerNamespace = '';

	/**
	 * @param ServerRequestInterface $request
	 * @param Response $response
	 * @param callable|null $next
	 * @return mixed
	 * @throws ApplicationException
	 */
	public function __invoke(ServerRequestInterface $request, Response $response, callable $next = null) {
		$controllerPattern = $this->getControllerPattern($request, $response, $this->routerNamespace,
			$this->getLanguages());
		if (empty($controllerPattern)) {
			return $response->withStatus(404);
		}

		$class = $controllerPattern['controller'];
		$action = $controllerPattern['action'];
		$params = $controllerPattern['params'];

		if (!class_exists($class)) {
			return $response->withStatus(404);
		}

		$view = Engine::$app->getDic()->get('view');
		$controller = new $class($request, $response, $view);

		if (!method_exists($controller, $action)) {
			return $response->withStatus(404);
		}
		elseif (!($controller instanceof WebController)) {
			return $response->withStatus(404);
		}

		$request = $request->withAttribute(self::CONTROLLER_KEY, $controller);
		$request = $request->withAttribute(self::ACTION_KEY, $action);
		$request = $request->withAttribute(self::PARAMS_KEY, $params);

		$controller->beforeAction($action);

		if (false === $this->checkAccess($controller->getAccessRules(), $action, $request)) {
			return $response->withStatus(405);
		}

		$response = call_user_func_array([$controller, $action], $params);

		return $next($request, $response);
	}

	/**
	 * @param array $rules
	 * @param string $action
	 * @param ServerRequestInterface $request
	 * @return bool
	 * @throws ForbiddenException
	 */
	protected function checkAccess(array $rules, $action, ServerRequestInterface $request): bool {
		$user = Engine::$app->getUser();
		if (empty($rules)) {
			return true;
		}
		$accessControl = $this->createAccessControl($rules);
		return $accessControl->resolve($user, $action, $request);
	}

	/**
	 * @param array $rules
	 * @return AccessControl
	 */
	protected function createAccessControl(array $rules = []): AccessControl {
		return new AccessControl($rules);
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param Response $response
	 * @param string $namespace
	 * @param array $languages
	 * @return array
	 */
	protected function getControllerPattern(
		ServerRequestInterface $request,
		Response $response,
		string $namespace,
		array $languages
	): array {
		$router = $this->getRouter($request, $response, $namespace, $languages);
		return $router->resolve($this->getUrlsForRouting());
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param Response $response
	 * @param string $namespace
	 * @param array $languages
	 * @return HttpRouter
	 */
	protected function getRouter(
		ServerRequestInterface $request,
		Response $response,
		string $namespace,
		array $languages
	): HttpRouter {
		return (new HttpRouterFactory())->createRouter($request, $response, $namespace, $languages);
	}

	/**
	 * @param array $urls
	 * @return $this
	 */
	public function setUrls(array $urls) {
		$this->urls = $urls;
		return $this;
	}

	/**
	 * @return array
	 */
	protected function getUrlsForRouting(): array {
		if (null !== $this->urls) {
			return $this->urls;
		}
		return $this->getUrlsFromConfig();
	}

	/**
	 * @return array
	 */
	protected function getUrlsFromConfig(): array {
		$urls = Engine::$app->getConfig('urls');
		return (!empty($urls)) ? $urls : [];
	}

	/**
	 * @param string $routerNamespace
	 * @return $this
	 */
	public function setRouterNamespace(string $routerNamespace) {
		$this->routerNamespace = $routerNamespace;
		return $this;
	}

	/**
	 * @return array
	 */
	protected function getLanguages(): array {
		$langs = Engine::$app->getConfig('site.languages');
		return (!empty($langs)) ? $langs : [];
	}
}