<?php


namespace Engine\Middleware\Middlewares;


use Engine\Http\Interfaces\ResponseInterface as Response;
use Engine\Http\Interfaces\ServerRequestInterface as Request;
use Engine\Middleware\Helpers;
use Engine\Middleware\Interfaces\MiddlewareInterface;
use Engine\Middleware\Traits\FormTrait;

class CsrfMiddleware implements MiddlewareInterface {

	use FormTrait;

	const CSRF_TOKEN_HEADER = 'x-csrf-token';
	const INPUT_NAME = 'csrf_token';

	/**
	 * @var string
	 */
	protected $sessionSalt = 'rg9843g$gj4w5fhgr545g45g';

	/**
	 * Validate CSRF token for user.
	 * If it's GET request, ssid is set, autoinsert is true and response body contain form, then insert CSRF token in form.
	 * If it's GET request and ssid not set, then forward execution to the next middleware
	 * If it's POST request and ssid is set, than validate it. If it's ok forward execution to the next middleware. Else set status 403 and return response.
	 * If it's POST request and ssid not set.then forward execution to the next middleware.
	 *
	 * @param Request $request
	 * @param Response $response
	 * @param callable $next
	 * @return Response
	 */
	public function __invoke(Request $request, Response $response, callable $next) {
		$ssid = $request->getAttribute(SessionIdMiddleware::KEY);

		if (!Helpers::isPost($request) && empty($ssid)) {
			return $next($request, $response);
		}

		$token = $this->getCsrfTokenFromRequest($request);
		$validationToken = $this->generateCsrfTokenForSession($ssid);

		if (Helpers::isPost($request) && !$this->validateToken($token, $validationToken)) {
			return $response->withStatus(403);
		}

		if (!$this->autoInsert) {
			return $next($request, $response);
		}

		$response = $next($request, $response);

		if (empty($ssid)) {
			return $response;
		}

		$generator = function () use ($validationToken) {
			return '<input type="text" name="' . self::INPUT_NAME . '" class="' . self::INPUT_NAME . '" hidden value="' . $validationToken . '">';
		};
		return $this->insertIntoPostForms($response, function ($match) use ($generator) {
			return $match[0] . $generator();
		});
	}

	/**
	 * @param string $token
	 * @param string $validationToken
	 * @return bool
	 */
	protected function validateToken(string $token, string $validationToken): bool {
		if ($token !== $validationToken) {
			return false;
		}
		return true;
	}


	/**
	 * @return string
	 */
	protected function generateCsrfTokenForSession(string $ssid): string {
		$csrfToken = '';
		if (!empty($ssid)) {
			$csrfToken = hash_hmac('sha256', (string)$ssid, $this->sessionSalt);
		}

		return $csrfToken;
	}

	/**
	 * @param string $ssid
	 * @return string
	 */
	protected function getHash(string $ssid): string {
		return hash_hmac('sha256', (string)$ssid, $this->sessionSalt);
	}

	/**
	 * @param Request $request
	 * @return string
	 */
	protected function getCsrfTokenFromRequest(Request $request): string {
		$token = '';
		if (($result = $this->getCsrfFromHeaders($request)) !== '') {
			$token = $result;
		}
		elseif (($result = $this->getCsrfTokenFromRequestBody($request))) {
			$token = $result;
		}

		return $token;
	}

	/**
	 * @param Request $request
	 * @return string
	 */
	protected function getCsrfFromHeaders(Request $request): string {
		return $request->getHeaderLine(self::CSRF_TOKEN_HEADER);
	}

	/**
	 * @param Request $request
	 * @return string
	 */
	protected function getCsrfTokenFromRequestBody(Request $request): string {
		if (!empty($request->getParsedBody()[self::INPUT_NAME])) {
			return $request->getParsedBody()[self::INPUT_NAME];
		}
		return '';
	}
}