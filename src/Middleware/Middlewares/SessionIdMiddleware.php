<?php


namespace Engine\Middleware\Middlewares;


use Engine\Http\Interfaces\ResponseInterface as Response;
use Engine\Http\Interfaces\ServerRequestInterface as Request;
use Engine\Middleware\Interfaces\MiddlewareInterface;

class SessionIdMiddleware implements MiddlewareInterface {

	const KEY = 'SSID';

	const COOKIE_NAME = 'epn_ssid';
	const HEADER_NAME = 'epn-ssid';

	public function __invoke(Request $request, Response $response, callable $next) {
		$ssid = '';
		if (($result = $this->getSsidFromCookies($request)) !== '') {
			$ssid = $result;
		}
		elseif (($result = $this->getSsidFromHeader($request)) !== '') {
			$ssid = $result;
		}

		$request = $request->withAttribute(self::KEY, $ssid);
		return $next($request, $response);
	}

	/**
	 * @param Request $request
	 * @return string
	 */
	protected function getSsidFromCookies(Request $request): string {
		$cookies = $request->getCookieParams();
		return (isset($cookies[self::COOKIE_NAME])) ? $cookies[self::COOKIE_NAME] : '';
	}

	/**
	 * @param Request $request
	 * @return string
	 */
	protected function getSsidFromHeader(Request $request): string {
		return $request->getHeaderLine(self::HEADER_NAME);
	}
}