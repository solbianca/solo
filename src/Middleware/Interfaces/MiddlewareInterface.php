<?php


namespace Engine\Middleware\Interfaces;

use Engine\Http\Interfaces\ResponseInterface as Response;
use Engine\Http\Interfaces\ServerRequestInterface as Request;

interface MiddlewareInterface {

	/**
	 * Middleware logic to be invoked.
	 *
	 * @param Request $request The request.
	 * @param Response $response The response.
	 * @param callable|MiddlewareInterface|null $next The next middleware.
	 * @return Response
	 *
	 */
	public function __invoke(Request $request, Response $response, callable $next);
}