<?php


namespace Engine\Middleware;


use Engine\Http\Interfaces\RequestInterface;
use Engine\Http\Interfaces\ResponseInterface;

class Helpers {

	private static $hash_equals;

	/**
	 * Check if a request is post or any similar method.
	 *
	 * @param RequestInterface $request
	 *
	 * @return bool
	 */
	public static function isPost(RequestInterface $request) {
		switch (strtoupper($request->getMethod())) {
			case 'GET':
			case 'HEAD':
			case 'CONNECT':
			case 'TRACE':
			case 'OPTIONS':
				return false;
		}
		return true;
	}

	/**
	 * Return the mime type.
	 *
	 * @param ResponseInterface $response
	 *
	 * @return string
	 */
	public static function getMimeType(ResponseInterface $response) {
		$mime = strtolower($response->getHeaderLine('Content-Type'));
		$mime = explode(';', $mime, 2);
		return trim($mime[0]);
	}

	/**
	 * Very short timing attack safe string comparison for PHP < 5.6
	 * http://php.net/manual/en/function.hash-equals.php#118384.
	 *
	 * @param string $a
	 * @param string $b
	 *
	 * @return bool
	 */
	public static function hashEquals($a, $b) {
		if (self::$hash_equals === null) {
			self::$hash_equals = function_exists('hash_equals');
		}
		if (self::$hash_equals) {
			return hash_equals($a, $b);
		}
		return substr_count($a ^ $b, "\0") * 2 === strlen($a . $b);
	}
}