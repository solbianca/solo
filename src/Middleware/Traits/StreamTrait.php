<?php


namespace Engine\Middleware\Traits;

use Engine\Http\Factory\StreamFactory;
use Engine\Http\Interfaces\StreamInterface;


/**
 * Trait used to create streams.
 */
trait StreamTrait {
	/**
	 * Get the stream factory.
	 *
	 * @param string|StreamInterface $file Filename or stream it's replacing
	 * @param string $mode
	 * @return StreamInterface
	 */
	private static function createStream($file = 'php://temp', $mode = 'r+') {
		return (new StreamFactory())->createStreamFromFile($file, $mode);
	}
}