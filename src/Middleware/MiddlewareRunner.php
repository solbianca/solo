<?php

namespace Engine\Middleware;


use Engine\Core\Interfaces\ContainerInterface;
use Engine\Http\Interfaces\ResponseInterface as Response;
use Engine\Http\Interfaces\ServerRequestInterface as Request;
use Engine\Middleware\Exceptions\MiddlewareException;
use Engine\Middleware\Exceptions\MiddlewareRuntimeException;
use Engine\Middleware\Interfaces\MiddlewareInterface;

/**
 * Class MiddlewareRunner
 * @package Engine\Middleware
 *
 * Example:
 *
 * $queue = [
 *        new Foo\Bar\BuzzMiddleware,
 *        new Foo\Bar\FooBarMiddleware,
 * ]
 *
 * $runner = new MiddlewareRunner($queue);
 * $response = $runner($request, $response);
 */
class MiddlewareRunner {

	const KEY = 'MIDDLEWARE';

	/**
	 * @var ContainerInterface middleware resolver
	 */
	private $resolver;

	/**
	 * @var mixed[] unresolved middleware queue
	 */
	private $queue;

	/**
	 * Create middleware runner
	 *
	 * @param (callable|MiddlewareInterface|mixed)[] $stack middleware stack (with at least one middleware component)
	 * @param ContainerInterface $resolver optional middleware resolver:
	 * function (string $name): MiddlewareInterface
	 * @throws MiddlewareException if an empty middleware stack was given
	 */
	public function __construct($queue, ContainerInterface $resolver = null) {
		if (count($queue) === 0) {
			throw new MiddlewareException("An empty middleware stack was given");
		}
		$this->queue = $queue;
		$this->resolver = $resolver;
	}

	/**
	 * {@inheritdoc}
	 */
	public function __invoke(Request $request, Response $response) {
		$entry = array_shift($this->queue);
		$middleware = $this->resolve($entry);
		return $middleware($request, $response, $this);
	}

	/**
	 * Converts a queue entry to a callable, using the resolver if present.
	 *
	 * @param mixed|callable|MiddlewareInterface $entry The queue entry.
	 * @return callable|MiddlewareInterface
	 *
	 */
	protected function resolve($entry) {
		// the default callable when the queue is empty
		if (!$entry) {
			return function (
				Request $request,
				Response $response,
				callable $next
			) {
				return $response;
			};
		}

		if (is_string($entry) && class_exists($entry)) {
			$obj = new $entry();
			if ($obj instanceof MiddlewareInterface) {
				return $obj;
			}
		}

		if (is_callable($entry)) {
			return $entry;
		}

		if (!$this->resolver) {
			throw new MiddlewareRuntimeException("Can't resolve middleware entry.");
		}

		return $this->resolver->get((string)$entry);
	}

	/**
	 * Create a middleware callable that acts as a "proxy" to a real middleware that must be returned by the given callback.
	 *
	 * Examples:
	 *
	 * $queue = [
	 *        // Lazy load
	 *        MiddlewareRunner::proxy(function () {
	 *            return new FooMiddleware();
	 *        })
	 *
	 *        // Middleware will be called in 'prod' contecxt
	 *        MiddlewareRunner::proxy(function () {
	 *            return (APP_ENV !== 'prod') ? false : new FooMiddleware();
	 *        }),
	 *
	 *        // Middleware will be called if request contain 'Foo' header
	 *        MiddlewareRunner::proxy(function ($request, $response) {
	 *            if ($request->hasHeader('Foo')) {
	 *                return new FooMiddleware();
	 *            }
	 *
	 *            return false;
	 *        }),
	 *
	 *        // Middleware will be called for specific uri
	 *        MiddlewareRunner::proxy('/admin', function () {
	 *            return new FooMiddleware();
	 *        }),
	 *
	 *        // Middleware will be called for specific uri and request contain header 'Foo'
	 *        MiddlewareRunner::proxy('/actions', function ($request, $response) {
	 *            if ($request->hasHeader('Foo')) {
	 *                return new FooMiddleware()
	 *            }
	 *
	 *            return false;
	 *        }),
	 * ];
	 *
	 * @param callable|string $basePath The base path in which the middleware is created (optional)
	 * @param callable $factory Takes no argument and MUST return a middleware callable or false
	 *
	 * @return callable
	 *     * @throws MiddlewareException
	 */
	public static function proxy($basePath, callable $factory = null) {
		if ($factory === null) {
			$factory = $basePath;
			$basePath = '';
		}

		if (!is_callable($factory)) {
			throw new MiddlewareException('Invalid callable provided');
		}

		return function (Request $request, Response $response, callable $next) use ($basePath, $factory) {
			$path = rtrim($request->getUri()->getPath(), '/');
			$basePath = rtrim($basePath, '/');

			if ($path === $basePath || strpos($path, "{$basePath}/") === 0) {
				$middleware = $factory($request, $response);
			}
			else {
				$middleware = false;
			}

			if ($middleware === false) {
				return $next($request, $response);
			}

			if (!is_callable($middleware)) {
				throw new MiddlewareException(sprintf('Factory returned "%s" instead of a callable or FALSE.',
					gettype($middleware)));
			}

			return $middleware($request, $response, $next);
		};
	}
}