<?php


namespace Engine\Middleware\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class MiddlewareException extends ApplicationException {
	
}