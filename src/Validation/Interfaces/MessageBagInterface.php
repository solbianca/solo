<?php

namespace Engine\Validation\Interfaces;

interface MessageBagInterface {
	public function has($key);

	public function first($key);

	public function get($key);

	public function all();

	public function keys();
}
