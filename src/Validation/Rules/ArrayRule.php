<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class ArrayRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return is_array($value);
    }

    public function error()
    {
        return '{field} must be an array.';
    }

    public function canSkip()
    {
        return true;
    }
}
