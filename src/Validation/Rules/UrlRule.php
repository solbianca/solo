<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class UrlRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }

    public function error()
    {
        return '{field} must be a valid URL.';
    }

    public function canSkip()
    {
        return true;
    }
}
