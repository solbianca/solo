<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class IntRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return is_numeric($value) && (int)$value == $value;
    }

    public function error()
    {
        return '{field} must be a number.';
    }

    public function canSkip()
    {
        return true;
    }
}
