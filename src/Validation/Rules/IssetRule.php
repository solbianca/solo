<?php


namespace Engine\Validation\Rules;


use Engine\Validation\Interfaces\RuleInterface;

class IssetRule implements RuleInterface {

	public function run($value, $input, $args) {
		return (null !== $value);
	}

	public function error() {
		return '{field} must be set.';
	}

	public function canSkip() {
		return true;
	}
}