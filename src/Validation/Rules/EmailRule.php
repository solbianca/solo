<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class EmailRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function error()
    {
        return '{field} must be a valid email address.';
    }

    public function canSkip()
    {
        return true;
    }
}
