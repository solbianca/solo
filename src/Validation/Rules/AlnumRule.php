<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class AlnumRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return (bool) preg_match('/^[\pL\pM\pN]+$/u', $value);
    }

    public function error()
    {
        return '{field} must be alphanumeric.';
    }

    public function canSkip()
    {
        return true;
    }
}
