<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class DateRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        if ($value instanceof \DateTime) {
            return true;
        }

        if (strtotime($value) === false) {
            return false;
        }

        $date = date_parse($value);

        return checkdate($date['month'], $date['day'], $date['year']);
    }

    public function error()
    {
        return '{field} must be a valid date.';
    }

    public function canSkip()
    {
        return true;
    }
}
