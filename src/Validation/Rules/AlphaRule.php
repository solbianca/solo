<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class AlphaRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return (bool) preg_match('/^[\pL\pM]+$/u', $value);
    }

    public function error()
    {
        return '{field} must be alphabetic.';
    }

    public function canSkip()
    {
        return true;
    }
}
