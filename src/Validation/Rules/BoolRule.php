<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class BoolRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return is_bool($value);
    }

    public function error()
    {
        return '{field} must be a boolean.';
    }

    public function canSkip()
    {
        return true;
    }
}
