<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class MatchesRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return $value === $input[$args[0]];
    }

    public function error()
    {
        return '{field} must match {$0}.';
    }

    public function canSkip()
    {
        return true;
    }
}
