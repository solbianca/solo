<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class RequiredRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        $value = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $value);

        return !empty($value);
    }

    public function error()
    {
        return '{field} is required.';
    }

    public function canSkip()
    {
        return false;
    }
}
