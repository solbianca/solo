<?php


namespace Engine\Validation\Rules;


use Engine\Validation\Interfaces\RuleInterface;

/**
 * Class StringRule
 * @package Engine\Validation\Rules
 */
class StringRule implements RuleInterface {

	/**
	 * @inheritdoc
	 */
	public function run($value, $input, $args) {
		return is_string($value);
	}

	/**
	 * @inheritdoc
	 */
	public function error() {
		return '{field} must be a string.';
	}

	/**
	 * @inheritdoc
	 */
	public function canSkip() {
		return true;
	}

}