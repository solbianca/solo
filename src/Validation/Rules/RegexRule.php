<?php

namespace Engine\Validation\Rules;

use Engine\Validation\Interfaces\RuleInterface;

class RegexRule implements RuleInterface
{
    public function run($value, $input, $args)
    {
        return (bool) preg_match($args[0], $value);
    }

    public function error()
    {
        return '{field} was not in the correct format.';
    }

    public function canSkip()
    {
        return true;
    }
}
