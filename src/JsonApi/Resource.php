<?php

namespace Engine\JsonApi;


use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Interfaces\ResourceInterface;
use Engine\JsonApi\Traits\ConvertObjectToArrayTrait;

class Resource implements ResourceInterface {

	use ConvertObjectToArrayTrait;

	/**
	 * @var string
	 */
	protected $type = '';

	/**
	 * @var string
	 */
	protected $id = '';

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * @var Meta
	 */
	protected $meta;

	/**
	 * @var ResourceInterface[]
	 */
	protected $included = [];


	/**
	 * Resource constructor.
	 *
	 * @param string $type
	 * @param string $id
	 */
	public function __construct(string $type = '', string $id = '') {
		$this->type = $type;
		$this->id = $id;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getArray(): array {
		if ('' === $this->type) {
			throw new JsonApiException("You must set 'type' value for resource.");
		}

		$data = [];
		$data['type'] = $this->type;

		if ($this->id) {
			$data['id'] = $this->id;
		}
		else {
			$data['id'] = '';
		}
		if ($this->attributes) {
			$data['attributes'] = $this->attributes;
		}

		if ($this->meta) {
			$data['meta'] = $this->meta->getArray();
		}

		return $data;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setId(string $id): ResourceInterface {
		$this->id = $id;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setType(string $type): ResourceInterface {
		$this->type = $type;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addAttribute(string $key, $value): ResourceInterface {
		if (is_object($value)) {
			$value = $this->convertObjectToArray($value);
		}

		$this->attributes[$key] = $value;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setAttributes($attributes): ResourceInterface {
		if (is_scalar($attributes) || null === $attributes) {
			throw new JsonApiException("Use method 'addParam()' for adding scalar values.");
		}

		if (is_object($attributes)) {
			$attributes = $this->convertObjectToArray($attributes);
		}

		foreach ($attributes as $key => $value) {
			$this->addAttribute($key, $value);
		}

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setMeta($meta): ResourceInterface {
		if (is_scalar($meta) || null === $meta) {
			throw new JsonApiException("Use method 'addParam()' for adding scalar values.");
		}

		if ($meta instanceof Meta) {
			$this->meta = $meta;
			return $this;
		}

		if (is_object($meta)) {
			$meta = $this->convertObjectToArray($meta);
		}

		if (count($meta) === 0) {
			return $this;
		}

		$this->meta = (new Meta())->setParams($meta);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getType(): string {
		return $this->type;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAttributes(): array {
		return $this->attributes;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getMeta(): Meta {
		return $this->meta;
	}
}