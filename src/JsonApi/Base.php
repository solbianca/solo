<?php

namespace Engine\JsonApi;

use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Interfaces\BaseInterface;
use Engine\JsonApi\Traits\ConvertObjectToArrayTrait;

abstract class Base implements BaseInterface {

	use ConvertObjectToArrayTrait;

	/**
	 * @var array
	 */
	protected $params = [];

	/**
	 * {@inheritdoc}
	 */
	public function getArray(): array {
		return $this->params;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addParam(string $key, $value): BaseInterface {
		if (is_object($value)) {
			$value = $this->convertObjectToArray($value);
		}

		$this->params[$key] = $value;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setParams($values): BaseInterface {
		if (is_scalar($values) || null === $values) {
			throw new JsonApiException("Use method 'addParam()' for adding scalar values.");
		}

		if (is_object($values)) {
			$values = $this->convertObjectToArray($values);
		}

		foreach ($values as $key => $value) {
			$this->addParam($key, $value);
		}

		return $this;
	}
}