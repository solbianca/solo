<?php


namespace Engine\JsonApi\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class JsonApiException extends ApplicationException {

}