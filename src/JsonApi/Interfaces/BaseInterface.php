<?php


namespace Engine\JsonApi\Interfaces;


use Engine\JsonApi\Exceptions\JsonApiException;

interface BaseInterface {

	/**
	 * Get object data as array.
	 *
	 * @return array
	 */
	public function getArray(): array;

	/**
	 * Add $value by $key to object parameters.
	 *
	 * @param string $key
	 * @param $value
	 * @return BaseInterface
	 */
	public function addParam(string $key, $value): BaseInterface;

	/**
	 * Set parameters for object. $values must be an object or array. Otherwise method throws exception.
	 *
	 * @param array|object $values
	 * @return self
	 * @throws JsonApiException
	 */
	public function setParams($values): BaseInterface;
}