<?php


namespace Engine\JsonApi\Interfaces;


use Engine\JsonApi\Error;
use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Meta;
use Engine\JsonApi\Request;

interface DocumentInterface {

	const PRIMARY_DATA_TYPE_OBJECT = 1;
	const PRIMARY_DATA_TYPE_COLLECTION = 2;

	/**
	 * Get document as array.
	 * You must define atleast one top-level element: meta, data, error. Or method throws exception.
	 *
	 * @return array
	 * @throws JsonApiException
	 */
	public function getAsArray(): array;

	/**
	 * Get document as valid json string.
	 * You must define atleast one top-level element: meta, data, error. Or method throws exception.
	 *
	 * @return string
	 * @throws JsonApiException
	 */
	public function getAsJson(): string;

	/**
	 * Set primary data type.
	 *
	 * @param int $type
	 * @return DocumentInterface
	 */
	public function setPrimaryDataType(int $type): DocumentInterface;

	/**
	 * Set result for document
	 *
	 * @param bool $result
	 * @return DocumentInterface
	 */
	public function setResult(bool $result): DocumentInterface;

	/**
	 * Add resource to document. Primary data always is collection. Empty primary data is empty array.
	 * Resource can be an instance of a class \Engine\JsonApi\Resource or array.
	 * When pass object, parameters $type and $id will be ignored.
	 * When pass array, you mast set $type. In method will be created instance of a class \Engine\JsonApi\Resource in given $type and $id.
	 * Method will set 'result' to true.
	 *
	 * Method will trows exception if earlier add error.
	 *
	 * @param array|\Engine\JsonApi\Interfaces\ResourceInterface $resource
	 * @param string $type Resource type
	 * @param string $id Resource id
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	public function addResource($resource, string $type = '', string $id = ''): DocumentInterface;

	/**
	 * Set resource to document. Primary data always is object. Empty primary data is null.
	 * Resource can be an instance of a class \Engine\JsonApi\Resource or array.
	 * When pass object, parameters $type and $id will be ignored.
	 * When pass array, you mast set $type. In method will be created instance of a class \Engine\JsonApi\Resource in given $type and $id.
	 * Method will set 'result' to true.
	 *
	 * Method will trows exception if earlier add error.
	 *
	 * @param array|\Engine\JsonApi\Interfaces\ResourceInterface $resource
	 * @param string $type Resource type
	 * @param string $id Resource id
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	public function setResource($resource, string $type = '', string $id = ''): DocumentInterface;

	/**
	 * Add meta to document. Meta can be an instance of a class \Engine\JsonApi\Meta or array. Otherwise method trows exception.
	 * When pass array, method create instance of a class \Engine\JsonApi\Meta with given parameters.
	 * Given data rewrite existing.
	 *
	 * @param array|BaseInterface $meta
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	public function setMeta($meta): DocumentInterface;

	/**
	 * Add request to document. Request can be an instance of a class \Engine\JsonApi\Request or array. Otherwise method trows exception.
	 * When pass array, method create instance of a class \Engine\JsonApi\Request with given parameters.
	 * Given data rewrite existing.
	 *
	 * @param array|BaseInterface $request
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	public function setRequest($request): DocumentInterface;

	/**
	 * Add error to document. Resource can be an instance of a class \Engine\JsonApi\Error or array.
	 * When pass array, method create instance of a class \Engine\JsonApi\Error with given parameters.
	 * Method will set 'result' to false.
	 *
	 * Method will trows exception if earlier add resource.
	 *
	 * @param array|BaseInterface $error
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	public function addError($error): DocumentInterface;

	/**
	 * Add include to document. Include can be an instance of a class \Engine\JsonApi\Resource or array.
	 * When pass object, parameters $type and $id will be ignored.
	 * When pass array, you mast set $type. In method will be created instance of a class \Engine\JsonApi\Resource in given $type and $id.
	 * Method will set 'result' to true.
	 *
	 * Method will trows exception if earlier add error.
	 *
	 * @param array|ResourceInterface $included
	 * @param string $type Resource type
	 * @param string $id Resource id
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	public function addInclude($included, string $type = '', string $id = ''): DocumentInterface;

	/**
	 * @return DocumentInterface
	 */
	public function clearErrors(): DocumentInterface;

	/**
	 * @return DocumentInterface
	 */
	public function clearIncluded(): DocumentInterface;

	/**
	 * @return DocumentInterface
	 */
	public function clearResources(): DocumentInterface;

	/**
	 * @return DocumentInterface
	 */
	public function clearMeta(): DocumentInterface;

	/**
	 * @return DocumentInterface
	 */
	public function clearRequest(): DocumentInterface;

	/**
	 * @return DocumentInterface
	 */
	public function clearDocument(): DocumentInterface;

	/**
	 * @return bool
	 */
	public function getResult(): bool;

	/**
	 * @return \Engine\JsonApi\Resource[]
	 */
	public function getResources(): array;

	/**
	 * @return Request|null
	 */
	public function getRequest();

	/**
	 * @return Error[]
	 */
	public function getErrors(): array;

	/**
	 * @return Resource[]
	 */
	public function getIncluded(): array;

	/**
	 * @return Meta|null
	 */
	public function getMeta();
}