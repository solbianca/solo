<?php


namespace Engine\JsonApi\Interfaces;


use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Meta;

interface ResourceInterface {

	/**
	 * Get resource data as array
	 *
	 * @return array
	 */
	public function getArray(): array;

	/**
	 * Set id for resource
	 *
	 * @param string $id
	 * @return ResourceInterface
	 */
	public function setId(string $id): ResourceInterface;

	/**
	 * Set type for resource
	 *
	 * @param string $type
	 * @return ResourceInterface
	 */
	public function setType(string $type): ResourceInterface;

	/**
	 * Add attribute for resource.
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return ResourceInterface
	 */
	public function addAttribute(string $key, $value): ResourceInterface;

	/**
	 * Set attributes for resource. Old attributes will be overwritten.
	 * Attributes can be array or object. If attributes is scalar, throws exception.
	 *
	 * @param array|object $attributes
	 * @return ResourceInterface
	 * @throws JsonApiException
	 */
	public function setAttributes($attributes): ResourceInterface;

	/**
	 * Set meta for resource. $meta can be array or instance of \Engine\JsonApi\Meta.
	 * When array pass, create instance of \Engine\JsonApi\Meta with given parameters.
	 *
	 * @param array|Meta $meta
	 * @return ResourceInterface
	 */
	public function setMeta($meta): ResourceInterface;

	/**
	 * @return string
	 */
	public function getId(): string;

	/**
	 * @return string
	 */
	public function getType(): string;

	/**
	 * @return array
	 */
	public function getAttributes(): array;

	/**
	 * @return Meta
	 */
	public function getMeta(): Meta;
}