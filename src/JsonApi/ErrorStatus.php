<?php

namespace Engine\JsonApi;

class ErrorStatus {
	const UNKNOWN_ERROR = 1000;
	const INVALID_PARAM = 1001;
	const NOT_DELETE = 1002;
	const NOT_SET_DEFAULT = 1003;
	const NOT_CONFIRM = 1004;
	const ERROR_FORMAT_PURSE = 1005;
	const NOT_EXIST_PURSE_TYPE = 1006;
	const ERROR_FORMAT_PURSE_JSON = 1007;
	const PURSE_EXIST = 1008;
}