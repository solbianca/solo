<?php


namespace Engine\JsonApi;


use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Interfaces\DocumentInterface;
use Engine\JsonApi\Interfaces\ResourceInterface;

class Document implements DocumentInterface {

	/**
	 * @var bool
	 */
	protected $result = true;

	/**
	 * @var ResourceInterface[]
	 */
	protected $resources = [];

	/**
	 * @var Meta|null
	 */
	protected $meta;

	/**
	 * @var Request|null
	 */
	protected $request;

	/**
	 * @var Error[]
	 */
	protected $errors = [];

	/**
	 * @var Resource[]
	 */
	protected $included;

	/**
	 * @var
	 */
	protected $primaryDataType = self::PRIMARY_DATA_TYPE_OBJECT;

	/**
	 * {@inheritdoc}
	 */
	public function getAsArray(): array {
		$output = [];
		if (!empty($this->errors)) {
			foreach ($this->errors as $error) {
				$output['errors'][] = $error->getArray();
			}
		}
		elseif (!empty($this->resources)) {
			if (self::PRIMARY_DATA_TYPE_OBJECT === $this->primaryDataType) {
				$output['data'] = $this->resources[0]->getArray();
			}
			else {
				foreach ($this->resources as $resource) {
					$output['data'][] = $resource->getArray();
				}
			}
		}
		else {
			if (self::PRIMARY_DATA_TYPE_OBJECT === $this->primaryDataType) {
				$output['data'] = null;
			}
			else {
				$output['data'] = [];
			}
		}

		if (!empty($this->meta)) {
			$output['meta'] = $this->meta->getArray();
		}

		if (empty($output)) {
			throw new JsonApiException("You must define atleast one of the folowing top-level member: data, meta, errors.");
		}

		$output['result'] = $this->result;

		if (!empty($this->request)) {
			$output['request'] = $this->request->getArray();
		}

		if (!empty($this->included)) {
			foreach ($this->included as $include) {
				$output['included'][] = $include->getArray();
			}
		}

		return $output;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAsJson(): string {
		return json_encode($this->getAsArray());
	}

	/**
	 * {@inheritdoc}
	 */
	public function setPrimaryDataType(int $type): DocumentInterface {
		if (!in_array($type, [self::PRIMARY_DATA_TYPE_OBJECT, self::PRIMARY_DATA_TYPE_COLLECTION])) {
			throw new JsonApiException("Bad primari data type: {$type}. Allowed values: 1 as object, 2 as collection.");
		}
		$this->primaryDataType = $type;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setResult(bool $result): DocumentInterface {
		$this->result = $result;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addResource($resource, string $type = '', string $id = ''): DocumentInterface {
		$this->setPrimaryDataType(self::PRIMARY_DATA_TYPE_COLLECTION);
		return $this->setResourceData($resource, $type, $id);
	}

	/**
	 * {@inheritdoc}
	 */
	public function setResource($resource, string $type = '', string $id = ''): DocumentInterface {
		$this->resources = [];
		$this->setPrimaryDataType(self::PRIMARY_DATA_TYPE_OBJECT);
		return $this->setResourceData($resource, $type, $id);
	}

	/**
	 * @param $resource
	 * @param string $type
	 * @param string $id
	 * @return DocumentInterface
	 * @throws JsonApiException
	 */
	protected function setResourceData($resource, string $type = '', string $id = ''): DocumentInterface {
		if (!empty($this->errors)) {
			throw new JsonApiException("You can't set errors and resource on single document: http://jsonapi.org/format/#document-top-level");
		}

		$this->setResult(true);

		if ($resource instanceof \Engine\JsonApi\Interfaces\ResourceInterface) {
			$this->resources[] = $resource;
			return $this;
		}

		if (!is_array($resource)) {
			throw new JsonApiException("Resource must be array or implement of Engine\\JsonApi\\Interfaces\\ResourceInterface.");
		}

		if ('' === $type) {
			throw new JsonApiException("You must set 'type' when pass array as resource.");
		}

		$this->resources[] = (new Resource($type, $id))->setAttributes($resource);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setMeta($meta): DocumentInterface {
		if ($meta instanceof Meta) {
			$this->meta = $meta;
			return $this;
		}

		if (!is_array($meta)) {
			throw new JsonApiException("Meta must be array or instance of Engine\\JsonApi\\Meta.");
		}

		$this->meta = (new Meta())->setParams($meta);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRequest($request): DocumentInterface {
		if ($request instanceof Request) {
			$this->request = $request;
			return $this;
		}

		if (!is_array($request)) {
			throw new JsonApiException("Request must be array or instance of Engine\\JsonApi\\Request.");
		}

		$this->request = (new Request())->setParams($request);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addError($error): DocumentInterface {
		if (!empty($this->resources)) {
			throw new JsonApiException("You can't set errors and resource on single document: http://jsonapi.org/format/#document-top-level");
		}

		$this->setResult(false);

		if ($error instanceof Error) {
			$this->errors[] = $error;
			return $this;
		}

		if (!is_array($error)) {
			throw new JsonApiException("\$error must be array or instance of Engine\\JsonApi\\Error.");
		}

		$this->errors[] = (new Error())->setParams($error);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addInclude($included, string $type = '', string $id = ''): DocumentInterface {
		if ($included instanceof ResourceInterface) {
			$this->included[] = $included;
			return $this;
		}

		if (!is_array($included)) {
			throw new JsonApiException("Included must be array or implement Engine\\JsonApi\\Interfaces\\InterfacesResource.");
		}

		if ('' === $type) {
			throw new JsonApiException("You must set 'type' when pass array as resource.");
		}

		$this->included[] = (new Resource($type, $id))->setAttributes($included);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearErrors(): DocumentInterface {
		$this->errors = [];
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearIncluded(): DocumentInterface {
		$this->included = [];
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearResources(): DocumentInterface {
		$this->resources = [];
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearMeta(): DocumentInterface {
		$this->meta = null;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearRequest(): DocumentInterface {
		$this->request = null;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearDocument(): DocumentInterface {
		return $this->clearResources()->clearErrors()->cle->clearMeta()->clearRequest()->setResult(true);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getResult(): bool {
		return $this->result;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getResources(): array {
		return $this->resources;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRequest() {
		return $this->request;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getErrors(): array {
		return $this->errors;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getIncluded(): array {
		return $this->included;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getMeta() {
		return $this->meta;
	}
}