<?php


namespace Engine\JsonApi\Traits;


use Engine\JsonApi\Error;
use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Meta;
use Engine\JsonApi\Request;
use Engine\JsonApi\Resource;

trait ConvertObjectToArrayTrait {

	/**
	 * Converting an object to an array
	 *
	 * @param object $object
	 * @return array
	 * @throws JsonApiException
	 */
	private function convertObjectToArray($object): array {
		if (is_object($object) === false) {
			throw new JsonApiException('Can convert only objects.');
		}

		if ($object instanceof Resource ||
			$object instanceof Error ||
			$object instanceof Request ||
			$object instanceof Meta
		) {
			return $object->getArray();
		}

		return get_object_vars($object);
	}
}