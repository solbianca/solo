<?php


namespace Engine\Debug;


use Engine\Engine;
use Engine\JsonRpc2\Exceptions\JsonRpcException;
use Engine\JsonRpc2\Exceptions\JsonRpcResponseException;
use Engine\Log\LogLevel;

class Debugger {

	/**
	 * @param \Exception $exception
	 */
	public static function trace($exception) {
		$trace = $exception->getTrace();
		echo "<h3>{$exception->getMessage()}</h3><br>" . PHP_EOL;

		if (!empty($exception->getCode())) {
			echo "<h4>Exception code: {$exception->getCode()}</h4>";
		}

		echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
		echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
		echo "<h4>{$exception->getFile()}: {$exception->getLine()}</h4>";
		echo '</pre>' . PHP_EOL;
		echo '</div>' . PHP_EOL;

		echo "<h4>Error trace:</h4>" . PHP_EOL;
		foreach ($trace as $row) {
			echo "<pre>";
			echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
			echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
			print_r($row);
			echo '</pre>' . PHP_EOL;
			echo '</div>' . PHP_EOL;
			echo "</pre>";
		}
	}

	/**
	 * @param $exception JsonRpcResponseException
	 */
	public static function traceJsonRpcResponceException($exception) {
		echo "<h3>Service Exception</h3>" . PHP_EOL;
		echo "<h3>{$exception->getMessage()}</h3><br>" . PHP_EOL;

		if (!empty($exception->getCode())) {
			echo "<h4>Exception code: {$exception->getCode()}</h4>";
		}

		$errorData = $exception->getErrorData();
		echo "<h4>{$errorData['file']}: {$errorData['line']}</h4>";
		echo "<h4>Service error trace:</h4>" . PHP_EOL;
		if (empty($exception->getErrorData())) {
			echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
			echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
			echo "---";
			echo '</div>' . PHP_EOL;
			echo "</pre>";
		}
		else {
			$errorData = $exception->getErrorData();
			foreach ($errorData['trace'] as $row) {
				echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
				echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
				print_r($row);
				echo '</pre>' . PHP_EOL;
				echo '</div>' . PHP_EOL;
			}
		}
	}

	/**
	 * @param JsonRpcException $exception
	 */
	public static function traceJsonRpcException($exception) {
		echo "<h3>JsonRpc Exception.</h3>" . PHP_EOL;
		$message = $exception->getMessage();
		if (strpos($message, 'X-Powered-By') === 0) {
			$message = explode("\n", $message);
			$message = $message[3];
		}

		echo '<h3 style="margin:0px;padding:0px;">' . $message . '</h3>' . PHP_EOL;

		if (null !== ($exception->getJson())) {
			echo '<h4>' . PHP_EOL;
			echo 'Response json: ';
			echo '</h4>' . PHP_EOL;
			if ('' === $exception->getJson()) {
				echo "'' (empty string)";
			}
			else {
				echo $exception->getJson();
			}
		}

		if (!empty($exception->getCode())) {
			echo "<h4>Exception code: {$exception->getCode()}</h4>";
		}

		echo '<br>';

		echo "<h4>Error trace:</h4>" . PHP_EOL;
		$trace = $exception->getTrace();
		foreach ($trace as $row) {
			echo "<pre>";
			echo '<div style="background:#f8f8f8;margin:5px;padding:5px;border: solid grey 1px;">' . PHP_EOL;
			echo '<pre style="margin:0px;padding:0px;">' . PHP_EOL;
			print_r($row);
			echo '</pre>' . PHP_EOL;
			echo '</div>' . PHP_EOL;
			echo "</pre>";
		}
	}

	/**
	 * @param \Exception $exception
	 */
	public static function saveInLog($exception) {
		$logger = Engine::$app->getDic()->get('logger');
		if (is_a($exception, JsonRpcResponseException::class)) {
			$errorData = $exception->getErrorData();
			$message = $exception->getMessage() . "\n" . $errorData['file'] . ': ' . $errorData['line'];
		}
		elseif (is_a($exception, JsonRpcException::class)) {
			$message = $exception->getMessage();
			if (strpos($message, 'X-Powered-By') === 0) {
				$message = explode("\n", $message);
				$message = $message[3];
			}
			if (null !== ($exception->getJson())) {
				$message .= "\n" . $exception->getJson();
			}
		}
		else {
			$message = $exception->getMessage() . "\n" . $exception->getFile() . ': ' . $exception->getLine();
		}
		$message .= "\n" . $exception->getTraceAsString();
		$logger->log(LogLevel::ERROR, $message);
	}
}