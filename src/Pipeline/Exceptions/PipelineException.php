<?php


namespace Engine\Pipeline\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class PipelineException extends ApplicationException {

}