<?php


namespace Engine\Pipeline;


use Engine\Pipeline\Interfaces\PipelineBuilderInterface;
use Engine\Pipeline\Interfaces\PipelineInterface;
use Engine\Pipeline\Interfaces\ProcessorInterface;

class PipelineBuilder implements PipelineBuilderInterface {
	
	/**
	 * @var callable[]
	 */
	private $pipes = [];

	/**
	 * Add a pipe.
	 *
	 * @param callable $pipe
	 * @return PipelineBuilderInterface
	 */
	public function add(callable $pipe): PipelineBuilderInterface {
		$this->pipes[] = $pipe;
		return $this;
	}

	/**
	 * Build a new Pipeline object
	 *
	 * @param  ProcessorInterface|null $processor
	 * @return PipelineInterface
	 */
	public function build(ProcessorInterface $processor = null): PipelineInterface {
		return new Pipeline($this->pipes, $processor);
	}
}