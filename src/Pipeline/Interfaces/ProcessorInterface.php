<?php


namespace Engine\Pipeline\Interfaces;


interface ProcessorInterface {

	/**
	 * Resolve a pipe execution
	 *
	 * @param array $pipes
	 * @param mixed $payload
	 * @return mixed
	 */
	public function process(array $pipes, $payload);
}