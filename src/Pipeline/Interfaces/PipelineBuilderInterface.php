<?php


namespace Engine\Pipeline\Interfaces;


interface PipelineBuilderInterface {

	/**
	 * Add a pipe.
	 *
	 * @param callable $pipe
	 * @return PipelineBuilderInterface
	 */
	public function add(callable $pipe): PipelineBuilderInterface;

	/**
	 * Build a new Pipeline object
	 *
	 * @param  ProcessorInterface|null $processor
	 * @return PipelineInterface
	 */
	public function build(ProcessorInterface $processor = null): PipelineInterface;
}