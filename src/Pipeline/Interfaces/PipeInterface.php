<?php


namespace Engine\Pipeline\Interfaces;


interface PipeInterface {

	/**
	 * @param mixed $payload
	 * @return mixed
	 */
	public function __invoke($payload);
}