<?php


namespace Engine\Pipeline\Interfaces;


interface PipelineInterface {

	/**
	 * Clone current pipeline, add the pipe to it and return the clone
	 *
	 * @param   callable $pipe
	 * @return  PipelineInterface
	 */
	public function pipe(callable $pipe): PipelineInterface;

	/**
	 * Process the pipeline
	 *
	 * @param  mixed $payload
	 * @return mixed
	 */
	public function process($payload = null);
}