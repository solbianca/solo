<?php


namespace Engine\Pipeline;


use Engine\Pipeline\Exceptions\PipelineException;
use Engine\Pipeline\Interfaces\PipelineInterface;
use Engine\Pipeline\Interfaces\ProcessorInterface;

class Pipeline implements PipelineInterface {

	/**
	 * Pipes
	 *
	 * @var array
	 */
	protected $pipes = [];

	/**
	 * Processor used by the pipeline
	 *
	 * @var ProcessorInterface
	 */
	protected $processor = null;

	/**
	 * Constructor
	 *
	 * @param array $pipes
	 * @param ProcessorInterface|null $processor
	 * @throws PipelineException
	 */
	public function __construct(array $pipes = [], ProcessorInterface $processor = null) {
		foreach ($pipes as $pipe) {
			if (false === is_callable($pipe)) {
				throw new PipelineException('All pipes should be callable.');
			}
		}

		$this->pipes = $pipes;
		$this->processor = $processor ?: new DefaultProcessor();
	}

	/**
	 * {@inheritdoc}
	 */
	public function pipe(callable $pipe): PipelineInterface {
		$pipeline = clone $this;
		$pipeline->pipes[] = $pipe;
		return $pipeline;
	}

	/**
	 * {@inheritdoc}
	 */
	public function process($payload = null) {
		return $this->processor->process($this->pipes, $payload);
	}
}