<?php


namespace Engine\Pipeline;


use Engine\Pipeline\Interfaces\ProcessorInterface;

class InterruptibleProcessor implements ProcessorInterface {

	/**
	 * Verification closure
	 *
	 * @var callable
	 */
	protected $check;

	/**
	 * StrictProcessor constructor.
	 *
	 * @param callable $check closure returning a boolean
	 */
	public function __construct(callable $check) {
		$this->check = $check;
	}

	/**
	 * @param array $pipes
	 * @param mixed $payload
	 *
	 * @return mixed
	 */
	public function process(array $pipes, $payload) {
		foreach ($pipes as $pipe) {
			$payload = call_user_func($pipe, $payload);
			if (true !== call_user_func($this->check, $payload)) {
				return $payload;
			}
		}
		return $payload;
	}
}