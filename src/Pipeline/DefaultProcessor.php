<?php


namespace Engine\Pipeline;


use Engine\Pipeline\Interfaces\ProcessorInterface;

class DefaultProcessor implements ProcessorInterface {

	/**
	 * Process pipes
	 *
	 * @param  array $pipes
	 * @param  mixed $payload
	 * @return mixed
	 */
	public function process(array $pipes, $payload) {
		foreach ($pipes as $pipe) {
			$payload = call_user_func($pipe, $payload);
		}
		return $payload;
	}
}