<?php


namespace Engine\JsonRpc2\Client;

use Engine\Helpers\ArrayHelper;

/**
 * Class Response
 * @package Engine\JsonRpc2\Client
 */
class Response {

	/**
	 * @var string
	 */
	public $result;

	/**
	 * @var null|string
	 */
	public $id;

	/**
	 * @var null|int
	 */
	public $errorCode;

	/**
	 * @var null|string
	 */
	public $errorMessage;

	/**
	 * @var array
	 */
	public $errorData;

	/**
	 * Response constructor.
	 * Create a new json rpc response object
	 *
	 * @param string|array $result
	 * @param null|string $id
	 * @param null|string $errorCode
	 * @param null|string $errorMessage
	 * @param mixed $errorData
	 */
	public function __construct($result, $id = null, $errorCode = null, $errorMessage = null, $errorData = null) {
		$this->result = (is_scalar($result) || null === $result) ? $result : $this->toArray($result);
		$this->id = $id;
		$this->errorCode = $errorCode;
		$this->errorMessage = $errorMessage;
		$this->errorData = $this->toArray($errorData);
	}

	/**
	 * Return result or error if applicable
	 *
	 * @return string
	 */
	public function __toString() {
		if ($this->errorMessage) {
			return "{$this->errorCode}: {$this->errorMessage}";
		}

		return $this->result;
	}

	/**
	 * Converts an object or an array of objects into an array.
	 * Кастомная версия ArrayHelper::toArray()
	 * отличается тем, что НЕ выпиливает из объектов null, при конвертации в массив
	 *
	 * @param $object
	 * @param array $properties
	 * @param bool $recursive
	 * @return array
	 */
	protected function toArray($object, $properties = [], $recursive = true) {
		if (is_array($object)) {
			if ($recursive) {
				foreach ($object as $key => $value) {
					if (is_array($value) || is_object($value)) {
						$object[$key] = $this->toArray($value, $properties, true);
					}
				}
			}
			return $object;
		}
		elseif (is_object($object)) {
			if (!empty($properties)) {
				$className = get_class($object);
				if (!empty($properties[$className])) {
					$result = [];
					foreach ($properties[$className] as $key => $name) {
						if (is_int($key)) {
							$result[$name] = $object->$name;
						}
						else {
							$result[$key] = ArrayHelper::getValue($object, $name);
						}
					}
					return $recursive ? $this->toArray($result, $properties) : $result;
				}
			}
			$result = [];
			foreach ($object as $key => $value) {
				$result[$key] = $value;
			}
			return $recursive ? $this->toArray($result, $properties) : $result;
		}
		else {
			return [$object];
		}
	}
}