<?php


namespace Engine\JsonRpc2\Client;

use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonRpc2\Exceptions\JsonRpcException;
use Engine\JsonRpc2\Exceptions\JsonRpcResponseException;
use Engine\JsonRpc2\Transports\TransportInterface;

class Client {

	/**
	 * @var TransportInterface
	 */
	protected $transport;

	/**
	 *  Addition metadata params
	 *
	 */
	protected $metadata = [];

	/**
	 * Client constructor.
	 * Create new client connection
	 *
	 * @param TransportInterface $transport
	 * @param $metadata array|false
	 */
	public function __construct(TransportInterface $transport, array $metadata = []) {
		$this->transport = $transport;
		$this->metadata = $metadata;
	}

	/**
	 * Set transport destination point
	 *
	 * @param string $destination
	 */
	public function setTransportDestination($destination) {
		if (isset($this->transport)) {
			$this->transport->setDestination($destination);
		}
	}

	/**
	 * Shortcut to call a single, non-notification request
	 *
	 * @param string $method
	 * @param array $params
	 * @return string
	 */
	public function __call($method, array $params) {
		$params = $params[0];

		if (isset($params['__METADATA']))
			throw new JsonApiException('Cannot use __METADATA as param name');

		$request = new Request($method, $params);
		$request->setMetadata($this->metadata);
		return $this->sendRequest($request);
	}

	/**
	 * Send a single request object
	 *
	 * @param Request $request
	 * @return string
	 * @throws \Exception
	 */
	public function sendRequest(Request $request) {
		$response = $this->send($request->getJSON());

		if (isset($response->errorCode)) {
			throw new JsonRpcResponseException($response->errorMessage,
				$response->errorCode, $response->errorData);
		}

		if ($response->id != $request->id) {
			throw new JsonRpcResponseException("Mismatched request id");
		}

		return $response->result;
	}

	/**
	 * Send a single notify request object
	 *
	 * @param Request $request
	 * @return bool
	 * @throws \Exception
	 */
	public function sendNotify($request) {
		if (property_exists($request, 'id') && $request->id != null) {
			throw new JsonRpcException("Notify requests must not have ID set");
		}
		$this->send($request->getJSON(), true);
		return true;
	}

	/**
	 * Send an array of request objects as a batch
	 *
	 * @param Request[] $requests
	 * @return array|bool
	 * @throws \Exception
	 */
	public function sendBatch($requests) {
		$arr = array();
		$ids = array();
		$allNotify = true;

		foreach ($requests as $request) {
			if ($request->id) {
				$allNotify = false;
				$ids[] = $request->id;
			}
			$arr[] = $request->getArray();
		}

		$response = $this->send(json_encode($arr), $allNotify);

		// no response if batch is all notifications
		if ($allNotify) {
			return true;
		}

		// check for missing ids and return responses in order of requests
		$orderedResponse = array();
		foreach ($ids as $id) {
			if (array_key_exists($id, $response)) {
				$orderedResponse[] = $response[$id];
				unset($response[$id]);
			}
			else {
				throw new JsonRpcException("Missing id in response");
			}
		}

		// check for extra ids in response
		if (count($response) > 0) {
			throw new JsonRpcException("Extra id(s) in response");
		}

		return $orderedResponse;
	}

	/**
	 * Send raw json to the server
	 *
	 * @param string $json
	 * @param bool $notify
	 * @return array|bool|Response
	 * @throws \Exception
	 */
	public function send($json, $notify = false) {
		// use http authentication header if set
		$response = $this->transport->send($json);

		// notify has no response
		if ($notify) {
			return true;
		}

		if (strpos($response, 'X-Powered-By') === 0) {
			$response = explode("\n", $response);
			$response = $response[3];
		}

		// try to decode json
		$jsonResponse = $this->decodeJSON($response);

		// handle response, create response object and return it
		return $this->handleResponse($jsonResponse);
	}

	/**
	 * Decode json throwing exception if unable
	 *
	 * @param string $json
	 * @return object
	 * @throws JsonRpcException
	 */
	protected function decodeJSON($json) {
		$jsonResponse = json_decode($json);

		if ($jsonResponse === null) {
			throw new JsonRpcException("Unable to decode JSON response from: {$json}");
		}
		return $jsonResponse;
	}

	/**
	 * Handle the response and return a result or an error
	 *
	 * @param object|array $response
	 * @return Response[]|Response
	 */
	public function handleResponse($response) {
		// recursion for batch
		if (is_array($response)) {
			$responseArray = array();
			foreach ($response as $res) {
				$responseArray[$res->id] = $this->handleResponse($res);
			}
			return $responseArray;
		}

		// return error response
		if (property_exists($response, 'error')) {
			$errorData = null;
			if (isset($response->error->data)) {
				$errorData = $response->error->data;
			}
			return new Response(null, $response->id, $response->error->code, $response->error->message, $errorData);
		}

		// return successful response
		return new Response($response->result, $response->id);
	}
}