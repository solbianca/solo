<?php


namespace Engine\JsonRpc2\Client;


use Engine\Core\Exceptions\InvalidConfigException;
use Engine\Helpers\ArrayHelper;
use Engine\JsonRpc2\Transports\PhpFpmTransport;
use Engine\JsonRpc2\Transports\SocketTransport;

/**
 * Class ClientFactory
 * @package Engine\JsonRpc2\Client
 */
class ClientFactory {

	/**
	 * @param array $configs Config to create clients for service
	 * @param array $metadata Metadata that will passed with request
	 * @return Client[]
	 * @throws InvalidConfigException
	 */
	public function createClients(array $configs, array $metadata = []): array {
		if (empty($configs)) {
			throw new InvalidConfigException("Config can't be empty");
		}

		$clients = [];

		foreach ($configs as $config) {
			$clients[] = $this->createClient($config, $metadata);
		}

		return $clients;
	}

	/**
	 * @param array $config Config to create client for service
	 * @param array $metadata Metadata that will passed with request
	 * @return Client
	 * @throws InvalidConfigException
	 */
	public function createClient(array $config, array $metadata): Client {
		if (!isset($config['transport'])) {
			throw new InvalidConfigException("Service configuration must contain 'transport' parameter.");
		}
		switch ($config['transport']) {
			case 'php-fpm':
				return $this->createPhpFpmClient($config, $metadata);
				break;
			case 'socket':
				return $this->createSocketClient($config, $metadata);
				break;
			default:
				throw new InvalidConfigException("Bad service configuration: invalid transport parameter.");
		}
	}

	/**
	 * @param array $config Config to create client for service
	 * @param array $metadata Metadata that will passed with request
	 * @return Client
	 * @throws InvalidConfigException
	 */
	protected function createPhpFpmClient(array $config, array $metadata): Client {
		if (!ArrayHelper::keysExist(['host', 'port', 'file'], $config)) {
			throw new InvalidConfigException("Service config must contain parameters: host, port, destination.");
		}
		$params = (isset($config['params'])) ? $config['params'] : [];
		return new Client(new PhpFpmTransport($config['host'], $config['port'], $config['file'], $params), $metadata);
	}

	/**
	 * @param array $config
	 * @param array $metadata
	 * @return Client
	 * @throws InvalidConfigException
	 */
	protected function createSocketClient(array $config, array $metadata): Client {
		if (!ArrayHelper::keysExist(['host', 'port'], $config)) {
			throw new InvalidConfigException("Service config must contain parameters: host, port");
		}
		$params = (isset($config['params'])) ? $config['params'] : [];
		return new Client(new SocketTransport($config['host'], $config['port'], $params), $metadata);
	}
}