<?php


namespace Engine\JsonRpc2\Client;


use Engine\JsonRpc2\Exceptions\JsonRpcException;

/**
 * Class Request
 * @package Engine\JsonRpc2\Client
 */
class Request {

	const JSON_RPC_VERSION = "2.0";

	/**
	 * @var string
	 */
	public $method;

	/**
	 * @var array|null
	 */
	public $params;

	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var array
	 */
	public $metadata;

	/**
	 * Request constructor.
	 * Create a new json rpc request object
	 *
	 * @param string $method
	 * @param null|array $params
	 * @param bool $notify
	 * @throws \Exception
	 */
	public function __construct($method, $params = null, $notify = false) {
		// ensure params are array
		if ($params !== null && !is_array($params)) {
			$params = array($params);
		}

		// check for illegal method prefix
		if (substr($method, 0, 4) == 'rpc.') {
			throw new JsonRpcException("Illegal method name; Method cannot start with 'rpc.'");
		}

		// ensure params are utf8

		/*
		if ($params !== null) {
			foreach ($params as &$param) {
				if (is_string($param)) {
					$param = utf8_encode($param);
				}
			}
		}
		*/

		$this->method = $method;
		$this->params = $params;

		// do not create id for notify
		if ($notify == false) {
			$this->id = uniqid();
		}
	}

	public function setMetadata (array $metadata = []) {
   	    $this->metadata = $metadata;
	}

	/**
	 * Return an associated array for this object
	 *
	 * @return array
	 */
	public function getArray() {

		$arr = array('jsonrpc' => self::JSON_RPC_VERSION, 'method' => $this->method);

		if ($this->params) {
			$arr['params'] = $this->params;
		}

		if ($this->metadata) {
			if (!isset($arr['params']))
				$arr['params'] = [];

			$arr['params']['__METADATA'] = $this->metadata;
		}

		if ($this->id !== null) {
			$arr['id'] = $this->id;
		}

		return $arr;
	}

	/**
	 * Return the json for this object
	 *
	 * @return string
	 */
	public function getJSON() {
		return json_encode($this->getArray());
	}
}