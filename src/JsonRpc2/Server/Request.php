<?php


namespace Engine\JsonRpc2\Server;


/**
 * Class Request
 * @package Engine\JsonRpc2\Server
 */
class Request {

	const JSON_RPC_VERSION = "2.0";
	const ERROR_PARSE_ERROR = -32700;
	const ERROR_INVALID_REQUEST = -32600;
	const ERROR_MISMATCHED_VERSION = -32000;
	const ERROR_RESERVED_PREFIX = -32001;
	const VALID_FUNCTION_NAME = '/^[a-zA-Z_.][a-zA-Z0-9_.]*$/';

	/**
	 * @var bool
	 */
	public $batch;

	/**
	 * @var string
	 */
	public $raw;

	/**
	 * @var mixed
	 */
	public $result;

	/**
	 * @var string
	 */
	public $json_rpc;

	/**
	 * @var string|int
	 */
	public $error_code;

	/**
	 * @var string
	 */
	public $error_message;

	/**
	 * @var string
	 */
	public $method;

	/**
	 * @var mixed
	 */
	public $params;

	/**
	 * @var array
	 */
	public $metadata;

	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var Request[]
	 */
	public $requests;

	/**
	 * Request constructor.
	 * Create new server request object from raw json
	 *
	 * @param string $json
	 */
	public function __construct($json) {
		$this->batch = false;
		$this->raw = $json;

		// handle empty request
		if ($this->raw === "") {
			$this->serErrorState(self::ERROR_INVALID_REQUEST, "Invalid Request.");
			return;
		}

		// parse json into array
		$obj = json_decode($json, true);

		// handle json parse error
		if ($obj === null) {
			$this->serErrorState(self::ERROR_PARSE_ERROR, "Parse error.");
			return;
		}

		// Если в массиве присутствует параметр jsonrpc - значит у нас простой запрос
		// Иначе Batch
		if (isset($obj['jsonrpc'])) {
			$this->handleSingleRequest($obj);
		}
		else {
			$this->handleBatchRequest($obj);
		}
	}

	protected function handleBatchRequest($obj) {
		// empty batch
		if (count($obj) == 0) {
			$this->serErrorState(self::ERROR_INVALID_REQUEST, "Invalid Request.");
			return;
		}
		// non-empty batch
		$this->batch = true;
		$this->requests = array();
		foreach ($obj as $req) {
			// recursion for bad requests
			// --?? Если bad requests должно возвращаться
			// "error": {"code": -32600, "message": "Invalid Request"}
			// Вообще, запись гениальная конечно...

			$this->requests[] = new Request(json_encode($req));
		}
	}

	protected function handleSingleRequest($obj) {
		$this->json_rpc = $obj['jsonrpc'];
		$this->method = $obj['method'];

		if (isset($obj['params'])) {
			$this->params = $obj['params'];

			// Проверка - если последний элемент в массиве - это метадата
			// устанавливаем метадату в переменную и удаляем из параметров
			if (sizeof($this->params) > 0 && array_reverse(array_keys($this->params))[0] === '__METADATA') {
				$this->metadata = $this->params['__METADATA'];
				unset($this->params['__METADATA']);
			}
			else $this->metadata = [];

		}
		if (isset($obj['id'])) {
			$this->id = $obj['id'];
		}
	}

	/**
	 * Set error state for request
	 *
	 * @param $message
	 */
	protected function serErrorState($code, $message) {
		$this->json_rpc = self::JSON_RPC_VERSION;
		$this->error_code = $code;
		$this->error_message = $message;
	}

	/**
	 * Returns true if request is valid or returns false assigns error
	 *
	 * @return bool
	 */
	public function checkValid() {
		// error code/message already set
		if ($this->error_code && $this->error_message) {
			return false;
		}
		// missing jsonrpc or method
		if (!$this->json_rpc || !$this->method) {
			$this->error_code = self::ERROR_INVALID_REQUEST;
			$this->error_message = "Invalid Request.";
			return false;
		}
		// reserved method prefix
		if (substr($this->method, 0, 4) == 'rpc.') {
			$this->error_code = self::ERROR_RESERVED_PREFIX;
			$this->error_message = "Illegal method name; Method cannot start with 'rpc.'";
			return false;
		}
		// illegal method name
		if (!preg_match(self::VALID_FUNCTION_NAME, $this->method)) {
			$this->error_code = self::ERROR_INVALID_REQUEST;
			$this->error_message = "Invalid Request.";
			return false;
		}
		// mismatched json-rpc version
		if ($this->json_rpc != "2.0") {
			$this->error_code = self::ERROR_MISMATCHED_VERSION;
			$this->error_message = "Client/Server JSON-RPC version mismatch; Expected '2.0'";
			return false;
		}
		// valid request
		return true;
	}

	/**
	 * Returns true if request is a batch
	 *
	 * @return bool
	 */
	public function isBatch() {
		return $this->batch;
	}

	/**
	 * Returns true if request is a notification
	 *
	 * @return bool
	 */
	public function isNotify() {
		return !isset($this->id);
	}

	/**
	 * Return raw JSON response
	 *
	 * @return string
	 */
	public function toResponseJSON() {
		// successful response
		$arr = array('jsonrpc' => self::JSON_RPC_VERSION);
		if ($this->result !== null) {
			$arr['result'] = $this->result;
			$arr['id'] = $this->id;
			return json_encode($arr);

		} // error response
		else {
			$arr['error'] = array('code' => $this->error_code, 'message' => $this->error_message);
			$arr['id'] = $this->id;
			return json_encode($arr);
		}
	}

	/**
	 * Recursively decode utf8
	 *
	 * @param string|array $result
	 * @return string
	 */
	private function recursiveUTF8Decode($result) {
		if (is_array($result)) {
			foreach ($result as &$value) {
				$value = $this->recursiveUTF8Decode($value);
			}
			return $result;
		}
		else {
			return utf8_decode($result);
		}
	}
}