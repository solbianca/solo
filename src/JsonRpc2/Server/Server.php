<?php


namespace Engine\JsonRpc2\Server;


use Engine\Base\Controller;
use Engine\Core\Exceptions\ApplicationException;
use Engine\JsonRpc2\Exceptions\JsonRpcException;

class Server {

	const ERROR_INVALID_REQUEST = -32600;
	const ERROR_METHOD_NOT_FOUND = -32601;
	const ERROR_INVALID_PARAMS = -32602;
	const ERROR_EXCEPTION = -32099;

	/**
	 * @var Controller
	 */
	public $exposedInstance;

	/**
	 * @var string
	 */
	public $input;

	/**
	 * Server constructor.
	 * Create new server
	 *
	 * @param $exposedInstance
	 * @throws \Exception
	 */
	public function __construct($exposedInstance = null) {
		if (null !== $exposedInstance && !is_object($exposedInstance)) {
			throw new JsonRpcException("Server requires an object");
		}
		$this->exposedInstance = $exposedInstance;
		$this->input = 'php://input';
	}

	/**
	 * @param object $exposedInstance
	 * @throws JsonRpcException
	 */
	public function setExposedInstance($exposedInstance) {
		if (!is_object($exposedInstance)) {
			throw new JsonRpcException("Server requires an object");
		}

		if (!($exposedInstance instanceof Controller)) {
			throw new ApplicationException("Controller must be instance of '\Engine\Base\Controller'");
		}

		$this->exposedInstance = $exposedInstance;
	}

	/**
	 * Check for method existence
	 *
	 * @param $methodName
	 * @return bool
	 */
	public function methodExists($methodName) {
		return method_exists($this->exposedInstance, $methodName);
	}

	/**
	 * Attempt to invoke the method with params
	 *
	 * @param string $method
	 * @param array|object $params
	 * @return mixed
	 * @throws \Exception
	 */
	public function invokeMethod($method, $params, $metadata = []) {
		$params = $this->prepareParams($params);
		$result = $this->exposedInstance
            ->setParams($params)
            ->setMetadata($metadata)
            ->$method($params);
		return $result;
	}

	/**
	 * For named parameters, convert from object to assoc array
	 * For no params, pass in empty array
	 *
	 * @param mixed $params
	 * @return array
	 */
	public function prepareParams($params) {
		if (is_object($params)) {
			// простое преобразование объекта в массив. Работает более быстро, чем проходить объект циклом
			$params = json_decode(json_encode($params), 1);
		}

		if ($params === null) {
			$params = [];
		}

		return $params;
	}

	/**
	 * Process json-rpc request
	 *
	 * @param  string $method
	 * @throws JsonRpcException
	 */
	public function process($method) {
		$json = $this->readInput();

		// handle communication errors
		if ($json === false) {
			throw new JsonRpcException("Server unable to read request body.");
		}

		// create request object
		$request = $this->makeRequest($json);

		header('Content-type: application/json');

		// handle json parse error and empty batch
		if ($request->error_code && $request->error_message) {
			echo $request->toResponseJSON();
			return;
		}

		// respond with json
		echo $this->handleRequest($method, $request);
	}

	/**
	 * Read input data
	 *
	 * @return string
	 * @throws JsonRpcException
	 */
	protected function readInput() {
		try {
			$json = file_get_contents($this->input);
		} catch (\Exception $e) {
			$message = "Server unable to read request body.";
			$message .= PHP_EOL . $e->getMessage();
			throw new JsonRpcException($message);
		}
		return $json;
	}

	/**
	 * Create new request (used for test mocking purposes)
	 *
	 * @param $json
	 * @return Request
	 */
	public function makeRequest($json) {
		return new Request($json);
	}

	/**
	 * Handle request object / return response json
	 *
	 * @param string $method
	 * @param Request $request
	 * @return null|string
	 */
	public function handleRequest($method, Request $request) {
		// recursion for batch
		if ($request->isBatch()) {
			return $this->handleBatchRequest($method, $request);
		}

		return $this->handleSingleRequest($method, $request);
	}

	/**
	 * Handle single request
	 *
	 * @param string $method
	 * @param Request $request
	 * @return null|string
	 */
	public function handleSingleRequest($method, Request $request) {
		if (!$request->checkValid()) {
			return $request->toResponseJSON();
		}

		if (!$this->methodExists($method)) {
			$request->error_code = self::ERROR_METHOD_NOT_FOUND;
			$request->error_message = "Method not found.";
			return $request->toResponseJSON();
		}

		try {
			$response = $this->invokeMethod($method, $request->params, $request->metadata);
			if (!$request->isNotify()) {
				$request->result = $response;
			}
			else {
				return null;
			}
		} catch (JsonRpcException $e) {
			$request->error_code = self::ERROR_EXCEPTION;
			$request->error_message = $e->getMessage();
		}

		return $request->toResponseJSON();
	}

	/**
	 * Handle recursion batch request
	 *
	 * @param string $method
	 * @param Request $request
	 * @return null|string
	 */
	public function handleBatchRequest($method, Request $request) {
		$batch = array();

		foreach ($request->requests as $req) {
			$batch[] = $this->handleRequest($method, $req);
		}

		$responses = implode(',', array_filter($batch, function ($a) {
			return $a !== null;
		}));

		if ($responses != null) {
			return "[{$responses}]";
		}

		return null;
	}
}