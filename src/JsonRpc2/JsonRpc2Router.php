<?php


namespace Engine\JsonRpc2;


use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Interfaces\RequestInterface;
use Engine\Core\Interfaces\ResponseInterface;
use Engine\Core\Interfaces\RouterInterface;
use Engine\Http\Response;
use Engine\Http\ServerRequest;

/**
 * Class JsonRpc2Router
 * @package Engine\JsonRpc2
 *
 * Usage. Set in config:
 * return [
 *        'urls' => [
 *              'login' => 'Site::login',
 *              'user.add' => 'Site\User::create',
 *        ],
 * ];
 */
class JsonRpc2Router implements RouterInterface {

	/**
	 * @var ServerRequest
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * JsonRpc2Router constructor.
	 *
	 * @param RequestInterface|ServerRequest $request
	 * @param ResponseInterface|Response $response
	 */
	public function __construct(RequestInterface $request, ResponseInterface $response) {
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * @inheritdoc
	 */
	public function resolve(array $urls) {
		$requestBody = $this->readRequestBody();
		$method = (isset($requestBody['method'])) ? $requestBody['method'] : null;
		
		if (!isset($urls[$method])) {
			throw new ApplicationException("URL not found.");
		}

		$controllerPattern = explode('::', $urls[$method]);
		if (count($controllerPattern) !== 2) {
			throw new ApplicationException('Bad url class pattern for url.');
		}

		if (empty($controllerPattern[0]) || empty($controllerPattern[1])) {
			throw new ApplicationException('Bad url class pattern for url.');
		}

		$class = 'App\Controllers\\' . ucfirst($controllerPattern[0]) . 'Controller';
		$action = $controllerPattern[1] . 'Action';

		return ['controller' => $class, 'action' => $action, 'params' => []];
	}

	/**
	 * @return array
	 */
	protected function readRequestBody() {
		return json_decode((string)$this->request->getBody(), true);
	}
}