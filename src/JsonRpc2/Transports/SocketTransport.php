<?php

namespace Engine\JsonRpc2\Transports;


use Adoy\FastCGI\Client;
use Engine\Helpers\ArrayHelper;

class SocketTransport extends AbstractTransport {

	/**
	 * @inheritdoc
	 */
	protected $destination;

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * @var string
	 */
	protected $host;

	/**
	 * @var int
	 */
	protected $port;

	/**
	 * @var array
	 */
	public $defaultParams = [
		'DOMAIN' => AF_INET, //IPv4 Internet based protocols. TCP and UDP are common protocols of this protocol family.
		'SOCKET_TYPE' => SOCK_STREAM, //The TCP protocol is based on this socket type.
		'SOCKET_PROTOCOL' => SOL_TCP,
		'TIMEOUT' => 0, //seconds for socket connection timeout
	];


	/**
	 * @var array
	 */
	protected $params = [];


	/**
	 * SocketTransport constructor.
	 * @param string $host
	 * @param int $port
	 * @param array $params
	 */
	public function __construct(string $host, int $port, array $params = []) {
		$this->host = $host;
		$this->port = $port;
		$this->params = $this->defaultParams;
		if (!empty($params)) {
			$this->params = ArrayHelper::merge($this->params, $params);
		}
	}

	/**
	 * @return array
	 */
	public function getParams() {
		return $this->params;
	}

	/**
	 * @param array $params
	 * @return $this
	 */
	public function setParams(array $params) {
		$this->params = $params;
		return $this;
	}

	/**
	 * @param string $name
	 * @param string $value
	 * @return $this
	 */
	public function setParam($name, $value) {
		$this->params[$name] = $value;
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getParam($name) {
		return $this->params[$name];
	}

	/**
	 * @inheritdoc
	 */
	public function send($json) {
		// Создаём сокет
		$s = socket_create($this->params['DOMAIN'], $this->params['SOCKET_TYPE'], $this->params['SOCKET_PROTOCOL']);
		if(false === $s) {
			throw new \Exception('Socket create error. Err: ' . socket_strerror(socket_last_error()));
		}
		// Соединяемся (с нашим микросервисом)
		socket_connect($s, $this->host, $this->port);

		// Отправляем данные в сокет
		socket_write($s, $json);
		// Закрываем сокет на запись
		// !!! ВАЖНО !!! Сервис концом входных данных считает именно этот сигнал!
		socket_shutdown($s, 1);
		// Читаем ответ от сервера
		// К сожалению делать это можно только кусками
		$answer = "";
		$ts = time();
		while (0 === $this->params['TIMEOUT'] || time() < ($ts +  $this->params['TIMEOUT'])) {
			$buf = socket_read($s, 2048);
			$answer .= $buf;
			if (empty($buf)) {
				break;
			}
		}
		// Закрываем сокет
		socket_close($s);

		// Делимся тем что получили:)
		return $answer;
	}
}