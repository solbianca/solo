<?php

namespace Engine\JsonRpc2\Transports;


use Adoy\FastCGI\Client;
use Engine\Helpers\ArrayHelper;

class PhpFpmTransport extends AbstractTransport {

	/**
	 * @inheritdoc
	 */
	protected $destination;

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * @var string
	 */
	protected $host;

	/**
	 * @var string
	 */
	protected $port;

	/**
	 * @var array
	 */
	public $defaultParams = [
		'GATEWAY_INTERFACE' => 'FastCGI/1.0',
		'REQUEST_METHOD' => 'POST',
		'SCRIPT_NAME' => '/index.php',
		'SERVER_SOFTWARE' => 'php/fcgiclient',
		'SERVER_PROTOCOL' => 'HTTP/1.1',
		'CONTENT_TYPE' => 'Content-Type: application/json',
	];

	/**
	 * @var array
	 */
	protected $params = [];

	/**
	 * PhpFpmTransport constructor.
	 *
	 * @param array $params
	 */
	public function __construct($host, $port, $destination, array $params = []) {
		$this->host = $host;
		$this->port = $port;
		$this->client = new Client($host, $port);
		$this->destination = $destination;
		$this->params = $this->defaultParams;
		if (!empty($params)) {
			$this->params = ArrayHelper::merge($this->params, $params);
		}
	}

	/**
	 * @return array
	 */
	public function getParams() {
		return $this->params;
	}

	/**
	 * @param array $params
	 * @return $this
	 */
	public function setParams(array $params) {
		$this->params = $params;
		return $this;
	}

	/**
	 * @param string $name
	 * @param string $value
	 * @return $this
	 */
	public function setParam($name, $value) {
		$this->params[$name] = $value;
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getParam($name) {
		return $this->params[$name];
	}

	/**
	 * @inheritdoc
	 */
	public function send($json) {
		$this->params['SCRIPT_FILENAME'] = $this->destination;
		if (empty($this->params['SCRIPT_FILENAME'])) {
			throw new \Exception("Missing 'SCRIPT_FILENAME' parameter.");
		}
		$content = $json;
		$this->params['CONTENT_LENGTH'] = strlen($content);
		return $this->client->request($this->params, $content);
	}
}	