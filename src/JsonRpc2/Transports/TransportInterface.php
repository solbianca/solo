<?php


namespace Engine\JsonRpc2\Transports;


interface TransportInterface {

	/**
	 * Send request
	 *
	 * @json string $json Data to send
	 * @return string
	 */
	public function send($json);

	/**
	 * Set point of destination
	 *
	 * @param string $destination
	 */
	public function setDestination($destination);

	/**
	 * Get point of destination
	 *
	 * @return string
	 */
	public function getDestination();
}