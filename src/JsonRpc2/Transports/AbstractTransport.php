<?php


namespace Engine\JsonRpc2\Transports;


abstract class AbstractTransport implements TransportInterface {

	/**
	 * Service that we want reach
	 *
	 * @var string
	 */
	protected $destination;

	/**
	 * @inheritdoc
	 */
	abstract public function send($json);

	/**
	 * @inheritdoc
	 */
	public function setDestination($destination) {
		$this->destination = $destination;
	}

	/**
	 * @inheritdoc
	 */
	public function getDestination() {
		return $this->destination;
	}
}