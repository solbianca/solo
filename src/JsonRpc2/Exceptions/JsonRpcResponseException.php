<?php


namespace Engine\JsonRpc2\Exceptions;


class JsonRpcResponseException extends JsonRpcException {
	/**
	 * @var mixed
	 */
	protected $errorData;

	/**
	 * JsonRpcResponseException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param null $errorData
	 * @param \Exception|null $previous
	 */
	public function __construct($message = "", $code = 0, $errorData = null, \Exception $previous = null) {
		parent::__construct($message, $code, $previous);
		$this->errorData = $errorData;
	}

	/**
	 * @return mixed|null
	 */
	public function getErrorData() {
		return $this->errorData;
	}
}