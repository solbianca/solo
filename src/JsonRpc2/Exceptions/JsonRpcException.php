<?php

namespace Engine\JsonRpc2\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class JsonRpcException extends ApplicationException {

	/**
	 * @var string
	 */
	protected $json;

	/**
	 * JsonRpcException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param string $json
	 * @param Exception|null $previous
	 */
	public function __construct($message = "", $code = 0, $json = '', \Exception $previous = null) {
		parent::__construct($message, $code, $previous);
		$this->json = $json;
	}

	/**
	 * @return string
	 */
	public function getJson() {
		return $this->json;
	}
}