<?php


namespace Engine\Log;


class FileLogger extends AbstractLogger {

	/**
	 * @var string
	 */
	private $filePath;

	/**
	 * FileLogger constructor.
	 *
	 * @param null|string $filePath
	 */
	public function __construct($filePath = null) {
		$this->setFilePath($filePath);
	}

	/**
	 * @return string
	 */
	public function getFilePath() {
		return $this->filePath;
	}

	/**
	 * @param null|string $filePath
	 * @return bool
	 */
	public function setFilePath($filePath) {
		if (!is_string($filePath)) {
			return false;
		}

		if (!file_exists(dirname($filePath)) || (!is_writable($filePath) && !is_writable(dirname($filePath)))) {
			return false;
		}

		$this->filePath = $filePath;

		return true;
	}

	public function log($level, $message, array $context = array()) {
		if (!in_array($level, array(
			LogLevel::EMERGENCY,
			LogLevel::ALERT,
			LogLevel::CRITICAL,
			LogLevel::ERROR,
			LogLevel::WARNING,
			LogLevel::NOTICE,
			LogLevel::INFO,
			LogLevel::DEBUG,
		))
		) {
			throw new \InvalidArgumentException('Invalid or unsupported log level ' . $level);
		}

		if (is_null($this->getFilePath())) {
			return false;
		}

		$buf = '';
		$buf .= date('r');
		$buf .= ' - ';
		if (isset($_SERVER) && is_array($_SERVER) && array_key_exists('REMOTE_ADDR', $_SERVER)) {
			$buf .= $_SERVER['REMOTE_ADDR'];
		}
		else {
			$buf .= '~';
		}
		$buf .= ' - ' . php_uname('n');
		$buf .= ' - ' . strtoupper($level);
		$buf .= ' - ' . $this->interpolate((string)$message, $context);
		file_put_contents($this->getFilePath(), $buf . PHP_EOL, FILE_APPEND);
		return true;
	}

	private function interpolate($message, array $context = array()) {
		$replace = array();
		foreach ($context as $k => $v) {
			$replace['{' . $k . '}'] = $v;
		}
		return strtr((string)$message, $replace);
	}

}