<?php


namespace Engine\Log;


class LoggerAggregator implements LoggerInterface {

	/**
	 * @var null|LoggerInterface[]
	 */
	protected $loggers;

	/**
	 * Add logger
	 *
	 * @param LoggerInterface $logger
	 */
	public function add(LoggerInterface $logger) {
		$this->loggers[get_class($logger)] = $logger;
	}

	/**
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 */
	public function log($level, $message, array $context = array()) {
		if (null === $this->loggers) {
			return;
		}

		foreach ($this->loggers as $logger) {
			$logger->log($level, $message, $context);
		}
	}
}