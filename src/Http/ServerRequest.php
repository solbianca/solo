<?php

declare(strict_types = 1);

namespace Engine\Http;

use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Http\Interfaces\StreamInterface;
use Engine\Http\Interfaces\UriInterface;

/**
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class ServerRequest extends Request implements ServerRequestInterface {
	/**
	 * @var array
	 */
	private $attributes = [];

	/**
	 * @var array
	 */
	private $cookieParams = [];

	/**
	 * @var null|array|object
	 */
	private $parsedBody;

	/**
	 * @var array
	 */
	private $queryParams = [];

	/**
	 * @var array
	 */
	private $serverParams;

	/**
	 * @var array
	 */
	private $uploadedFiles = [];

	/**
	 * @param string $method HTTP method
	 * @param string|UriInterface $uri URI
	 * @param array $headers Request headers
	 * @param string|null|resource|StreamInterface $body Request body
	 * @param string $version Protocol version
	 * @param array $serverParams Typically the $_SERVER superglobal
	 */
	public function __construct(
		$method,
		$uri,
		array $headers = [],
		$body = null,
		$version = '1.1',
		array $serverParams = []
	) {
		$this->serverParams = $serverParams;

		parent::__construct($method, $uri, $headers, $body, $version);
	}

	public function getServerParams() {
		return $this->serverParams;
	}

	/**
	 * @return array
	 */
	public function getUploadedFiles() {
		return $this->uploadedFiles;
	}

	/**
	 * @param array $uploadedFiles
	 * @return ServerRequest
	 */
	public function withUploadedFiles(array $uploadedFiles) {
		$new = clone $this;
		$new->uploadedFiles = $uploadedFiles;

		return $new;
	}

	/**
	 * @return array
	 */
	public function getCookieParams() {
		return $this->cookieParams;
	}

	public function withCookieParams(array $cookies) {
		$new = clone $this;
		$new->cookieParams = $cookies;

		return $new;
	}

	/**
	 * @return array
	 */
	public function getQueryParams() {
		return $this->queryParams;
	}

	/**
	 * @param array $query
	 * @return ServerRequest
	 */
	public function withQueryParams(array $query) {
		$new = clone $this;
		$new->queryParams = $query;

		return $new;
	}

	/**
	 * @return array|null|object
	 */
	public function getParsedBody() {
		return $this->parsedBody;
	}

	/**
	 * @param array|null|object $data
	 * @return ServerRequest
	 */
	public function withParsedBody($data) {
		$new = clone $this;
		$new->parsedBody = $data;

		return $new;
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->attributes;
	}

	/**
	 * @param string $attribute
	 * @param null $default
	 * @return mixed|null
	 */
	public function getAttribute($attribute, $default = null) {
		if (false === array_key_exists($attribute, $this->attributes)) {
			return $default;
		}

		return $this->attributes[$attribute];
	}

	/**
	 * @param string $attribute
	 * @param mixed $value
	 * @return ServerRequest
	 */
	public function withAttribute($attribute, $value) {
		$new = clone $this;
		$new->attributes[$attribute] = $value;

		return $new;
	}

	/**
	 * @param string $attribute
	 * @return $this|ServerRequest
	 */
	public function withoutAttribute($attribute) {
		if (false === array_key_exists($attribute, $this->attributes)) {
			return $this;
		}

		$new = clone $this;
		unset($new->attributes[$attribute]);

		return $new;
	}
}
