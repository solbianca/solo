<?php

namespace Engine\Http;

use Engine\Core\Exceptions\ApplicationException;
use Engine\Core\Interfaces\RequestInterface;
use Engine\Core\Interfaces\ResponseInterface;
use Engine\Core\Interfaces\RouterInterface;

/**
 * Provides an easy way to map URLs to classes. URLs can be literal
 * strings or regular expressions.
 *
 * When the URLs are processed:
 *      * delimiter (/) are automatically escaped: (\/)
 *      * The beginning and end are anchored (^ $)
 *      * An optional end slash is added (/?)
 *          * The i option is added for case-insensitive searches
 *
 * Example:
 *
 * $urls = [
 *     '/' => 'site::index',
 *     '/page/:int' => 'site::view',
 *     '/article/:string => 'site::article'
 *     '/catalog/:any => 'site::catalog'
 * )];
 *
 * class SiteController {
 *      public function indexAction() {}
 *
 *      public function viewAction($id) {}
 * }
 *
 */
class HttpRouter implements RouterInterface {

	/**
	 * @var ServerRequest;
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * @var string
	 */
	protected $clientId;

	/**
	 * @var string
	 */
	protected $defaultController = 'site';

	/**
	 * @var string
	 */
	protected $defaultAction = 'index';

	/**
	 * @var string
	 */
	protected $namespace = 'App\Controllers\\';

	/**
	 * @var array
	 */
	protected $languages = [];

	/**
	 * Router constructor.
	 *
	 * @param RequestInterface|Request $request
	 * @param ResponseInterface|Response $response
	 * @param string $namespace Namespace for controller. MUST ended with a backslash. Example: 'Foo\Bar\Buzz\\'
	 * @param array $languages
	 */
	public function __construct(
		RequestInterface $request,
		ResponseInterface $response,
		string $namespace = '',
		array $languages = []
	) {
		$this->request = $request;
		$this->response = $response;
		$this->languages = $languages;
		if ('' !== $namespace) {
			$this->namespace = $namespace;
		}
	}

	/**
	 * Resolve request
	 *
	 * @param array $urls The regex-based url to class mapping
	 * @return array
	 * @throws ApplicationException Thrown if corresponding class is not found
	 */
	public function resolve(array $urls = []): array {
		$path = $this->getPath();

		if (!empty($urls)) {
			$controllerPattern = $this->resolveByConfig($urls, $path);
		}

		if (empty($controllerPattern)) {
			$controllerPattern = $this->resolveByUri($path);
		}

		if (empty($controllerPattern)) {
			throw new ApplicationException("URL, $path, not found.");
		}

		$class = $this->namespace . ucfirst($controllerPattern[0]) . 'Controller';
		$action = $controllerPattern[1] . 'Action';
		$params = $controllerPattern[2];

		return ['controller' => $class, 'action' => $action, 'params' => $params];
	}

	/**
	 * @param string $uri
	 * @return string
	 */
	private function getPath(): string {
		$uri = $this->request->getUri()->getPath();
		$pathParts = explode('/', $uri);
		if (in_array($pathParts[1], $this->languages)) {
			return substr($uri, 3);
		}
		return $uri;
	}

	/**
	 * @param string $path
	 * @return array
	 * @throws ApplicationException
	 */
	public function resolveByUri(string $path): array {
		if ('/' === $path) {
			return [ucfirst($this->defaultController), $this->defaultAction, []];
		}

		$controllerPattern = explode('?', $path);
		if (empty($controllerPattern)) {
			throw new ApplicationException('Bad url class pattern for url.');
		}
		$controllerPattern = $controllerPattern[0];
		$array = explode('/', $controllerPattern);

		unset($array[0]);
		$arrayCount = count($array);
		$controller = '';
		$action = '';
		foreach ($array as $key => $row) {
			if ($key !== $arrayCount) {
				$controller .= '\\' . ucfirst($this->camelize($row));
			}

			if ($key === $arrayCount && $row === '') {
				$action = $this->defaultAction;
			}
			else {
				$action = lcfirst($this->camelize(strtolower($row)));
			}
		}
		$controller = substr($controller, 1);

		return [$controller, $action, []];
	}

	/**
	 * @param string $string
	 * @return string
	 */
	public function camelize(string $string): string {
		return lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $string))));
	}

	/**
	 * @param array $urls
	 * @param string $path
	 * @return array
	 * @throws ApplicationException
	 */
	public function resolveByConfig(array $urls, string $path): array {
		$aliases = null;
		if (!empty($urls['aliases'])) {
			$aliases = $urls['aliases'];
			unset($urls['aliases']);
		}

		if (null !== ($result = $this->getPatternFromUrls($urls, $path))) {
			return $result;
		}

		if (null === $aliases) {
			return [];
		}

		foreach ($aliases as $alias) {
			if (null !== ($result = $this->getPatternFromUrls($alias, $path))) {
				return $result;
			}
		}

		return [];
	}

	/**
	 * @param $urls
	 * @param $path
	 * @return array|null
	 * @throws ApplicationException
	 */
	protected function getPatternFromUrls(array $urls, string $path) {
		foreach ($urls as $regex => $urlPattern) {
			$regex = str_replace('/', '\/', $regex);
			$regex = str_replace(array_keys($this->patterns()), array_values($this->patterns()), $regex);
			$regex = '^' . $regex . '\/?$';
			if (preg_match("/$regex/i", $path, $matches)) {
				$controllerPattern = explode('::', $urlPattern);

				if (count($controllerPattern) !== 2) {
					throw new ApplicationException('Bad url class pattern for url.');
				}

				if (count($matches) > 0) {
					unset($matches[0]);
				}
				$controllerPattern[] = $matches;
				$controllerPattern[0] = ucfirst($controllerPattern[0]);

				return $controllerPattern;
			}
		}
		return null;
	}

	/**
	 * Optional Parameter checks for the URL.
	 * e.g. /my-uri/:string/:int
	 * - /my-uri/title/1
	 */
	public function patterns(): array {
		return array(
			':string' => '([^\/]+)',
			':int' => '([0-9]+)',
			':any' => '(.+)',
		);
	}
}