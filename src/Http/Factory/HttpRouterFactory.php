<?php


namespace Engine\Http\Factory;


use Engine\Http\HttpRouter;
use Engine\Http\Interfaces\ResponseInterface;
use Engine\Http\Interfaces\ServerRequestInterface;

class HttpRouterFactory {

	/**
	 * Create router object.
	 *
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @param string $namespace Namesapce for controllers. Example: $namespace = Tests\Http\\'. If not set will be use default value 'App\Controllers\\'
	 * @param array $languages
	 * @return HttpRouter
	 */
	public function createRouter(
		ServerRequestInterface $request,
		ResponseInterface $response,
		string $namespace = '',
		array $languages = []
	) {

		return new HttpRouter($request, $response, $namespace, $languages);
	}
}