<?php

declare(strict_types = 1);

namespace Engine\Http\Factory;

use Engine\Http\Factory\Interfaces\RequestFactoryInterface;
use Engine\Http\Factory\Interfaces\ResponseFactoryInterface;
use Engine\Http\Request;
use Engine\Http\Response;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class MessageFactory implements RequestFactoryInterface, ResponseFactoryInterface {

	/**
	 * {@inheritdoc
	 */
	public function createRequest(
		$method,
		$uri,
		array $headers = [],
		$body = null,
		$protocolVersion = '1.1'
	) {
		return new Request($method, $uri, $headers, $body, $protocolVersion);
	}

	/**
	 * {@inheritdoc
	 */
	public function createResponse(
		$statusCode = 200,
		$reasonPhrase = null,
		array $headers = [],
		$body = null,
		$protocolVersion = '1.1'
	) {
		return new Response((int)$statusCode, $headers, $body, $protocolVersion, $reasonPhrase);
	}
}
