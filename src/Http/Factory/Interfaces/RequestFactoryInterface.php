<?php

namespace Engine\Http\Factory\Interfaces;

use Engine\Http\Interfaces\RequestInterface;
use Engine\Http\Interfaces\UriInterface;

interface RequestFactoryInterface {
	/**
	 * Create a new request.
	 *
	 * @param string $method
	 * @param UriInterface|string $uri
	 *
	 * @return RequestInterface
	 */
	public function createRequest($method, $uri);
}
