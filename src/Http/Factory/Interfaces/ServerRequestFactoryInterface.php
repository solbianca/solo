<?php

namespace Engine\Http\Factory\Interfaces;

use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Http\Interfaces\UriInterface;

interface ServerRequestFactoryInterface {
	/**
	 * Create a new server request.
	 *
	 * @param string $method
	 * @param UriInterface|string $uri
	 *
	 * @return ServerRequestInterface
	 */
	public function createServerRequest($method = null, $uri = null);

	/**
	 * Create a new server request from server variables.
	 *
	 * @param array $server Typically $_SERVER or similar structure.
	 *
	 * @return ServerRequestInterface
	 *
	 * @throws \InvalidArgumentException
	 *  If no valid method or URI can be determined.
	 */
	public function createServerRequestFromArray(array $server);
}
