<?php

namespace Engine\Http\Factory\Interfaces;

use Engine\Http\Interfaces\ResponseInterface;

interface ResponseFactoryInterface {
	/**
	 * Create a new response.
	 *
	 * @param integer $code HTTP status code
	 *
	 * @return ResponseInterface
	 */
	public function createResponse($code = 200);
}
