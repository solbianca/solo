<?php


namespace Engine\Renderers\Interfaces;


interface RenderInterface {

	/**
	 * Send response to client with template
	 *
	 * @param string $template
	 * @param array $params
	 * @return mixed
	 */
	public function render($template, array $params = []);

	/**
	 * Fetch template as string
	 *
	 * @param string $template
	 * @param array $params
	 * @return string
	 */
	public function fetch($template, array $params = []);

	/**
	 * Set file extension for templates
	 *
	 * @param string $fileExtension
	 * @return $this
	 */
	public function setFileExtension($fileExtension);

	/**
	 * Get file extension for templates
	 *
	 * @return string
	 */
	public function getFileExtension();

	/**
	 * Get template name with file extension
	 *
	 * @param string $template
	 * @return string
	 */
	public function getFullTemplateName($template);
}