<?php


namespace Engine\Renderers;


use Engine\Renderers\Interfaces\RenderInterface;

class TwigRenderer extends AbstractRenderer implements RenderInterface {
	/**
	 * Twig loader
	 *
	 * @var \Twig_LoaderInterface
	 */
	protected $loader;

	/**
	 * Twig environment
	 *
	 * @var \Twig_Environment
	 */
	protected $environment;

	/**
	 * @var string
	 */
	protected $fileExtension = '.twig';

	/**
	 * @var array Twig options.
	 * @see http://twig.sensiolabs.org/doc/api.html#environment-options
	 */
	protected $options = [];

	/**
	 * Create new Twig view
	 *
	 * @param string|array $path Path(s) to templates directory
	 * @param array $options Twig environment settings
	 */
	public function __construct($path, $options = []) {
		parent::__construct($path, $options);
		$this->loader = $this->createLoader(is_string($path) ? [$path] : $path);
		$this->environment = $this->createEnvironment($this->loader, $options);
	}

	/**
	 * Create a loader with the given path
	 *
	 * @param array $paths
	 * @return \Twig_Loader_Filesystem
	 */
	private function createLoader(array $paths) {
		$loader = new \Twig_Loader_Filesystem();
		foreach ($paths as $namespace => $path) {
			if (is_string($namespace)) {
				$loader->setPaths($path, $namespace);
			}
			else {
				$loader->addPath($path);
			}
		}
		return $loader;
	}

	/**
	 * Create Twig_Environment with given settings
	 *
	 * @param \Twig_Loader_Filesystem $loader
	 * @param array $options
	 * @return \Twig_Environment
	 */
	private function createEnvironment($loader, $options = []) {
		return new \Twig_Environment($loader, $options);
	}

	/**
	 * Return Twig loader
	 *
	 * @return \Twig_LoaderInterface
	 */
	public function getLoader() {
		return $this->loader;
	}

	/**
	 * Return Twig environment
	 *
	 * @return \Twig_Environment
	 */
	public function getEnvironment() {
		return $this->environment;
	}

	/**
	 * Output rendered template
	 *
	 * @param  string $template
	 * @param  array $params
	 * @return string
	 */
	public function render($template, array $params = []) {
		$file = $this->getFullTemplateName($template);
		return $this->environment->render($file, $params);
	}

	/**
	 * Fetch rendered template
	 *
	 * @param  string $template Template pathname relative to templates directory
	 * @param  array $params Associative array of template variables
	 * @return string
	 */
	public function fetch($template, array $params = []) {
		return $this->environment->loadTemplate($template)->render($params);
	}
}