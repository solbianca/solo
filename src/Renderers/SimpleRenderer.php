<?php


namespace Engine\Renderers;


use Engine\Renderers\Exceptions\RendererException;
use Engine\Renderers\Interfaces\RenderInterface;

class SimpleRenderer extends AbstractRenderer implements RenderInterface {

	/**
	 * {@inheritdoc}
	 */
	public function render($template, array $params = []) {
		return $this->fetch((string)$template, $params);
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetch($template, array $params = []) {
		$file = $this->getFullPath((string)$template);

		if (!file_exists($file)) {
			throw new RendererException("Template file {$template} not found by path: {$file}.");
		}

		ob_start();
		extract($params);
		include $file;
		$output = ob_get_clean();

		return $output;
	}

	/**
	 * Gets the full path to a template file.
	 *
	 * @param string $file Template file
	 * @return string Template file location
	 */
	public function getFullPath($template) {
		return $this->path . '/' . (string)$template . $this->fileExtension;
	}
}