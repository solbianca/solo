<?php


namespace Engine\Renderers\Exceptions;


use Engine\Core\Exceptions\ApplicationException;

class RendererException extends ApplicationException {

}