<?php


namespace Engine\Renderers;


use Engine\Renderers\Exceptions\RendererException;
use Engine\Renderers\Interfaces\RenderInterface;

abstract class AbstractRenderer implements RenderInterface {

	/**
	 * Path to directory with templates files
	 *
	 * @var string
	 */
	protected $path;

	/**
	 * Renderer options
	 *
	 * @var array
	 */
	protected $options = [];

	/**
	 * Templates files extension
	 *
	 * @var string
	 */
	protected $fileExtension = '.php';

	/**
	 * AbstractRenderer constructor.
	 *
	 * @param string $path
	 * @param array $options
	 * @throws RendererException
	 */
	public function __construct($path, array $options = []) {
		$this->path = (string)$path;
		$this->options = $options;
	}

	/**
	 * {@inheritdoc}
	 */
	abstract public function render($template, array $params = []);

	/**
	 * {@inheritdoc}
	 */
	abstract public function fetch($template, array $params = []);

	/**
	 * Get template name with file extension
	 *
	 * @param string $template
	 * @return string
	 * @throws RendererException
	 */
	public function getFullTemplateName($template) {
		return (string)$template . $this->fileExtension;
	}

	/**
	 * Get path to directory with templates files
	 *
	 * @return string
	 */
	public function getPath() {
		return $this->path;
	}

	/**
	 * Get file extension
	 *
	 * @return string
	 */
	public function getFileExtension() {
		return $this->fileExtension;
	}

	/**
	 * Set file extension
	 *
	 * @param $fileExtension
	 * @return $this
	 */
	public function setFileExtension($fileExtension) {
		$this->fileExtension = (string)$fileExtension;
		return $this;
	}
}