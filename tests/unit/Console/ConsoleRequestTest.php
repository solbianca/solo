<?php


class ConsoleRequestTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Console\Request
	 */
	protected $request;


	public function _before() {
		$this->request = new \Engine\Console\Request();
	}

	public function testSetAndGetParamsSuccessful() {
		$r = $this->request;

		$data = ['route' => 'action'];
		$r->setParams($data);
		$this->tester->assertEquals($data, $r->getParams());

	}

	public function testResolveMethodSuccessful() {
		$r = $this->request;

		$data = ['route/action', 'param1', '--param2', '--param3=3', '-param4'];
		$r->setParams($data);

		$result = $r->resolve();

		$this->tester->assertEquals('route/action', $result[0]);
		$this->tester->assertEquals('param1', $result[1][0]);
		$this->tester->assertEquals(true, $result[1]['param2']);
		$this->tester->assertEquals('3', $result[1]['param3']);
		$this->tester->assertEquals(true, $result[1]['_aliases']['param4']);
	}


}