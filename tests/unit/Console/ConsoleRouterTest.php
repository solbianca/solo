<?php


class ConsoleRouterTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Console\ConsoleRouter
	 */
	protected $consoleRouter;


	public function _before() {
		$request = new \Engine\Console\Request();
		$request->setParams(['route/action', 'param1', 'param2']);
		$this->consoleRouter = new \Engine\Console\ConsoleRouter($request, new \Engine\Console\Response());
	}

	public function testResolveMethodSuccessful() {
		$result = $this->consoleRouter->resolve(['route/action' => 'Test::test']);

		$this->tester->assertEquals('App\Console\Controllers\TestController', $result['controller']);
		$this->tester->assertEquals('testAction', $result['action']);
		$this->tester->assertEquals([], $result['params']);
	}

	public function testResolveWrongUrlParamsName() {
		$cr = $this->consoleRouter;
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($cr) {
			$cr->resolve(['fail' => 'Fail::fail']);
		});
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($cr) {
			$cr->resolve(['fail' => 'Fail--fail']);
		});
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($cr) {
			$cr->resolve(['fail']);
		});
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($cr) {
			$cr->resolve(['fail' => '::asdfasdf']);
		});
	}


}



