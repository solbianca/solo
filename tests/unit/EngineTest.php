<?php

use Engine\Engine;

class EngineTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testLog() {
		$this->tester->expectException(Exception::class, function () {
			\Engine\Engine::log(\Engine\Log\LogLevel::ERROR, 'error');
		});
	}

	public function testCreateInstance() {
		$object = Engine::createInstance(CreateInstanceTestClass::class, ['Hello, World!']);
		$this->tester->assertInstanceOf(CreateInstanceTestClass::class, $object);
		$this->tester->assertEquals('Hello, World!', $object->say());
	}
}

class CreateInstanceTestClass {
	public $hello;

	public function __construct($hello) {
		$this->hello = $hello;
	}

	public function say() {
		return $this->hello;
	}
}