<?php

namespace Tests\unit\Pipeline;

use Engine\Pipeline\Interfaces\PipelineBuilderInterface;
use Engine\Pipeline\Interfaces\PipelineInterface;
use Engine\Pipeline\InterruptibleProcessor;
use Engine\Pipeline\Pipeline;
use Engine\Pipeline\PipelineBuilder;
use Tests\unit\Pipeline\Pipes\Pipe1;
use Tests\unit\Pipeline\Pipes\Pipe2;
use Tests\unit\Pipeline\Pipes\Pipe3;

class PipelineTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {

	}

	public function testCreatePipelineClass() {
		$payload = 0;
		$pipeline = new Pipeline();
		$payload = $pipeline->pipe(new Pipe1())->process($payload);
		$this->tester->assertTrue($payload == 1);

		$payload = 0;
		$payload = (new Pipeline())->pipe(new Pipe1())->process($payload);
		$this->tester->assertTrue($payload == 1);

		$payload = 0;
		$payload = (new Pipeline([new Pipe1(), new Pipe2()]))->pipe(new Pipe3())->process($payload);
		$this->tester->assertTrue(3 == $payload);
	}

	public function testClosure() {
		$pipeline = new Pipeline();
		$payload = 0;
		$payload = $pipeline->pipe(function ($payload) {
			return ++$payload;
		})->pipe(function ($payload) {
			return ++$payload;
		})->process($payload);
		$this->tester->assertTrue($payload == 2);
	}

	public function testBuilder() {
		$pipelineBuilder = (new PipelineBuilder())->add(new Pipe1())->add(new Pipe2())->add(new Pipe3());
		$this->tester->assertInstanceOf(PipelineBuilderInterface::class, $pipelineBuilder);
		$pipeline = $pipelineBuilder->build();
		$this->tester->assertInstanceOf(PipelineInterface::class, $pipeline);
	}

	public function testInterruptibleProcessor() {
		$processor = new InterruptibleProcessor(function ($payload) {
			return ($payload > 5) ? false : true;
		});
		$pipeline = new Pipeline([
			function ($payload) {
				$payload += 2;
				return $payload;
			},
			function ($payload) {
				$payload += 4;
				return $payload;
			},
			function ($payload) {
				$payload += 4;
				return $payload;
			},
		], $processor);
		$payload = $pipeline->process(0);
		$this->tester->assertTrue($payload == 6);
	}
}