<?php


namespace Tests\unit\Pipeline\Pipes;

use Engine\Pipeline\Interfaces\PipeInterface;


/**
 * Pipe with invoke
 */
class Pipe2 implements PipeInterface {
	public function __invoke($payload) {
		return ++$payload;
	}
}