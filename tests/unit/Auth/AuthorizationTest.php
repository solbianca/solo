<?php

use Engine\Core\Exceptions\ApplicationException;

class AuthorizationTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Auth\Authorization
	 */
	protected $auhorization;

	protected function _before() {
		$user = new \Engine\Base\UserIdentity();
		$request = new \Engine\Http\Request('GET', '/');
		$responce = new \Engine\Http\Response();
		$this->auhorization = new \Engine\Auth\Authorization($user, $request, $responce);
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testSuccessfulResolve() {
		$this->assertTrue($this->auhorization->resolve(NullAuthMethod::class, ''));
	}

	public function testFailureResolve() {
		$this->tester->expectException(ApplicationException::class, function () {
			$this->auhorization->resolve('NotExistMethod', '');
		});
	}

}

class NullAuthMethod extends \Engine\Auth\Methods\AbstractAuthMethod {
	public function authenticate($clientId) {
		return true;
	}
}