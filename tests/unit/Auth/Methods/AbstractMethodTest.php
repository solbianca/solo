<?php

use Engine\Auth\Exceptions\UnauthorizedException;
use Engine\Http\Interfaces\ResponseInterface;

class AbstractMethodTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Auth\Methods\AbstractAuthMethod
	 */
	protected $method;

	protected function _before() {
		$user = new \Engine\Base\UserIdentity();
		$request = new \Engine\Http\Request('GET', '/');
		$responce = new \Engine\Http\Response();
		$this->method = new AuthMethod($user, $request, $responce);
	}

	public function testAuthenticate() {
		$this->tester->assertTrue($this->method->authenticate(1));
	}

	public function testChallenge() {
		$response = (new \Engine\Http\Factory\MessageFactory())->createResponse();
		$this->tester->assertTrue($this->method->challenge($response));
	}

	public function testHandleFailure() {
		$response = new \Engine\Http\Response();
		$this->tester->expectException(UnauthorizedException::class, function () use ($response) {
			$this->method->handleFailure($response);
		});
	}
}

class AuthMethod extends \Engine\Auth\Methods\AbstractAuthMethod {
	public function authenticate($clientId) {
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function challenge(ResponseInterface $response) {
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function handleFailure(ResponseInterface $response) {
		throw new UnauthorizedException('Your request was made with invalid credentials.');
	}
}