<?php

use Engine\Auth\Exceptions\UnauthorizedException;

class HttpBearerAuthTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Auth\Methods\AbstractAuthMethod
	 */
	protected $method;

	/**
	 * @var User
	 */
	protected $user;

	protected function _before() {
		$user = new User();
		$this->user = $user;
		$request = new \Engine\Http\Request('GET', '/', ['HTTP_AUTHORIZATION' => 'Bearer qwerty']);
		$response = new \Engine\Http\Response();
		$this->method = new \Engine\Auth\Methods\HttpBearerAuth($user, $request, $response);
	}

	public function testSuccessfulAuthenticate() {
		$result = $this->method->authenticate('');
		$this->tester->assertNotEmpty($result);
		$this->tester->assertEquals(1, $result['id']);
		$this->tester->assertEquals('qwerty', $result['token']);
	}

	public function testEmptyAuthenticate() {
		unset($_SERVER['HTTP_AUTHORIZATION']);
		$user = new User();
		$request = new \Engine\Http\Request('GET', '/');
		$responce = new \Engine\Http\Response();
		$method = new \Engine\Auth\Methods\HttpBearerAuth($user, $request, $responce);

		$result = $method->authenticate('');
		$this->tester->assertNull($result);
	}

	public function testFailureAuthenticate() {
		$user = new User();
		$request = new \Engine\Http\Request('GET', '/', ['HTTP_AUTHORIZATION' => 'Bearer qwerty-fail']);
		$response = new \Engine\Http\Response();
		$method = new \Engine\Auth\Methods\HttpBearerAuth($user, $request, $response);

		$this->tester->expectException(UnauthorizedException::class, function () use ($method) {
			$method->authenticate('');
		});
	}

	public function testChallenge() {
		$response = new \Engine\Http\Response();
		$response = $this->method->challenge($response);
		$this->tester->assertEquals($response->getHeaderLine('WWW-AUTHENTICATE'), 'Bearer realm="api"');
	}
}

class User implements \Engine\Base\Interfaces\UserInterface {

	public $identity = [
		'id' => 1,
		'username' => 'admin',
		'email' => 'admin@example.com',
		'password' => 'secret',
		'token' => 'qwerty',
	];

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		// TODO: Implement getRole() method.
	}

	public function isGuest() {
		// TODO: Implement isGuest() method.
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		if ($this->identity['token'] === $token) {
			return $this->identity;
		}
		return null;
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}