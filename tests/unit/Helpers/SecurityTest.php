<?php


class SecurityTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testGenerateRandomString() {
		$string = \Engine\Helpers\Security::generateRandomString();
		$this->tester->assertTrue(32 === strlen($string));
		$this->assertEquals(1, preg_match('/[A-Za-z0-9]+/', $string));

		$string = \Engine\Helpers\Security::generateRandomString(10);
		$this->tester->assertTrue(10 === strlen($string));
		$this->assertEquals(1, preg_match('/[A-Za-z0-9]+/', $string));

		$string = \Engine\Helpers\Security::generateRandomString(10, '-_=+');
		$this->tester->assertTrue(10 === strlen($string));
		$this->assertEquals(1, preg_match('/[-_=+]+/', $string));
	}
}