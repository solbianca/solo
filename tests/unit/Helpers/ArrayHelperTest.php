<?php

namespace tests\unit\Helpers;

use Codeception\Test\Unit;
use Engine\Helpers\ArrayHelper;

class ArrayHelperTest extends Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testKeysExist() {
		$array = ['id' => 41, 'name' => 'John Doe', 'test' => 'Test Value'];
		$this->tester->assertTrue(ArrayHelper::keysExist(['id', 'name'], $array));

		$this->tester->assertFalse(ArrayHelper::keysExist(['fail'], $array));
	}

	public function testMerge() {
		$arr1 = ['key1' => 11, 'key2' => ['subKey1' => 55], 'key3' => 33, 4];
		$arr2 = ['key2' => ['subKey1' => 220, 'subKey2' => 'subValue']];
		$arr3 = [5];

		$this->tester->assertEquals([
			'key1' => 11,
			'key2' => ['subKey1' => 220, 'subKey2' => 'subValue'],
			'key3' => 33,
			4,
			5,
		], ArrayHelper::merge($arr1, $arr2, $arr3));
	}
}