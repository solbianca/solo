<?php


class SocketTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\JsonRpc2\Transports\SocketTransport
	 */
	protected $transport;

	public function _before() {
		$this->transport = new \Engine\JsonRpc2\Transports\SocketTransport('127.0.0.1', 10000, []);
	}

	public function testConstructor() {
		$transport = new \Engine\JsonRpc2\Transports\SocketTransport('127.0.0.1', 10000, []);

		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Transports\SocketTransport::class, $transport);
		$params = $transport->getParams();
		$this->tester->assertNotEmpty($params);
		$this->tester->assertEquals(AF_INET, $params['DOMAIN']);
		$this->tester->assertEquals(SOCK_STREAM, $params['SOCKET_TYPE']);
		$this->tester->assertEquals(SOL_TCP, $params['SOCKET_PROTOCOL']);
		$this->tester->assertEquals(0, $params['TIMEOUT']);
	}

	public function testGetParams() {
		$transport = new \Engine\JsonRpc2\Transports\SocketTransport('127.0.0.1', 10000, [
			'DOMAIN' => 11,
			'SOCKET_TYPE' => 22,
			'SOCKET_PROTOCOL' => 33,
			'TIMEOUT' => 44,
		]);

		$params = $transport->getParams();
		$this->tester->assertNotEmpty($params);
		$this->tester->assertEquals(11, $params['DOMAIN']);
		$this->tester->assertEquals(22, $params['SOCKET_TYPE']);
		$this->tester->assertEquals(33, $params['SOCKET_PROTOCOL']);
		$this->tester->assertEquals(44, $params['TIMEOUT']);
	}

	public function testParamsMethods() {
		$params = ['TEST1' => 'TEST_VALUE_1', 'TEST2' => 'TEST_VALUE_2'];
		$this->transport->setParams($params);
		$params2 = $this->transport->getParams();
		$this->tester->assertNotEmpty($params2);
		$this->tester->assertEquals($params, $params2);

		$this->transport->setParams([]);
		$this->tester->assertEmpty($this->transport->getParams());
	}

	public function testParamMethods() {
		$this->transport->setParam('TEST_KEY', 'TEST_VALUE');
		$this->tester->assertEquals('TEST_VALUE', $this->transport->getParam('TEST_KEY'));
	}
}