<?php


class PhpFpmTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\JsonRpc2\Transports\PhpFpmTransport
	 */
	protected $transport;

	public function _before() {
		$this->transport = new \Engine\JsonRpc2\Transports\PhpFpmTransport('127.0.0.1', '9000', '/path/to');
	}

	public function testConstructor() {
		$transport = new \Engine\JsonRpc2\Transports\PhpFpmTransport('127.0.0.1', '9000', '/path/to',
			['KEY' => 'VALUE', 'GATEWAY_INTERFACE' => 'TEST_VALUE']);
		$params = $transport->getParams();
		$this->tester->assertNotEmpty($params);
		$this->tester->assertEquals('TEST_VALUE', $params['GATEWAY_INTERFACE']);
		$this->tester->assertEquals('POST', $params['REQUEST_METHOD']);
		$this->tester->assertEquals('/index.php', $params['SCRIPT_NAME']);
		$this->tester->assertEquals('php/fcgiclient', $params['SERVER_SOFTWARE']);
		$this->tester->assertEquals('HTTP/1.1', $params['SERVER_PROTOCOL']);
		$this->tester->assertEquals('Content-Type: application/json', $params['CONTENT_TYPE']);
		$this->tester->assertEquals('VALUE', $params['KEY']);
	}

	public function testGetParams() {
		$params = $this->transport->getParams();
		$this->tester->assertNotEmpty($params);
		$this->tester->assertEquals('FastCGI/1.0', $params['GATEWAY_INTERFACE']);
		$this->tester->assertEquals('POST', $params['REQUEST_METHOD']);
		$this->tester->assertEquals('/index.php', $params['SCRIPT_NAME']);
		$this->tester->assertEquals('php/fcgiclient', $params['SERVER_SOFTWARE']);
		$this->tester->assertEquals('HTTP/1.1', $params['SERVER_PROTOCOL']);
		$this->tester->assertEquals('Content-Type: application/json', $params['CONTENT_TYPE']);
	}

	public function testParamsMethods() {
		$params = ['TEST1' => 'TEST_VALUE_1', 'TEST2' => 'TEST_VALUE_2'];
		$this->transport->setParams($params);
		$params2 = $this->transport->getParams();
		$this->tester->assertNotEmpty($params2);
		$this->tester->assertEquals($params, $params2);

		$this->transport->setParams([]);
		$this->tester->assertEmpty($this->transport->getParams());
	}

	public function testParamMethods() {
		$this->tester->assertEquals('Content-Type: application/json', $this->transport->getParam('CONTENT_TYPE'));
		$this->transport->setParam('TEST_KEY', 'TEST_VALUE');
		$this->tester->assertEquals('TEST_VALUE', $this->transport->getParam('TEST_KEY'));
	}
}