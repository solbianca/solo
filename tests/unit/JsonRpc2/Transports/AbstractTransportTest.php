<?php


class AbstractTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var AbstractTransportTestObject
	 */
	protected $transport;

	public function _before() {
		$this->transport = new AbstractTransportTestObject();
	}

	public function testSend() {
		$this->tester->assertTrue($this->transport->send(''));
	}

	public function testDestination() {
		$destination = '/path/to/some/point';
		$this->transport->setDestination($destination);
		$this->assertEquals($destination, $this->transport->getDestination());
	}
}

class AbstractTransportTestObject extends \Engine\JsonRpc2\Transports\AbstractTransport {
	public function send($json) {
		return true;
	}
}