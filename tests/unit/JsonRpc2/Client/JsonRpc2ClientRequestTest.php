<?php


use Engine\JsonRpc2\Client\Request;

class JsonRpc2ClientRequestTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function testConstants() {
		$this->tester->assertEquals('2.0', Request::JSON_RPC_VERSION);
	}

	public function testConstructor() {
		$request = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], false);
		$this->tester->assertEquals('test', $request->method);
		$this->tester->assertEquals(['id' => 1, 'name' => 'John Doe'], $request->params);
		$this->tester->assertNotNull($request->id);
	}

	public function testConstructNotify() {
		$request = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], true);
		$this->tester->assertEquals('test', $request->method);
		$this->tester->assertEquals(['id' => 1, 'name' => 'John Doe'], $request->params);
		$this->tester->assertEmpty($request->id);
	}

	public function testConstructIllegalMethod() {
		$this->tester->expectException(\Engine\JsonRpc2\Exceptions\JsonRpcException::class, function () {
			new \Engine\JsonRpc2\Client\Request('rpc.test', ['id' => 1, 'name' => 'John Doe'], true);
		});
	}

	public function testConstructParamsToArray() {
		$request = new \Engine\JsonRpc2\Client\Request('test', 'test-param', true);
		$this->tester->assertTrue(is_array($request->params));

		$request = new \Engine\JsonRpc2\Client\Request('test', 42, true);
		$this->tester->assertTrue(is_array($request->params));

		$obj = new stdClass();
		$obj->id = 42;
		$obj->name = 'John Doe';
		$request = new \Engine\JsonRpc2\Client\Request('test', $obj, true);
		$this->tester->assertTrue(is_array($request->params));

		$request = new \Engine\JsonRpc2\Client\Request('test', '', true);
		$this->tester->assertTrue(is_array($request->params));

		$request = new \Engine\JsonRpc2\Client\Request('test', true, true);
		$this->tester->assertTrue(is_array($request->params));
	}

	public function testConstructorParamsIsNull() {
		$request = new \Engine\JsonRpc2\Client\Request('test', null, true);
		$this->tester->assertNull($request->params);
	}

	public function testGetArray() {
		$request = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], true);
		$array = $request->getArray();
		$this->tester->assertTrue(is_array($array));
		$this->tester->assertEquals('2.0', $array['jsonrpc']);
		$this->tester->assertEquals('test', $array['method']);
		$this->tester->assertFalse(isset($array['id']));
		$this->tester->assertEquals(1, $array['params']['id']);
		$this->tester->assertEquals('John Doe', $array['params']['name']);
	}

	public function testGetArrayNotify() {
		$request = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], false);
		$array = $request->getArray();
		$this->tester->assertTrue(is_array($array));
		$this->tester->assertEquals('2.0', $array['jsonrpc']);
		$this->tester->assertEquals('test', $array['method']);
		$this->tester->assertNotNull($array['id']);
		$this->tester->assertEquals(1, $array['params']['id']);
		$this->tester->assertEquals('John Doe', $array['params']['name']);
	}

	public function testGetArrayNullParams() {
		$request = new \Engine\JsonRpc2\Client\Request('test', null, false);
		$array = $request->getArray();
		$this->tester->assertTrue(is_array($array));
		$this->tester->assertEquals('2.0', $array['jsonrpc']);
		$this->tester->assertEquals('test', $array['method']);
		$this->tester->assertNotNull($array['id']);
		$this->assertFalse(isset($array['params']));
	}

	public function testGetJson() {
		$request = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], true);
		$this->assertEquals('{"jsonrpc":"2.0","method":"test","params":{"id":1,"name":"John Doe"}}',
			$request->getJSON());
	}
}