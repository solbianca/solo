<?php


use Engine\JsonRpc2\Client\Client;

class JsonRpc2ClientTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->client = new Client(new NulledTestTransport());
	}

	public function testSendBatch() {
		$request[] = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], true);
		$request[] = new \Engine\JsonRpc2\Client\Request('test2', ['id' => 2, 'name' => 'Adam Smith'], true);
		$result = $this->client->sendBatch($request);
		$this->tester->assertTrue($result);

		$request = [];
		$client = new Client(new NulledTestTransport());
		$request[] = new \Engine\JsonRpc2\Client\Request('test', ['id' => 1, 'name' => 'John Doe'], false);
		$request[] = new \Engine\JsonRpc2\Client\Request('test2', ['id' => 2, 'name' => 'Adam Smith'], false);
		$result = $client->sendBatch($request);
		$this->tester->assertTrue(is_array($result));

		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Response::class, $result[0]);
		$this->assertEquals('qwerty', $result[0]->result);
		$this->assertNotEmpty($result[0]->id);
		$this->assertEquals(null, $result[0]->errorCode);
		$this->assertEquals(null, $result[0]->errorMessage);

		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Response::class, $result[1]);
		$this->assertEquals('qwerty', $result[1]->result);
		$this->assertNotEmpty($result[1]->id);
		$this->assertEquals(null, $result[1]->errorCode);
		$this->assertEquals(null, $result[1]->errorMessage);
	}

	public function testSend() {
		$result = $this->client->send('{"jsonrpc":"2.0","method":"test","params":{"id":1,"name":"John Doe"},"id":"5889efa44dbfa"}}',
			true);
		$this->tester->assertTrue($result);

		$response = $this->client->send('{"jsonrpc":"2.0","method":"test","params":{"id":1,"name":"John Doe"},"id":"5889efa44dbfa"}',
			false);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Response::class, $response);
		$this->assertEquals('qwerty', $response->result);
		$this->assertNotEmpty($response->id);
		$this->assertEquals(null, $response->errorCode);
		$this->assertEquals(null, $response->errorMessage);
	}

	public function testSendRequestWithResponseException() {
		$client = new Client(new NulledExceptionTransport());
		$request = new \Engine\JsonRpc2\Client\Request('test', []);
		$this->tester->expectException(\Engine\JsonRpc2\Exceptions\JsonRpcResponseException::class,
			function () use ($client, $request) {
				$client->sendRequest($request);
			}
		);

		try {
			$client->sendRequest($request);
		} catch (\Engine\JsonRpc2\Exceptions\JsonRpcResponseException $e) {
			$this->tester->assertEquals(-32099, $e->getCode());
			$this->tester->assertEquals('Application exception', $e->getMessage());
			$this->tester->assertTrue(is_array($e->getErrorData()));
			$this->tester->assertEquals(78, $e->getErrorData()['line']);
			$this->tester->assertEquals('/path/to/one', $e->getErrorData()['trace'][0]);
			$this->tester->assertEquals('/path/to/two', $e->getErrorData()['trace'][1]);
		}
	}

	public function testSendRequestWithMistmatchResponseIdException() {
		$client = new Client(new NulledExceptionTransport2());
		$request = new \Engine\JsonRpc2\Client\Request('test', ['id' => 42]);
		$this->tester->expectException(\Engine\JsonRpc2\Exceptions\JsonRpcException::class,
			function () use ($client, $request) {
				$client->sendRequest($request);
			}
		);
	}

	public function testHandleResponse() {
		$response = new stdClass();
		$response->jsonrpc = '2.0';
		$response->result = '{"token":"qwerty"}';
		$response->id = '5889e6193dccd';

		$response = $this->client->handleResponse($response);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Response::class, $response);
		$this->assertEquals('{"token":"qwerty"}', $response->result);
		$this->assertEquals('5889e6193dccd', $response->id);
		$this->assertEquals(null, $response->errorCode);
		$this->assertEquals(null, $response->errorMessage);
	}

	public function testHandleResponseWithError() {
		$response = new stdClass();
		$response->jsonrpc = '2.0';
		$response->result = '{"token":"qwerty"}';
		$response->id = '5889e6193dccd';
		$error = new stdClass();
		$error->code = 404;
		$error->message = 'Not Found';
		$response->error = $error;

		$response = $this->client->handleResponse($response);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Response::class, $response);
		$this->assertEmpty($response->result);
		$this->assertEquals('5889e6193dccd', $response->id);
		$this->assertEquals(404, $response->errorCode);
		$this->assertEquals('Not Found', $response->errorMessage);
	}
}

class NulledTestTransport extends \Engine\JsonRpc2\Transports\AbstractTransport {

	/**
	 * @inheritdoc
	 */
	public function send($json) {
		$object = json_decode($json);

		$response = [];
		if (is_array($object)) {
			foreach ($object as $key => $row) {
				$response[$key] = [
					'jsonrpc' => '2.0',
					'result' => 'qwerty',
				];
				if (isset($row->id)) {
					$response[$key]['id'] = $row->id;
				}
			}
		}
		else {
			$response = [
				'jsonrpc' => '2.0',
				'result' => 'qwerty',
			];
			if (isset($object->id)) {
				$response['id'] = $object->id;
			}

		}

		return json_encode($response);
	}
}

class NulledExceptionTransport extends \Engine\JsonRpc2\Transports\AbstractTransport {

	public function send($json) {
		return '{"jsonrpc":"2.0","error":{"code":"-32099","message":"Application exception","data":{"line":"78","trace":["/path/to/one","/path/to/two"]}},"id":null}';
	}
}

class NulledExceptionTransport2 extends \Engine\JsonRpc2\Transports\AbstractTransport {
	public function send($json) {
		return json_encode(['jsonrpc' => '2.0', 'result' => 'qwerty', 'id' => '123']);
	}
}