<?php


use Engine\JsonRpc2\Client\Response;

class JsonRpc2ClientResponseTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function testConstructorSuccess() {
		$response = new Response('{"token":"qwerty"}');
		$this->tester->assertEquals('{"token":"qwerty"}', $response->result);
		$this->tester->assertNull($response->id);
		$this->tester->assertNull($response->errorCode);
		$this->tester->assertNull($response->errorMessage);

		$response = new Response('{"token":"qwerty"}', 42);
		$this->tester->assertEquals('{"token":"qwerty"}', $response->result);
		$this->tester->assertEquals(42, $response->id);
		$this->tester->assertNull($response->errorCode);
		$this->tester->assertNull($response->errorMessage);
	}

	public function testConstructorErrors() {
		$response = new Response('{"token":"qwerty"}', 42, 404, 'Not Found');
		$this->tester->assertEquals('{"token":"qwerty"}', $response->result);
		$this->tester->assertEquals(42, $response->id);
		$this->tester->assertEquals(404, $response->errorCode);
		$this->tester->assertEquals('Not Found', $response->errorMessage);

		$response = new Response('{"token":"qwerty"}', null, -32099, 'Application exception',
			['line' => 78, 'trace' => ['/path/to/one', '/path/to/two']]);
		$this->tester->assertEquals('{"token":"qwerty"}', $response->result);
		$this->tester->assertNull($response->id);
		$this->tester->assertEquals('-32099', $response->errorCode);
		$this->tester->assertEquals('Application exception', $response->errorMessage);
		$this->tester->assertTrue(is_array($response->errorData));
		$this->tester->assertEquals(2, count($response->errorData));
		$this->tester->assertEquals(78, $response->errorData['line']);
		$this->tester->assertEquals(2, count($response->errorData['trace']));
		$this->tester->assertEquals('/path/to/one', $response->errorData['trace'][0]);
		$this->tester->assertEquals('/path/to/two', $response->errorData['trace'][1]);
	}

	public function testToString() {
		$response = new Response('{"token":"qwerty"}');
		$this->tester->assertEquals('{"token":"qwerty"}', (string)$response);

		$response = new Response('{"token":"qwerty"}', 42, 404, 'Not Found');
		$this->tester->assertEquals('404: Not Found', (string)$response);
	}

	public function testConstructorWithArrayOfObjectsInResult() {
		$json = '{"jsonrpc":"2.0",
				"result":[
					{"id":"1","name":"John Doe","code":"offer_start_date","type":"timestamp","params":[{"id":"42","type":"string"},{"id":"43","type":"int"}]},
					{"id":"2","name":"Alex Prker","code":"offer_goods_export"}]
				,"id":"58947a3969b88"}';
		$object = json_decode($json);

		$response = new Response($object->result, '58947a3969b88');
		$this->tester->assertEquals(2, count($response->result));
		$this->tester->assertTrue(is_array($response->result[0]));
		$this->tester->assertTrue(is_array($response->result[1]));

		$this->tester->assertEquals('1', $response->result[0]['id']);
		$this->tester->assertEquals('John Doe', $response->result[0]['name']);
		$this->tester->assertEquals('offer_start_date', $response->result[0]['code']);
		$this->tester->assertEquals('timestamp', $response->result[0]['type']);

		$this->tester->assertTrue(is_array($response->result[0]['params']));
		$this->tester->assertEquals(2, count($response->result[0]['params']));
		$this->tester->assertEquals('42', $response->result[0]['params'][0]['id']);
		$this->tester->assertEquals('string', $response->result[0]['params'][0]['type']);
		$this->tester->assertEquals('43', $response->result[0]['params'][1]['id']);
		$this->tester->assertEquals('int', $response->result[0]['params'][1]['type']);

		$this->tester->assertEquals('2', $response->result[1]['id']);
		$this->tester->assertEquals('Alex Prker', $response->result[1]['name']);
		$this->tester->assertEquals('offer_goods_export', $response->result[1]['code']);
		$this->tester->assertNull($response->errorCode);
		$this->tester->assertNull($response->errorMessage);
	}
}