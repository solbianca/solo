<?php


class ClientFactoryTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\JsonRpc2\Client\ClientFactory
	 */
	protected $clientFactory;

	public function _before() {
		$this->clientFactory = new \Engine\JsonRpc2\Client\ClientFactory();
	}

	public function testSuccessfulCreatePhpFpmClients() {
		$config[] = [
			'transport' => 'php-fpm',
			'host' => '127.0.0.1',
			'port' => 9000,
			'file' => '/app/services/Auth/public/index.php',
		];
		$config[] = [
			'transport' => 'php-fpm',
			'host' => '192.168.1.1',
			'port' => 9001,
			'file' => '/app/services/Auth/public/index.php',
		];
		$clients = $this->clientFactory->createClients($config);
		$this->tester->assertNotEmpty($clients);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[0]);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[1]);
	}

	public function testSuccessfulCreateSocketClients() {
		$config[] = [
			'transport' => 'socket',
			'host' => '127.0.0.1',
			'port' => 9000,
		];
		$config[] = [
			'transport' => 'socket',
			'host' => '192.168.1.1',
			'port' => 9001,
		];
		$clients = $this->clientFactory->createClients($config);
		$this->tester->assertNotEmpty($clients);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[0]);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[1]);
	}


	public function testEmptyConfigForCreateClients() {
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () {
				$this->clientFactory->createClient([], []);
			}
		);
	}

	public function testSuccessfulCreateClient() {
		$config = [
			'transport' => 'php-fpm',
			'host' => '127.0.0.1',
			'port' => 9000,
			'file' => '/app/services/Auth/public/index.php',
		];
		$client = $this->clientFactory->createClient($config, []);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $client);
	}

	public function testInvalidTransportParameterForCreateClient() {
		$config = [
			'transport' => 'php-fail',
			'host' => '127.0.0.1',
			'port' => 9000,
			'file' => '/app/services/Auth/public/index.php',
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);

		$config = [
			'host' => '127.0.0.1',
			'port' => 9000,
			'file' => '/app/services/Auth/public/index.php',
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);
	}

	public function testInvalidConfigParametersForPhpFpmTransport() {
		$config = [
			'transport' => 'php-fpm',
			'port' => 9000,
			'file' => '/app/services/Auth/public/index.php',
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);

		$config = [
			'transport' => 'php-fpm',
			'host' => '127.0.0.1',
			'file' => '/app/services/Auth/public/index.php',
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);

		$config = [
			'transport' => 'php-fpm',
			'host' => '127.0.0.1',
			'port' => 9000,
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);
	}

	public function testInvalidConfigParametersForSocketTransport() {
		$config = [
			'transport' => 'socket',
			'port' => 9000,
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);

		$config = [
			'transport' => 'socket',
			'host' => '127.0.0.1',
		];
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($config) {
				$this->clientFactory->createClient($config, []);
			}
		);
	}
}