<?php


class JsonRpc2RouterTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\JsonRpc2\JsonRpc2Router
	 */
	protected $router;

	protected $urls = [
		'login' => 'Site::login',
		'user.add' => 'Site\User::create',
		'user.view' => 'Site\User',
		'user.delete' => 'Site\User:create',
		'user.update' => '::update',
	];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$_SERVER['REQUEST_METHOD'] = 'POST';
		$_SERVER['REQUEST_URI'] = '/';
	}

	public function testResolveSuccessful() {
		$stream = \Engine\Http\Stream::create(json_encode(['method' => 'login']));
		$request = new \Engine\Http\ServerRequest('POST', '/');
		$request = $request->withBody($stream);

		$router = new \Engine\JsonRpc2\JsonRpc2Router($request, new \Engine\Http\Response());
		$result = $router->resolve($this->urls);
		$this->tester->assertNotEmpty($result);
		$this->tester->assertEquals('App\Controllers\SiteController', $result['controller']);
		$this->tester->assertEquals('loginAction', $result['action']);
		$this->tester->assertEmpty($result['params']);

		$stream = \Engine\Http\Stream::create(json_encode(['method' => 'user.add']));
		$request = new \Engine\Http\ServerRequest('POST', '/');
		$request = $request->withBody($stream);
		$router = new \Engine\JsonRpc2\JsonRpc2Router($request, new \Engine\Http\Response());
		$result = $router->resolve($this->urls);
		$this->tester->assertEquals('App\Controllers\Site\UserController', $result['controller']);
		$this->tester->assertEquals('createAction', $result['action']);
		$this->tester->assertEmpty($result['params']);
	}

	public function testResolveException() {
		$stream = \Engine\Http\Stream::create(json_encode(['method' => 'fail']));
		$request = new \Engine\Http\ServerRequest('POST', '/');
		$request = $request->withBody($stream);
		$router = new \Engine\JsonRpc2\JsonRpc2Router($request, new \Engine\Http\Response());
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($router) {
			$router->resolve($this->urls);
		});

		$stream = \Engine\Http\Stream::create(json_encode(['method' => 'user.view']));
		$request = new \Engine\Http\ServerRequest('POST', '/');
		$request = $request->withBody($stream);
		$router = new \Engine\JsonRpc2\JsonRpc2Router($request, new \Engine\Http\Response());
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($router) {
			$router->resolve($this->urls);
		});

		$stream = \Engine\Http\Stream::create(json_encode(['method' => 'user.delete']));
		$request = new \Engine\Http\ServerRequest('POST', '/');
		$request = $request->withBody($stream);
		$router = new \Engine\JsonRpc2\JsonRpc2Router($request, new \Engine\Http\Response());
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($router) {
			$router->resolve($this->urls);
		});

		$stream = \Engine\Http\Stream::create(json_encode(['method' => 'user.update']));
		$request = new \Engine\Http\ServerRequest('POST', '/');
		$request = $request->withBody($stream);
		$router = new \Engine\JsonRpc2\JsonRpc2Router($request, new \Engine\Http\Response());
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($router) {
			$router->resolve($this->urls);
		});
	}
}