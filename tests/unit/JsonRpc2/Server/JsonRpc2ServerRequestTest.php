<?php


class JsonRpc2ServerRequestTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testConstructSingleRequest() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}';
		$request = new \Engine\JsonRpc2\Server\Request($json);

		$this->tester->assertFalse($request->batch);
		$this->tester->assertEquals('{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}',
			$request->raw);
		$this->tester->assertNull($request->result);
		$this->tester->assertEquals('2.0', $request->json_rpc);
		$this->tester->assertEquals('login', $request->method);
		$this->tester->assertNull($request->error_code);
		$this->tester->assertNull($request->error_message);
		$this->tester->assertTrue(is_array($request->params));
		$this->tester->assertEquals('admin@example.com', $request->params['username']);
		$this->tester->assertEquals('secret', $request->params['password']);
		$this->tester->assertEquals('588a006eb89bf', $request->id);
		$this->assertEmpty($request->requests);
	}

	public function testConstructSingleRequestWithMetadata() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret","__METADATA":{"serverIp":"192.168.21.21","clientId":"testGate1"}},"id":"588a006eb89bf"}';
		$request = new \Engine\JsonRpc2\Server\Request($json);

		$this->tester->assertTrue(is_array($request->metadata));
		$this->tester->assertFalse(isset($request->params['__METADATA']));
		$this->tester->assertEquals('192.168.21.21', $request->metadata['serverIp']);
		$this->tester->assertEquals('testGate1', $request->metadata['clientId']);
	}

	public function testConstructSingleRequestWithMetadataNotInLastPlace() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","__METADATA":{"serverIp":"192.168.21.21","clientId":"testGate1"},"password":"secret"},"id":"588a006eb89bf"}';
		$request = new \Engine\JsonRpc2\Server\Request($json);

		$this->tester->assertTrue(empty($request->metadata));
		$this->tester->assertTrue(isset($request->params['__METADATA']));
		$this->tester->assertEquals('192.168.21.21', $request->params['__METADATA']['serverIp']);
		$this->tester->assertEquals('testGate1', $request->params['__METADATA']['clientId']);
	}

	public function testConstructEmptyJson() {
		$request = new \Engine\JsonRpc2\Server\Request('');

		$this->tester->assertFalse($request->batch);
		$this->tester->assertEmpty($request->raw);
		$this->tester->assertNull($request->result);
		$this->tester->assertEquals('2.0', $request->json_rpc);
		$this->tester->assertEquals(-32600, $request->error_code);
		$this->tester->assertEquals('Invalid Request.', $request->error_message);
		$this->tester->assertNull($request->method);
		$this->tester->assertNull($request->params);
		$this->tester->assertNull($request->id);
		$this->assertEmpty($request->requests);
	}

	public function testConstructBatchRequest() {
		$json = '[
			{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"},
			{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}
		]';

		$request = new \Engine\JsonRpc2\Server\Request($json);
		$this->tester->assertTrue($request->batch);
		$this->tester->assertEquals($json, $request->raw);
		$this->tester->assertNull($request->result);
		$this->tester->assertNull($request->json_rpc);
		$this->tester->assertNull($request->method);
		$this->tester->assertNull($request->error_code);
		$this->tester->assertNull($request->error_message);
		$this->tester->assertNull($request->params);
		$this->tester->assertNull($request->id);

		$this->tester->assertNotEmpty($request->requests);
		$this->tester->assertNotEmpty($request->requests[0]);
		$this->tester->assertNotEmpty($request->requests[1]);

		$request1 = $request->requests[0];
		$this->tester->assertFalse($request1->batch);
		$this->tester->assertEquals('{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}',
			$request1->raw);
		$this->tester->assertNull($request1->result);
		$this->tester->assertEquals('2.0', $request1->json_rpc);
		$this->tester->assertEquals('login', $request1->method);
		$this->tester->assertNull($request1->error_code);
		$this->tester->assertNull($request1->error_message);
		$this->tester->assertTrue(is_array($request1->params));
		$this->tester->assertEquals('admin@example.com', $request1->params['username']);
		$this->tester->assertEquals('secret', $request1->params['password']);
		$this->tester->assertEquals('588a006eb89bf', $request1->id);

		$request2 = $request->requests[1];
		$this->tester->assertFalse($request2->batch);
		$this->tester->assertEquals('{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}',
			$request2->raw);
		$this->tester->assertNull($request2->result);
		$this->tester->assertEquals('2.0', $request2->json_rpc);
		$this->tester->assertEquals('user.get', $request2->method);
		$this->tester->assertNull($request2->error_code);
		$this->tester->assertNull($request2->error_message);
		$this->tester->assertTrue(is_array($request2->params));
		$this->tester->assertEquals('admin', $request2->params['username']);
		$this->tester->assertEquals('588a0cre5545f', $request2->id);
	}

	public function testChechValidSuccessful() {
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$request->checkValid();
		$this->assertTrue($request->checkValid());

		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"}}');
		$this->assertTrue($request->checkValid());
	}

	public function testCheckValidWhenErrorAlreadySet() {
		$request = new \Engine\JsonRpc2\Server\Request('');
		$this->assertFalse($request->checkValid());
	}

	public function testCheckValidWhenMissingMethodOrJsonRpc() {
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$this->assertFalse($request->checkValid());
		$this->assertEquals(\Engine\JsonRpc2\Server\Request::ERROR_INVALID_REQUEST, $request->error_code);
		$this->assertEquals('Invalid Request.', $request->error_message);

		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$this->assertFalse($request->checkValid());
		$this->assertEquals(\Engine\JsonRpc2\Server\Request::ERROR_INVALID_REQUEST, $request->error_code);
		$this->assertEquals('Invalid Request.', $request->error_message);
	}

	public function testCheckValidWhenUseReservedPrefix() {
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"rpc.user.get","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$this->assertFalse($request->checkValid());
		$this->assertEquals(\Engine\JsonRpc2\Server\Request::ERROR_RESERVED_PREFIX, $request->error_code);
		$this->assertEquals("Illegal method name; Method cannot start with 'rpc.'", $request->error_message);
	}

	public function testCheckValidWhenUseIllegalMethodName() {
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"user!.get","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$this->assertFalse($request->checkValid());
		$this->assertEquals(\Engine\JsonRpc2\Server\Request::ERROR_INVALID_REQUEST, $request->error_code);
		$this->assertEquals('Invalid Request.', $request->error_message);
	}

	public function testCheckValidWhenMismatchedJsonRpvVersion() {
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"1.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$this->assertFalse($request->checkValid());
		$this->assertEquals(\Engine\JsonRpc2\Server\Request::ERROR_MISMATCHED_VERSION, $request->error_code);
		$this->assertEquals("Client/Server JSON-RPC version mismatch; Expected '2.0'", $request->error_message);
	}

	public function testIsBatch() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$this->tester->assertFalse($request->isBatch());

		$json = '[
			{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"},
			{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}
		]';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$this->tester->assertTrue($request->isBatch());
	}

	public function testIsNotify() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"}}';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$this->tester->assertTrue($request->isNotify());

		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$this->tester->assertFalse($request->isNotify());
	}

	public function testToResponseJsonSuccess() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$request->result = true;
		$this->tester->assertEquals('{"jsonrpc":"2.0","result":true,"id":"588a006eb89bf"}', $request->toResponseJSON());
	}

	public function testToResponseJsonFailure() {
		$json = '';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$this->tester->assertEquals('{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request."},"id":null}',
			$request->toResponseJSON());
	}
}