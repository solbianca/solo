<?php

use Engine\Base\Controller;
use Engine\Http\Request;
use Engine\Http\Response;

class JsonRpc2ServerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testConstruct() {
		$server = new \Engine\JsonRpc2\Server\Server();
		$this->tester->assertNull($server->exposedInstance);

		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$this->tester->assertInstanceOf(ExposedInstanceForTesting::class, $server->exposedInstance);

		$this->tester->expectException(\Engine\JsonRpc2\Exceptions\JsonRpcException::class, function () {
			new \Engine\JsonRpc2\Server\Server('not good at all');
		});
	}

	public function testSetExposedInstance() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$this->tester->assertInstanceOf(ExposedInstanceForTesting::class, $server->exposedInstance);

		$this->tester->expectException(\Engine\JsonRpc2\Exceptions\JsonRpcException::class, function () {
			new \Engine\JsonRpc2\Server\Server('not good at all');
		});
	}

	public function testMethodExist() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$this->tester->assertTrue($server->methodExists('action'));
		$this->tester->assertFalse($server->methodExists('notAction'));
	}

	public function testInvokeMethodSuccessful() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$method = 'action';
		$params = null;
		$result = $server->invokeMethod($method, $params);
		$this->tester->assertTrue($result);
	}

	public function testInvokeMethodWithParamsSuccessful() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$method = 'actionWithParams';
		$params = new stdClass();
		$params->id = 42;
		$params->name = 'John Doe';
		$result = $server->invokeMethod($method, $params);
		$this->tester->assertNotEmpty($result);
		$this->tester->assertEquals(42, $result['new_id']);
		$this->tester->assertEquals('John Doe', $result['new_name']);
	}

	public function testInvokeMethodWithWrongParametersNumber() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$method = 'actionWithWrongParams';
		$params = new stdClass();
		$params->id = 42;
		$this->tester->expectException(Exception::class, function () use ($server, $method, $params) {
			$server->invokeMethod($method, $params);
		});
	}

	public function testPrepareParams() {
		$server = new \Engine\JsonRpc2\Server\Server();
		$params = new stdClass();
		$params->id = 42;
		$params->name = 'John Doe';
		$result = $server->prepareParams($params);
		$this->tester->assertNotEmpty($result);
		$this->tester->assertEquals(42, $result['id']);
		$this->tester->assertEquals('John Doe', $result['name']);

		$result = $server->prepareParams(null);
		$this->tester->assertEquals([], $result);
	}

	public function testMakeRequest() {
		$json = '{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"}';
		$server = new \Engine\JsonRpc2\Server\Server();
		$request = $server->makeRequest($json);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Server\Request::class, $request);
	}

	public function testHandleSingleRequestSuccessful() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}');
		$method = 'action';
		$this->tester->assertEquals('{"jsonrpc":"2.0","result":true,"id":"588a0cre5545f"}',
			$server->handleSingleRequest($method, $request));
	}

	public function testHandleSingleRequestSeccessfulIfNotify() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$request = new \Engine\JsonRpc2\Server\Request('{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"}}');
		$method = 'action';
		$this->tester->assertNull($server->handleSingleRequest($method, $request));
	}

	public function testHandleBatchRequestSuccessful() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$method = 'action';
		$json = '[
			{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"},"id":"588a006eb89bf"},
			{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}
		]';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$result = $server->handleBatchRequest($method, $request);
		$this->tester->assertEquals('[{"jsonrpc":"2.0","result":true,"id":"588a006eb89bf"},{"jsonrpc":"2.0","result":true,"id":"588a0cre5545f"}]',
			$result);
	}

	public function testHandleBatchRequestSuccessfulIfNotify() {
		$server = new \Engine\JsonRpc2\Server\Server(new ExposedInstanceForTesting(new Request('POST', '/'),
			new Response()));
		$method = 'action';
		$json = '[
			{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"}},
			{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"},"id":"588a0cre5545f"}
		]';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$result = $server->handleBatchRequest($method, $request);
		$this->tester->assertEquals('[{"jsonrpc":"2.0","result":true,"id":"588a0cre5545f"}]', $result);

		$json = '[
			{"jsonrpc":"2.0","method":"login","params":{"username":"admin@example.com","password":"secret"}},
			{"jsonrpc":"2.0","method":"user.get","params":{"username":"admin"}}
		]';
		$request = new \Engine\JsonRpc2\Server\Request($json);
		$result = $server->handleBatchRequest($method, $request);
		$this->tester->assertNull($result);
	}
}

class ExposedInstanceForTesting extends Controller {
	public function beforeAction($action) {
	}

	public function action() {
		return true;
	}

	public function actionWithParams($params) {
		return ['new_id' => $params['id'], 'new_name' => $params['name']];
	}

	public function actionWithWrongParams($params) {
		return ['new_id' => $params['id'], 'new_name' => $params['name']];
	}


	protected function protectedAction() {
		return true;
	}
}