<?php

namespace Tests\unit\Http;


class HttpRouterTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Http\HttpRouter
	 */
	protected $router;

	protected $urls = [
		'/' => 'site::index',
		'/login' => 'site::login',
		'/auth' => 'site::auth',
		'/banners/create' => 'banners::create',
		'/banners/delete' => 'banners::delete',
		'/banners/:int' => 'banners::integer',
		'/banners/show/:string' => 'banners::string',
		'/banners/:any/show' => 'banners::any',
		'/banners/:int/:string/view/:any' => 'banners::view',
		'/banners/list/view' => 'Banners\List::view',
		'/catalog/:string/product/:any/:int' => 'Catalog\Product::view',
		'aliases' => [
			'epn.bz' => [
				'/user' => 'User::index',
				'/user/show' => 'User::view',
			],
			'tapicash.ru' => [
				'/make/money' => 'Tapicash\Money::make',
			],
		],
	];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->router = $this->createRouter();
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	/**
	 * @param string $method
	 * @param string $uri
	 * @param string $ns
	 * @param array $langs
	 * @return \Engine\Http\HttpRouter
	 */
	protected function createRouter($method = 'GET', $uri = '/', $ns = '', $langs = []) {
		$_SERVER['REQUEST_METHOD'] = $method;
		$_SERVER['REQUEST_URI'] = $uri;
		return new \Engine\Http\HttpRouter(new \Engine\Http\Request($method, $uri), new \Engine\Http\Response(), $ns,
			$langs);
	}

	public function testNamespace() {
		$request = new \Engine\Http\Request('GET', '/');
		$response = new \Engine\Http\Response();
		$router = new \Engine\Http\HttpRouter($request, $response, 'Tests\unit\Http\\');
		$urls = ['/' => 'Foo::index'];
		$result = $router->resolve($urls);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Tests\unit\Http\FooController', $result['controller']);
		$this->tester->assertEquals('indexAction', $result['action']);
		$this->assertEmpty($result['params']);
	}

	public function testCamelize() {
		$action = $this->router->camelize('one-two');
		$this->tester->assertEquals('oneTwo', $action);
		$this->tester->assertNotEquals('one-two', $action);
	}

	public function testPatterns() {
		$patterns = $this->router->patterns();
		$this->tester->assertArrayHasKey(':string', $patterns);
		$this->tester->assertArrayHasKey(':int', $patterns);
		$this->tester->assertArrayHasKey(':any', $patterns);

		$this->tester->assertEquals($patterns[':string'], '([^\/]+)');
		$this->tester->assertEquals($patterns[':int'], '([0-9]+)');
		$this->tester->assertEquals($patterns[':any'], '(.+)');
	}

	public function testResolveByConfigOnlySlash() {
		$path = '/';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Site', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByConfigSimpleUri() {
		$path = '/login';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Site', $result[0]);
		$this->tester->assertEquals('login', $result[1]);
		$this->assertEmpty($result[2]);

		$path = '/banners/create';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Banners', $result[0]);
		$this->tester->assertEquals('create', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByConfigWithIntParam() {
		$path = '/banners/20';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Banners', $result[0]);
		$this->tester->assertEquals('integer', $result[1]);
		$this->assertNotEmpty($result[2]);
		$this->assertEquals(1, count($result[2]));
		$this->assertEquals('20', $result[2][1]);
	}

	public function testResolveByConfigWithStringParam() {
		$path = '/banners/show/super-puper-banner';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Banners', $result[0]);
		$this->tester->assertEquals('string', $result[1]);
		$this->assertNotEmpty($result[2]);
		$this->assertEquals(1, count($result[2]));
		$this->assertEquals('super-puper-banner', $result[2][1]);
	}

	public function testResolveByConfigWithAnyParam() {
		$path = '/banners/qwe123/show/';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Banners', $result[0]);
		$this->tester->assertEquals('any', $result[1]);
		$this->assertNotEmpty($result[2]);
		$this->assertEquals(1, count($result[2]));
		$this->assertEquals('qwe123', $result[2][1]);
	}

	public function testResolveByConfigWithParams() {
		$path = '/banners/1/catalog/view/qwe123';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Banners', $result[0]);
		$this->tester->assertEquals('view', $result[1]);
		$this->assertNotEmpty($result[2]);
		$this->assertEquals(3, count($result[2]));
		$this->assertEquals('1', $result[2][1]);
		$this->assertEquals('catalog', $result[2][2]);
		$this->assertEquals('qwe123', $result[2][3]);
	}

	public function testResolveByConfigWithNamespace() {
		$path = '/banners/list/view';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Banners\List', $result[0]);
		$this->tester->assertEquals('view', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByConfigHardWay() {
		$path = '/catalog/animals/product/1elephant/42';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Catalog\Product', $result[0]);
		$this->tester->assertEquals('view', $result[1]);
		$this->assertNotEmpty($result[2]);
		$this->assertEquals(3, count($result[2]));
		$this->assertEquals('animals', $result[2][1]);
		$this->assertEquals('1elephant', $result[2][2]);
		$this->assertEquals('42', $result[2][3]);
	}

	public function testResolveByUriOnlySlash() {
		$path = '/';
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Site', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByUriWithoutAction() {
		$path = '/site/';
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Site', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByUri() {
		$path = '/site/index';
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Site', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByUriWithNamespace() {
		$path = '/catalog/product/view';
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Catalog\Product', $result[0]);
		$this->tester->assertEquals('view', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByUriWithNamespaceWithoutAction() {
		$path = parse_url('/catalog/product/');
		$path = $path['path'];
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Catalog\Product', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveByUriWithQueryParams() {
		$path = parse_url('/?id=1&city=moscow');
		$path = $path['path'];
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Site', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveSimpleRoute() {
		$result = $this->router->resolve($this->urls);
		$this->tester->assertNotEmpty($result);
		$this->tester->assertEquals('App\Controllers\SiteController', $result['controller']);
		$this->tester->assertEquals('indexAction', $result['action']);
		$this->tester->assertEmpty($result['params']);
	}

	public function testResolveWithParamsByConfig() {
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['REQUEST_URI'] = '/catalog/animals/product/1elephant/42';
		$router = new \Engine\Http\HttpRouter(new \Engine\Http\Request('GET', '/catalog/animals/product/1elephant/42'),
			new \Engine\Http\Response());
		$result = $router->resolve($this->urls);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('App\Controllers\Catalog\ProductController', $result['controller']);
		$this->tester->assertEquals('viewAction', $result['action']);
		$this->assertNotEmpty($result['params']);
		$this->tester->assertTrue((is_array($result['params'])));
		$this->assertEquals(3, count($result['params']));
		$this->assertEquals('animals', $result['params'][1]);
		$this->assertEquals('1elephant', $result['params'][2]);
		$this->assertEquals('42', $result['params'][3]);
	}

	public function testResolvePathWithDash() {
		$path = '/catalog/some-path/view-super-product';
		$result = $this->router->resolveByUri($path);
		$this->tester->assertTrue((is_array($result)));
		$this->tester->assertEquals('Catalog\SomePath', $result[0]);
		$this->tester->assertEquals('viewSuperProduct', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveUrlFromAlias() {
		$path = '/user';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertEquals('User', $result[0]);
		$this->tester->assertEquals('index', $result[1]);
		$this->assertEmpty($result[2]);

		$path = '/make/money';
		$result = $this->router->resolveByConfig($this->urls, $path);
		$this->tester->assertEquals('Tapicash\Money', $result[0]);
		$this->tester->assertEquals('make', $result[1]);
		$this->assertEmpty($result[2]);
	}

	public function testResolveWithLang() {
		$path = '/ru/user';
		$result = $this->createRouter('GET', $path, '', ['ru', 'en'])->resolve($this->urls);
		$this->tester->assertEquals('App\Controllers\UserController', $result['controller']);
		$this->tester->assertEquals('indexAction', $result['action']);
		$this->assertEmpty($result['params']);

		$path = '/en/login';
		$result = $this->createRouter('GET', $path, '', ['ru', 'en'])->resolve($this->urls);
		$this->tester->assertEquals('App\Controllers\SiteController', $result['controller']);
		$this->tester->assertEquals('loginAction', $result['action']);
		$this->assertEmpty($result['params']);

	}
}