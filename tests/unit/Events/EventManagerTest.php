<?php


namespace Tests\unit\Events;


use Engine\Events\EventManager;
use Engine\Events\Exceptions\EventManagerException;

class EventManagerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
		parent::_before();
	}

	public function createEventManager() {
		$eventManager = new EventManager();
		$eventManager->attach('foo', function () {
			Test::$count += 1;
			return 1;
		}, 1);
		$eventManager->attach('foo', function () {
			Test::$name = 'John Doe';
			return 0;
		});
		$eventManager->attach('foo', function () {
			Test::$count += 4;
			return 4;
		}, 4);

		return $eventManager;
	}

	protected function resetTestClass() {
		Test::$count = 0;
		Test::$name = null;
	}

	public function testAttach() {
		$eventManager = new EventManager();
		$result = $eventManager->attach('foo', function () {
			return 1;
		}, 1);
		$this->tester->assertTrue($result);
	}


	public function testDetach() {
		$eventManager = $this->createEventManager();

		$this->tester->assertTrue($eventManager->detach('foo', function () {
			return 0;
		}));
	}

	public function testTrigger() {
		$eventManager = $this->createEventManager();
		$eventManager->trigger('foo');
		$this->tester->assertEquals(5, Test::$count);
		$this->tester->assertEquals('John Doe', Test::$name);

		$this->resetTestClass();

		$eventManager->clearListeners('foo');
		$eventManager->trigger('foo');
		$this->tester->assertEquals(0, Test::$count);
		$this->tester->assertEquals(null, Test::$name);
	}

	public function badEventsProvider() {
		return [
			[1],
			[null],
			[true],
			[['string']],
			[new \stdClass()],
		];
	}

	/**
	 * @dataProvider badEventsProvider
	 */
	public function testBadEventOnTrigger($event) {
		$eventManager = $this->createEventManager();
		$this->tester->expectException(EventManagerException::class, function () use ($eventManager, $event) {
			$eventManager->trigger($event);
		});
	}
}

class Test {
	public static $count = 0;

	public static $name = null;
}