<?php


namespace Tests\unit\Events;


use Engine\Events\Event;

class EventTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
		parent::_before();
	}

	public function testSetAndGet() {
		$events = new Event('foo', 'bar', ['foo' => 'bar', 'fooBar' => 'buz'], true);
		$this->tester->assertEquals('foo', $events->getName());
		$this->tester->assertEquals('bar', $events->getTarget());
		$this->tester->assertEquals(['foo' => 'bar', 'fooBar' => 'buz'], $events->getParams());
		$this->tester->assertEquals('bar', $events->getParam('foo'));
		$this->tester->assertEquals(true, $events->isPropagationStopped());
	}
}