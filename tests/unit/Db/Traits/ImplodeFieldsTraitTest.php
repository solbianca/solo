<?php


class ImplodeFieldsTraitTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var ImplodeFieldsTraitTestObject
	 */
	protected $object;

	public function _before() {
		parent::_before();
		$this->object = new ImplodeFieldsTraitTestObject();
	}

	public function testSuccessImplode() {
		$this->tester->assertEquals('', $this->object->implodeFields([]));

		$result = $this->object->implodeFields(['foo', 'bar']);
		$this->tester->assertEquals('foo,bar', $result);

		$result = $this->object->implodeFields(['username' => 'foo', 'password' => 'bar']);
		$this->tester->assertEquals('foo as username,bar as password', $result);
	}

	public function testFailImplode() {
		try {
			$this->object->implodeFields('');
		} catch (\TypeError $e) {
			$pos = strpos($e->getMessage(),
				'Argument 1 passed to ImplodeFieldsTraitTestObject::implodeFields() must be of the type array');
			$this->tester->assertEquals(0, $pos);
		}

		try {
			$this->object->implodeFields(false);
		} catch (\TypeError $e) {
			$pos = strpos($e->getMessage(),
				'Argument 1 passed to ImplodeFieldsTraitTestObject::implodeFields() must be of the type array');
			$this->tester->assertEquals(0, $pos);
		}

		try {
			$this->object->implodeFields(null);
		} catch (\TypeError $e) {
			$pos = strpos($e->getMessage(),
				'Argument 1 passed to ImplodeFieldsTraitTestObject::implodeFields() must be of the type array');
			$this->tester->assertEquals(0, $pos);
		}

		try {
			$this->object->implodeFields('value');
		} catch (\TypeError $e) {
			$pos = strpos($e->getMessage(),
				'Argument 1 passed to ImplodeFieldsTraitTestObject::implodeFields() must be of the type array');
			$this->tester->assertEquals(0, $pos);
		}

		try {
			$this->object->implodeFields(123);
		} catch (\TypeError $e) {
			$pos = strpos($e->getMessage(),
				'Argument 1 passed to ImplodeFieldsTraitTestObject::implodeFields() must be of the type array');
			$this->tester->assertEquals(0, $pos);
		}
	}
}

class ImplodeFieldsTraitTestObject {
	use \Engine\Db\Traits\ImplodeFieldsTrait;
}