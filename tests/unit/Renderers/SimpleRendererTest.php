<?php


class SimpleRendererTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {

	}

	public function testConstructor() {
		$path = __DIR__;
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$this->tester->assertEquals($path, $renderer->getPath());
	}

	public function testRender() {
		$path = __DIR__ . '/../../_data/templates';
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$result = $renderer->render('template', ['foo' => 'foo!', 'bar' => 'bar!']);
		$response = '<!DOCTYPE html>
<html>
<head><title>My Webpage</title></head>
<body>
<h1>My Webpage</h1>
<p>foo!</p>
<p>bar!</p>
</body>
</html>';
		$this->tester->assertEquals($response, $result);

		$this->tester->expectException(\Exception::class, function () use ($renderer) {
			$renderer->render('not-exist');
		});
	}

	public function testFetch() {
		$path = __DIR__ . '/../../_data/templates';
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$result = $renderer->render('template', ['foo' => 'foo!', 'bar' => 'bar!']);
		$response = '<!DOCTYPE html>
<html>
<head><title>My Webpage</title></head>
<body>
<h1>My Webpage</h1>
<p>foo!</p>
<p>bar!</p>
</body>
</html>';
		$this->tester->assertEquals($response, $result);

		$this->tester->expectException(\Exception::class, function () use ($renderer) {
			$renderer->render('not-exist');
		});
	}

	public function testGetFileExtension() {
		$path = __DIR__ . '/../../_data/templates';
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$this->tester->assertEquals('.php', $renderer->getFileExtension());
	}

	public function testSetFileExtension() {
		$path = __DIR__ . '/../../_data/templates';
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$renderer->setFileExtension('.test');
		$this->tester->assertEquals('.test', $renderer->getFileExtension());
	}

	public function testGetPath() {
		$path = __DIR__ . '/../../_data/templates';
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$this->tester->assertEquals($path, $renderer->getPath());
	}

	public function testGetFullTemplateName() {
		$path = __DIR__ . '/../../_data/templates';
		$renderer = new \Engine\Renderers\SimpleRenderer($path, []);
		$this->tester->assertEquals('template.php', $renderer->getFullTemplateName('template'));
	}
}