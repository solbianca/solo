<?php


namespace Tests\unit\JsonApi;


use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Meta;
use Engine\JsonApi\Request;
use Engine\JsonApi\Resource;

class ResourceTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	protected function createResource(string $id = '1', string $type = 'test') {
		return new Resource($type, $id);
	}

	public function testSetAndGetIdAndType() {
		$resource = $this->createResource();
		$this->tester->assertEquals(['id' => '1', 'type' => 'test'], $resource->getArray());

		$resource->setId('42');
		$this->tester->assertEquals(['id' => '42', 'type' => 'test'], $resource->getArray());

		$resource->setType('foo');
		$this->tester->assertEquals(['id' => '42', 'type' => 'foo'], $resource->getArray());

		$resource = new Resource('test', '');
		$this->tester->assertEquals(['id' => '', 'type' => 'test'], $resource->getArray());

		$resource = new Resource('', '');
		$this->tester->expectException(JsonApiException::class, function () use ($resource) {
			$resource->getArray();
		});
	}

	public function paramsProvider() {
		$object = new \stdClass();
		$object->copyright = 'Copyright 2015 Example Corp.';
		$object->author = ['John Doe'];
		$request = new Request();
		$request->addParam('copyright', 'Copyright 2015 Example Corp.');
		$request->addParam('author', ['John Doe']);
		return [
			[
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$object,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$request,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				'test',
				'result' => 'exception',
				null,
			],
			[
				1,
				'result' => 'exception',
				null,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
			[
				true,
				'result' => 'exception',
				null,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
		];
	}

	/**
	 * @dataProvider paramsProvider
	 */
	public function testSetAndGetAttributes($params, $result, $toCompare) {
		$resource = $this->createResource();
		if ($result === 'success') {
			$resource->setAttributes($params);
			$this->tester->assertEquals($toCompare, $resource->getAttributes());
		}
		elseif ($result === 'exception') {
			$this->tester->expectException(JsonApiException::class, function () use ($resource, $params) {
				$resource->setAttributes($params);
			});
		}
	}

	public function testSetAttributesByEmptyArray() {
		$resource = $this->createResource();
		$resource->setAttributes([]);
		$result = $resource->getArray();
		$this->tester->assertTrue(!isset($result['attributes']));
	}

	public function testAddAttribute() {
		$resource = $this->createResource();
		$this->tester->assertEquals(['id' => '1', 'type' => 'test'], $resource->getArray());

		$resource->addAttribute('foo', 'bar');
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$resource->getArray());

		$resource->setAttributes(['foo' => 'buz']);
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'buz']],
			$resource->getArray());
	}

	public function metaProvider() {
		$metaArray = ['copyright' => 'Copyright 2015 Example Corp.', 'author' => ['John Doe']];
		$metaObj = (new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.', 'author' => ['John Doe']]);
		$metaStdObject = new \stdClass();
		$metaStdObject->copyright = 'Copyright 2015 Example Corp.';
		$metaStdObject->author = ['John Doe'];
		return [
			[
				$metaArray,
				'result' => 'success',
				$metaArray,
			],
			[
				$metaObj,
				'result' => 'success',
				$metaArray,
			],
			[
				$metaStdObject,
				'result' => 'success',
				$metaArray,
			],
			[
				1,
				'result' => 'exception',
			],
			[
				'test',
				'result' => 'exception',
			],
			[
				true,
				'result' => 'exception',
			],
			[
				null,
				'result' => 'exception',
			],
		];
	}

	/**
	 * @dataProvider metaProvider
	 */
	public function testSetAndGetMeta($params, $result, $toCompare = null) {
		$resource = $this->createResource();

		if ($result === 'success') {
			$resource->setMeta($params);
			$this->tester->assertEquals($toCompare, $resource->getMeta()->getArray());
		}
		elseif ($result === 'exception') {
			$this->tester->expectException(JsonApiException::class, function () use ($resource, $params) {
				$resource->setMeta($params);
			});
		}
	}

	public function testSetMetaAsEmptyArray() {
		$resource = $this->createResource();
		$resource->setMeta([]);
		$result = $resource->getArray();
		$this->tester->assertTrue(!isset($result['meta']));
	}
}