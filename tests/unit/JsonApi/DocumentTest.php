<?php


namespace Tests\unit\JsonApi;


use Engine\JsonApi\Document;
use Engine\JsonApi\Error;
use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Meta;
use Engine\JsonApi\Request;
use Engine\JsonApi\Resource;

class DocumentTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function testSetAndGetResult() {
		$document = (new Document())->setResult(true);
		$this->tester->assertEquals(true, $document->getResult());

		$document = (new Document())->setResult(false);
		$this->tester->assertEquals(false, $document->getResult());
	}

	public function testSetPrimaryDataType() {
		$document = (new Document());
		$this->assertEquals(['data' => null, 'result' => true], $document->getAsArray());

		$document->setPrimaryDataType(2);
		$this->assertEquals(['data' => [], 'result' => true], $document->getAsArray());

		$document->setPrimaryDataType(1);
		$this->assertEquals(['data' => null, 'result' => true], $document->getAsArray());

		$this->tester->expectException(JsonApiException::class, function () use ($document) {
			$document->setPrimaryDataType(3);
		});
	}

	public function testAddSetAndGetResource() {
		$document = (new Document())->addResource(['foo' => 'bar'], 'test', '1');
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getResources()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = (new Document())->addResource(['foo' => 'bar'], 'test');
		$this->tester->assertEquals(['id' => '', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getResources()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = (new Document())->addResource(new Resource('test'));
		$this->tester->assertEquals(['id' => '', 'type' => 'test'],
			$document->getResources()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = (new Document())->addResource(new Resource('test', '1'));
		$this->tester->assertEquals(['id' => '1', 'type' => 'test'],
			$document->getResources()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = (new Document())->addResource(['foo' => 'bar'], 'test');
		$this->tester->assertEquals(['id' => '', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getResources()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = (new Document())->addResource(['foo' => 'bar'], 'test', '1');
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getResources()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->addResource(['foo' => 'bar']);
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->addResource(new Resource())->getAsArray();
		});

		$document = (new Document())
			->addResource(['foo' => 'bar'], 'test', '1')
			->addResource(['foo' => 'buz'], 'test', '2');
		$result = $document->getAsArray();
		$this->tester->assertEquals([
			['type' => 'test', 'id' => 1, 'attributes' => ['foo' => 'bar']],
			['type' => 'test', 'id' => 2, 'attributes' => ['foo' => 'buz']],
		], $result['data']);
		$this->tester->assertTrue($document->getResult());
	}

	public function testSetMeta() {
		$document = (new Document())->setMeta(['foo', 'bar']);
		$this->tester->assertEquals(['foo', 'bar'], $document->getMeta()->getArray());

		$document = (new Document())->setMeta((new Meta())->setParams(['foo', 'bar']));
		$this->tester->assertEquals(['foo', 'bar'], $document->getMeta()->getArray());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setMeta((new Meta())->setParams(1));
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setMeta((new Meta())->setParams('test'));
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setMeta((new Meta())->setParams(true));
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setMeta((new Meta())->setParams(null));
		});

		$document = (new Document())->setMeta((new Meta())->setParams([]));
		$this->tester->assertEquals([], $document->getMeta()->getArray());
	}

	public function testSetRequest() {
		$document = (new Document())->setRequest(['foo', 'bar']);
		$this->tester->assertEquals(['foo', 'bar'], $document->getRequest()->getArray());

		$document = (new Document())->setRequest((new Request())->setParams(['foo', 'bar']));
		$this->tester->assertEquals(['foo', 'bar'], $document->getRequest()->getArray());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setRequest((new Request())->setParams(1));
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setRequest((new Request())->setParams('test'));
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setRequest((new Request())->setParams(true));
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->setRequest((new Request())->setParams(null));
		});

		$document = (new Document())->setRequest((new Request())->setParams([]));
		$this->tester->assertEquals([], $document->getRequest()->getArray());
	}

	public function testAddAndGetErrors() {
		$document = (new Document())->addError(['foo' => 'bar']);
		$this->tester->assertEquals(['foo' => 'bar'], $document->getErrors()[0]->getArray());
		$this->tester->assertFalse($document->getResult());

		$document = (new Document())->addError((new Error())->setParams(['foo' => 'bar']));
		$this->tester->assertEquals(['foo' => 'bar'], $document->getErrors()[0]->getArray());
		$this->tester->assertFalse($document->getResult());

		$document = (new Document())
			->addError(['foo' => 'bar'])
			->addError(['foo' => 'buz']);
		$result = $document->getAsArray();
		$this->tester->assertEquals([
			['foo' => 'bar'],
			['foo' => 'buz'],
		], $result['errors']);
		$this->tester->assertFalse($document->getResult());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->addError(1);
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->addError(true);
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->addError(null);
		});

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())->addError('test');
		});

		$document = (new Document())->addError([]);
		$this->tester->assertEquals([], $document->getErrors()[0]->getArray());
		$this->tester->assertEquals([[]], $document->getAsArray()['errors']);
	}

	public function testClearErrors() {
		$document = (new Document())->addError(['foo' => 'bar']);
		$this->tester->assertEquals(['foo' => 'bar'], $document->getErrors()[0]->getArray());
		$document->clearErrors();
		$this->tester->assertEmpty($document->getErrors());
	}

	public function testClearResources() {
		$document = (new Document())->addResource(['foo' => 'bar'], 'test', '1');
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getResources()[0]->getArray());
		$document->clearResources();
		$this->tester->assertEmpty($document->getResources());
	}

	public function testClearMeta() {
		$document = (new Document())->setMeta(['foo', 'bar']);
		$this->tester->assertEquals(['foo', 'bar'], $document->getMeta()->getArray());
		$document->clearMeta();
		$this->tester->assertEmpty($document->getMeta());
	}

	public function testClearRequest() {
		$document = (new Document())->setRequest(['foo', 'bar']);
		$this->tester->assertEquals(['foo', 'bar'], $document->getRequest()->getArray());
		$document->clearRequest();
		$this->tester->assertEmpty($document->getRequest());
	}

	public function testObjectAsArray() {
		$document = (new Document())
			->setResult(true)
			->setResource(['foo' => 'bar'], 'test', '1')
			->setRequest(['name' => 'John Doe'])
			->setMeta(['copyright' => 'Copyright 2015 Example Corp.'])
			->addInclude(['foo' => 'bar'], 'test', '1')
			->addInclude(new Resource('test', '2'));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					'type' => 'test',
					'id' => '1',
					'attributes' => [
						'foo' => 'bar',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],
				'included' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
					],
					[
						'type' => 'test',
						'id' => '2',
					],
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->setResource((new Resource('test', '1'))->setAttributes(['foo' => 'bar']))
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					'type' => 'test',
					'id' => '1',
					'attributes' => [
						'foo' => 'bar',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->setResource(
				(new Resource('test', '1'))
					->setAttributes(['foo' => 'bar'])
					->setMeta((['foo' => 'buz']))
			)
			->setResource(['foo' => 'buz'], 'test')
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					'type' => 'test',
					'id' => '',
					'attributes' => [
						'foo' => 'buz',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->addError((new Error())->setParams(['foo' => 'bar']))
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => false,
				'errors' => [
					[
						'foo' => 'bar',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => null,
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->setPrimaryDataType(1)
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => null,
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())->setRequest((new Request())->setParams(['name' => 'John Doe']));
		$this->tester->assertEquals(['data' => null, 'result' => true, 'request' => ['name' => 'John Doe']],
			$document->getAsArray());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())
				->addResource((new Resource('test', '1'))->setAttributes(['foo' => 'bar']))
				->addError((new Error())->setParams(['foo' => 'bar']));
		});
	}

	public function testCollectionAsArray() {
		$document = (new Document())
			->setResult(true)
			->addResource(['foo' => 'bar'], 'test', '1')
			->setRequest(['name' => 'John Doe'])
			->setMeta(['copyright' => 'Copyright 2015 Example Corp.'])
			->addInclude(['foo' => 'bar'], 'test', '1')
			->addInclude(new Resource('test', '2'));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],
				'included' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
					],
					[
						'type' => 'test',
						'id' => '2',
					],
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->addResource((new Resource('test', '1'))->setAttributes(['foo' => 'bar']))
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->addResource(
				(new Resource('test', '1'))
					->setAttributes(['foo' => 'bar'])
					->setMeta((['foo' => 'buz']))
			)
			->addResource(['foo' => 'buz'], 'test')
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
						'meta' => [
							'foo' => 'buz',
						],
					],
					[
						'type' => 'test',
						'id' => '',
						'attributes' => [
							'foo' => 'buz',
						],
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->addError((new Error())->setParams(['foo' => 'bar']))
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => false,
				'errors' => [
					[
						'foo' => 'bar',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())
			->setPrimaryDataType(2)
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);

		$document = (new Document())->setRequest((new Request())->setParams(['name' => 'John Doe']));
		$this->tester->assertEquals(['data' => null, 'result' => true, 'request' => ['name' => 'John Doe']],
			$document->getAsArray());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = (new Document())
				->addResource((new Resource('test', '1'))->setAttributes(['foo' => 'bar']))
				->addError((new Error())->setParams(['foo' => 'bar']));
		});
	}

	public function testObjectAndCollectionAsArray() {
		$document = (new Document())
			->setResult(true)
			->addResource(['foo' => 'bar'], 'test', '1');

		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
					],
				],
			],
			$document->getAsArray()
		);

		$document->addResource(['foo' => 'buz'], 'test', '2');
		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					[
						'type' => 'test',
						'id' => '1',
						'attributes' => [
							'foo' => 'bar',
						],
					],
					[
						'type' => 'test',
						'id' => '2',
						'attributes' => [
							'foo' => 'buz',
						],
					],
				],
			],
			$document->getAsArray()
		);

		$document->setResource(['foo' => 'buz bar'], 'test', '3');
		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					'type' => 'test',
					'id' => '3',
					'attributes' => [
						'foo' => 'buz bar',
					],
				],
			],
			$document->getAsArray()
		);

		$document->addResource(['foo' => 'buz buz'], 'test', '4');
		$this->tester->assertEquals(
			[
				'result' => true,
				'data' => [
					[
						'type' => 'test',
						'id' => '3',
						'attributes' => [
							'foo' => 'buz bar',
						],
					],
					[
						'type' => 'test',
						'id' => '4',
						'attributes' => [
							'foo' => 'buz buz',
						],
					],
				],
			],
			$document->getAsArray()
		);
	}

	public function testObjectAsJason() {
		$document = (new Document())
			->setResult(true)
			->setResource(['foo' => 'bar'], 'test', '1')
			->setRequest(['name' => 'John Doe'])
			->setMeta(['copyright' => 'Copyright 2015 Example Corp.'])
			->addInclude(['foo' => 'bar'], 'test', '1')
			->addInclude(new Resource('test', '2'));

		$this->tester->assertEquals('{"data":{"type":"test","id":"1","attributes":{"foo":"bar"}},"meta":{"copyright":"Copyright 2015 Example Corp."},"result":true,"request":{"name":"John Doe"},"included":[{"type":"test","id":"1","attributes":{"foo":"bar"}},{"type":"test","id":"2"}]}',
			$document->getAsJson());

		$document = (new Document())
			->addError((new Error())->setParams(['foo' => 'bar']))
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => false,
				'errors' => [
					[
						'foo' => 'bar',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);
		$this->tester->assertEquals('{"errors":[{"foo":"bar"}],"meta":{"copyright":"Copyright 2015 Example Corp."},"result":false,"request":{"name":"John Doe"}}',
			$document->getAsJson());
	}

	public function testCollectionAsJason() {
		$document = (new Document())
			->setResult(true)
			->addResource(['foo' => 'bar'], 'test', '1')
			->setRequest(['name' => 'John Doe'])
			->setMeta(['copyright' => 'Copyright 2015 Example Corp.'])
			->addInclude(['foo' => 'bar'], 'test', '1')
			->addInclude(new Resource('test', '2'));

		$this->tester->assertEquals('{"data":[{"type":"test","id":"1","attributes":{"foo":"bar"}}],"meta":{"copyright":"Copyright 2015 Example Corp."},"result":true,"request":{"name":"John Doe"},"included":[{"type":"test","id":"1","attributes":{"foo":"bar"}},{"type":"test","id":"2"}]}',
			$document->getAsJson());

		$document = (new Document())
			->addError((new Error())->setParams(['foo' => 'bar']))
			->setRequest((new Request())->setParams(['name' => 'John Doe']))
			->setMeta((new Meta())->setParams(['copyright' => 'Copyright 2015 Example Corp.']));

		$this->tester->assertEquals(
			[
				'result' => false,
				'errors' => [
					[
						'foo' => 'bar',
					],
				],
				'meta' => [
					'copyright' => 'Copyright 2015 Example Corp.',
				],
				'request' => [
					'name' => 'John Doe',
				],

			],
			$document->getAsArray()
		);
		$this->tester->assertEquals('{"errors":[{"foo":"bar"}],"meta":{"copyright":"Copyright 2015 Example Corp."},"result":false,"request":{"name":"John Doe"}}',
			$document->getAsJson());
	}

	/**
	 * @return \Engine\JsonApi\Interfaces\DocumentInterface
	 */
	protected function createDocument() {
		return (new Document())->addResource(new Resource('test', '1'));
	}

	public function testAddSetAndGetIncluded() {
		$document = $this->createDocument()->addInclude(['foo' => 'bar'], 'test', '1');
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getIncluded()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = $this->createDocument()->addInclude(['foo' => 'bar'], 'test');
		$this->tester->assertEquals(['id' => '', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getIncluded()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = $this->createDocument()->addInclude(new Resource('test'));
		$this->tester->assertEquals(['id' => '', 'type' => 'test'],
			$document->getIncluded()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = $this->createDocument()->addInclude(new Resource('test', '1'));
		$this->tester->assertEquals(['id' => '1', 'type' => 'test'],
			$document->getIncluded()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = $this->createDocument()->addInclude(['foo' => 'bar'], 'test');
		$this->tester->assertEquals(['id' => '', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getIncluded()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$document = $this->createDocument()->addInclude(['foo' => 'bar'], 'test', '1');
		$this->tester->assertEquals(['id' => '1', 'type' => 'test', 'attributes' => ['foo' => 'bar']],
			$document->getIncluded()[0]->getArray());
		$this->tester->assertTrue($document->getResult());

		$this->tester->expectException(JsonApiException::class, function () {
			$document = $document = $this->createDocument()->addInclude(['foo' => 'bar']);
		});

		$document = $this->createDocument()
			->addInclude(['foo' => 'bar'], 'test', '1')
			->addInclude(['foo' => 'buz'], 'test', '2');
		$result = $document->getAsArray();
		$this->tester->assertEquals([
			['type' => 'test', 'id' => 1, 'attributes' => ['foo' => 'bar']],
			['type' => 'test', 'id' => 2, 'attributes' => ['foo' => 'buz']],
		], $result['included']);
		$this->tester->assertTrue($document->getResult());
	}
}