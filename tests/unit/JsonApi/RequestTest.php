<?php


namespace Tests\unit\JsonApi;


use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Request;

class RequestTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function paramsProvider() {
		$object = new \stdClass();
		$object->copyright = 'Copyright 2015 Example Corp.';
		$object->author = ['John Doe'];
		$request = new Request();
		$request->addParam('copyright', 'Copyright 2015 Example Corp.');
		$request->addParam('author', ['John Doe']);
		return [
			[
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$object,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$request,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				'test',
				'result' => 'exception',
				null,
			],
			[
				1,
				'result' => 'exception',
				null,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
			[
				true,
				'result' => 'exception',
				null,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
			[
				[],
				'result' => 'success',
				[],
			],
		];
	}

	/**
	 * @dataProvider paramsProvider
	 */
	public function testRequest($params, $result, $toCompare) {
		$request = new Request();
		if ($result === 'success') {
			$request->setParams($params);
			$this->tester->assertEquals($toCompare, $request->getArray());
		}
		elseif ($result === 'exception') {
			$this->tester->expectException(JsonApiException::class, function () use ($request, $params) {
				$request->setParams($params);
			});
		}
	}

	public function testAddParam() {
		$request = new Request();
		$request->addParam('foo', 'bar');
		$this->tester->assertEquals(['foo' => 'bar'], $request->getArray());
	}
}