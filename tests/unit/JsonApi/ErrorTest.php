<?php


namespace Tests\unit\JsonApi;


use Engine\JsonApi\Error;
use Engine\JsonApi\Exceptions\JsonApiException;

class ErrorTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function paramsProvider() {
		$object = new \stdClass();
		$object->copyright = 'Copyright 2015 Example Corp.';
		$object->author = ['John Doe'];
		$error = new Error();
		$error->addParam('copyright', 'Copyright 2015 Example Corp.');
		$error->addParam('author', ['John Doe']);
		return [
			[
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$object,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$error,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				'test',
				'result' => 'exception',
				'test',
			],
			[
				1,
				'result' => 'exception',
				1,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
			[
				true,
				'result' => 'exception',
				true,
			],
			[
				[],
				'result' => 'success',
				[],
			],
		];
	}

	/**
	 * @dataProvider paramsProvider
	 */
	public function testRequest($params, $result, $toCompare) {
		$error = new Error();
		if ($result === 'success') {
			$error->setParams($params);
			$this->tester->assertEquals($toCompare, $error->getArray());
		}
		elseif ($result === 'exception') {
			$this->tester->expectException(JsonApiException::class, function () use ($error, $params) {
				$error->setParams($params);
			});
		}
	}

	public function testAddParam() {
		$error = new Error();
		$error->addParam('foo', 'bar');
		$this->tester->assertEquals(['foo' => 'bar'], $error->getArray());
	}
}