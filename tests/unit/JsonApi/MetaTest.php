<?php


namespace Tests\unit\JsonApi;


use Engine\JsonApi\Exceptions\JsonApiException;
use Engine\JsonApi\Meta;

class MetaTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function paramsProvider() {
		$object = new \stdClass();
		$object->copyright = 'Copyright 2015 Example Corp.';
		$object->author = ['John Doe'];
		$meta = new Meta();
		$meta->addParam('copyright', 'Copyright 2015 Example Corp.');
		$meta->addParam('author', ['John Doe']);
		return [
			[
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$object,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				$meta,
				'result' => 'success',
				[
					'copyright' => 'Copyright 2015 Example Corp.',
					'author' => ['John Doe'],
				],
			],
			[
				'test',
				'result' => 'exception',
				null,
			],
			[
				1,
				'result' => 'exception',
				null,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
			[
				true,
				'result' => 'exception',
				null,
			],
			[
				null,
				'result' => 'exception',
				null,
			],
			[
				[],
				'result' => 'success',
				[],
			],
		];
	}

	/**
	 * @dataProvider paramsProvider
	 */
	public function testMeta($params, $result, $toCompare) {
		$meta = new Meta();
		if ($result === 'success') {
			$meta->setParams($params);
			$this->tester->assertEquals($toCompare, $meta->getArray());
		}
		elseif ($result === 'exception') {
			$this->tester->expectException(JsonApiException::class, function () use ($meta, $params) {
				$meta->setParams($params);
			});
		}
	}

	public function testAddParam() {
		$meta = new Meta();
		$meta->addParam('foo', 'bar');
		$this->tester->assertEquals(['foo' => 'bar'], $meta->getArray());
	}
}