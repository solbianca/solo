<?php


namespace Tests\unit\Middleware;


use Engine\Middleware\MiddlewareRunner;
use Engine\Middleware\MiddlewareRunnerFactory;

class MiddlewareRunnerFactoryTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testCreate() {
		$queue = [
			'foo',
			'bar',
		];
		$factory = new MiddlewareRunnerFactory();
		$middleware = $factory->createRunner($queue);
		$this->tester->assertInstanceOf(MiddlewareRunner::class, $middleware);
	}
}