<?php


namespace Tests\unit\Middleware\Middlewares\Controllers;


use Engine\Base\WebController;

class FooController extends WebController {
	public function indexAction() {
		$this->response->getBody()->write('FooController' . '::indexAction');
		return $this->response;
	}
}