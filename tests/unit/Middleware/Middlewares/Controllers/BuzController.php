<?php


namespace Tests\unit\Middleware\Middlewares\Controllers;


use Engine\Base\WebController;

class BuzController extends WebController {

	protected $rules = [['allow' => false, 'roles' => ['guest']]];

	public function indexAction() {
		$this->response->getBody()->write('BuzController' . '::indexAction');
		return $this->response;
	}
}