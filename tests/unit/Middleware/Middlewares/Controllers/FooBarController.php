<?php


namespace Tests\unit\Middleware\Middlewares\Controllers;


use Engine\Base\Controller;

class FooBarController extends Controller {
	public function indexAction() {
		$this->response->getBody()->write('FooBarController' . '::indexAction');
		return $this->response;
	}
}