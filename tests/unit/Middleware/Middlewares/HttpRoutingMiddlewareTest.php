<?php


namespace Tests\unit\Middleware\Middlewares;


use Engine\Engine;
use Engine\Middleware\Middlewares\HttpRoutingMiddleware;

class HttpRoutingMiddlewareTest extends Base {
	protected $urls = [
		'/' => 'Foo::index',
		'/bar' => 'Bar::index',
		'/foo/bar' => 'Foo::bar',
		'/foo-bar' => 'FooBar::index',
		'/buz' => 'Buz::index',
		'aliases' => [
			'epn.bz' => [
				'/user' => 'Foo::index',
			],
		],
	];

	protected $namespace = 'Tests\unit\Middleware\Middlewares\Controllers\\';

	public function testSuccess() {
		$response = $this->execute(
			[
				(new HttpRoutingMiddleware())->setUrls($this->urls)->setRouterNamespace($this->namespace),
			],
			'',
			'/',
			[]
		);
		$body = (string)$response->getBody();
		$this->tester->assertEquals(200, $response->getStatusCode());
		$this->tester->assertEquals('FooController::indexAction', $body);
	}

	public function testBadRouting() {
		$response = $this->execute(
			[
				(new HttpRoutingMiddleware())->setUrls($this->urls)->setRouterNamespace($this->namespace),
			],
			'',
			'/bad-routing',
			[]
		);
		$this->tester->assertEquals(404, $response->getStatusCode());
	}

	public function testControllerNotExist() {
		$response = $this->execute(
			[
				(new HttpRoutingMiddleware())->setUrls($this->urls)->setRouterNamespace($this->namespace),
			],
			'',
			'/bar',
			[]
		);
		$this->tester->assertEquals(404, $response->getStatusCode());
	}

	public function testMethodNotExist() {
		$response = $this->execute(
			[
				(new HttpRoutingMiddleware())->setUrls($this->urls)->setRouterNamespace($this->namespace),
			],
			'',
			'/foo/bar',
			[]
		);
		$this->tester->assertEquals(404, $response->getStatusCode());
	}

	public function testControllerClassInstanceOff() {
		$response = $this->execute(
			[
				(new HttpRoutingMiddleware())->setUrls($this->urls)->setRouterNamespace($this->namespace),
			],
			'',
			'/foo-bar',
			[]
		);
		$this->tester->assertEquals(404, $response->getStatusCode());
	}

	public function testCheckAccessNotAllowed() {
		Engine::$app->user->setIdentity(['id' => 1, 'role' => 'guest']);
		$response = $this->execute(
			[
				(new HttpRoutingMiddleware())->setUrls($this->urls)->setRouterNamespace($this->namespace),
			],
			'',
			'/buz',
			[]
		);
		$this->tester->assertEquals(405, $response->getStatusCode());
	}
}
