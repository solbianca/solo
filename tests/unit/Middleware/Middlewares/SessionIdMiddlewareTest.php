<?php


namespace Tests\unit\Middleware\Middlewares;


use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Http\Response;
use Engine\Middleware\Middlewares\SessionIdMiddleware;

class SessionIdMiddlewareTest extends Base {

	public function testSessionIdFromCookies() {
		$request = ($this->request())->withCookieParams([SessionIdMiddleware::COOKIE_NAME => 'test']);
		$response = $this->execute(
			[
				(new SessionIdMiddleware()),
				function (ServerRequestInterface $request, Response $response, $next) {
					$ssid = $request->getAttribute(SessionIdMiddleware::KEY);
					$response->getBody()->write(json_encode([
						SessionIdMiddleware::KEY => $ssid,
					]));
					return $response;
				},
			],
			'GET',
			'',
			[],
			$request
		);
		$body = json_decode((string)$response->getBody(), true);
		$this->tester->assertEquals('test', $body[SessionIdMiddleware::KEY]);
	}

	public function testSessionIdFromHeader() {
		$response = $this->execute(
			[
				(new SessionIdMiddleware()),
				function (ServerRequestInterface $request, Response $response, $next) {
					$ssid = $request->getAttribute(SessionIdMiddleware::KEY);
					$response->getBody()->write(json_encode([
						SessionIdMiddleware::KEY => $ssid,
					]));
					return $response;
				},
			],
			'GET',
			'',
			['epn-ssid' => 'test']
		);
		$body = json_decode((string)$response->getBody(), true);
		$this->tester->assertEquals('test', $body[SessionIdMiddleware::KEY]);
	}
}