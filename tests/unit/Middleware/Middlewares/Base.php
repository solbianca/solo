<?php

namespace Tests\unit\Middleware\Middlewares;

use Engine\Http\Factory\MessageFactory;
use Engine\Http\Factory\StreamFactory;
use Engine\Http\Interfaces\ResponseInterface;
use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Http\Response;
use Engine\Http\ServerRequest;
use Engine\Http\Stream;
use Engine\Middleware\MiddlewareRunner;

class Base extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @param string $method
	 * @param string $uri
	 * @param array $headers
	 * @param array $server
	 * @return ServerRequest
	 */
	protected function request($method = 'GET', $uri = '', array $headers = [], array $server = []) {
		return new ServerRequest($method, $uri, $headers, 'php://temp', '1.1', $server);
	}

	/**
	 * @param array $headers
	 * @return Response
	 */
	protected function response(array $headers = []) {
		return (new MessageFactory())->createResponse(200, null, $headers, '');
	}

	/**
	 * @param string $content
	 *
	 * @return Stream
	 */
	protected function stream($content = '') {
		$stream = (new StreamFactory())->createStreamFromFile('php://temp', 'r+');
		if ($content) {
			$stream->write($content);
		}
		return $stream;
	}

	/**
	 * @param callable[] $middlewares
	 * @param string $method
	 * @param string $url
	 * @param array $headers
	 * @param ServerRequestInterface $request
	 *
	 * @return ResponseInterface
	 */
	protected function execute(
		array $middlewares,
		$method = 'GET',
		$url = '',
		array $headers = [],
		ServerRequestInterface $request = null,
		Response $response = null
	) {
		if (null === $request) {
			$request = $this->request($method, $url, $headers);
		}
		if (null === $response) {
			$response = $this->response();
		}
		$runner = new MiddlewareRunner($middlewares);
		return $runner($request, $response);
	}
}