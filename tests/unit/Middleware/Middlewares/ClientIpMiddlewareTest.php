<?php

namespace Tests\unit\Middleware\Middlewares;

use Engine\Middleware\Middlewares\ClientIpMiddleware;

class ClientIpMiddlewareTest extends Base {

	public function ipsProvider() {
		return [
			[
				[
					'Client-Ip' => 'unknow,123.456.789.10,123.234.123.10',
					'X-Forwarded' => '123.234.123.10',
				],
				['123.234.123.10'],
				'123.234.123.10',
			],
			[
				[
					'Client-Ip' => 'unknow,123.456.789.10,123.234.123.10',
					'X-Forwarded' => '123.234.123.11',
				],
				['123.234.123.10', '123.234.123.11'],
				'123.234.123.10',
			],
		];
	}

	/**
	 * @dataProvider ipsProvider
	 */
	public function testIps(array $headers, array $CLIENT_IPS, $CLIENT_IP) {
		$response = $this->execute(
			[
				(new ClientIpMiddleware())->headers(),
				function ($request, $response, $next) {
					$response->getBody()->write(json_encode([
						'CLIENT_IPS' => ClientIpMiddleware::getIps($request),
						'CLIENT_IP' => ClientIpMiddleware::getIp($request),
					]));
					return $response;
				},
			],
			'GET',
			'',
			$headers
		);
		$body = json_decode((string)$response->getBody(), true);
		$this->assertEquals($body['CLIENT_IPS'], $CLIENT_IPS);
		$this->assertEquals($body['CLIENT_IP'], $CLIENT_IP);
	}
}