<?php


namespace Tests\unit\Middleware\Middlewares;


use Engine\Middleware\MiddlewareRunner;
use Engine\Middleware\Middlewares\CsrfMiddleware;
use Engine\Middleware\Middlewares\SessionIdMiddleware;

class CsrfMiddlewareTest extends Base {

	public function testByGetRequestWithoutAutounsert() {
		$request = ($this->request())
			->withMethod('GET')
			->withAttribute(SessionIdMiddleware::KEY, 'test');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware()),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$body = (string)$response->getBody();
		$this->tester->assertEquals(200, $response->getStatusCode());
		$this->tester->assertEquals('<form method="post"><input type="text" name="firstname"></form>', $body);

	}

	public function testByGetRequestWithWithAutoinsert() {
		$request = ($this->request())
			->withMethod('GET')
			->withAttribute(SessionIdMiddleware::KEY, 'test');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$body = (string)$response->getBody();
		$this->tester->assertEquals(200, $response->getStatusCode());
		$this->tester->assertEquals('<form method="post"><input type="text" name="csrf_token" class="csrf_token" hidden value="f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770"><input type="text" name="firstname"></form>',
			$body);
	}

	public function testPostRequestWithTokenInFrom() {
		$request = ($this->request())
			->withMethod('POST')
			->withParsedBody(['csrf_token' => 'f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770'])
			->withAttribute(SessionIdMiddleware::KEY, 'test');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="csrf_token" class="csrf_token" hidden value="f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(200, $response->getStatusCode());
	}

	public function testPostRequestWithTokenInHeader() {
		$request = ($this->request())
			->withMethod('POST')
			->withHeader(CsrfMiddleware::CSRF_TOKEN_HEADER,
				'f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770')
			->withAttribute(SessionIdMiddleware::KEY, 'test');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(200, $response->getStatusCode());
	}

	public function testPostRequestWithoutTokenInForm() {
		$request = ($this->request())
			->withMethod('POST')
			->withAttribute(SessionIdMiddleware::KEY, 'test');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(403, $response->getStatusCode());
	}

	/**
	 * GET запросы без SSID всегда проходят успешно вне зависимости от наличия или остутствия токена.
	 */
	public function testPostRequestTokenWithoutSsid() {
		$request = ($this->request())
			->withMethod('GET');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="csrf_token" class="csrf_token" hidden value="f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(200, $response->getStatusCode());

		$request = ($this->request())
			->withHeader(CsrfMiddleware::CSRF_TOKEN_HEADER,
				'f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770')
			->withMethod('GET');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="csrf_token" class="csrf_token" hidden value="f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(200, $response->getStatusCode());

		$request = ($this->request())
			->withMethod('GET');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(200, $response->getStatusCode());

		$request = ($this->request())
			->withMethod('GET');
		$response = $this->response();
		$response->getBody()->write('<form method="post"><input type="text" name="csrf_token" class="csrf_token" hidden value="f2e66e305025372c148bf33c76c827b65982845cc7ea1bd4624d9468beffe770"><input type="text" name="firstname"></form>');
		$response = $this->execute(
			[
				(new CsrfMiddleware())->autoInsert(true),
			],
			'',
			'',
			[],
			$request,
			$response
		);
		$this->tester->assertEquals(200, $response->getStatusCode());
	}
}