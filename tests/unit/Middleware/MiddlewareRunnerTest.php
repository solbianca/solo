<?php


namespace Tests\unit\Middleware;

use Engine\Core\DIC;
use Engine\Core\Exceptions\DependencyContainerException;
use Engine\Http\Factory\MessageFactory;
use Engine\Http\Factory\ServerRequestFactory;
use Engine\Http\Interfaces\ResponseInterface;
use Engine\Http\Interfaces\ServerRequestInterface;
use Engine\Middleware\Exceptions\MiddlewareException;
use Engine\Middleware\Exceptions\MiddlewareRuntimeException;
use Engine\Middleware\Interfaces\MiddlewareInterface;
use Engine\Middleware\MiddlewareRunner;

class MiddlewareRunnerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function _before() {
		FakeMiddleware::$count = 0;
	}

	public function testWithoutResolver() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/');
		$response = (new MessageFactory())->createResponse();

		FakeMiddleware::$count = 0;
		$queue[] = new FakeMiddleware();
		$queue[] = new FakeMiddleware();
		$queue[] = FakeMiddleware::class;
		$runner = new MiddlewareRunner($queue);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('123456', $actual);
	}

	public function testResolveFailWithoutResolver() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/');
		$response = (new MessageFactory())->createResponse();

		$queue[] = Fake::class;
		$runner = new MiddlewareRunner($queue);
		$this->tester->expectException(MiddlewareRuntimeException::class,
			function () use ($runner, $request, $response) {
				$response = $runner($request, $response);
			}
		);
	}

	public function testWithResolver() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/');
		$response = (new MessageFactory())->createResponse();

		FakeMiddleware::$count = 0;
		$queue[] = '\Tests\unit\Middleware\FakeMiddleware';
		$queue[] = '\Tests\unit\Middleware\FakeMiddleware';
		$queue[] = '\Tests\unit\Middleware\FakeMiddleware';
		$resolver = new DIC();
		$resolver->register('\Tests\unit\Middleware\FakeMiddleware', function () {
			return new FakeMiddleware();
		});
		$runner = new MiddlewareRunner($queue, $resolver);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('123456', $actual);
	}

	public function testWithResolverFail() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/');
		$response = (new MessageFactory())->createResponse();
		FakeMiddleware::$count = 0;
		$queue[] = '\Tests\unit\Middleware\FakeFailMiddleware';
		$queue[] = '\Tests\unit\Middleware\FakeFailMiddleware';
		$queue[] = '\Tests\unit\Middleware\FakeFailMiddleware';
		$resolver = new DIC();
		$resolver->register('\Not\Found\Fail', function () {
			return new FakeMiddleware();
		});
		$runner = new MiddlewareRunner($queue, $resolver);
		$this->tester->expectException(DependencyContainerException::class,
			function () use ($runner, $request, $response) {
				$runner($request, $response);
			}
		);


		$request = (new ServerRequestFactory())->createServerRequest('GET', '/');
		$response = (new MessageFactory())->createResponse();
		$queue[] = Fake::class;
		$resolver = new DIC();
		$resolver->register('\Not\Found\Fail', function () {
			return new FakeMiddleware();
		});
		$runner = new MiddlewareRunner($queue, $resolver);
		$this->tester->expectException(DependencyContainerException::class,
			function () use ($runner, $request, $response) {
				$response = $runner($request, $response);
			}
		);
	}

	public function testEmptyQueue() {
		$queue = [];
		$this->tester->expectException(MiddlewareException::class, function () use ($queue) {
			new MiddlewareRunner($queue);
		});
	}

	public function testProxyForLazyCreation() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/');
		$response = (new MessageFactory())->createResponse();

		$queue[] = MiddlewareRunner::proxy(function () {
			return new FakeMiddleware();
		});
		$queue[] = MiddlewareRunner::proxy(function () {
			return new FakeMiddleware();
		});
		$queue[] = MiddlewareRunner::proxy(function () {
			return new FakeMiddleware();
		});

		$runner = new MiddlewareRunner($queue);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('123456', $actual);
	}

	public function testProxyForBasePath() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/actions');
		$request = $request->withHeader('Foo', 'Bar');
		$response = (new MessageFactory())->createResponse();

		$queue[] = MiddlewareRunner::proxy('/actions', function ($request, $response) {
			if ($request->hasHeader('Foo')) {
				return new FakeMiddleware();
			}
			return false;
		});
		$runner = new MiddlewareRunner($queue);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('12', $actual);


		$request = (new ServerRequestFactory())->createServerRequest('GET', '/not/actions');
		$request = $request->withHeader('Foo', 'Bar');
		$response = (new MessageFactory())->createResponse();

		$queue[] = MiddlewareRunner::proxy('/actions', function ($request, $response) {
			if ($request->hasHeader('Foo')) {
				return new FakeMiddleware();
			}
			return false;
		});
		$runner = new MiddlewareRunner($queue);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('', $actual);
	}

	public function testProxyWhenMiddlewareNeededInSomeCases() {
		$request = (new ServerRequestFactory())->createServerRequest('GET', '/actions');
		$request = $request->withHeader('Foo', 'Bar');
		$response = (new MessageFactory())->createResponse();

		$queue[] = MiddlewareRunner::proxy(function ($request, $response) {
			if ($request->hasHeader('Foo')) {
				return new FakeMiddleware();
			}
			return false;
		});
		$runner = new MiddlewareRunner($queue);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('12', $actual);


		$request = (new ServerRequestFactory())->createServerRequest('GET', '/actions');
		$request = $request->withHeader('Buz', 'Bar');
		$response = (new MessageFactory())->createResponse();

		$queue[] = MiddlewareRunner::proxy(function ($request, $response) {
			if ($request->hasHeader('Foo')) {
				return new FakeMiddleware();
			}
			return false;
		});
		$runner = new MiddlewareRunner($queue);
		$response = $runner($request, $response);
		$actual = (string)$response->getBody();
		$this->assertSame('', $actual);
	}
}

class FakeMiddleware implements MiddlewareInterface {
	public static $count = 0;

	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
		$response->getBody()->write((string)++static::$count);
		$response = $next($request, $response);
		$response->getBody()->write((string)++static::$count);
		return $response;
	}
}

class Fake {
	public static $count = 0;

	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
		$response->getBody()->write((string)++static::$count);
		$response = $next($request, $response);
		$response->getBody()->write((string)++static::$count);
		return $response;
	}

	public function __toString() {
		return '\Fake';
	}
}