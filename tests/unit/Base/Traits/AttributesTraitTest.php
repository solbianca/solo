<?php


class AttributesTraitTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var TestAttributesTraitObject
	 */
	protected $testObject;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->testObject = new TestAttributesTraitObject();
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testAttributes() {
		$this->tester->assertEquals(['name', 'age'], $this->testObject->attributes());
	}

	public function testGetAllAttributes() {
		$this->tester->assertEquals(['name' => null, 'age' => null], $this->testObject->getAttributes());
		$this->testObject->setAttributes(['name' => 'John Doe', 'age' => 20]);
		$this->tester->assertEquals(['name' => 'John Doe', 'age' => 20], $this->testObject->getAttributes());
	}

	public function testGetSpecificAttributes() {
		$this->tester->assertEquals(['age' => null], $this->testObject->getAttributes(['age']));
		$this->testObject->setAttributes(['age' => 20]);
		$this->tester->assertEquals(['age' => 20], $this->testObject->getAttributes(['age']));
		$this->tester->assertEquals(['name' => null, 'age' => 20], $this->testObject->getAttributes());
	}

	public function testGetExceptAttributes() {
		$this->tester->assertEquals(['name' => null], $this->testObject->getAttributes(null, ['age']));
		$this->testObject->setAttributes(['name' => 'John Doe']);
		$this->tester->assertEquals(['name' => 'John Doe'], $this->testObject->getAttributes(null, ['age']));
		$this->tester->assertEquals(['name' => 'John Doe', 'age' => null], $this->testObject->getAttributes());
	}
}

class TestAttributesTraitObject {
	use \Engine\Base\Traits\AttributesTrait;

	public $name;

	public $age;
}