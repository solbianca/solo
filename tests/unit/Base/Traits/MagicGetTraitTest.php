<?php


class MagicGetTraitTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var TestObject
	 */
	protected $testObject;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->testObject = new TestObject();
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testMagicGet() {
		$object = $this->testObject;
		$this->tester->assertEquals(42, $object->id);
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($object) {
			$object->fail;
		});
	}
}

class TestObject {
	use \Engine\Base\Traits\MagicGetTrait;

	protected $id = 42;

	public function getId() {
		return $this->id;
	}
}