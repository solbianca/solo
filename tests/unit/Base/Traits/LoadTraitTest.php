<?php


class LoadTraitTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var TestObject
	 */
	protected $testObject;

	/**
	 * @var array
	 */
	protected $data = ['name' => 'John Doe', 'age' => 20];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->testObject = new TestLoadTraitObject();
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testSuccessfulLoadWithoutScope() {
		$this->tester->assertTrue($this->testObject->load($this->data));
		$this->tester->assertEquals('John Doe', $this->testObject->name);
		$this->tester->assertEquals(20, $this->testObject->age);
	}

	public function testFailedLoadWithoutScope() {
		$this->tester->assertFalse($this->testObject->load($this->data, 'FailtestObject'));
		$this->tester->assertFalse($this->testObject->load([]));
		$this->tester->assertFalse($this->testObject->load(null));
	}

	public function testSuccessfulLoadBySkope() {
		$data = ['testObject' => $this->data];

		$this->tester->assertTrue($this->testObject->load($data));
		$this->tester->assertNull($this->testObject->name);
		$this->tester->assertNull($this->testObject->age);

		$this->tester->assertTrue($this->testObject->load($data, 'testObject'));
		$this->tester->assertEquals('John Doe', $this->testObject->name);
		$this->tester->assertEquals(20, $this->testObject->age);
	}

	public function testFailedLoadByScope() {
		$this->tester->assertFalse($this->testObject->load([], 'testObject'));
	}
}

class TestLoadTraitObject {
	use \Engine\Base\Traits\LoadTrait;

	public $name;

	public $age;
}