<?php


class SphinxConnectionTraitTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected $config = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
		'databases' => [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
				'sphinx' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		],
	];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		\Engine\Engine::$app = new \Engine\Core\ServiceApplication($this->config);
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testGetSphinxConnectionMethodSuccessful() {
		$testObject = new TestSphinxConnectionTraitObject();
		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $testObject->getSphinxConnection());
	}
}

class TestSphinxConnectionTraitObject {
	use \Engine\Base\Traits\SphixConnectionTrait;

}