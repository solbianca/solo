<?php


class DbConnectionTraitTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var TestDbConnectionTraitObject
	 */
	protected $testObject;

	protected $config = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
		'databases' => [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		],
	];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->testObject = new TestDbConnectionTraitObject();
		\Engine\Engine::$app = new \Engine\Core\ServiceApplication($this->config);
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testSetAndGetDefaultConnection() {
		$this->testObject->setDefaultConnection($this->config['databases']['defaultConnectionName']);
		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $this->testObject->getConnection());
	}


	public function testSetAndGetConnection() {
		$testObject = new TestDbConnectionTraitObject();
		$pdo = \Engine\Engine::$app->getDic()->get('dbpool')->get('db');
		$connection = new \Engine\Db\Database($pdo);

		$testObject->setConnection($connection);
		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $testObject->getConnection());
	}

	public function testSetDefaultConnectionFail() {
		$testObject = new TestDbConnectionTraitObject();
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class,
			function () use ($testObject) {
				$testObject->setDefaultConnection('Fail');
			});
	}

	public function testGetConnectionFail() {
		$testObject = new TestDbConnectionTraitObject();
		$this->tester->assertEquals(null, $testObject->getConnection());
	}
}

class TestDbConnectionTraitObject {
	use \Engine\Base\Traits\DbConnectionTrait;

}