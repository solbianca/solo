<?php

class ServiceTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Base\Service
	 */
	protected $service;

	public function _before() {
		defined('APP_DEBUG') or define('APP_DEBUG', false);
		$sericesConfig = file_get_contents(__DIR__ . '/../../_data/services.json');
		$application = new \Engine\Core\WebApplication([
			'clientId' => 'test',
			'path' => [
				'web' => __DIR__,
				'app' => __DIR__,
				'root' => __DIR__,
			],
		]);
		$application->initServicesManager($sericesConfig)->run();
	}

	public function testGetName() {
		$service = new AuthTestService('Auth');
		$this->assertEquals('Auth', $service->getName());

		$service = new NamedTestService();
		$this->assertEquals('Test', $service->getName());
	}

	public function testCreateClients() {
		$service = new AuthTestService('Auth');
		$clients = $service->getClients();
		$this->tester->assertNotEmpty($clients);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[0]);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[1]);
	}

	public function testEmptyClientsForService() {
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class, function () {
			new FailedTestService('FailName');
		});
	}

}

class AuthTestService extends \Engine\Base\Service {

}

class FailedTestService extends \Engine\Base\Service {

}

class NamedTestService extends \Engine\Base\Service {
	protected $name = 'Test';
}