<?php

class SphinxModelTestClass extends \Engine\Base\SphinxModel {

}

class SphinxModelTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Base\Model
	 */
	protected $model;

	protected $dbConfig = [
		'dsn' => 'mysql',
		'host' => 'localhost',
		'dbname' => 'epn',
		'charset' => 'utf8',
		'user' => 'epn',
		'password' => 'some_pass',
	];

	protected $config = [
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'databases' => [
			'connections' => [
				'sphinx' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		],
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
	];

	public function _before() {
		$application = new \Engine\Core\ServiceApplication($this->config);
		\Engine\Engine::$app = $application;
	}

	public function testCreate() {
		$model = new SphinxModelTestClass();
		$this->tester->assertInstanceOf(\Engine\Base\SphinxModel::class, $model);
	}

	public function testGetConnection() {
		$model = new SphinxModelTestClass();
		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $model->getConnection());
	}

	public function testCreateCustomConnection() {
		/** @var \Engine\Db\Pool $dbpool */
		$dbpool = \Engine\Engine::$app->getDic()->get('dbpool');
		$pdo = $dbpool->get('sphinx');
		$connection = new \Engine\Db\Database($pdo);

		$model = new SphinxModelTestClass($connection);

		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $model->getConnection());
		$model->setConnection($connection);
		$this->tester->assertEquals($connection, $model->getConnection());
	}

}