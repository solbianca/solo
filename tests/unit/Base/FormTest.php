<?php


class FormTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var UserForm
	 */
	protected $form;

	/**
	 * @var array
	 */
	protected $rules = ['name' => 'required', 'age' => 'required|int'];

	/**
	 * @var array
	 */
	protected $data = ['name' => 'John Doe', 'age' => 20];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->form = new UserForm();
	}

	/**
	 * Initiate after test
	 */
	protected function _after() {
	}

	public function testGetValidator() {
		$this->tester->assertNotNull($this->form->getValidator());
		$this->tester->assertInstanceOf(\Engine\Validation\Interfaces\ValidatorInterface::class,
			$this->form->getValidator());
		$this->form->setValidator(new \Engine\Validation\Validator());
		$this->tester->assertInstanceOf(\Engine\Validation\Validator::class, $this->form->getValidator());
	}

	public function testGetErrors() {
		$this->tester->assertNull($this->form->getValidationErrors());
		$this->form->setValidationRules($this->rules);
		$this->form->load(['name' => 'John Doe', 'age' => 20]);
		$this->form->validate();
		$this->tester->assertTrue($this->form->getValidationErrors()->isEmpty());
		$this->form->load(['name' => 'John Doe', 'age' => null]);
		$this->form->validate();
		$this->tester->assertFalse($this->form->getValidationErrors()->isEmpty());
		$this->tester->assertNotNull($this->form->getValidationErrors()->get('age')[0]);
		$this->tester->assertNotNull($this->form->getValidationErrors()->get('age')[1]);
	}

	public function testSuccessfulValidate() {
		$this->tester->assertTrue($this->form->validate());

		$this->form->setValidationRules($this->rules);
		$this->form->load($this->data);
		$this->tester->assertTrue($this->form->validate());

		$this->form->setValidationRules(['age' => 'required|int']);
		$this->form->load(['age' => 20]);
		$this->tester->assertTrue($this->form->validate(['age']));
	}

	public function testInvalidConfigValidation() {
		$form = new UserFormTwo();
		$form->setValidationRules($this->rules);
		$form->load($this->data);
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class, function () use ($form) {
			$form->validate();
		});
	}

	public function testFailValidation() {
		$this->form->setValidationRules($this->rules);
		$this->form->load(['name' => 'John Doe']);
		$this->tester->assertFalse($this->form->validate());
		$this->form->load(['name' => 'John Doe', 'age' => 20]);
		$this->tester->assertTrue($this->form->validate());
	}

	public function testGetValidationRules() {
		$this->tester->assertEmpty($this->form->getValidationRules());
		$this->form->setValidationRules($this->rules);
		$this->tester->assertNotEmpty($this->form->getValidationRules());
		$this->tester->assertEquals($this->rules, $this->form->getValidationRules());
	}

	public function testSuccessfulLoadWithoutScope() {
		$this->tester->assertTrue($this->form->load($this->data));
		$this->tester->assertEquals('John Doe', $this->form->name);
		$this->tester->assertEquals(20, $this->form->age);
	}

	public function testFailedLoadWithoutScope() {
		$this->tester->assertFalse($this->form->load($this->data, 'FailForm'));
		$this->tester->assertFalse($this->form->load([]));
	}

	public function testSuccessfulLoadBySkope() {
		$data = ['Form' => $this->data];

		$this->tester->assertTrue($this->form->load($data));
		$this->tester->assertNull($this->form->name);
		$this->tester->assertNull($this->form->age);

		$this->tester->assertTrue($this->form->load($data, 'Form'));
		$this->tester->assertEquals('John Doe', $this->form->name);
		$this->tester->assertEquals(20, $this->form->age);
	}

	public function testFailedLoadByScope() {
		$this->tester->assertFalse($this->form->load([], 'Form'));
	}

	public function testAttributes() {
		$this->tester->assertEquals(['name', 'age'], $this->form->attributes());
	}

	public function testGetAllAttributes() {
		$this->tester->assertEquals(['name' => null, 'age' => null], $this->form->getAttributes());
		$this->form->setAttributes(['name' => 'John Doe', 'age' => 20]);
		$this->tester->assertEquals(['name' => 'John Doe', 'age' => 20], $this->form->getAttributes());
	}

	public function testGetSpecificAttributes() {
		$this->tester->assertEquals(['age' => null], $this->form->getAttributes(['age']));
		$this->form->setAttributes(['age' => 20]);
		$this->tester->assertEquals(['age' => 20], $this->form->getAttributes(['age']));
		$this->tester->assertEquals(['name' => null, 'age' => 20], $this->form->getAttributes());
	}

	public function testGetExceptAttributes() {
		$this->tester->assertEquals(['name' => null], $this->form->getAttributes(null, ['age']));
		$this->form->setAttributes(['name' => 'John Doe']);
		$this->tester->assertEquals(['name' => 'John Doe'], $this->form->getAttributes(null, ['age']));
		$this->tester->assertEquals(['name' => 'John Doe', 'age' => null], $this->form->getAttributes());
	}
}

class UserForm extends \Engine\Base\Form {
	public $name;

	public $age;
}


class UserFormTwo extends \Engine\Base\Form {
	public $name;

	public $age;

	protected function createValidator() {
		return null;
	}
}