<?php

class ConsoleControllerTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var ConsoleControllerTestObject
	 */
	protected $controller;


	public function _before() {
		$this->controller = new ConsoleControllerTestObject(
			new \Engine\Console\Request(),
			new \Engine\Console\Response(),
			['test1' => 12]
		);
	}

	public function testCreate() {
		$this->tester->assertInstanceOf(\Engine\Base\Console\Controller::class, $this->controller);
	}

	public function testGetRequest() {
		$this->tester->assertInstanceOf(\Engine\Console\Request::class, $this->controller->getRequest());
	}

	public function testGetResponse() {
		$this->tester->assertInstanceOf(\Engine\Console\Response::class, $this->controller->getResponse());
	}


	public function testGetParamsSuccessful() {
		$this->tester->assertEquals(12, $this->controller->getParamPublic('test1'));
	}
}

class ConsoleControllerTestObject extends \Engine\Base\Console\Controller {
	public function getParamPublic($name) {
		return $this->getParam($name);
	}
}
