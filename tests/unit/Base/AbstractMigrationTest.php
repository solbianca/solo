<?php

class AbstractMigrationTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;


	protected $config = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
		'databases' => [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
				'sphinx' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		],
	];

	public function _before() {
		$application = new \Engine\Core\ServiceApplication($this->config);
		\Engine\Engine::$app = $application;
	}

	public function testCreateMigrationObjectSuccessful() {
		$testObject = new TestMigrationObject();
		$this->tester->assertInstanceOf(\Engine\Base\AbstractMigration::class, $testObject);
	}

	public function testDatabaseConnectionSuccessful() {
		$pdo = \Engine\Engine::$app->getDic()->get('dbpool')->get('db');
		$connection = new \Engine\Db\Database($pdo);

		$model = new TestMigrationObject($connection);

		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $model->getConnection());
		$this->tester->assertInstanceOf(\Engine\Base\AbstractMigration::class, $model->setDefaultConnection('db'));
		$model->setConnection($connection);
		$this->tester->assertEquals($connection, $model->getConnection());
	}

	public function testGetSphinxConnectionSuccessful() {
		$model = new TestMigrationObject();
		$this->tester->assertInstanceOf(\Engine\Db\Database::class, $model->getSphinxConnection());
	}
}

class TestMigrationObject extends \Engine\Base\AbstractMigration {

	public function up() {
		// TODO: Implement up() method.
	}

	public function down() {
		// TODO: Implement down() method.
	}
}