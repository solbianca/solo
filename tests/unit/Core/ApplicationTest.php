<?php


class ApplicationTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\Application
	 */
	protected $application;

	protected $config = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
		'databases' => [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		],
	];

	public function _before() {
		$this->application = new ApplicationTestObject($this->config);
	}

	public function testInitDic() {
		$this->tester->assertInstanceOf(\Engine\Core\DIC::class, \Engine\Engine::$app->getDic());
	}

	public function testGetConfig() {
		$this->tester->assertEquals($this->config, $this->application->getConfig());

		$config = [
			'path' => [
				'web' => __DIR__,
				'app' => __DIR__,
				'root' => __DIR__,
			],
		];
		$this->application = new ApplicationTestObject($config);
		$this->tester->assertEquals($config, $this->application->getConfig());
	}

	public function testGetConfigSection() {
		$config = [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		];
		$this->tester->assertEquals($config, $this->application->getConfig('urls'));
		$this->tester->assertNull($this->application->getConfig('fail'));
	}

	public function testGetConfigSectionField() {
		$this->tester->assertEquals('offers::index', $this->application->getConfig('urls.index'));
		$this->tester->assertNull($this->application->getConfig('urls.fail'));
	}

	public function testHandleRequest() {
		$this->tester->assertTrue($this->application->handleRequest());
	}

	public function testCreateRouterException() {
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () {
			($this->application->createRouter(new \Engine\Http\Request('GET', '/'),
				new \Engine\Http\Response()));
		});
	}

	public function testCreateRouterSuccessful() {
		$config = [
			'path' => [
				'web' => __DIR__,
				'app' => __DIR__,
				'root' => __DIR__,
			],
			'router' => \Engine\Http\HttpRouter::class,
		];
		$this->application = new ApplicationTestObject($config);
		$this->tester->assertInstanceOf(\Engine\Http\HttpRouter::class,
			$this->application->createRouter(new \Engine\Http\Request('GET', '/'),
				new \Engine\Http\Response()));
	}
}

class ApplicationTestObject extends \Engine\Core\Application {
	public function handleRequest() {
		return true;
	}
}