<?php

use Engine\JsonRpc2\Client\ClientFactory;

class ServiceManagerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * @var \Engine\Core\ServiceManager
	 */
	protected $serviceManager;

	public function _before() {
		$this->config = file_get_contents(__DIR__ . '/../../_data/services.json');
		$this->serviceManager = new \Engine\Core\ServiceManager(new ClientFactory(), $this->config);
	}

	public function testConfigLoading() {
		$config = $this->serviceManager->getConfig();
		$this->tester->assertNotEmpty($config);
		$this->tester->assertNotEmpty($config['Auth']);
		$this->tester->assertNotEmpty($config['Auth'][0]);
		$this->tester->assertEquals('php-fpm', $config['Auth'][0]['transport']);
		$this->tester->assertEquals('127.0.0.1', $config['Auth'][0]['host']);
		$this->tester->assertEquals('9000', $config['Auth'][0]['port']);
		$this->tester->assertEquals('/app/services/Auth/public/index.php', $config['Auth'][0]['file']);
	}

	public function testIsServiceExist() {
		$this->tester->assertTrue($this->serviceManager->isServiceExist('Auth'));
		$this->tester->assertFalse($this->serviceManager->isServiceExist('Fail'));
	}

	public function testGetServiceConfig() {
		$testConfig[] = [
			'transport' => 'php-fpm',
			'host' => '127.0.0.1',
			'port' => 9000,
			'file' => '/app/services/Auth/public/index.php',
		];
		$testConfig[] = [
			'transport' => 'php-fpm',
			'host' => '192.168.1.1',
			'port' => 9001,
			'file' => '/app/services/Auth/public/index.php',
		];
		$this->tester->assertEquals($testConfig, $this->serviceManager->getServiceConfig('Auth'));

		$this->tester->assertEquals([], $this->serviceManager->getServiceConfig('Fail'));
	}

	public function testSuccessfulGetClientsForService() {
		$clients = $this->serviceManager->getClients('Auth');
		$this->tester->assertNotEmpty($clients);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[0]);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[1]);
	}

	public function testNotDefinedServiceInConfig() {
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class, function () {
			$this->serviceManager->getClients('Fail');
		});
	}
}