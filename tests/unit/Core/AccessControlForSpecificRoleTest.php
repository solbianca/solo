<?php


class AccessControlForSpecificRoleTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testAllowAccessToSpecificAction() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([
			['allow' => true, 'actions' => ['test'], 'roles' => ['admin']],
		]);
		$this->tester->assertTrue($access->resolve($user, 'testAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));
	}

	public function testAllowAndDenyAccessToSpecificAction() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([['allow' => true, 'actions' => ['test'], 'roles' => ['admin']]]);
		$this->tester->assertTrue($access->resolve($user, 'testAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));

		$access = new \Engine\Core\AccessControl([
			[
				'allow' => false,
				'actions' => ['test2'],
				'roles' => ['admin'],
			],
		]);
		$this->tester->assertFalse($access->resolve($user, 'test2Action', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));
	}

	public function testAllowAccessToAllActions() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([['allow' => true, 'roles' => ['admin']]]);
		$this->tester->assertTrue($access->resolve($user, 'testAction', $request));
		$this->tester->assertTrue($access->resolve($user, 'testForbiddenAction', $request));
		$this->tester->assertTrue($access->resolve($user, 'someRandomAction', $request));
	}

	public function testDenyAccessToSpecificAction() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([
			[
				'allow' => false,
				'actions' => ['test'],
				'roles' => ['admin'],
			],
		]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));
	}

	public function testDenyAccessToAllActions() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([['allow' => false, 'roles' => ['admin']]]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'someRandomAction', $request));
	}

	public function testDenyAccessOtherRoles() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([['allow' => true, 'roles' => ['guest']]]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'someRandomAction', $request));
	}

	public function testRoleExceptions() {
		$user = new UserSpecific();
		$access = new \Engine\Core\AccessControl([['allow' => true, 'actions' => ['test']]]);
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($access, $user) {
				$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
				$access->resolve($user, 'testAction', $request);
			}
		);
	}

	public function testDenyAccessWithWrongAllowType() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([
			[
				'allow' => 'true',
				'actions' => ['test'],
				'roles' => ['admin'],
			],
		]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
		$access = new \Engine\Core\AccessControl([['allow' => 1, 'actions' => ['test'], 'roles' => ['admin']]]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
		$access = new \Engine\Core\AccessControl([['allow' => null, 'actions' => ['test'], 'roles' => ['admin']]]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
	}

	public function testBadRoleDefinitionException() {
		$access = new \Engine\Core\AccessControl([['allow' => true, 'roles' => 1]]);
		$user = new UserSpecific();
		$this->tester->expectException(\Engine\Core\Exceptions\InvalidConfigException::class,
			function () use ($access, $user) {
				$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
				$access->resolve($user, 'testAction', $request);
			}
		);
	}

	public function testAllowByVerb() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([
			['allow' => true, 'actions' => ['test'], 'roles' => ['admin'], 'verbs' => ['GET', 'POST']],
		]);
		$this->tester->assertTrue($access->resolve($user, 'testAction', $request));

		$access = new \Engine\Core\AccessControl([
			['allow' => true, 'actions' => ['test'], 'roles' => ['admin'], 'verbs' => ['PUT', 'POST']],
		]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
	}

	public function testAllowByCallback() {
		$user = new UserSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$access = new \Engine\Core\AccessControl([
			[
				'allow' => true,
				'actions' => ['test'],
				'roles' => ['admin'],
				'callback' => function (\Engine\Core\AccessRule $rule, $action) use ($user) {
					return (2 + 2) === 4;
				},
			],
		]);
		$this->tester->assertTrue($access->resolve($user, 'testAction', $request));

		$access = new \Engine\Core\AccessControl([
			[
				'allow' => true,
				'actions' => ['test'],
				'roles' => ['admin'],
				'callback' => function (\Engine\Core\AccessRule $rule, $action) use ($user) {
					return (2 + 2) === 42;
				},
			],
		]);
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
	}

	public function testDenyAccessByRole() {
		$access = new \Engine\Core\AccessControl([['allow' => true, 'roles' => ['admin']]]);
		$user = new GuestSpecific();
		$request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
		$this->tester->assertFalse($access->resolve($user, 'testAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'testForbiddenAction', $request));
		$this->tester->assertFalse($access->resolve($user, 'someRandomAction', $request));
	}
}

class GuestSpecific implements \Engine\Base\Interfaces\UserInterface {

	public $identity;

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		return null;
	}

	public function isGuest() {
		return true;
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		// TODO: Implement setIdentity() method.
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}

class UserSpecific implements \Engine\Base\Interfaces\UserInterface {

	public $identity;

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		return 'admin';
	}

	public function isGuest() {
		return false;
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		// TODO: Implement setIdentity() method.
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}