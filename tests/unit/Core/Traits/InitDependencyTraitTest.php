<?php

class InitDependencyTraitTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var InitDependencyTestObject
	 */
	protected $testObject;

	/**
	 * @var array
	 */
	protected $components;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->components = [
			'logger' => [
				'immutable' => false,
				'callback' => function () {
					return 123;
				},
			],
			'test1' => [
				'immutable' => true,
				'callback' => function () {
					return new stdClass();
				}
			],
		];
		$this->testObject = new InitDependencyTestObject(new \Engine\Core\DIC());
	}

	public function testInitDependencySuccessful() {
		$this->testObject->runInitDependency($this->components);
		$this->tester->assertEquals(123, $this->testObject->getDic()->get('logger'));
		$this->tester->assertInstanceOf(\stdClass::class, $this->testObject->getDic()->get('test1'));
	}

	public function testInitDependencyWrongParamFail() {
		$components = [
			'logger' => [
				'immutable' => false,
				'fail' => function () {
					return 123;
				},
			],
		];
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class,
			function () use ($components) {
				$this->testObject->runInitDependency($components);
			}
		);
	}
}

class InitDependencyTestObject {
	use \Engine\Core\Traits\InitDependencyTrait;
	/**
	 * @var \Engine\Core\DIC
	 */
	private $dic;

	public function __construct(\Engine\Core\DIC $dic) {
		$this->dic = $dic;
	}

	public function getDic() {
		return $this->dic;
	}

	public function runInitDependency(array $components) {
		return $this->initDependency($components);
	}
}