<?php


class ConsoleApplicationTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\ConsoleApplication
	 */
	protected $app;

	protected $config = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'console_commands' => [
			'test' => 'Test::test',
		],
		'databases' => [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		],
	];

	public function _before() {
		defined('APP_DEBUG') or define('APP_DEBUG', false);

		$application = new \Engine\Core\ConsoleApplication($this->config);
		$this->app = $application;
	}

	public function testGetCoreCommands() {
		$this->tester->assertTrue(is_array($this->app->coreCommands()));
	}

	public function testRouteCoreCommandMethod() {
		$app = new CoreCommandTestObject($this->config);
		$coreCommandNames = array_keys($app->coreCommands());

		foreach ($coreCommandNames as $name) {
			$controllerPattern = $app->routeCoreCommandT($name);
			$this->tester->assertTrue(key_exists('controller', $controllerPattern));
			$this->tester->assertTrue(key_exists('action', $controllerPattern));
		}
	}

	public function testCheckControllerMethodSuccessful() {
		$app = new CoreCommandTestObject($this->config);
		$this->tester->assertTrue($app->checkControllerT(MyConsoleController::class));
	}

	public function testCheckControllerMethodFail() {
		$app = new CoreCommandTestObject($this->config);
		$this->tester->expectException(
			\Engine\Core\Exceptions\ApplicationException::class, function () use ($app) {
			$app->checkControllerT('Fail');
		});
	}

	public function testCheckActionMethodSuccessful() {
		$app = new CoreCommandTestObject($this->config);
		$controller = new MyConsoleController(new \Engine\Console\Request(), new \Engine\Console\Response(), []);
		$this->tester->assertTrue($app->checkActionT($controller, 'MyAction'));
	}

	public function testCheckActionMethodFail() {
		$app = new CoreCommandTestObject($this->config);
		$controller = new MyConsoleController(new \Engine\Console\Request(), new \Engine\Console\Response(), []);
		$this->tester->expectException(
			\Engine\Core\Exceptions\ApplicationException::class, function () use ($app, $controller) {
			$app->checkActionT($controller, 'fail');
		});
	}

	public function testRunActionMethod() {
		$app = new CoreCommandTestObject($this->config);
		$controller = new MyConsoleController(new \Engine\Console\Request(), new \Engine\Console\Response(), []);

		$this->tester->assertEquals(123, $app->runActionT($controller, 'MyAction'));
	}

	public function testCacheConfigSuccessful() {
		$config['path'] = [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		];
		$config['cache'] = [
			'connections' => [
				'memcache' => [
					'host' => 'localhost',
					'port' => '11211',
					'cacheKeyPrefix' => 'keyPrefix',
				],
			],
		];
		$application = new \Engine\Core\ConsoleApplication($config);
		$dic = $application->getDic();
		$this->tester->assertInstanceOf(\Engine\Cache\MemcacheCache::class, $dic->get('cachepool')->get('memcache'));
	}

	public function testDatabasesConfigSuccessful() {
		$config['path'] = [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		];
		$config['databases'] = [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		];
		$application = new \Engine\Core\ConsoleApplication($config);
		$dic = $application->getDic();
		$this->tester->assertInstanceOf(\PDO::class, $dic->get('dbpool')->get('db'));
	}

	public function testComponentsConfigurationSuccessful() {
		$config['path'] = [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		];
		$config['components'] = [
			'logger' => [
				'immutable' => false,
				'callback' => function () {
					return 123;
				},
			],
		];

		$application = new \Engine\Core\ServiceApplication($config);
		$dic = $application->getDic();
		$this->tester->assertEquals(123, $dic->get('logger'));
	}


}


class CoreCommandTestObject extends \Engine\Core\ConsoleApplication {
	public function routeCoreCommandT($route) {
		return $this->routeCoreCommand($route);

	}

	public function checkControllerT($class) {
		return $this->checkController($class);
	}

	public function checkActionT(Engine\Base\Console\Controller $controller, $action) {
		return $this->checkAction($controller, $action);
	}

	public function runActionT(Engine\Base\Console\Controller $controller, $action) {
		return $this->runAction($controller, $action);
	}
}

class MyConsoleController extends \Engine\Base\Console\Controller {
	public function myAction() {
		return 123;
	}
}



