<?php


class AccessControlTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\AccessRule
	 */
	protected $accessRule;

	/**
	 * @var GuestAccessControl
	 */
	protected $guest;

	/**
	 * @var UserAccessControl
	 */
	protected $user;

	/**
	 * @var AdminAccessControl
	 */
	protected $admin;

	/**
	 * @var \Engine\Http\Request
	 */
	protected $request;

	/**
	 *
	 */
	protected function _before() {
		$this->guest = new GuestAccessControl();
		$this->user = new UserAccessControl();
		$this->admin = new AdminAccessControl();
		$this->request = (new \Engine\Http\Factory\ServerRequestFactory())->createServerRequest('GET', '/');
	}

	public function testAccess() {
		$rules = [
			[
				'allow' => true,
				'roles' => ['guest', 'authorized'],
				'actions' => ['test', 'test2'],
			],
			[
				'allow' => false,
				'roles' => ['admin'],
				'actions' => ['test'],
			],
		];
		$access = new \Engine\Core\AccessControl($rules);
		$this->tester->assertTrue($access->resolve($this->guest, 'testAction', $this->request));
		$this->tester->assertTrue($access->resolve($this->user, 'testAction', $this->request));
		$this->tester->assertFalse($access->resolve($this->admin, 'testAction', $this->request));

		$rules = [
			[
				'allow' => false,
				'roles' => ['guest', 'authorized'],
				'actions' => ['test', 'test2'],
			],
			[
				'allow' => true,
				'roles' => ['admin'],
				'actions' => ['test'],
			],
		];
		$access = new \Engine\Core\AccessControl($rules);
		$this->tester->assertFalse($access->resolve($this->guest, 'testAction', $this->request));
		$this->tester->assertFalse($access->resolve($this->user, 'testAction', $this->request));
		$this->tester->assertFalse($access->resolve($this->admin, 'testAction', $this->request));

		$rules = [
			[
				'allow' => true,
				'roles' => ['guest', 'authorized', 'admin'],
			],
		];
		$access = new \Engine\Core\AccessControl($rules);
		$this->tester->assertTrue($access->resolve($this->guest, 'testAction', $this->request));
		$this->tester->assertTrue($access->resolve($this->user, 'testAction', $this->request));
		$this->tester->assertTrue($access->resolve($this->admin, 'testAction', $this->request));

		$rules = [
			[
				'allow' => true,
				'roles' => ['guest', 'authorized'],
			],
		];
		$access = new \Engine\Core\AccessControl($rules);
		$this->tester->assertTrue($access->resolve($this->guest, 'testAction', $this->request));
		$this->tester->assertTrue($access->resolve($this->user, 'testAction', $this->request));
		$this->tester->assertTrue($access->resolve($this->admin, 'testAction', $this->request));

		$rules = [
			[
				'allow' => true,
				'roles' => ['guest', 'admin'],
				'actions' => ['test'],
			],
			[
				'allow' => false,
				'roles' => ['admin'],
				'actions' => ['test'],
			],
		];
		$access = new \Engine\Core\AccessControl($rules);
		$this->tester->assertTrue($access->resolve($this->guest, 'testAction', $this->request));
		$this->tester->assertFalse($access->resolve($this->admin, 'testAction', $this->request));
	}

	public function testRoleRule() {
		$access = new \Engine\Core\AccessControl([]);
		$user = new UserAccessControl();
		$admin = new AdminAccessControl();

		$rule = [
			'allow' => true,
			'roles' => ['authorized'],
			'actions' => ['test'],
		];
		$accessRule = new \Engine\Core\AccessRule($rule);
		$this->tester->assertTrue($access->matchRole($user, $accessRule));

		$rule = [
			'allow' => true,
			'roles' => ['guest'],
			'actions' => ['test'],
		];
		$accessRule = new \Engine\Core\AccessRule($rule);
		$this->tester->assertFalse($access->matchRole($user, $accessRule));

		$rule = [
			'allow' => true,
			'roles' => ['guest', 'admin'],
			'actions' => ['test'],
		];
		$accessRule = new \Engine\Core\AccessRule($rule);
		$this->tester->assertTrue($access->matchRole($admin, $accessRule));
	}
}

class GuestAccessControl implements \Engine\Base\Interfaces\UserInterface {

	public $identity;

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		return null;
	}

	public function isGuest() {
		return true;
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		// TODO: Implement setIdentity() method.
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}

class UserAccessControl implements \Engine\Base\Interfaces\UserInterface {

	public $identity;

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		return null;
	}

	public function isGuest() {
		return false;
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		// TODO: Implement setIdentity() method.
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}

class AdminAccessControl implements \Engine\Base\Interfaces\UserInterface {

	public $identity;

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		return 'admin';
	}

	public function isGuest() {
		return false;
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		// TODO: Implement setIdentity() method.
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}