<?php


namespace Tests\unit\Core\Factory;


use Engine\Core\Factory\ViewFactory;
use Engine\Core\View;
use Engine\Http\Factory\MessageFactory;
use Engine\Renderers\SimpleRenderer;

class ViewFactoryTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testCreateWithEmptyConfig() {
		$response = (new MessageFactory())->createResponse();
		$factory = new ViewFactory();
		$config = [];
		$view = $factory->create($response, $config);
		$this->tester->assertInstanceOf(View::class, $view);
		$view = $factory->create($response);
		$this->tester->assertInstanceOf(View::class, $view);
	}

	public function testCreateWithConfig() {
		$response = (new MessageFactory())->createResponse();
		$factory = new ViewFactory();
		$config = [
			'renderer' => SimpleRenderer::class,
			'rendererOptions' => [],
			'fileExtension' => '.phtml',
		];
		$view = $factory->create($response, $config);
		$this->tester->assertInstanceOf(View::class, $view);
	}
}