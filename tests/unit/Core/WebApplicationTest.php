<?php

namespace Tests\unit\Core;

use Engine\Http\Interfaces\ResponseInterface as Response;
use Engine\Http\Interfaces\ServerRequestInterface as Request;

class WebApplicationTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\WebApplication
	 */
	protected $application;

	/**
	 * @var array
	 */
	protected $config;

	public function _before() {
		$this->config = [
			'clientId' => 'test',
			'path' => [
				'web' => __DIR__,
				'app' => __DIR__,
				'root' => __DIR__,
			],
			'urls' => [
				'/' => 'site::index',
				'/login' => 'auth::index',
				'/test' => 'site::test',
				'aliases' => [
					'epn.bz' => [
						'foo' => 'Site::foo',
					],
				],
			],
			'middleware' => [
				'default' => [
					new \Engine\Middleware\Middlewares\ClientIpMiddleware(),
					new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
				],
			],
		];
		$this->application = new \Engine\Core\WebApplication($this->config);
	}

	public function testViewInitiateInContainer() {
		$view = $this->application->dic->get('view');
		$this->tester->assertNotNull($view);
		$this->tester->assertInstanceOf(\Engine\Core\View::class, $view);
	}

	public function testGetUserIdentityNotDefinedInConfig() {
		$this->tester->assertInstanceOf(\Engine\Base\UserIdentity::class, $this->application->getUser());
	}

	public function testCorrectUserIdentityDefinitionInConfig() {
		$config = [
			'clientId' => 'test',
			'path' => [
				'web' => __DIR__,
				'app' => __DIR__,
				'root' => __DIR__,
			],
			'user' => [
				'identity' => WebApplicationTestUser::class,
			],
			'middleware' => [
				'default' => [
					new \Engine\Middleware\Middlewares\ClientIpMiddleware(),
					new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
				],
			],
		];
		$application = new \Engine\Core\WebApplication($config);
		$this->tester->assertInstanceOf(WebApplicationTestUser::class, $application->getUser());
	}

	public function testIncorrectConfigUserDefinition() {
		$config = [
			'clientId' => 'test',
			'path' => [
				'web' => __DIR__,
				'app' => __DIR__,
				'root' => __DIR__,
			],
			'user' => [
				'identity' => WebApplicationTestUserWithoutUserInterface::class,
			],
			'middleware' => [
				'default' => [
					new \Engine\Middleware\Middlewares\ClientIpMiddleware(),
					new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
				],
			],
		];
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($config) {
			new \Engine\Core\WebApplication($config);
		});
	}

	public function testCorrectDefinitionClientId() {
		$this->tester->assertEquals('test', $this->application->getClientId());
	}

	public function testExceptionDefinitionClientId() {
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () {
			new \Engine\Core\WebApplication([]);
		});
	}

	public function testInitServiceManager() {
		$servicesConfig = file_get_contents(__DIR__ . '/../../_data/services.json');
		$application = new \Engine\Core\WebApplication($this->config);
		$application->initServicesManager($servicesConfig);
		$dic = $application->getDic();
		/**
		 * @var \Engine\Core\ServiceManager $serviceManager
		 */
		$serviceManager = $dic->get('serviceManager');
		$clients = $serviceManager->getClients('Auth');
		$this->tester->assertNotEmpty($clients);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[0]);
		$this->tester->assertInstanceOf(\Engine\JsonRpc2\Client\Client::class, $clients[1]);
	}
}

class FakeMiddleware {
	public static $count = 0;

	public function __invoke(Request $request, Response $response, callable $next) {
		$response->getBody()->write((string)++static::$count);
		$response = $next($request, $response);
		$response->getBody()->write((string)++static::$count);
		return $response;
	}
}

class WebApplicationTestUser implements \Engine\Base\Interfaces\UserInterface, \Engine\Base\Interfaces\IdentityInterface {

	public $identity = [
		'id' => 1,
		'username' => 'admin',
		'email' => 'admin@example.com',
		'password' => 'secret',
		'token' => 'qwerty',
	];

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		// TODO: Implement getRole() method.
	}

	public function isGuest() {
		// TODO: Implement isGuest() method.
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		if ($this->identity['token'] === $token) {
			return $this->identity;
		}
		return null;
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}

	public static function findIdentity($id) {
		// TODO: Implement findIdentity() method.
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		// TODO: Implement findIdentityByAccessToken() method.
	}
}

class WebApplicationTestUserWithoutUserInterface implements \Engine\Base\Interfaces\IdentityInterface {

	public static function findIdentity($id) {
		// TODO: Implement findIdentity() method.
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		// TODO: Implement findIdentityByAccessToken() method.
	}
}