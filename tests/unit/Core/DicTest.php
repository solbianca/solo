<?php

use Engine\Core\Exceptions\DependencyContainerException;

class DicTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\DIC
	 */
	protected $dic;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->dic = new \Engine\Core\DIC();
	}

	public function testRegister() {
		$this->tester->assertInstanceOf(\Engine\Core\DIC::class, $this->dic->register('test', function () {
			return true;
		}));

		$this->tester->expectException(DependencyContainerException::class, function () {
			$this->dic->register('test-immutable', 'qwerty');
		});
	}

	public function testGet() {
		$this->dic->register('test', function () {
			return true;
		});
		$this->tester->assertTrue($this->dic->get('test'));
		$this->tester->expectException(DependencyContainerException::class, function () {
			$this->dic->get('qwerty');
		});
	}

	public function testImmutable() {
		$this->dic->register('test-immutable', function () {
			return true;
		}, false);
		$this->tester->assertTrue($this->dic->get('test-immutable'));

		$this->dic->register('test-immutable', function () {
			return true;
		}, true);
		$this->tester->expectException(DependencyContainerException::class, function () {
			$this->dic->register('test-immutable', function () {
				return true;
			});
		});
	}

	public function testUnregistry() {
		$this->dic->register('test-mutable', function () {
			return 'FOO-1';
		});

		$this->dic->register('test-immutable', function () {
			return 'FOO-2';
		}, true);

		$this->dic->unregister('test-mutable');
		$this->dic->unregister('test-immutable');

		$this->tester->expectException(DependencyContainerException::class, function () {
			$this->dic->get('test-mutable');
		});
		$this->tester->expectException(DependencyContainerException::class, function () {
			$this->dic->get('test-immutable');
		});

		$this->dic->register('test-mutable', function () {
			return 'BUZZ-1';
		});

		$this->dic->register('test-immutable', function () {
			return 'BUZZ-2';
		}, true);
		$this->tester->assertEquals('BUZZ-1', $this->dic->get('test-mutable'));
		$this->tester->assertEquals('BUZZ-2', $this->dic->get('test-immutable'));
	}
}