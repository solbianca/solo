<?php


class AccessRuleTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\AccessRule
	 */
	protected $accessRule;

	protected function _before() {
	}

	protected function _after() {
	}

	public function testActionRule() {
		$accessRule = new \Engine\Core\AccessRule([
			'allow' => true,
			'roles' => ['guest', 'authorized'],
			'actions' => ['test'],
		]);

		$this->tester->assertTrue($accessRule->matchAction('test'));
		$this->tester->assertFalse($accessRule->matchAction('test2'));
	}

	public function testVerbRule() {
		$accessRule = new \Engine\Core\AccessRule([
			'allow' => true,
			'roles' => ['authorized'],
			'actions' => ['test'],
			'verbs' => ['GET', 'POST'],
		]);
		$this->tester->assertTrue($accessRule->matchVerb('get'));
		$accessRule = new \Engine\Core\AccessRule([
			'allow' => true,
			'roles' => ['authorized'],
			'actions' => ['test'],
			'verbs' => ['POST'],
		]);
		$this->tester->assertFalse($accessRule->matchVerb('get'));
	}

	public function testCallbackRule() {
		$accessRule = new \Engine\Core\AccessRule([
			'allow' => true,
			'roles' => ['authorized'],
			'actions' => ['test'],
			'verbs' => ['GET'],
			'callback' => function ($rule, $action) {
				return true;
			},
		]);
		$this->tester->assertTrue($accessRule->matchCallback('test'));
		$accessRule = new \Engine\Core\AccessRule([
			'allow' => true,
			'roles' => ['authorized'],
			'actions' => ['test'],
			'verbs' => ['GET'],
			'callback' => function ($rule, $action) {
				return false;
			},
		]);
		$this->tester->assertFalse($accessRule->matchCallback('test'));
	}
}

class UserAccessRule implements \Engine\Base\Interfaces\UserInterface {

	public $identity;

	public function getId() {
		// TODO: Implement getId() method.
	}

	public function getIdentity() {
		// TODO: Implement getIdentity() method.
	}

	public function getRole() {
		return null;
	}

	public function isGuest() {
		return false;
	}

	public function login($username, $password) {
		// TODO: Implement login() method.
	}

	public function loginByAccessToken($token, $clientId) {
		// TODO: Implement setIdentity() method.
	}

	public function setIdentity(array $identity) {
		// TODO: Implement setIdentity() method.
	}
}