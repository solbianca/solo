<?php


class ServiceApplicationTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Core\ServiceApplication
	 */
	protected $application;

	protected $config = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
	];

	public function testDatabasesConfigSuccessful() {
		$config['path'] = [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		];
		$config['databases'] = [
			'defaultConnectionName' => 'db',
			'connections' => [
				'db' => [
					'dsn' => 'sqlite',
					'path' => 'tests/_data/sqlite.db',
				],
			],
		];
		$application = new \Engine\Core\ServiceApplication($config);
		$dic = $application->getDic();
		$this->tester->assertInstanceOf(\PDO::class, $dic->get('dbpool')->get('db'));
	}

	public function testCacheConfigSuccessful() {
		$config['path'] = [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		];
		$config['cache'] = [
			'connections' => [
				'memcache' => [
					'host' => 'localhost',
					'port' => '11211',
					'cacheKeyPrefix' => 'keyPrefix',
				],
			],
		];
		$application = new \Engine\Core\ServiceApplication($config);
		$dic = $application->getDic();
		$this->tester->assertInstanceOf(\Engine\Cache\MemcacheCache::class, $dic->get('cachepool')->get('memcache'));
	}

	public function testDatabaseConfigFailure() {
		$config = [
			'databases' => ['connections' => []],
		];
		$this->tester->expectException(\Engine\Core\Exceptions\ApplicationException::class, function () use ($config) {
			new \Engine\Core\ServiceApplication($config);
		});
	}

	public function testComponentsConfigurationSuccessful() {
		$config['path'] = [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		];
		$config['components'] = [
			'logger' => [
				'immutable' => false,
				'callback' => function () {
					return 123;
				},
			],
		];

		$application = new \Engine\Core\ServiceApplication($config);
		$dic = $application->getDic();
		$this->tester->assertEquals(123, $dic->get('logger'));
	}
}	