<?php


class ViewTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function _before() {
		$config = [
			'clientId' => 'test',
			'path' => [
				'web' => __DIR__ . '/../../_data',
				'app' => __DIR__ . '/../../_data',
				'root' => __DIR__ . '/../../_data',
			],
		];
		$sericesConfig = file_get_contents(__DIR__ . '/../../_data/services.json');
		$application = new \Engine\Core\WebApplication($config);
		$application->initServicesManager($sericesConfig)->run();
	}

	protected $config = [
		'renderer' => \Engine\Renderers\SimpleRenderer::class,
		'viewDirectory' => 'templates',
		'rendererOptions' => [
			'foo' => 'bar',
		],
		'fileExtension',
	];

	public function testConstructor() {
		$view = new \Engine\Core\View(new \Engine\Http\Response(), $this->config);
		$this->tester->assertInstanceOf(\Engine\Renderers\Interfaces\RenderInterface::class, $view->getRenderer());

		$view = new \Engine\Core\View(new \Engine\Http\Response(), []);
		$this->tester->assertInstanceOf(\Engine\Renderers\Interfaces\RenderInterface::class, $view->getRenderer());
	}

	public function testFetch() {
		$view = new \Engine\Core\View(new \Engine\Http\Response(), $this->config);
		$result = $view->fetch('template', ['foo' => 'foo!', 'bar' => 'bar!']);
		$response = '<!DOCTYPE html>
<html>
<head><title>My Webpage</title></head>
<body>
<h1>My Webpage</h1>
<p>foo!</p>
<p>bar!</p>
</body>
</html>';
		$this->tester->assertEquals($response, $result);
	}
}