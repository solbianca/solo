<?php

class CachePoolTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Cache\Pool
	 */
	protected $pool;

	protected $config = ['host' => 'localhost', 'port' => '11211', 'cacheKeyPrefix' => 'prefix'];

	protected $appConfig = [
		'path' => [
			'web' => __DIR__,
			'app' => __DIR__,
			'root' => __DIR__,
		],
		'urls' => [
			'index' => 'offers::index',
			'get' => 'offers::get',
			'origin' => 'origin::index',
			'provider' => 'offers::provider',
		],
		'cache' => [
			'connections' => [
				'memcache' => [
					'host' => 'localhost',
					'port' => '11211',
					'cacheKeyPrefix' => 'keyPrefix',
				],
			],
		],
	];

	public function _before() {
		$this->pool = new \Engine\Cache\Pool();
	}

	public function testSuccessfulMemcacheConnection() {
		$this->pool->register($this->config, 'memcache');
		$this->tester->assertInstanceOf(\Engine\Cache\MemcacheCache::class, $this->pool->get('memcache'));
	}


	public function testEmptyAliasValue() {
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () {
			$this->pool->register($this->config, '');
		});
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () {
			$this->pool->register($this->config, null);
		});
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () {
			$this->pool->register($this->config, false);
		});
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () {
			$this->pool->register($this->config, true);
		});
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () {
			$this->pool->register($this->config, []);
		});
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () {
			$this->pool->register($this->config, ['alias' => 'fail']);
		});
	}

	public function testConnectionMethodException() {
		$config = ['host' => 'localhost', 'port' => '11211'];
		$this->tester->expectException(\Engine\cache\Exception\CachePoolException::class, function () use ($config) {
			$this->pool->register($config, 'memcache');
			$this->pool->connect('fail', $config);
		});
	}


	public function testBadMemcacheConfigDefinition() {
		$this->tester->expectException(\Engine\Cache\Exception\CachePoolException::class, function () {
			$this->pool->connect('memcache', ['host' => '127.1.1.11', 'port' => '10']);
		});
		$this->tester->expectException(\Engine\Cache\Exception\CachePoolException::class, function () {
			$this->pool->connect('memcache', ['host' => '127.0.0.1', 'port' => '']);
		});
		$this->tester->expectException(\Engine\Cache\Exception\CachePoolException::class, function () {
			$this->pool->connect('memcache', ['host' => '', 'port' => '11211']);
		});
		$this->tester->expectException(\Engine\Cache\Exception\CachePoolException::class, function () {
			$this->pool->connect('memcache', ['host' => '', 'port' => '']);
		});
	}

	public function testCachePoolInApplicationSuccessful() {

		$application = new \Engine\Core\ServiceApplication($this->appConfig);
		\Engine\Engine::$app = $application;

		$pool = \Engine\Engine::$app->getDic()->get('cachepool');
		$this->tester->assertInstanceOf(\Engine\Cache\Pool::class, $pool);
	}

}