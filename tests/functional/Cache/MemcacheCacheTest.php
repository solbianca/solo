<?php


class MemcacheCacheTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Cache\MemcacheCache
	 */
	protected $cache;

	protected $config = ['host' => 'localhost', 'port' => '11211', 'prefix' => 'prefix'];

	public function _before() {
		$this->cache = new \Engine\Cache\MemcacheCache(
			$this->config['host'],
			$this->config['port'],
			$this->config['prefix']
		);
	}

	public function testSuccessfulSetMethod() {
		$this->tester->assertTrue(true === $this->cache->set('key', 12));
	}

	public function testSuccessfulGetMethod() {
		$value = 123123;
		$key = 'my_key';
		$this->cache->set($key, $value);
		$this->tester->assertTrue($value === $this->cache->get($key));
	}

	public function testSuccessfulGetDefault() {
		$value = 123123;
		$default = 999;
		$key = 'my_key';
		$this->cache->set($key, $value);
		$this->tester->assertTrue($default === $this->cache->get($key . time(), $default));
	}

	public function testSuccessfulGetMultiple() {
		$data = [
			'key_01' => 11,
			'key_02' => 12,
		];
		$this->cache->setMultiple($data);
		$data = $this->cache->getMultiple(['key_01', 'key_02']);
		$this->tester->assertTrue(11 === $data['key_01']);
		$this->tester->assertTrue(12 === $data['key_02']);
	}

	public function testDeleteKeySuccessful() {
		$key = 'key_for_delete';
		$this->cache->set($key, 123123123);
		$this->cache->delete($key);
		$this->tester->assertTrue(null === $this->cache->get($key));
	}

	public function testFlushAllSuccessful() {
		$key = 'before_flush_key';
		$value = 123123123;
		$this->cache->set($key, $value);
		$this->tester->assertEquals($value, $this->cache->get($key));
		$this->cache->clear();
		$this->tester->assertEquals(null, $this->cache->get($key));
	}

	public function testDeleteMultiple() {
		$data = [
			'before_delete_01' => 'some_text',
			'before_delete_02' => 'some_text',
			'before_delete_03' => 'some_text',
		];
		$keys = ['before_delete_01', 'before_delete_02', 'before_delete_03'];
		$this->cache->setMultiple($data);
		$this->tester->assertEquals($data, $this->cache->getMultiple($keys));
		$this->cache->deleteMultiple($keys);
		$this->tester->assertEquals([
			'before_delete_01' => null,
			'before_delete_02' => null,
			'before_delete_03' => null,
		],
			$this->cache->getMultiple($keys)
		);
	}

	public function testHasKeySuccessful() {
		$key = 'key_for_has';
		$this->cache->set($key, 123123);
		$this->tester->assertTrue(true === $this->cache->has($key));
	}

	public function testIncrementKeySuccessful() {
		$key = 'key_increment';
		$value = 22;
		$this->cache->set($key, $value);
		$this->cache->incrementKey($key);
		$this->tester->assertEquals(1, ($this->cache->get($key) - $value));
	}

	public function testIncrementNotExistingKey() {
		$this->tester->assertEquals(1, $this->cache->incrementKey('not_existing_key'));
	}

	public function testIncrementNotANumberKey() {
		$key = 'not_a_number_key';
		$this->cache->set($key, ['some array']);
		$this->tester->expectException(\Engine\Cache\Exception\CacheException::class, function () use ($key) {
			$this->cache->incrementKey($key);
		});
	}

	public function testDecrementKeySuccessful() {
		$key = 'key_increment';
		$value = 22;
		$this->cache->set($key, $value);
		$this->cache->decrementKey($key);
		$this->tester->assertEquals(1, ($value - $this->cache->get($key)));
	}

	public function testDecrementNotExistingKey() {
		$this->tester->assertEquals(0, $this->cache->decrementKey('not_existing_key'));
	}

	public function testDecrementNotANumberKey() {
		$key = 'not_a_number_key';
		$this->cache->set($key, ['some array']);
		$this->tester->expectException(\Engine\Cache\Exception\CacheException::class, function () use ($key) {
			$this->cache->decrementKey($key);
		});
	}
}