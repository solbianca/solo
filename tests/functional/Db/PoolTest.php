<?php

namespace Tests\functional\Db;


class PoolTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var \Engine\Db\Pool
	 */
	protected $pool;

	protected $config = ['dsn' => 'sqlite', 'path' => 'tests/_data/sqlite.db'];

	public function _before() {
		$this->pool = new \Engine\Db\Pool();
	}

	public function testSuccessfulConnection() {
		$this->pool->register($this->config, 'test');
		$this->tester->assertInstanceOf(\PDO::class, $this->pool->get('test'));
	}

	public function testConnectionImmutability() {
		$this->pool->register($this->config, 'immutable');
		$this->tester->assertInstanceOf(\PDO::class, $this->pool->get('immutable'));

		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, 'immutable');
		});

		$this->pool->register($this->config, 'mutable', false);
		$this->tester->assertInstanceOf(\PDO::class, $this->pool->get('mutable'));
		$this->pool->register($this->config, 'mutable');
		$this->tester->assertInstanceOf(\PDO::class, $this->pool->get('mutable'));
	}

	public function testMissingDsnException() {
		$config = ['path' => 'tests/_data/sqlite.db'];
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () use ($config) {
			$this->pool->register($config, 'test');
		});
	}

	public function testEmptiAliasValue() {
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, '');
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, null);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, false);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, true);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, []);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->register($this->config, ['alias' => 'fail']);
		});
	}

	public function testConnectionMethodException() {
		$config = ['dsn' => 'mssql', 'path' => 'tests/_data/sqlite.db'];
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () use ($config) {
			$this->pool->register($config, 'mssql');
			$this->pool->connect($config);
		});
	}

	public function testSuccessfulGet() {
		$this->pool->register($this->config, 'test');
		$this->tester->assertInstanceOf(\PDO::class, $this->pool->get('test'));
	}

	public function testGetNotExistingConnection() {
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->get('mssql');
		});
	}

	public function testBadMysqlConfigDefinition() {
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->connect(['dsn' => 'mysql', 'dbname' => '', 'user' => '', 'password' => '']);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->connect(['dsn' => 'mysql', 'host' => '', 'user' => '', 'password' => '']);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->connect(['dsn' => 'mysql', 'host' => '', 'dbname' => '', 'password' => '']);
		});
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->connect(['dsn' => 'mysql', 'host' => '', 'user' => '', 'dbname' => '']);
		});
	}

	public function testBadSqliteConfigDefinition() {
		$this->tester->expectException(\Engine\Db\Exception\DbPoolException::class, function () {
			$this->pool->connect(['dsn' => 'sqlite']);
		});
	}
}