<?php


namespace Tests\functional\RabbitMQ;


use Engine\RabbitMQ\Consumer\Consumer;
use Engine\RabbitMQ\RabbitMQ;

class ConsumerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var RabbitMQ
	 */
	protected $rabitmq;

	/**
	 * @var Consumer
	 */
	protected $consumer;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->rabitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$this->consumer = $this->rabitmq->createConsumer('test.');
		$this->getModule('AMQP')->pushToQueue('test.queue', 's:3:"bar";');
		$this->getModule('AMQP')->pushToQueue('test.queue', 's:3:"bar";');
	}

	public function testConsumeWithAutoAck() {
		$result = $this->consumer->consume('queue');
		$this->assertEquals('bar', $result);
	}

	public function testConsumeWithoutAutoAck() {
		$result = $this->consumer->consumeNoAutoAck('queue');
		$this->assertEquals('s:3:"bar";', $result);
	}
}