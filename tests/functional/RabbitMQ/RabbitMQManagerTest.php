<?php


namespace Tests\functional\RabbitMQ;


use Engine\RabbitMQ\Exceptions\RabbitMQManagerException;
use Engine\RabbitMQ\RabbitMQ;
use Engine\RabbitMQ\RabbitMQFactory;
use Engine\RabbitMQ\RabbitMQManager;

class RabbitMQManagerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var RabbitMQManager
	 */
	protected $manager;

	protected $config = [
		'rabbit_main' => [
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		],
		'rabbit_second' => [
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		],
	];

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->manager = new RabbitMQManager(new RabbitMQFactory(), $this->config);
	}

	public function testManager() {
		$this->tester->assertInstanceOf(RabbitMQ::class, $this->manager->getRabbitMQ('rabbit_main'));
		$this->tester->assertInstanceOf(RabbitMQ::class, $this->manager->getRabbitMQ('rabbit_second'));
		
		$this->tester->expectException(RabbitMQManagerException::class, function () {
			$this->manager->getRabbitMQ('fail');
		});
	}
}