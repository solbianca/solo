<?php


namespace Tests\functional\RabbitMQ;


use Engine\RabbitMQ\Consumer\Consumer;
use Engine\RabbitMQ\Exceptions\RabbitMQException;
use Engine\RabbitMQ\Producer\Producer;
use Engine\RabbitMQ\RabbitMQ;

class RabbitMQTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var array
	 */
	protected $config = [
		'connection' => [
			'host' => '127.0.0.1',
			'port' => '5672',
			'username' => 'guest',
			'password' => 'guest',
			'vhost' => '/',
		],
		'exchange' => [
			'name' => 'test.exchange',
			'type' => RabbitMQ::EXCHANGE_DIRECT,
		],
	];

	/**
	 * Initiate before test
	 */
	protected function _before() {
	}

	public function testSuccessfullyCreate() {
		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$this->tester->assertInstanceOf(RabbitMQ::class, $rabbitmq);

		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$this->tester->assertInstanceOf(RabbitMQ::class, $rabbitmq);
	}

	public function testEmptyConfig() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([]);
		});
	}

	public function testConfigWithoutConnection() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});
	}

	public function testConfigWithoutExchange() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});
	}

	public function testInvalidConnectionConfig() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'port' => '5672',
					'username' => 'guest',
					'password' => 'guest',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});

		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.0.0.1',
					'username' => 'guest',
					'password' => 'guest',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});

		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.0.0.1',
					'port' => '5672',
					'password' => 'guest',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});

		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.0.0.1',
					'port' => '5672',
					'username' => 'guest',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});

		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.42.42.1',
					'port' => '888',
					'username' => 'fail',
					'password' => 'fail',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});
	}

	public function testConnectionWithWrongUser() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.42.42.1',
					'port' => '5672',
					'username' => 'fail',
					'password' => 'fail',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});
	}

	public function testConnectionWithWrongPort() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.42.42.1',
					'port' => '88',
					'username' => 'fail',
					'password' => 'fail',
				],
				'exchange' => [
					'name' => 'test.exchange',
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});
	}


	public function testInvalidExchangeConfig() {
		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.0.0.1',
					'port' => '5672',
					'username' => 'guest',
					'password' => 'guest',
				],
				'exchange' => [
					'type' => RabbitMQ::EXCHANGE_DIRECT,
				],
			]);
		});

		$this->tester->expectException(RabbitMQException::class, function () {
			new RabbitMQ([
				'connection' => [
					'host' => '127.0.0.1',
					'port' => '5672',
					'username' => 'guest',
					'password' => 'guest',
				],
				'exchange' => [
					'name' => 'test.exchange',
				],
			]);
		});
	}

	public function testCreateProducer() {
		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$producer = $rabbitmq->createProducer('test');
		$this->assertInstanceOf(Producer::class, $producer);
		$this->tester->assertEquals('test', $producer->getKeyPrefix());
	}

	public function testCreateCustomer() {
		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$consumer = $rabbitmq->createConsumer('test');
		$this->assertInstanceOf(Consumer::class, $consumer);
		$this->tester->assertEquals('test', $consumer->getKeyPrefix());
		$this->tester->assertEquals('test.exchange', $consumer->getExchangeName());

		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$consumer = $rabbitmq->createConsumer('test', 'test-exchange-name');
		$this->assertInstanceOf(Consumer::class, $consumer);
		$this->tester->assertEquals('test', $consumer->getKeyPrefix());
		$this->tester->assertEquals('test-exchange-name', $consumer->getExchangeName());

		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$consumer = $rabbitmq->createConsumer('test', 'test-exchange-name', RabbitMQ::DURABLE);
		$this->assertInstanceOf(Consumer::class, $consumer);
		$this->tester->assertEquals('test', $consumer->getKeyPrefix());
		$this->tester->assertEquals('test-exchange-name', $consumer->getExchangeName());
		$this->tester->assertEquals(2, $consumer->getFlags());
	}

	public function testCreateExchangeWithFlags() {
		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange.durable',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
				'flags' => AMQP_DURABLE,
			],
		]);
		$this->tester->assertInstanceOf(RabbitMQ::class, $rabbitmq);
	}

	public function testCreateExchangeWithArguments() {
		$rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
				'arguments' => ['test' => 'test'],
			],
		]);
		$this->tester->assertInstanceOf(RabbitMQ::class, $rabbitmq);
	}
}