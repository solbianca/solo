<?php


namespace Tests\functional\RabbitMQ;


use Engine\RabbitMQ\Producer\Producer;
use Engine\RabbitMQ\RabbitMQ;

class ProducerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var RabbitMQ
	 */
	protected $rabitmq;

	/**
	 * @var Producer
	 */
	protected $producer;

	/**
	 * Initiate before test
	 */
	protected function _before() {
		$this->getModule('AMQP')->declareExchange('test.exchange', 'direct', false, false, false);
		$this->getModule('AMQP')->declareQueue('test.exchange', false, false, false, false);
		$this->getModule('AMQP')->bindQueueToExchange('test.queue', 'test.exchange');
		$this->rabitmq = $rabbitmq = new RabbitMQ([
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		]);
		$this->producer = $rabbitmq->createProducer('test.');
	}

	public function testPublishSerializeData() {
		$this->producer->publish('queue', 'bar');
		$this->getModule('AMQP')->seeMessageInQueueContainsText('test.queue', 's:3:"bar";');
	}

	public function testPublishJsonData() {
		$this->producer->publishJson('queue', 'bar');
		$this->getModule('AMQP')->seeMessageInQueueContainsText('test.queue', '"bar"');
	}
}