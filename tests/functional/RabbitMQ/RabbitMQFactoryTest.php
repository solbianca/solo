<?php


namespace Tests\functional\RabbitMQ;


use Engine\RabbitMQ\RabbitMQ;
use Engine\RabbitMQ\RabbitMQFactory;

class RabbitMQFactoryTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testCreate() {
		$config = [
			'connection' => [
				'host' => '127.0.0.1',
				'port' => '5672',
				'username' => 'guest',
				'password' => 'guest',
				'vhost' => '/',
			],
			'exchange' => [
				'name' => 'test.exchange',
				'type' => RabbitMQ::EXCHANGE_DIRECT,
			],
		];
		$factory = new RabbitMQFactory();
		$rabbitmq = $factory->createRabbitMQ($config);

		$this->tester->assertInstanceOf(RabbitMQ::class, $rabbitmq);
	}
}