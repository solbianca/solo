<?php

//defined('APP_DEBUG') or define('APP_DEBUG', true);
//defined('APP_ENV') or define('APP_ENV', 'dev');
$config = [
	'clientId' => 'test',
	'path' => [
		'web' => __DIR__ . '/../_data',
		'app' => __DIR__ . '/../_data',
		'root' => __DIR__ . '/../_data',
	],
	'middleware' => [
		'default' => [
			new \Engine\Middleware\Middlewares\ClientIpMiddleware(),
			new \Engine\Middleware\Middlewares\HttpRoutingMiddleware(),
		],
	],
];
$sericesConfig = file_get_contents(__DIR__ . '/../_data/services.json');
$application = new \Engine\Core\WebApplication($config);
$application->initServicesManager($sericesConfig);

